<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
$ruta_raiz = isset($ruta_raiz) ? $ruta_raiz : "..";
require_once("$ruta_raiz/funciones.php");
//p_register_globals(array("verrad", "textrad", "nivelRad", "grbNivel" ));   //No funciono para este caso, bug por variable definida en session.

 	session_start();
	include_once "$ruta_raiz/rec_session.php";

    	include_once("$ruta_raiz/include/db/ConnectionHandler.php");
	$db = new ConnectionHandler("$ruta_raiz");


    $verrad = $_GET['verrad'];   //se incluyo por register_globals
    $textrad = $_GET['textrad'];
   // $nivelRad = $_GET['nivelRad'];
    $grbNivel = $_POST['grbNivel'];

if ( !isset( $_GET['nivelRad'] ) )
{
   $nivelRad  = $_POST['nivelRad'];

}
else
{
   if ( empty( $nivelRad ) ) {
       $nivelRad  = $_POST['nivelRad'];
   }
   else
    $nivelRad  = $_GET['nivelRad'] ;
}

?>
<html>
<head>
<title>Nivel de Seguridad</title>
<link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
<script>
    function regresar() {
	document.TipoDocu.submit();
    }
</script>
</head>
<body>

<form method="post" action="radicado.php?verrad=<?=$verrad?>&textrad=<?=$textrad?>">
<table border=0 width=100% align="center" class="borde_tab" cellspacing="0">
    <tr align="center" class="titulos2">
	<td class="titulos2">NIVEL DE SEGURIDAD DEL <?=strtoupper($_SESSION["descRadicado"])?> No. <?=$textrad?></td>
    </tr>
</table>
<br/>
<table width="100%" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">
    <tr >
	<td width="50%" class="titulos2" >Nivel Actual</td>
	<td width="50%" class="listado2" >
	    <select name="nivelRad" class="select">
		<option value="0" <?if($nivelRad==0) echo "selected"?>>P&uacute;blico</option>
		<option value="1" <?if($nivelRad==1) echo "selected"?>>Reservado</option>
	    </select>
	</td>
    </tr>
    <tr>
	<td class="listado2" colspan="2"><center>
	    Si selecciona Reservado, El usuario actual del <?=$_SESSION["descRadicado"]?> ser&aacute; el &uacute;nico que lo podra ver.
	</center></td>
    </tr>
    <tr>
 	<td class="listado2"  align="center">
	    <center><input type="submit" class="botones" name=grbNivel value="Grabar Nivel"></center>
	</td>
	<td class=listado2  align="center">
	    <center><input name="Cerrar" type="button" class="botones" id="envia22" onClick="opener.regresar();window.close();"value="Cerrar"></center>
	</td>
    </tr>
</table>
<br/>
</form>

<?
if($grbNivel and $verrad)
{
	$query = "UPDATE RADICADO SET radi_permiso=$nivelRad where radi_nume_radi=$verrad";
	$observa = "Cambió nivel de seguridad a ";
	if ($nivelRad==1) $observa .= "Reservado"; else $observa .= "Público";
	if($db->conn->Execute($query))
	{
		echo "<span class=leidos>El nivel de seguridad se actualiz&oacute; correctamente.";
		include_once "$ruta_raiz/include/tx/Historico.php";
		$Historico = new Historico($db);
        $cod_usuario = $_SESSION["codusuario"];
		$Historico->insertarHistorico($verrad, $codusuario, $codusuario, $observa, 54);
	}else
	{
		echo "<span class=titulosError> !No se pudo actualizar el nivel de seguridad!";
	}
}
?>
<?=$mensaje_err?>
</body>
</html>
