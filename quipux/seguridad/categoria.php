<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
$ruta_raiz = isset($ruta_raiz) ? $ruta_raiz : "..";
require_once("$ruta_raiz/funciones.php");
//p_register_globals(array("verrad", "textrad", "nivelRad", "grbNivel" ));   //No funciono para este caso, bug por variable definida en session.

session_start();
include_once "$ruta_raiz/rec_session.php";

include_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");


$verrad = $_GET['verrad'];   //se incluyo por register_globals
$textrad = $_GET['textrad'];
// $nivelRad = $_GET['nivelRad'];
if(isset($verrad))
{
    $sqlRad = "select esta_codi from radicado where radi_nume_radi=$verrad";
    $rsRadi = $db->conn->Execute($sqlRad);
    $estadoDoc = $rsRadi->fields['ESTA_CODI'];

    $grbCategoria = $_POST['grbCategoria'];

    if ( !isset( $_GET['cat_codi'] ) )
    {
        $cat_codi  = $_POST['cat_codi'];
    }
    else
    {
        if ( empty( $cat_codi ) )
            $cat_codi  = $_POST['cat_codi'];
        else
            $cat_codi  = $_GET['cat_codi'] ;
    }
    ?>
    <html>
    <head>
    <title>Categoría</title>
    <link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
    <script>
        function guardarDesc()
        {
            document.forms[0].cat_codiDesc.value = document.forms[0].cat_codi[document.forms[0].cat_codi.selectedIndex].text;
        }

        function regresar() {
            document.TipoDocu.submit();
        }
    </script>
    </head>
    <body>

    <form method="post" action="categoria.php?verrad=<?=$verrad?>&textrad=<?=$textrad?>">
    <table border=0 width=100% align="center" class="borde_tab" cellspacing="0">
        <tr align="center" class="titulos2">
        <td class="titulos2">CAMBIO DE CATEGOR&Iacute;A DEL <?=strtoupper($_SESSION["descRadicado"])?> No. <?=$textrad?></td>
        </tr>
    </table>
    <br/>
    <table width="100%" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">
        <input type="hidden" name="cat_codiAnte" value="<?php if ( isset( $_GET['cat_codi'] ) ) echo $_GET['catDesc']; else if($_POST['cat_codiDesc']!='') echo $_POST['cat_codiDesc']; else echo $_POST['cat_codiAnte']; ?>">
        <input type="hidden" name="cat_codiDesc" value="<?php $_POST['cat_codiDesc']; ?>">
        <tr >
        <td width="50%" class="titulos2" >Categor&iacute;a:</td>
        <td width="50%" class="listado2" >
        <?
        if($estadoDoc!=1)
            $desabilitar = "disabled";
        else
            $desabilitar = "";
        $queryCat = "Select cat_descr, cat_codi from categoria";
        $rsCat=$db->conn->query($queryCat);
        if(!$rsCat->EOF)
            print $rsCat->GetMenu2("cat_codi", $cat_codi,  "0:Normal", false,"","class='select' onChange='guardarDesc();' $desabilitar" );
        ?>
        </td>
        </tr>
        <tr>
        <td class="listado2" colspan="2"><center>
        <?php if($estadoDoc!=1)
                echo "El Tipo de Documento no es editable.";
        ?>
        </center></td>
        </tr>
        <tr>
        <td class="listado2"  align="center">
            <center><input type="submit" class="botones" name=grbCategoria value="Grabar"></center>
        </td>
        <td class=listado2  align="center">
            <center><input name="Cerrar" type="button" class="botones" id="Cerrar" onClick="opener.regresar();window.close();" value="Cerrar"></center>
        </td>
        </tr>
    </table>
    <br/>
    </form>

    <?

    if($grbCategoria and $verrad)
    {
        if($cat_codiAnte != $cat_codiDesc and $cat_codiDesc != '')
        {
            $query = "UPDATE RADICADO SET cat_codi=$cat_codi where radi_nume_radi=$verrad";
            $observa = "Cambió de Categoría de $cat_codiAnte a $cat_codiDesc";
            if($db->conn->Execute($query))
            {
                $cat_codiAnte = $cat_codiDesc;
                echo "<span class=leidos>La Categor&iacute;a se actualiz&oacute; correctamente.";
                include_once "$ruta_raiz/include/tx/Historico.php";
                $Historico = new Historico($db);
                $cod_usuario = $_SESSION["codusuario"];
                $Historico->insertarHistorico($verrad, $codusuario, $codusuario, $observa, 11);
            }
            else
            {
                echo "<span class=titulosError> !No se pudo actualizar la categor&iacute;a!";
            }
        }
        else
            echo "<span class=titulosError> !La categor&iacute;a no ha sido modificada!";
    }?>
</body>
</html>
<?php
}
?>
<?=$mensaje_err?>
