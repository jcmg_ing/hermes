<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
$ruta_raiz = isset($ruta_raiz) ? $ruta_raiz : "..";
require_once("$ruta_raiz/funciones.php");
//p_register_globals(array("verrad", "textrad", "nivelRad", "grbNivel" ));   //No funciono para este caso, bug por variable definida en session.

session_start();
include_once "$ruta_raiz/rec_session.php";

include_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");


$verrad = $_GET['verrad'];   //se incluyo por register_globals
$textrad = $_GET['textrad'];
// $nivelRad = $_GET['nivelRad'];
if(isset($verrad))
{
    $sqlRad = "select esta_codi from radicado where radi_nume_radi=$verrad";
    $rsRadi = $db->conn->Execute($sqlRad);
    $estadoDoc = $rsRadi->fields['ESTA_CODI'];

    $grbTipificacion = $_POST['grbTipificacion'];

    if ( !isset( $_GET['cod_codi'] ) )
    {
        $cod_codi  = $_POST['cod_codi'];
    }
    else
    {
        if ( empty( $cod_codi ) )
            $cod_codi  = $_POST['cod_codi'];
        else
            $cod_codi  = $_GET['cod_codi'] ;
    }
    ?>
    <html>
    <head>
    <title>Tipificación</title>
    <link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
    <script>
        function guardarDesc()
        {
            document.forms[0].cod_codiDesc.value = document.forms[0].cod_codi[document.forms[0].cod_codi.selectedIndex].text;
        }

        function regresar() {
            document.TipoDocu.submit();
        }
    </script>
    </head>
    <body>

    <form method="post" action="tipificacion.php?verrad=<?=$verrad?>&textrad=<?=$textrad?>">
    <table border=0 width=100% align="center" class="borde_tab" cellspacing="0">
        <tr align="center" class="titulos2">
        <td class="titulos2">CAMBIO DE TIPIFICACI&Oacute;N DEL <?=strtoupper($_SESSION["descRadicado"])?> No. <?=$textrad?></td>
        </tr>
    </table>
    <br/>
    <table width="100%" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">
        <input type="hidden" name="cod_codiAnte" value="<?php if ( isset( $_GET['cod_codi'] ) ) echo $_GET['codDesc']; else if($_POST['cod_codiDesc']!='') echo $_POST['cod_codiDesc']; else echo $_POST['cod_codiAnte']; ?>">
        <input type="hidden" name="cod_codiDesc" value="<?php $_POST['cod_codiDesc']; ?>">
        <tr >
        <td width="50%" class="titulos2" >Tipificaci&oacute;n:</td>
        <td width="50%" class="listado2" >
        <?
        if($estadoDoc!=1)
            $desabilitar = "disabled";
        else
            $desabilitar = "";
        $queryCat = "Select cod_descripcion, cod_codi from codificacion where inst_codi = ".$_SESSION['inst_codi'];
        $rsCat=$db->conn->query($queryCat);
        if(!$rsCat->EOF)
            print $rsCat->GetMenu2("cod_codi", $cod_codi,  "0:Sin Tipificación", false,"","class='select' onChange='guardarDesc();' $desabilitar" );
        ?>
        </td>
        </tr>
        <tr>
        <td class="listado2" colspan="2"><center>
        <?php if($estadoDoc!=1)
                echo "El Tipo de Documento no es editable.";
        ?>
        </center></td>
        </tr>
        <tr>
        <td class="listado2"  align="center" colspan="2">
            <center><input type="submit" class="botones" name=grbTipificacion value="Grabar">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input name="Cerrar" type="button" class="botones" id="Cerrar" onClick="opener.regresar();window.close();" value="Cerrar"></center>
        </td>
        </tr>
    </table>
    <br/>
    </form>

    <?

    if($grbTipificacion and $verrad)
    {
        if($cod_codiAnte != $cod_codiDesc and $cod_codiDesc != '')
        {
            $query = "UPDATE RADICADO SET cod_codi=$cod_codi where radi_nume_radi=$verrad";
            $observa = "Cambió de Tipificación de $cod_codiAnte a $cod_codiDesc";
            if($db->conn->Execute($query))
            {
                $cod_codiAnte = $cod_codiDesc;
                echo "<span class=leidos>La Tipificaci&oacute;n se actualiz&oacute; correctamente.";
                include_once "$ruta_raiz/include/tx/Historico.php";
                $Historico = new Historico($db);
                $cod_usuario = $_SESSION["codusuario"];
                $Historico->insertarHistorico($verrad, $codusuario, $codusuario, $observa, 11);
            }
            else
            {
                echo "<span class=titulosError> !No se pudo actualizar la tipificaci&oacute;n!";
            }
        }
        else
            echo "<span class=titulosError> !La tipificaci&oacute;n no ha sido modificada!";
    }?>
</body>
</html>
<?php
}
?>
<?=$mensaje_err?>
