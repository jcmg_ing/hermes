<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
$ruta_raiz = isset($ruta_raiz) ? $ruta_raiz : "..";
require_once("$ruta_raiz/funciones.php");
//p_register_globals(array("verrad", "textrad", "nivelRad", "grbNivel" ));   //No funciono para este caso, bug por variable definida en session.

session_start();
include_once "$ruta_raiz/rec_session.php";

include_once("$ruta_raiz/include/db/ConnectionHandler.php");
$db = new ConnectionHandler("$ruta_raiz");


$verrad = $_GET['verrad'];   //se incluyo por register_globals
$textrad = $_GET['textrad'];
// $nivelRad = $_GET['nivelRad'];
if(isset($verrad))
{
    $sqlRad = "select esta_codi from radicado where radi_nume_radi=$verrad";
    $rsRadi = $db->conn->Execute($sqlRad);
    $estadoDoc = $rsRadi->fields['ESTA_CODI'];
    
    $grbTipDoc = $_POST['grbTipDoc'];

    if ( !isset( $_GET['raditipo'] ) )
    {
        $raditipo  = $_POST['raditipo'];
    }
    else
    {
        if ( empty( $raditipo ) )
            $raditipo  = $_POST['raditipo'];
        else
            $raditipo  = $_GET['raditipo'] ;
    }
    ?>
    <html>
    <head>
    <title>Tipo de Documento</title>
    <link href="../estilos/orfeo.css" rel="stylesheet" type="text/css">
    <script>
        function guardarDesc()
        {
            document.forms[0].radiTipoDesc.value = document.forms[0].raditipo[document.forms[0].raditipo.selectedIndex].text;
        }

        function regresar() {
            document.TipoDocu.submit();
        }
    </script>
    </head>
    <body>

    <form method="post" action="tipoDocumento.php?verrad=<?=$verrad?>&textrad=<?=$textrad?>">
    <table border=0 width=100% align="center" class="borde_tab" cellspacing="0">
        <tr align="center" class="titulos2">
        <td class="titulos2">CAMBIO DE TIPO DE <?=strtoupper($_SESSION["descRadicado"])?> No. <?=$textrad?></td>
        </tr>
    </table>
    <br/>
    <table width="100%" border="0" cellspacing="1" cellpadding="0" align="center" class="borde_tab">
        <input type="hidden" name="radiTipoAnte" value="<?php if ( isset( $_GET['raditipo'] ) ) echo $_GET['descradi']; else if($_POST['radiTipoDesc']!='') echo $_POST['radiTipoDesc']; else echo $_POST['radiTipoAnte']; ?>">
        <input type="hidden" name="radiTipoDesc" value="<?php $_POST['radiTipoDesc']; ?>">
        <tr >
        <td width="50%" class="titulos2" >Tipo de Documento:</td>
        <td width="50%" class="listado2" >
        <?
        if($estadoDoc!=1)
            $desabilitar = "disabled";
        else
            $desabilitar = "";
        $query = "Select trad_descr, trad_codigo from tiporad where trad_tipo='S' ";
        $rs=$db->conn->query($query);
        if(!$rs->EOF)
            print $rs->GetMenu2("raditipo", $raditipo, "", false,"","class='select' onChange='guardarDesc();' $desabilitar" );
        ?>
        </td>
        </tr>
        <tr>
        <td class="listado2" colspan="2"><center>
        <?php if($estadoDoc!=1)
                echo "El Tipo de Documento no es editable.";
        ?>
        </center></td>
        </tr>
        <tr>
        <td class="listado2"  align="center">
            <center><input type="submit" class="botones" name=grbTipDoc value="Grabar"></center>
        </td>
        <td class=listado2  align="center">
            <center><input name="Cerrar" type="button" class="botones" id="Cerrar" onClick="opener.regresar();window.close();" value="Cerrar"></center>
        </td>
        </tr>
    </table>
    <br/>
    </form>

    <?

    if($grbTipDoc and $verrad)
    {
        if($radiTipoAnte != $radiTipoDesc and $radiTipoDesc != '')
        {
            $query = "UPDATE RADICADO SET radi_tipo=$raditipo where radi_nume_radi=$verrad";
            $observa = "Cambió de Tipo de Documento de $radiTipoAnte a $radiTipoDesc";
            if($db->conn->Execute($query))
            {
                $radiTipoAnte = $radiTipoDesc;
                echo "<span class=leidos>El tipo de codumento se actualiz&oacute; correctamente.";
                include_once "$ruta_raiz/include/tx/Historico.php";
                $Historico = new Historico($db);
                $cod_usuario = $_SESSION["codusuario"];
                $Historico->insertarHistorico($verrad, $codusuario, $codusuario, $observa, 11);
            }
            else
            {
                echo "<span class=titulosError> !No se pudo actualizar el tipo de documento!";
            }
        }
        else
            echo "<span class=titulosError> !El tipo de documento no ha sido modificado!";
    }
}
?>
<?=$mensaje_err?>
</body>
</html>