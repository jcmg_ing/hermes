<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
include "$ruta_raiz/funciones_interfaz.php";

//var_dump($_POST);

$error = "";

/* if ($_SESSION["admin_institucion"] != 1) {
  echo html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
  die("");
  } */

$usuarios_lista = limpiar_sql($_POST["txt_usuarios_lista"]);
$lista_usr = str_replace("-", "", str_replace("--", ",", $usuarios_lista));
$asunto = limpiar_sql($_POST["txt_asunto"]);

$mensaje = limpiar_sql($_POST['txt_texto_sobre'], 0);

echo "<html>" . html_head();
?>
<script type="text/JavaScript">
    function inicio() {

        document.getElementById('div_envio_mail').innerHTML = '<center>Por favor espere mientras se realiza el env&iacute;o de Correos.<br>&nbsp;<br>' +
            '<img src="<?= $ruta_raiz ?>/imagenes/progress_bar.gif"><br>&nbsp;</center>';
        return;
    }
    function ocultar(){
        document.getElementById('div_envio_mail').style.display = 'none';
        return;
    }

</script>

<body bgcolor="#FFFFFF" onload="inicio();" >
    <form method="post" name="formEnvio" id="formEnvio" >
        <center>
            <table border=0 width="100%" class="borde_tab" cellpadding="0" cellspacing="5">
                <tr >
                    <td colspan="3" class="titulos4"><div align="center"><strong>Administraci&oacute;n de Notificaciones a Correo --Env&iacute;o de Correos--</strong></div></td>
                </tr>
            </table>
            <div id="div_envio_mail" style="width:100%; height:80; border:1px solid #000000;"></div>
            <?php
            $isql = "select usua_codi, usua_nomb, inst_nombre, depe_nomb, usua_cargo, usua_email   from usuario where usua_codi in ($lista_usr) and usua_esta = 1";
            $rs = $db->conn->Execute($isql);
            if (!$rs->EOF) {

                while (!$rs->EOF) {
                    $usr_codi = $rs->fields["USUA_CODI"];
                    $usr_nombre = $rs->fields["USUA_NOMB"];
                    $usr_institucion = $rs->fields["INST_NOMBRE"];
                    $usr_area = $rs->fields["DEPE_NOMB"];
                    $usr_cargo = $rs->fields["USUA_CARGO"];
                    $usr_email = $rs->fields["USUA_EMAIL"];
                    if ($usua_email != '') {
                        $mail = "<html><title>Informaci&oacute;n Quipux</title>";
                        $mail .= "<body><center><h1>QUIPUX</h1><br /><h2>Sistema de Gesti&oacute;n Documental</h2><br /><br /></center>";
                        $mail .= "Estimado(a) $usr_nombre.<br /><br />";

                        $mail .= "<br/>" . $mensaje . "<br/>";

                        $mail .= "<br /><br />Gracias por su comprensión.<br /><br />";
                        $mail .= "<br /><br />Saludos cordiales,<br /><br />Administraci&oacute;n Quipux.";
                        $mail .= "<br /><br /><b>Nota: </b>Este mensaje fue enviado autom&aacute;ticamente por el sistema, por favor no lo responda.";
                        $mail .= "<br />Si tiene alguna inquietud respecto a este mensaje, comun&iacute;quese con <a href='mailto:$cuenta_mail_soporte'>$cuenta_mail_soporte</a>";
                        $mail .= "</body></html>";
                        //enviar 5 minutuos antes
                        enviarMail($mail, $asunto, $usr_email, $usr_nomb, $ruta_raiz);
                    }
                    $rs->MoveNext();
                }
                $error = "Los correos se enviaron exitosamente";

                echo "<script>ocultar();</script>";
                echo "<br/><table width='100%'><tr><td align='center'><font color='blue' face='Arial' size='3'>$error</font></td></tr></table>";
            }
            else {
                $error = "Los correos no se enviaron, verifique";
                echo "<script>ocultar();</script>";
                echo "<br/><table width='100%'><tr><td align='center'><font color='red' face='Arial' size='3'>$error</font></td></tr></table>";
            }
            ?>
            <input type='button' id="regresar" name="regresar" value='Regresar' class="botones"  onclick="window.location='notifica_busqueda.php'">

        </center>
    </form>

</body>
</html>
