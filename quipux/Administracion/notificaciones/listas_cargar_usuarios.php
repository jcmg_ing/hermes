<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
 *    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
 *    Quipux    www.gestiondocumental.gov.ec
 * ------------------------------------------------------------------------------
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see http://www.gnu.org/licenses.
 * ------------------------------------------------------------------------------
 * */
$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";
if (isset ($replicacion) && $replicacion && $config_db_replica_not_listas_cargar_usuarios!="") $db = new ConnectionHandler($ruta_raiz,$config_db_replica_not_listas_cargar_usuarios);

include_once "$ruta_raiz/obtenerdatos.php";

$usuarios_lista = limpiar_sql($_POST["txt_usuarios_lista"]);



//$lista_orden = 0 + $_POST["txt_lista_orden"];
$accion = 0 + $_POST["txt_accion"];
$borrar = 0 + $_POST["txt_borrar"];
$idmail = 0 + $_POST["txt_idmail"];

function obtener_mensaje($idmail, $db) {

    if (trim($idmail) == "")
        return array();

    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    $isql = "select * from mail_notificacion where mail_codi=" . trim($idmail);
    $rs = $db->query($isql);
    $vector["ASUNTO"] = $rs->fields["ASUNTO"];
    $vector["MENSAJE"] = $rs->fields["MENSAJE"];
    return $vector;
}

//echo $accion;
?>

<table class=borde_tab width="100%" cellpadding="0" cellspacing="4">
    <tr>
        <td colspan="9" align="right">
            <input class='botones_azul' title='Quitar todos los usuarios de la lista' type='button' value='Borrar Todos' onClick="borrar_usuario_lista('0');">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr align="center" >
        <td width="10%"  class="titulos5">Tipo</td>
        <td width="15%" class="titulos5">Nombres</td>
        <td width="15%" class="titulos5">Instituci&oacute;n</td>
        <td width="5%"  class="titulos5">T&iacute;tulo</td>
        <td width="15%" class="titulos5"><?= $descCargo ?></td>
        <td width="15%" class="titulos5">&Aacute;rea</td>
        <td width="10%" class="titulos5">E-mail</td>
        <td width="5%"  class="titulos5">Estado</td>
        <td width="10%" class="titulos5">Acci&oacute;n</td>
    </tr>

    <?
    if ($accion == 1) {
        $lista_usr = explode("-", $usuarios_lista);
        for ($i = 0; $i <= count($lista_usr) + 1; $i++) {
            $cod_usr = 0 + trim($lista_usr[$i]);
            if ($cod_usr != 0) {
                $sql = "select * from datos_usuarios where usua_codi=$cod_usr";
                $rs = $db->conn->query($sql);
                mostrar_usuario();
            }
        }
    }

    if ($accion == 2) { //para cargar lo seleccionado + lo existente
        
        if ($borrar == 0) {
            $lista_usr = str_replace("-", "", str_replace("--", ",", $usuarios_lista));
            $lista_usr1 = $lista_usr;
            $sql = "select * from datos_usuarios where usua_codi in ($lista_usr)";
            $sql2 = "select * from datos_usuarios u where u.usua_codi in
            (select usua_destinatario from usuario_notificacion where id_mail = " . $idmail . " )";

            $sql3 = $sql . " union " . $sql2;
           // echo $sql3;
            $mensaje_anterior = obtener_mensaje($idmail, $db);
            $mensaje_anterior['MENSAJE'] = str_replace('<br type="_moz" />', '', $mensaje_anterior['MENSAJE']);


            $rs = $db->conn->query($sql3);
            if (!$rs)
                die("");
            $lista_usr = '';
            while (!$rs->EOF) {
                $lista_usr .= "-" . $rs->fields["USUA_CODI"] . "-";
                mostrar_usuario();
                $rs->MoveNext();
            }
        } else {
            $lista_usr = str_replace("-", "", str_replace("--", ",", $usuarios_lista));
            ////echo $lista_usr;
            $mensaje_anterior = obtener_mensaje($idmail, $db);
            $mensaje_anterior['MENSAJE'] = str_replace('<br type="_moz" />', '', $mensaje_anterior['MENSAJE']);
            
            $sql = "select * from datos_usuarios where usua_codi in ($lista_usr) order by usua_esta asc, usua_nombre asc";
            $rs = $db->conn->query($sql);
            if (!$rs)
                die("");
            while (!$rs->EOF) {
                mostrar_usuario();
                $rs->MoveNext();
            }
        }
    }
    else {
        $lista_usr = str_replace("-", "", str_replace("--", ",", $usuarios_lista));
        $sql = "select * from datos_usuarios where usua_codi in ($lista_usr) order by usua_esta asc, usua_nombre asc";
        $rs = $db->conn->query($sql);
        if (!$rs)
            die("");
        while (!$rs->EOF) {
            mostrar_usuario();
            $rs->MoveNext();
        }
    }
    ?>
</table>

<?

    function mostrar_usuario() {
        global $rs;
?>
        <tr onmouseover="this.style.background='#e3e8ec'" onmouseout="this.style.background='white', this.style.color='black'">
            <td><font size=1><?
        if ($rs->fields["TIPO_USUARIO"] == 1)
            echo "Servidor P&uacute;blico"; else
            echo "Ciudadano"; ?></font></td>
    <td><font size=1><?= $rs->fields["USUA_NOMBRE"] ?></font></td>
    <td><font size=1><?= $rs->fields["INST_NOMBRE"] ?></font></td>
    <td><font size=1><?= $rs->fields["USUA_TITULO"] ?></font></td>
    <td><font size=1><?= $rs->fields["USUA_CARGO"] ?> </font></td>
    <td><font size=1><?= $rs->fields["DEPE_NOMB"] ?></font></td>
    <td><font size=1><?= $rs->fields["USUA_EMAIL"] ?></font></td>
    <td align="center" valign="middle"><font size=1><?
            if ($rs->fields["USUA_ESTA"] == 1)
                echo "Activo"; else
                echo "<font color='red'>Inactivo</font>"; ?></font></td>
    <td align="center" valign="middle" ><font size=1>
            <input class='botones_azul' title='Quitar usuario de la lista' type='button' value='Borrar' onClick="borrar_usuario_lista('<?= $rs->fields["USUA_CODI"] ?>');"></font>
    </td>
</tr>
<?
            return;
        }
?>
        <textarea id="txt_usuarios_lista_total" style="display: none" name="txt_usuarios_lista_total"  cols="10" rows="1"><?= $lista_usr ?></textarea>
        <input type ="hidden" id="hd_texto_sobre" name="hd_texto_sobre" value="<?=$mensaje_anterior['MENSAJE']?>">


