<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
 * autor:   teya
 * fecha:   20110531
 * motivo:  para obtener lista de usuarios para notificaciones
 * ------------------------------------------------------------------------------
**/
include_once "$ruta_raiz/funciones.php";

    switch($db->driver)	{
	case 'postgres':
	    $txt_nombreci = trim(strtoupper($txt_nombreci));

            $mensaje = '<font COLOR="#000000">Eliminar</font>';
            if($cmb_estado==0){ //usuarios conectados
                $sql = "select
                         u.usua_codi  as \"CHK_CHKANULAR\"
                         ,u.usua_cedula as \"Cedula\", u.usua_nombre AS \"Nombre\"
                         ,u.usua_email AS \"Email\", u.usua_cargo as \"Cargo\"
                         ,u.depe_nomb AS \"Area\", u.inst_nombre as \"Institución\"";
                $sql .= " from datos_usuarios u ";
                $sql .= " JOIN usuarios_sesion s ON u.usua_codi = s.usua_codi
                            and s.usua_fech_sesion::date = now()::date
                          where u.usua_codi <> 0  ";

            }elseif($cmb_estado==1){ //para usuarios seleccionados
                  $sql = "select
                         u.usua_codi  as \"CHK_CHKANULAR\"
                         ,u.usua_cedula as \"Cedula\", u.usua_nombre AS \"Nombre\"
                         ,u.usua_email AS \"Email\", u.usua_cargo as \"Cargo\"
                         ,u.depe_nomb AS \"Area\", u.inst_nombre as \"Institución\"";
                  $sql .= " from datos_usuarios u where usua_codi <> 0";
            }


            if (($txt_institucion_codi)!=0) $sql .= " and u.inst_codi=".(0+$txt_institucion_codi);
            if (($txt_area_codi)!=0) $sql .= " and u.depe_codi=".(0+$txt_area_codi);
            if ($txt_nombreci != "") $sql .= ' and ' . buscar_nombre_cedula($txt_nombreci);
            if ($txt_puesto != "") $sql .= " and u.usua_cargo like '%" . $txt_puesto . "%'";
            //$sql .= " order by u.usua_nombre";
             $sql .= " order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*";
             //echo $sql;
	    break;
    }
?>
