<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
 * autor:   teya
 * fecha:   20110621
 * motivo:  para obtener lista de notificaciones
 * ------------------------------------------------------------------------------
**/
//NO VA....

include_once "$ruta_raiz/funciones.php";

    switch($db->driver)	{
	case 'postgres':
	    $mensaje = '<font COLOR="#000000">Eliminar</font>';

            if($cmb_tipo==0){ //notificaciones pendientes
                  $sql = "select
                            m.asunto AS \"Asunto\", u.usua_nombre AS \"Nombre\", u.email AS \"Email\", m.fecha_envio AS \"Fecha Envío\",
                             case when m.estado = 0 then ' (Pendiente)' else  '(Enviado)' end AS \"Estado\"
                            from usuario_notificacion u, mail_notificacion m
                            where m.mail_codi = u.id_mail and m.estado = 0 ";

            }
            if($cmb_tipo==1){ //notificaciones enviadas
                  $sql = "select
                            m.asunto AS \"Asunto\", u.usua_nombre AS \"Nombre\", u.email AS \"Email\", m.fecha_envio AS \"Fecha Programada para el Envío\",
                             case when m.estado = 0 then ' (Pendiente)' else  '(Enviado)' end AS \"Estado\"
                            from usuario_notificacion u, mail_notificacion m
                            where m.mail_codi = u.id_mail and m.estado = 1 ";
            }
            if($cmb_tipo==2){ //todas las notificaciones
                  $sql = "select
                            m.asunto AS \"Asunto\", u.usua_nombre AS \"Nombre\", u.email AS \"Email\", m.fecha_envio AS \"Fecha Programada para el Envío\",
                             case when m.estado = 0 then ' (Pendiente)' else  '(Enviado)' end AS \"Estado\"
                            from usuario_notificacion u, mail_notificacion m
                            where m.mail_codi = u.id_mail ";
            }
            $sql .= " order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*";
            //echo $sql;
 	    break;
    }
?>
