<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * *------------------------------------------------------------------------------
 * autor:   teya
 * fecha:   20110606
 * motivo:  para obtener lista de usuarios para envio de notificaciones programadas
 * ------------------------------------------------------------------------------

 */
$ruta_raiz = "../..";
require_once "$ruta_raiz/funciones.php";
require_once("$ruta_raiz/funciones_interfaz.php");
include_once ("$ruta_raiz/include/db/ConnectionHandler.php");
include_once ("$ruta_raiz/config.php");
$db = new ConnectionHandler("$ruta_raiz");
session_start();


//validar fecha y hora de envio

$sqlFecha =  "select m.mail_codi , m.fecha_envio::date as fechaE, m.fecha_envio::time as horaE, m.usua_remite, m.asunto, m.mensaje,
                u.usua_destinatario, u.usua_nombre, u.email
                from mail_notificacion m, usuario_notificacion u
                where m.estado = 0 and m.mail_codi = u.id_mail
                and m.fecha_envio::date = now()::date";
//echo $sqlFecha;

$fechaSistema = "'" . date('Y-m-d') . "'";
$horaSistema = "'" . date('H:i') . ":00'";

$rsFecha = $db->conn->query($sqlFecha);
while (!$rsFecha->EOF) {

    $fecha_envio = $rsFecha->fields['FECHAE'];
    $fecha_envio = "'" . $fecha_envio ."'";
    $hora_envio = $rsFecha->fields['HORAE'];
    $hora_envio = "'" . $hora_envio ."'";

    if (($fecha_envio == $fechaSistema) && ($horaSistema == $hora_envio)) {

        $mail_codi = $rsFecha->fields['MAIL_CODI'];
        $usr_nombre = $rsFecha->fields['USUA_NOMBRE'];
        $usr_email = $rsFecha->fields['EMAIL'];
        $asunto = $rsFecha->fields['ASUNTO'];
        $mensaje = $rsFecha->fields['MENSAJE'];

        if ($usr_email!='') {
            $mail = "<html><title>Informaci&oacute;n Quipux</title>";
            $mail .= "<body><center><h1>QUIPUX</h1><br /><h2>Sistema de Gesti&oacute;n Documental</h2><br /><br /></center>";
            $mail .= "<br/><right>Notificación programada: $fecha_envio a las $hora_envio</right><br/>";
            $mail .= "Estimado(a) $usr_nombre.<br /><br />";

            $mail .= "<br/><br/>" . $mensaje . "<br/><br/>";


            $mail .= "<br /><br />Gracias por su comprensión.<br /><br />";
            $mail .= "<br /><br />Saludos cordiales,<br /><br />Administraci&oacute;n Quipux.";
            $mail .= "<br /><br /><b>Nota: </b>Este mensaje fue enviado autom&aacute;ticamente por el sistema, por favor no lo responda.";
            $mail .= "<br />Si tiene alguna inquietud respecto a este mensaje, comun&iacute;quese con <a href='mailto:$cuenta_mail_soporte'>$cuenta_mail_soporte</a>";
            $mail .= "</body></html>";  //enviar 5 minutuos antes
          
            enviarMail($mail, $asunto, $usr_email, $usr_nombre, $ruta_raiz);

        }
            // actualizar el estado a 1, de enviado
        $recordMail["ESTADO"]= 1;
        $recordMail["MAIL_CODI"]= $mail_codi;
            $ok1 = $db->conn->Replace("MAIL_NOTIFICACION", $recordMail, "MAIL_CODI", false,false,false,false);
    
    }
    $rsFecha->MoveNext();
}
?>
