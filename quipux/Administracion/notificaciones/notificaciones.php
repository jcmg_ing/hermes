<?php
/* * ***************************************************************************************
 * * Creado para el envio masivo de notificaciones a los usuarios del quipux              **
 * *                                                                                      **
 * * autor: teya                                                                            **
 * fecha: 20110531
 * motivo: para administracion de notificaciones, usuarios conectados o seleccionados       **
 * *************************************************************************************** */

$ruta_raiz = "../..";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post

include "$ruta_raiz/funciones_interfaz.php";

p_register_globals(array());


if ($_SESSION["usua_admin_sistema"] != 1) {
    echo html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
    die("");
}

include_once "$ruta_raiz/rec_session.php";
include_once "$ruta_raiz/js/ajax.js";

$txt_institucion_codi = $_SESSION["inst_codi"];
$txt_area_codi = $_SESSION["depe_codi"];
$txt_buscar = 0;

$txt_fecha_desde = limpiar_sql($_POST["txt_fecha_desde"]);
$txt_fecha_desde = date("Y-m-d");
$txt_fecha_actual = "'" . date('Y-m-d') . "'";

$paginador = new ADODB_Pager_Ajax($ruta_raiz, "div_usuarios_notificaciones", "paginador_notificaciones.php",
                "cmb_estado,txt_institucion_codi,txt_area_codi,txt_nombreci,txt_puesto");
?>



<html>
    <? echo html_head(); /* Imprime el head definido para el sistema */ ?>
    <script type="text/javascript" src="../../js/fckeditor/fckeditor.js"></script>
    <script type="text/javascript" src='../../js/base64.js'></script>

    <script type="text/javascript">
        //
<? include_once "../../js/spiffyCal/spiffyCal_v2_1.js"; ?>
    //
    //    var dateAvailable1 = new ctlSpiffyCalendarBox("dateAvailable1", "formDatos", "txt_fecha_desde","btnDate1","<?= $txt_fecha_desde ?>",scBTNMODE_CUSTOMBLUE);
    //    dateAvailable1.dateFormat="yyyy-MM-dd";

    function cargar_combo_area(txt_area_codi) {
        institucion = document.getElementById("txt_institucion_codi").value;
        nuevoAjax('div_combo_area', 'POST', 'cargar_combo_area.php', 'txt_institucion_codi='+institucion+'&txt_area_codi='+txt_area_codi);
    }

    function verificar_chk() {
        for(i=0;i<document.formDatos.elements.length;i++) {
            if(document.formDatos.elements[i].checked==1 )
                return true;
        }
        return false;
    }

    function verifica_texto_mail(){
        var oEditor = FCKeditorAPI.GetInstance('txt_texto_sobre') ;
        texto_sobre = oEditor.GetData().replace(/<[^>]+>/g,'');
        if (texto_sobre != ''){
            return true;
        }
        else {
            return false;
        }
    }

    function cambiar_editor_sobre(){
        var oEditor = FCKeditorAPI.GetInstance('txt_texto_sobre') ;
        texto_sobre = Base64.encode(Base64.encode(oEditor.GetData()));
        parametros += "&txt_texto_sobre=" + texto_sobre;
    }

    /**
     * Creacion del objeto para el editor de texto
     **/
    function crearEditor()
    {
        // Automatically calculates the editor base path based on the _samples directory.
        // This is usefull only for these samples. A real application should use something like this:
        // oFCKeditor.BasePath = '/fckeditor/' ;	// '/fckeditor/' is the default value.
        var sBasePath = "../../js/fckeditor/" ;
        var oFCKeditor = new FCKeditor( 'txt_texto_sobre' ) ;
        $raditexto = "De mi consideraci&oacute;n:<br><br><br><br>Con sentimientos de distinguida consideraci&oacute;n.<br>&nbsp;<br>&nbsp;";

        oFCKeditor.BasePath	= sBasePath ;
        oFCKeditor.ReplaceTextarea() ;
        //oFCKeditor.Value = stripslashes($raditexto) ;
    }

    // 20110602 para validar la fecha
    function fecha_ver(fechaenvio){
        //
        if (document.formDatos.txt_fecha_desde.value!=''){

            if(compare_dates(fechaenvio, document.formDatos.txt_fecha_desde.value) == true){
                alert("La fecha de programación no puede ser menor a la fecha actual")
            }
            else{
                paginador_reload_div('');
            }//si tiene fechas
        }
        else{
            alert("Ingresar Fecha")
        }
    }

    function compare_dates(fecha, fecha2)
    {
        //alert("compare");
        var xMonth=fecha.substring(5, 7);//primer mes
        var xDay=fecha.substring(8, 10);
        var xYear=fecha.substring(0,4);//primer año
        var yMonth=fecha2.substring(5, 7);
        var yDay=fecha2.substring(8, 10);
        var yYear=fecha2.substring(0,4);//segundo año


        if (xYear> yYear)
        {

            return(true)
        }
        else
        {
            if (xYear == yYear)
            {
                if (xMonth> yMonth)
                {
                    return(true)
                }
                else
                {
                    if (xMonth == yMonth)
                    {
                        if (xDay> yDay)
                            return(true);
                        else
                            return(false);
                    }
                    else{
                        return(false);
                    }
                }
            }
            else
                return(false);
        }
    }

            
    function markAll()
    {
        if(document.formDatos.elements['checkAll'].checked)
            for(i=1;i<document.formDatos.elements.length;i++)
                document.formDatos.elements[i].checked=1;
        else
            for(i=1;i<document.formDatos.elements.length;i++)
                document.formDatos.elements[i].checked=0;
    
    }

    function buscar() {
        document.getElementById("txt_buscar").value = "1";
        //document.formDatos.action = "notificaciones.php";
        //document.formDatos.submit();
        realizar_busqueda();
    }

    function realizar_busqueda() {
        paginador_reload_div('');
    }
    //


    function enviar_mail(){
        //autor:    teya
        //fecha:    20110601
        //motivo:   para enviar el mail a los usuarios de la grid
        if(!verificar_chk()) {
            alert ('No existen usuarios seleccionados.');
            return false;
        }

        if(!verifica_texto_mail()) {
            alert ('No ha ingresado texto para el mail.');
            return false;
        }

        document.formDatos.hd_programar.value=0;
        document.formDatos.submit();
        
    }
    

    function programar_envio(fechaenvio){
        //autor:    teya
        //fecha:    20110601
        //motivo:   para enviar el mail a los usuarios de la grid
        //cotrolar la fecha de programacion
        //alert(document.formDatos.hora_desde.value)
        if(!verificar_chk()) {
            alert ('No existen usuarios seleccionados.');
            return false;
        }

        if(!verifica_texto_mail()) {
            alert ('No ha ingresado texto para el mail.');
            return false;
        }

        if (document.formDatos.txt_fecha_desde.value!=''){

            if(compare_dates(fechaenvio, document.formDatos.txt_fecha_desde.value) == true){
                alert("La fecha de programación no puede ser menor a la fecha actual")
            }
            else{
                document.formDatos.hd_programar.value=1;
                document.formDatos.submit();
            }//si tiene fechas
        }
        else{
            alert("Ingresar Fecha")
        }
    }

    </script>
    <body onload="crearEditor()">
        <!--        <div id="spiffycalendar" class="text"></div>-->
        <form name="formDatos" method="post" action="notificaciones_mail.php">

            <input type="hidden" id="hd_programar" name="hd_programar">
            <table border=0 width="100%" class="borde_tab" cellpadding="0" cellspacing="5">
                <tr >
                    <td colspan="3" class="titulos4"><div align="center"><strong>Administraci&oacute;n de Notificaciones </strong></div></td>
                </tr>
            </table>
            <table width="100%" class="borde_tab">
                <tr>
                    <td width="15%" class="titulos5"><font class="tituloListado">Criterios de B&uacute;squeda: </font></td>
                    <td class="listado5" valign="middle">
                        <table>
                            <tr>
                                <td><span class="listado5">Estado del Usuario</span></td>
                                <td>
                                    <select name="cmb_estado" id="cmb_estado" class='select'>
                                        <option value='0' <? if ($cmb_estado == 1)
        echo "selected"; ?>>Usuarios Conectados</option>
                                        <option value='1' <? if ($cmb_estado == 0)
                                                    echo "selected"; ?>>Usuarios Seleccionados</option>

                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><span class="listado5"><?= $descEmpresa ?></span></td>
                                <td class="listado2">
                                    <?php
                                                $sql = "select inst_nombre, inst_codi from institucion where inst_codi<>0 order by 1 asc";
                                                $rs = $db->conn->query($sql);
                                                if ($rs)
//print $rs->GetMenu2("cmb_institucion", $cmb_institucion, "0:&lt;&lt; Seleccione &gt;&gt;", false, "", "id='cmb_institucion' class='select'");
                                                    print $rs->GetMenu2("txt_institucion_codi", $txt_institucion_codi, "0:&lt;&lt Todas las Instituciones &gt;&gt;", false, "", "class='select' id='txt_institucion_codi' onChange='cargar_combo_area(\"0\");'");
                                    ?>
                                            </td>


                                        </tr>
                                        <tr>
            <!--                                            <td><span class="listado5"><?= $descDependencia ?></span></td>-->

                                            <td>Area:</td>
                                            <td class="listado2">
                                                <div id="div_combo_area">
                                                    <input type="hidden" name="txt_area_codi" id="txt_area_codi" value="<?= $txt_area_codi ?>"></div>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td><span class="listado5">Nombre / C.I. </span> </td>
                                            <td><input type=text id="txt_nombreci" name="txt_nombreci" value="<?= $txt_nombreci ?>" class="tex_area"></td>

                                            <td><span class="listado5">Puesto</span> </td>
                                            <td><input type=text id="txt_puesto" name="txt_puesto" value="<?= $txt_puesto ?>" class="tex_area"></td>
                                        </tr>


                                    </table>
                                </td>
                                <td width="20%" align="center" class="titulos5" >
                                    <input type="hidden" name="txt_buscar" id="txt_buscar" value="<?= $txt_buscar ?>">
                                    <input type="button" name="btn_buscar" value="Buscar" class="botones" onClick="buscar();"><!--"realizar_busqueda();">-->
                                </td>
                            </tr>
                        </table>

                        <center><div id='div_usuarios_notificaciones' style="width: 99%"></div></center>
                        <br />
                        <table  width="100%" borde="1">
                            <tr>
                                <td width="15%" class="titulos5"><font class="tituloListado">Detalle de Correo: </font></td>
                                <td width="70%" align="center">
                                    <textarea name="txt_texto_sobre" rows="10" cols="80" style="width: 100%; height: 200px"><?php echo $OpcImpr["TEXTO_SOBRE"] ?></textarea>
                                </td>

                                <td class="titulos5">
                                    <center>
                                        <input  name="btn_enviar" type="button" class="botones" value="Enviar e-mail" onClick="enviar_mail();" title="Env&iacute;a el correo a los usuarios de la lista">
                                        <br>
                                        <br>
                                        Fecha (aaaa-mm-dd):<br>
                                    </center>
                                    <? echo dibujar_calendario("txt_fecha_desde", date('Y-m-d'), "$ruta_raiz") ?>

                        <!--                        <script language="javascript">
                                    dateAvailable1.writeControl();
                        </script>-->
                                                <!--inicio horas -->
                                                <center>
                                                    <br>
                                                    <br>
                                                    Hora Envío:
                                                    <select id="hora_desde" name="hora_desde">
                                                        <option value="<?php echo "07:00"; ?>"><?php echo "07:00"; ?></option>
                                                        <option value="<?php echo "20:00"; ?>"><?php echo "20:00"; ?></option>
                                                    </select>
                                                    <!--fin horas -->
                                                    <br>

                                                    <input  name="btn_programar" type="button" class="botones" value="Programar" onClick="programar_envio(<?= $txt_fecha_actual ?>);" title="Calendariza para enviar el correo posteriormente"/>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <input  name="btn_accion" type="button" class="botones" value="Regresar" onClick="history.back();" title="Regresa a la página anterior, sin guardar los cambios"/>
                                                </center>
                                            </td>

                                        </tr>
                                    </table>
                                </form>
                            </body>
                            <script language="javascript" type="">
                                cargar_combo_area("<?= $txt_area_codi ?>");
        if (document.getElementById("txt_buscar").value == "1") {
        realizar_busqueda();
        }
    </script>
</html>

