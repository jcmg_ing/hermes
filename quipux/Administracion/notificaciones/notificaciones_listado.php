<?php


/* * ***************************************************************************************
 * * Creado para el envio masivo de notificaciones a los usuarios del quipux              **
 * *                                                                                      **
 * * autor: teya                                                                            **
 * fecha: 20110621
 * motivo: para administracion de notificaciones, listado de notifciaciones                **
 * *************************************************************************************** */

$ruta_raiz = "../..";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post

include "$ruta_raiz/funciones_interfaz.php";

p_register_globals(array());


if ($_SESSION["usua_admin_sistema"] != 1) {
    echo html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
    die("");
}

include_once "$ruta_raiz/rec_session.php";
include_once "$ruta_raiz/js/ajax.js";

$txt_buscar = 0;

$paginador = new ADODB_Pager_Ajax($ruta_raiz, "div_listado_notificaciones", "paginador_notificaciones2.php",
                                    "cmb_tipo");
?>

<html>
    <? echo html_head(); /* Imprime el head definido para el sistema */ ?>

    <script type="text/javascript">

    function buscar() {
        document.getElementById("txt_buscar").value = "1";
        realizar_busqueda();
    }

    function realizar_busqueda() {
        paginador_reload_div('');
    }

    </script>
    <body>
        <form name="formListado">

            <input type="hidden" id="hd_programar" name="hd_programar">
            <table border=0 width="100%" class="borde_tab" cellpadding="0" cellspacing="5">
                <tr >
                    <td colspan="3" class="titulos4"><div align="center"><strong>Listado de Notificaciones a Correo </strong></div></td>
                </tr>
            </table>
            <table width="100%" class="borde_tab">
                <tr>
                    <td width="15%" class="titulos5"><font class="tituloListado">Criterios de B&uacute;squeda: </font></td>
                    <td class="listado5" valign="middle">
                        <table>
                            <tr>
                                <td><span class="listado5">Estado Notificaciones</span></td>
                                <td>
                                    <select name="cmb_tipo" id="cmb_tipo" class='select'>
                                        <option value='0' <? if ($cmb_tipo == 0)
                                                echo "selected"; ?>>Pendiente</option>
                                        <option value='1' <? if ($cmb_tipo == 1)
                                                    echo "selected"; ?>>Enviada</option>
                                        <option value='2' <? if ($cmb_tipo == 2)
                                                    echo "selected"; ?>>Todas</option>
                                    </select>
                                </td>
                            </tr>

                                    </table>
                                </td>
                                <td width="20%" align="center" class="titulos5" >
                                    <input type="hidden" name="txt_buscar" id="txt_buscar" value="<?= $txt_buscar ?>">
                                    <input type="button" name="btn_buscar" value="Buscar" class="botones" onClick="buscar();" title="Consulta la informaci&oacute;n en base a los Criterios"><!--"realizar_busqueda();">-->
                                </td>
                            </tr>
                        </table>

                        <center><div id='div_listado_notificaciones' style="width: 99%"></div></center>


                        <table  width="100%" borde="1">
                            <!--seccion botones -->
                            <tr>
                                 <td class="titulos5" align="center" >
                                                <input  name="btn_accion" type="button" class="botones" value="Regresar" onClick="history.back();" title="Regresa a la página anterior, sin guardar los cambios"/>
                                 </td>

                             </tr>
                           </table>
                                  
                                </form>
                            </body>
                            <script language="javascript" type="">
                               
        if (document.getElementById("txt_buscar").value == "1") {
        realizar_busqueda();
        }
    </script>
</html>


