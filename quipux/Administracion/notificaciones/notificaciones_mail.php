<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$ruta_raiz = "../..";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post

include "$ruta_raiz/funciones_interfaz.php";
include_once ("$ruta_raiz/include/db/ConnectionHandler.php");
include_once ("$ruta_raiz/config.php");
$db = new ConnectionHandler("$ruta_raiz");

p_register_globals(array());
//var_dump($_POST);

if ($_SESSION["usua_admin_sistema"] != 1) {
    echo html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
    die("");
}

include_once "$ruta_raiz/rec_session.php";
include_once "$ruta_raiz/js/ajax.js";


//fechas
$txt_fecha_desde = limpiar_sql($_POST["txt_fecha_desde"]);
//$txt_fecha_desde = date("Y-m-d");

//horas
$txt_hora_desde = limpiar_sql($_POST["hora_desde"]);
$hora_desde = substr($txt_hora_desde, 0, 2);
$minuto_desde = substr($txt_hora_desde, 3, 2);
$txt_hora_desde = $hora_desde . ":" . $minuto_desde;

//criterio de busqueda
$whereFiltro = "-99";
if (isset($_POST['checkValue'])) {  //Si se escogieron radicados de la lista
    foreach ($_POST['checkValue'] as $usr_codi => $chk) {
        $whereFiltro .= ",$usr_codi";
    }
}
//if ($whereFiltro === "-99") { //Si no se escogio ningun radicado
//  YA SE ESTA VALIDANDO EN LA PAG PRINCIPAL
//    die("No hay documentos seleccionados.");
//}

//tomo los valores de la pagina


$fecha = $_POST['txt_fecha_desde'];
//var_dump($fecha);

$hd_accion = $_POST['hd_programar'];
$asunto = "Quipux Administraci&oacute;n";
$usr = limpiar_sql($_SESSION["usua_codi"]);
$txt_texto_sobre = limpiar_sql($_POST['txt_texto_sobre'], 0);
$mensaje = $db->conn->qstr($txt_texto_sobre);


$fecha = $fecha . " " . $txt_hora_desde;
//echo $whereFiltro;
//echo $fecha;
//$fecha = '2011-06-17 12:05';


?>

<html>
    <? echo html_head(); /* Imprime el head definido para el sistema */ ?>

    <script type="text/javascript">

    
        function accion_boton(hd_accion){
 alert("entro a la acciom");
            if(hd_accion == 0){
               
                envio_mail();
            }
            if(hd_accion == 1){
            
                programa_envio();
            }
        }

        function programa_envio(){
            //autor:    teya
            //fecha:    20110608
            //motivo:   para programar el envio del mail a los usuarios seleccionados
           
<?php

    //fin mensaje completo

    $bandera=0;
    $isql = "select usua_codi, usua_nombre, inst_nombre, depe_nomb, usua_cargo, usua_email   from usuario where usua_codi in ($whereFiltro)";
//echo $isql;
    $rs = $db->conn->Execute($isql);
    if (!$rs->EOF) {
        $secuencial = $db->nextId("sec_mail_notificacion");
        $recordR["MAIL_CODI"] = $secuencial;
        $recordR["FECHA_REGISTRO"] = $db->conn->sysTimeStamp;
        $recordR["FECHA_ENVIO"] = $db->conn->DBDate($fecha);
        $recordR["USUA_REMITE"] = $usr;
        $recordR["ASUNTO"] = $db->conn->qstr(substr($asunto, 0, 350));
        $recordR["MENSAJE"] = $db->conn->qstr($txt_texto_sobre); // $txt_texto_sobre
        $recordR["ESTADO"] = "0";
        $db->conn->BeginTrans();
        $insertSQL = $db->conn->Replace("MAIL_NOTIFICACION", $recordR, "", false, false, true, true);
        if ($insertSQL) {
            while (!$rs->EOF) {
                if ($rs->fields["USUA_EMAIL"] != '') {
                    $recordR2["ID_MAIL"] = $secuencial;
                    $recordR2["USUA_DESTINATARIO"] = $rs->fields["USUA_CODI"];
                    $recordR2["USUA_NOMBRE"] = $db->conn->qstr($rs->fields["USUA_NOMBRE"]) ;
                    $recordR2["EMAIL"] = $db->conn->qstr($rs->fields["USUA_EMAIL"]);
                    
                    $insertSQL2 = $db->conn->Replace("USUARIO_NOTIFICACION", $recordR2, "", false, false, true, true);
                    if (!$insertSQL2) {
                        $bandera=1;
                        exit;
                    }
                }
                $rs->MoveNext();
            }
            if ($bandera==0){
                $db->conn->CommitTrans();
            } else {
                $db->conn->RollbackTrans();
            }
        } else {
            $db->conn->RollbackTrans();
        }
    }
?>
    }

        function envio_mail(){
        //autor:    teya
        //fecha:    20110601
        //motivo:   para enviar el mail a los usuarios de la grid
        alert('fin envio .........');

<?php
    $isql = "select usua_codi, usua_nomb, inst_nombre, depe_nomb, usua_cargo, usua_email   from usuario where usua_codi in ($whereFiltro)";
//echo $isql;
    $rs = $db->conn->Execute($isql);

    while (!$rs->EOF) {
        $usr_codi = $rs->fields["USUA_CODI"];
        $usr_nombre = $rs->fields["USUA_NOMB"];
        $usr_institucion = $rs->fields["INST_NOMBRE"];
        $usr_area = $rs->fields["DEPE_NOMB"];
        $usr_cargo = $rs->fields["USUA_CARGO"];
        $usr_email = $rs->fields["USUA_EMAIL"];
        if ($usua_email != '') {
            $mail = "<html><title>Informaci&oacute;n Quipux</title>";
            $mail .= "<body><center><h1>QUIPUX</h1><br /><h2>Sistema de Gesti&oacute;n Documental</h2><br /><br /></center>";
            $mail .= "Estimado(a) $usr_nombre.<br /><br />";

            $mail .= "<br/>" . $mensaje . "<br/>";
            
            $mail .= "<br /><br />Gracias por su comprensión.<br /><br />";
            $mail .= "<br /><br />Saludos cordiales,<br /><br />Administraci&oacute;n Quipux.";
            $mail .= "<br /><br /><b>Nota: </b>Este mensaje fue enviado autom&aacute;ticamente por el sistema, por favor no lo responda.";
            $mail .= "<br />Si tiene alguna inquietud respecto a este mensaje, comun&iacute;quese con <a href='mailto:$cuenta_mail_soporte'>$cuenta_mail_soporte</a>";
            $mail .= "</body></html>";
            //enviar 5 minutuos antes
            enviarMail($mail, "Quipux: Administraci&oacute;n.", $usr_email, $usr_nomb, $ruta_raiz);
        }
        $rs->MoveNext();
    }
?>
    
    document.location.href = "notificaciones.php";
    }

    </script>
    <body>
        <form name="formEnviar"  method="post"  action="notificaciones_mensaje.php">
            <div id="spiffycalendar" class="text"></div>

            <table border=0 width="100%" class="borde_tab" cellpadding="0" cellspacing="5">

                <input type=hidden id="buscar_usuario" name="buscar_usuario" value="1" class="tex_area">
                <tr >
                    <td colspan="3" class="titulos4"><div align="center"><strong>Administraci&oacute;n de Notificaciones </strong></div></td>
                </tr>
            </table>

            <!-- mostrar los usuarios a los que se van a enviar los correos-->
<?php
            $sqlU = "SELECT u.usua_cedula, u.usua_nombre, u.usua_email, u.usua_cargo
                     ,u.depe_nomb, u.inst_nombre";
            $sqlU .= " from datos_usuarios u where usua_codi in ($whereFiltro)";

            $rsU = $db->conn->Execute($sqlU);
            if (!$rsU->EOF){
?>
            <table>
                <tr>
                    <td class="titulos4">Cedula</td>
                    <td class="titulos4">Nombre</td>
                    <td class="titulos4">Mail</td>
                    <td class="titulos4">Cargo</td>
                    <td class="titulos4">Area</td>
                    <td class="titulos4">Institucion</td>
                </tr>
<?php
                 while (!$rsU->EOF) {
?>
                <tr>
                    <td class="listado2_ver"> <?php echo $rsU->fields["USUA_CEDULA"];?></td>
                    <td class="listado2_ver"> <?php echo $rsU->fields["USUA_NOMBRE"];?></td>
                    <td class="listado2_ver"> <?php echo $rsU->fields["USUA_EMAIL"];?></td>
                    <td class="listado2_ver"> <?php echo $rsU->fields["USUA_CARGO"];?></td>
                    <td class="listado2_ver"> <?php echo $rsU->fields["DEPE_NOMB"];?></td>
                    <td class="listado2_ver"> <?php echo $rsU->fields["INST_NOMBRE"];?></td>

                </tr>
 <?php
            $rsU->MoveNext();
 }
?>
            </table>
<?php
 }
?>
           

            <center><div id='div_usuarios_seleccionados' style="width: 99%"></div></center>
            <br />
            <table width="100%">
                <tr>
                    <td align="center" class="listado2_ver">
                        <?php $hd_accion="'".$hd_accion."'"?>
                        <input  name="btn_confirmar" type="button" class="botones" value="Confirmar" onClick="accion_boton(<?=$hd_accion?>);" title="Env&iacute;a o Programa el correo a los usuarios de la lista"/>
                    </td>

                    <td align="center" class="listado2_ver">
                        <input  name="btn_accion" type="button" class="botones" value="Regresar" onClick="history.back();" title="Regresa a la página anterior, sin guardar los cambios"/>
                    </td>

                </tr>
            </table>

            <br>

        </form>
    </body>
</html>


