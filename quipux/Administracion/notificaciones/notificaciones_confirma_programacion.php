<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//validar si existen
$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
include "$ruta_raiz/funciones_interfaz.php";

$fecha = $_POST['txt_fecha_desde'];


//horas
$txt_hora_desde = limpiar_sql($_POST["hora_desde"]);
$hora_desde = substr($txt_hora_desde, 0, 2);
$minuto_desde = substr($txt_hora_desde, 3, 2);
$txt_hora_desde = $hora_desde . ":" . $minuto_desde;

$fecha = $fecha . " " . $txt_hora_desde;

$usr = limpiar_sql($_SESSION["usua_codi"]);
$error = "";

$bandera_control = limpiar_sql($_POST['hd_editar'], 0);
$hd_mail = limpiar_sql($_POST['hd_mail'], 0);

$usuarios_lista = limpiar_sql($_POST["txt_usuarios_lista"]);
$usuarios_lista = limpiar_sql($_POST["txt_usuarios_lista_total"]);

$lista_codi = str_replace("-", "", str_replace("--", ",", $usuarios_lista));

$asunto = limpiar_sql($_POST['txt_asunto']);
$txt_texto_sobre = limpiar_sql($_POST['txt_texto_sobre'], 0);


echo "<html>" . html_head();
?>
<script type="text/JavaScript">
    function inicio() {

        document.getElementById('div_programacion_mail').innerHTML = '<center>Por favor espere mientras se realiza la programaci&oacute;n de Correos.<br>&nbsp;<br>' +
            '<img src="<?= $ruta_raiz ?>/imagenes/progress_bar.gif"><br>&nbsp;</center>';
        return;
    }
    function ocultar(){
        document.getElementById('div_programacion_mail').style.display = 'none';
        return;
    }

</script>

<body bgcolor="#FFFFFF" onload="inicio();" >
    <form method="post" name="formEnvio" id="formEnvio" >
        <center>
            <table border=0 width="100%" class="borde_tab" cellpadding="0" cellspacing="5">
                <tr >
                    <td colspan="3" class="titulos4"><div align="center"><strong>Administraci&oacute;n de Notificaciones a Correo -Programaci&oacute;n de Correos</strong></div></td>
                </tr>
            </table>
            <div id="div_programacion_mail" style="width:100%; height:80; border:1px solid #000000;"></div>
            <?php
            if ($bandera_control == 0) {
                //echo 'insertar nueva programacion';
                $bandera = 0;
                $isql = "select usua_codi, usua_nombre, inst_nombre, depe_nomb, usua_cargo, usua_email   from usuario where usua_codi in ($lista_codi) and usua_esta = 1";

                $rs = $db->conn->Execute($isql);
                if (!$rs->EOF) {
                    $secuencial = $db->nextId("sec_mail_notificacion");
                    $recordR["MAIL_CODI"] = $secuencial;
                    $recordR["FECHA_REGISTRO"] = $db->conn->sysTimeStamp;
                    $recordR["FECHA_ENVIO"] = $db->conn->DBDate($fecha);
                    $recordR["USUA_REMITE"] = $usr;
                    $recordR["ASUNTO"] = $db->conn->qstr(substr($asunto, 0, 350));
                    $recordR["MENSAJE"] = $db->conn->qstr($txt_texto_sobre); // $txt_texto_sobre
                    $recordR["ESTADO"] = "0";
                    //echo 'insertar nueva programacion2';
                    $db->conn->BeginTrans();
                    $insertSQL = $db->conn->Replace("MAIL_NOTIFICACION", $recordR, "", false, false, true, false);
                    if ($insertSQL) {
                        while (!$rs->EOF) {
                            //echo 'insertar nueva programacion3';
                            if ($rs->fields["USUA_EMAIL"] != '') {
                                $recordR2["ID_MAIL"] = $secuencial;
                                $recordR2["USUA_DESTINATARIO"] = $rs->fields["USUA_CODI"];
                                $recordR2["USUA_NOMBRE"] = $db->conn->qstr($rs->fields["USUA_NOMBRE"]);
                                $recordR2["EMAIL"] = $db->conn->qstr($rs->fields["USUA_EMAIL"]);

                                $insertSQL2 = $db->conn->Replace("USUARIO_NOTIFICACION", $recordR2, "", false, false, true, false);
                                if (!$insertSQL2) {
                                    $bandera = 1;
                                    exit;
                                }
                            }
                            $rs->MoveNext();
                        }
                        if ($bandera == 0) {
                            //echo 'insertar nueva programacion4';
                            $db->conn->CommitTrans();
                            $error = "Los correos se programaron exitosamente";

                            echo "<script>ocultar();</script>";
                            echo "<br/><table width='100%'><tr><td align='center'><font color='blue' face='Arial' size='3'>$error</font></td></tr></table>";
                        } else {
                            //echo 'insertar nueva programacion5';
                            $db->conn->RollbackTrans();
                            $error = "Los correos no se programaron, verifique";

                            echo "<script>ocultar();</script>";
                            echo "<br/><table width='100%'><tr><td align='center'><font color='red' face='Arial' size='3'>$error</font></td></tr></table>";
                        }
                    } else {
                        $db->conn->RollbackTrans();
                        //echo 'insertar nueva programacion6';
                        $error = "Los correos no se programaron, verifique";

                        echo "<script>ocultar();</script>";
                        echo "<br/><table width='100%'><tr><td align='center'><font color='red' face='Arial' size='3'>$error</font></td></tr></table>";
                    }
                }
            }
            if ($bandera_control == 2) {
  //              echo 'actualiza la programacion';
                // grabar la programacion existente
                $bandera = 0;
                $db->conn->BeginTrans();

                $recordMail["ASUNTO"] = $db->conn->qstr(substr($asunto, 0, 350));
                $recordMail["MENSAJE"] = $db->conn->qstr($txt_texto_sobre);

                $recordMail["MAIL_CODI"] = $hd_mail;
                $ok1 = $db->conn->Replace("MAIL_NOTIFICACION", $recordMail, "MAIL_CODI", false, false, true, false);
                if (ok1) {
                    //echo 'actualiza la programacion2';
                    $db->conn->query("delete from usuario_notificacion where id_mail =" . $hd_mail);
//echo 'actualiza la programacion3';
                    $isql = "select usua_codi , usua_nombre, inst_nombre, depe_nomb, usua_cargo, usua_email  from usuario where usua_codi in ($lista_codi) and usua_esta = 1";

                    $rs = $db->conn->query($isql);

                    if (!$rs->EOF) {
                        while (!$rs->EOF) {
  //                          echo 'actualiza la programacion4';

                            if ($rs->fields["USUA_EMAIL"] != '') {

                                $recordR2["ID_MAIL"] = $hd_mail;
                                $recordR2["USUA_DESTINATARIO"] = $rs->fields["USUA_CODI"];
                                $recordR2["USUA_NOMBRE"] = $db->conn->qstr($rs->fields["USUA_NOMBRE"]);
                                $recordR2["EMAIL"] = $db->conn->qstr($rs->fields["USUA_EMAIL"]);
//echo 'actualiza la programacion5';
                                $insertSQL2 = $db->conn->Replace("USUARIO_NOTIFICACION", $recordR2, "", false, false, true, false);
                                if (!$insertSQL2) {
                                    $bandera = 1;
                                    exit;
                                }
                            }
                            $rs->MoveNext();
                        }
                        if ($bandera == 0) {
                       //     echo 'actualiza la programacion6';
                            $db->conn->CommitTrans();
                            $error = "Los correos se programaron exitosamente";

                            echo "<script>ocultar();</script>";
                            echo "<br/><table width='100%'><tr><td align='center'><font color='blue' face='Arial' size='3'>$error</font></td></tr></table>";
                        } else {
                            $db->conn->RollbackTrans();
                     //       echo 'actualiza la programacion7';
                            $error = "Los correos no se programaron, verifique";

                            echo "<script>ocultar();</script>";
                            echo "<br/><table width='100%'><tr><td align='center'><font color='red' face='Arial' size='3'>$error</font></td></tr></table>";
                        }
                    }
                }
            }
            ?>
            <input type='button' id="regresar" name="regresar" value='Regresar' class="botones"  onclick="window.location='notifica_busqueda.php'">

        </center>
    </form>

</body>
</html>
