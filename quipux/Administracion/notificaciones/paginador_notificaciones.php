<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
* autor:   teya
* fecha:   20110601
* motivo:  para obtener lista de usuarios para notificaciones
* ------------------------------------------------------------------------------
 * **/

$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";
if (isset ($replicacion) && $replicacion && $config_db_replica_not_paginador_notificaciones!="") $db = new ConnectionHandler($ruta_raiz,$config_db_replica_not_paginador_notificaciones);

include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();

$cmb_estado = $_GET['cmb_estado'];
$cmb_institucion = $_GET['cmb_institucion'];
$cmb_area = $_GET['cmb_area'];
$txt_nombreci = $_GET['txt_nombreci'];
$txt_puesto = $_GET['txt_puesto'];
//para que funcione el orden en la grid, tambien hay q aumentar una linea en el query
if($orden_cambio==1)
    {
 	if ($orderTipo=="asc")
	   $orderTipo="desc";
	else
	   $orderTipo="asc";
    }
    if (!$orderTipo) $orderTipo="asc";
    if (!$orderNo) $orderNo=0;


?>

  <body>
<?

            include "queryNotificaciones1.php";
       
	$pager = new ADODB_Pager($db,$sql,'adodb', true,$orderNo,$orderTipo,true);
	$pager->checkAll = false; //para activar el check, se presenta checkeados
	$pager->checkTitulo = true; // para q se muestre para check todo
	$pager->toRefLinks = $linkPagina;
	$pager->toRefVars = $encabezado;
	$pager->descCarpetasGen=$descCarpetasGen;
	$pager->descCarpetasPer=$descCarpetasPer;
	$pager->Render($rows_per_page=8,$linkPagina,$checkbox=chkAnulados); //chkAnulados
?>

  </body>
</html>

