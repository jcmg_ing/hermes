<?php
/* * ***************************************************************************************
 * * Creado para el envio masivo de notificaciones a los usuarios del quipux              **
 * *                                                                                      **
 * * autor: teya                                                                            **
 * fecha: 20110627
 * motivo: para administracion de notificaciones, usuarios conectados o seleccionados       **
 *          pagina inicial
 * *************************************************************************************** */

$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
include_once "$ruta_raiz/obtenerdatos.php";
include "$ruta_raiz/funciones_interfaz.php";

echo "<html>" . html_head();

// LIBRERIAS PARA GENERADOR DE ARBOL AJAX
echo '<link rel="StyleSheet" href="' . $ruta_raiz . '/js/nornix-treemenu-2.2.0/example/style/menu.css" type="text/css" media="screen" />
      <script type="text/javascript" src="' . $ruta_raiz . '/js/nornix-treemenu-2.2.0/treemenu/nornix-treemenu.js"></script>';



include_once "$ruta_raiz/js/ajax.js";

//fechas
$txt_fecha_actual = "'" . date('Y-m-d') . "'";
$txt_fecha_desde = limpiar_sql($_POST["txt_fecha_desde"]);
$txt_fecha_desde = date("Y-m-d");

//horas
$txt_hora_actual = getdate(time());
$hora_actual = $txt_hora_actual["hours"] . ":" . $txt_hora_actual["minutes"];
?>



<script type="text/javascript" src="../../js/fckeditor/fckeditor.js"></script>
<script type="text/javascript" src='../../js/base64.js'></script>

<script type="text/JavaScript">

    var timerID;

    function fecha_ver(fechaenvio){
        //
        if (document.formulario.txt_fecha_desde.value!=''){

            if(compare_dates(fechaenvio, document.formulario.txt_fecha_desde.value) == true){
                alert("La fecha de programación no puede ser menor a la fecha actual")
            }
            else{
                paginador_reload_div('');
            }//si tiene fechas
        }
        else{
            alert("Ingresar Fecha")
        }
    }

    function comparar_hora(hora1, hora2){
        var horasis=hora1.substring(0,2);
        var horasel=hora2.substring(0,2);
        if (horasel<horasis){
            return(true);
        }
        else
        {
            return(false);
        }
    }

    function compare_dates(fecha, fecha2)
    {
        var xMonth=fecha.substring(5, 7);//primer mes
        var xDay=fecha.substring(8, 10);
        var xYear=fecha.substring(0,4);//primer año
        var yMonth=fecha2.substring(5, 7);
        var yDay=fecha2.substring(8, 10);
        var yYear=fecha2.substring(0,4);//segundo año


        if (xYear> yYear)
        {

            return(true)
        }
        else
        {
            if (xYear == yYear)
            {
                if (xMonth> yMonth)
                {
                    return(true)
                }
                else
                {
                    if (xMonth == yMonth)
                    {
                        if (xDay> yDay)
                            return(true);
                        else
                            return(false);
                    }
                    else{
                        return(false);
                    }
                }
            }
            else
                return(false);
        }
    }



    function crearEditor()
    {
        // Automatically calculates the editor base path based on the _samples directory.
        // This is usefull only for these samples. A real application should use something like this:
        // oFCKeditor.BasePath = '/fckeditor/' ;	// '/fckeditor/' is the default value.
        var sBasePath = "../../js/fckeditor/" ;
        var oFCKeditor = new FCKeditor( 'txt_texto_sobre' ) ;

        raditexto = "De mi consideraci&oacute;n:<br><br><br><br>Con sentimientos de distinguida consideraci&oacute;n.<br>&nbsp;<br>&nbsp;";
        
        oFCKeditor.BasePath	= sBasePath ;
        oFCKeditor.ReplaceTextarea() ;
        //oFCKeditor.Value = stripslashes(raditexto) ;
    }


    function verifica_texto_mail(){
        var oEditor = FCKeditorAPI.GetInstance('txt_texto_sobre') ;
        texto_sobre = oEditor.GetData().replace(/<[^>]+>/g,'');

        if (texto_sobre != ''){
            return true;
        }
        else {
            return false;
        }
    }
    function verifica_usuarios_seleccionados(){
        var lista = document.formulario.txt_usuarios_lista.value;
        if (lista == ''){
            return false;
        }
        else{
            return true;
        }
    }

    // ACCIONES
    // SIGUIENTE:
    function siguiente(){
        if(!verifica_usuarios_seleccionados()) {
            alert('No ha seleccionado ningún usuario para Enviar/Programar Notificaciones');
            return false;
        }

        if (document.formulario.hd_editar.value == 2){
            var oEditor2 = FCKeditorAPI.GetInstance('txt_texto_sobre') ;

            texto_sobrea = document.formulario.hd_texto_sobre.value;
            texto_sobren = oEditor2.GetData().replace(/<[^>]+>/g,'');

            var mensaje_final = texto_sobren + texto_sobrea;

            oEditor2.SetHTML(mensaje_final);

        }
       
        //ocultar
        document.getElementById('div_primera_pagina').style.display = 'none';
        document.getElementById('div_segunda_pagina').style.display = '';
        document.getElementById('div_busqueda_pagina').style.display = 'none';

        document.getElementById('btn_siguiente').style.display = 'none';
        document.getElementById('btn_listado').style.display = 'none';
        document.getElementById('cancelar').style.display = 'none';

        //mostrar
        document.getElementById('btn_enviar').style.display = '';
        document.getElementById('btn_programar').style.display = '';
        document.getElementById('regresar').style.display = '';
        document.getElementById('div_botones2').style.display = '';
        

    }

    function enviar_mail_lista_usuarios() {
        if(!verifica_texto_mail()) {
            alert ('No ha ingresado texto para el mail.');
            return false;
        }
        if(!verifica_usuarios_seleccionados()) {
            alert('No ha seleccionado ningún usuario para Enviar/Programar Notificaciones');
            return false;
        }
        if (document.formulario.txt_asunto.value != ''){
            document.formulario.action = 'notificaciones_confirma_envio.php';
            document.formulario.submit();
        }
        else
        {
            alert('No ha ingresado texto en el asunto del mail');
        }
        //window.close();
        //document.location.href = "notifica_busqueda.php";
    }

    function verifica_programacion(){
        var fecha=new Date()
        var anio= fecha.getFullYear()
        var mes= fecha.getMonth()
        var dia = fecha.getDate()

        mes = mes +1
        if (mes<=9)
        mes="0"+mes

        if (dia<=9)
        dia="0"+dia

        var fechaenvio = anio + '-' + mes + '-' + dia
          
        var horasel = document.formulario.hora_desde.value;
        if (horasel != 0)
        {
            if (document.formulario.txt_fecha_desde.value!=''){

                if(compare_dates(fechaenvio, document.formulario.txt_fecha_desde.value) == true){
                    alert("La fecha de programación no puede ser menor a la fecha actual")
                    return false;
                }
                var fecha = document.formulario.txt_fecha_desde.value
                var hora = document.formulario.hora_desde.value
                var lista = document.formulario.txt_usuarios_lista.value
        
                nuevoAjax('div_programar_mail', 'POST', 'notificaciones_verifica_existentes.php', 'fechaprog=' + fecha + '&horaprog=' + hora + '&lista_codi='+ lista);
            }
            else{
                alert("Ingrese una fecha")
            }
        }
        else{
            alert('Favor seleccione la hora a programar');
        }
    }

    function programar_envio(fechaenvio, horasistema){
        //verificaciones
        if(!verifica_usuarios_seleccionados()) {
            alert('No ha seleccionado ningún usuario para Enviar/Programar Notificaciones');
            return false;
        }
        if(!verifica_texto_mail()) {
            alert ('No ha ingresado texto para el mail.');
            return false;
        }
        if (document.formulario.txt_asunto.value == ''){
            alert ('No ha ingresado un asunto para el mail.');
        }
        var horasel = document.formulario.hora_desde.value;
        if (horasel == 0){
            alert('Favor seleccione la hora a programar');
            return false;
        }

        if (document.formulario.hd_verificar1.value == 1){
            alert('Existe una notificación programada para la fecha/hora seleccionada. Edítela o seleccione otra fecha/hora');
            return false;
        }

        if (document.formulario.txt_fecha_desde.value!=''){
            if(compare_dates(fechaenvio, document.formulario.txt_fecha_desde.value) == true){
                alert("La fecha de programación no puede ser menor a la fecha actual")
                return false;
            }
            else{
                if (fechaenvio == document.formulario.txt_fecha_desde.value){
                    if(comparar_hora(horasistema, document.formulario.hora_desde.value) == true){
                        alert("La hora de programación no puede ser menor a la hora actual")
                        return false;
                    }
                    else
                    {

                        document.formulario.action = 'notificaciones_confirma_programacion.php';
                        document.formulario.submit();
                    }
                       
                }
                else
                {

                    document.formulario.action = 'notificaciones_confirma_programacion.php';
                    document.formulario.submit();
                }

            }//si tiene fechas
        }
        else{
            alert("Ingrese una fecha")
        }
    }

    function regresar_pagina(){
        

        document.getElementById('div_primera_pagina').style.display = '';
        document.getElementById('div_busqueda_pagina').style.display = '';
        document.getElementById('div_segunda_pagina').style.display = 'none';

        //mostrar
        document.getElementById('btn_siguiente').style.display = '';
        document.getElementById('btn_listado').style.display = '';
        document.getElementById('cancelar').style.display = '';

        //ocultar
        document.getElementById('btn_enviar').style.display = 'none';
        document.getElementById('btn_programar').style.display = 'none';
        document.getElementById('regresar').style.display = 'none';
        document.getElementById('div_botones2').style.display = 'none';

        //
    }

    function seleccionar_id_mail(idmail){
        document.formulario.hd_editar.value= 2;
        document.formulario.hd_borrar.value= 0;
        document.formulario.hd_mail.value= idmail;
        document.formulario.hd_verificar1.value= 0;

        document.formulario.txt_usuarios_lista.value = document.formulario.txt_usuarios_lista_total.value
        //aqui debo tomar los usuarios de ese mail ?

        var datos = "txt_usuarios_lista=" + document.formulario.txt_usuarios_lista.value +
            "&txt_accion= " + document.formulario.hd_editar.value +
            "&txt_borrar= " + document.formulario.hd_borrar.value +
            "&txt_idmail= " + idmail;

        nuevoAjax('div_lista_usuarios', 'POST', 'listas_cargar_usuarios.php', datos);

        document.getElementById('div_programar_mail').style.display = 'none';
        //mostrar
        document.getElementById('btn_siguiente').style.display = '';
        document.getElementById('btn_listado').style.display = '';
        document.getElementById('cancelar').style.display = '';
        //ocultar
        document.getElementById('btn_enviar').style.display = 'none';
        document.getElementById('btn_programar').style.display = 'none';
        document.getElementById('regresar').style.display = 'none';

        document.getElementById('div_primera_pagina').style.display = '';
        document.getElementById('div_segunda_pagina').style.display = 'none';
        document.getElementById('div_botones2').style.display = 'none';
        document.getElementById('div_busqueda_pagina').style.display = '';
        //deberia cargar lo seleccionado + lo existente
    }

    //fin teya

    function cargar_datos_lista(lista_codi) {
        document.getElementById('div_datos_lista').innerHTML = '';
        nuevoAjax("div_datos_lista", "POST", "listas_datos_lista.php", "txt_lista_codi="+lista_codi);
        cargar_lista_usuarios();
    }

    function cambiar_tipo_usuario() {
        
        document.getElementById('tr_institucion').style.display = '';
        if (document.getElementById('txt_buscar_institucion').value != '0') {
            document.getElementById('tr_dependencia').style.display = '';
            document.getElementById('tr_area').style.display = '';
        }
        
    }

    function buscar_usuarios() {
        if(document.formulario.txt_buscar_tipo_usuario.value == 1 ){
            if( document.formulario.txt_buscar_nombre.value == '' &&
                document.formulario.txt_buscar_cargo.value == '' &&
                document.formulario.txt_buscar_institucion.value == 0){
                alert('Debe seleccionar al menos un criterio de búsqueda');
                return;
            }
        }
        document.getElementById('div_buscar_usuarios').innerHTML = '<center>Por favor espere mientras se realiza la b&uacute;squeda.<br>&nbsp;<br>' +
            '<img src="<?= $ruta_raiz ?>/imagenes/progress_bar.gif"><br>&nbsp;</center>';

        var datos = "txt_buscar_tipo_usuario=" + document.formulario.txt_buscar_tipo_usuario.value +
            "&txt_buscar_nombre=" + document.formulario.txt_buscar_nombre.value +
            "&txt_buscar_cargo=" + document.formulario.txt_buscar_cargo.value +
            "&txt_buscar_institucion=" + document.formulario.txt_buscar_institucion.value +
            "&txt_buscar_dependencia=" + document.formulario.txt_buscar_dependencia.value;
        nuevoAjax('div_buscar_usuarios', 'POST', 'listas_buscar_usuarios.php', datos);
        return;
    }

    function seleccionar_usuario(cod_usr) {
          
        try {
            // Verificamos que no se encuentren ya seleccionados los usuarios

            if (document.formulario.txt_usuarios_lista.value.indexOf('-' + cod_usr + '-') >= 0) {
                alert("El usuario ya esta en lista, por favor verifique.");
                return;
            }
          
            //            alert(cod_usr);
            document.formulario.txt_usuarios_lista.value += '-' + cod_usr + '-';


            cargar_lista_usuarios();
        } catch (e) {}
    }

    function seleccionar_todos_usuarios() {
        try {
            flag = false;
            cod_usr = document.formulario.txt_buscar_usuarios_lista.value.split(",");
            for (i=0 ; i<cod_usr.length ; ++i) {
                if (cod_usr[i] != '') {
                    // Verificamos que no se encuentren ya seleccionados los usuarios
                    if (document.formulario.txt_usuarios_lista.value.indexOf('-' + cod_usr[i] + '-') >= 0) {
                        flag = true;
                    } else {
                        document.formulario.txt_usuarios_lista.value += '-' + cod_usr[i] + '-';
                    }
                }
            }
            cargar_lista_usuarios();
            if (flag)
                alert("Uno o varios usuarios ya estaban seleccionados previamente en lista, por favor verifique.");
        } catch (e) {}
    }


    function cargar_lista_usuarios() {
        try {
            if (document.formulario.txt_usuarios_lista.type == 'textarea' && document.formulario.txt_usuarios_lista.type == 'textarea') {
                clearTimeout(timerID);
                document.getElementById('div_lista_usuarios').innerHTML = '<center>Por favor espere mientras se realiza la b&uacute;squeda.<br>&nbsp;<br>' +
                    '<img src="<?= $ruta_raiz ?>/imagenes/progress_bar.gif"><br>&nbsp;</center>';
                if (document.formulario.hd_editar.value != 2 ){
                    var datos = "txt_usuarios_lista=" + document.formulario.txt_usuarios_lista.value +
                        "&txt_accion= " + document.formulario.hd_editar.value +
                        "&txt_borrar = " + document.formulario.hd_borrar.value +
                        "&txt_idmail= " + document.formulario.hd_mail.value;
                }
                else
                {
                    document.formulario.hd_borrar.value = 1;
                   // alert(document.formulario.txt_usuarios_lista_total);
                    var datos = "txt_usuarios_lista=" + document.formulario.txt_usuarios_lista_total.value +
                        "&txt_accion= " + document.formulario.hd_editar.value +
                        "&txt_borrar= " + document.formulario.hd_borrar.value +
                        "&txt_idmail= " + document.formulario.hd_mail.value;
                }
                //alert(datos);
                nuevoAjax('div_lista_usuarios', 'POST', 'listas_cargar_usuarios.php', datos);
            } else {
                timerID = setTimeout("cargar_lista_usuarios()", 200);
            }
            return;
        } catch (e) {
            timerID = setTimeout("cargar_lista_usuarios()", 200);
        }
    }

    function borrar_usuario_lista(codigo)
    {
//        alert(codigo);
//        alert(document.formulario.txt_usuarios_lista.value);
//        alert(document.formulario.txt_usuarios_lista_total.value);
//        alert(document.formulario.hd_editar.value);
        
        if (codigo == 0) {
            if (confirm ('¿Desea borrar todos los usuarios de la lista?'))
                document.formulario.txt_usuarios_lista.value = '';
            else
                return;
        } else {

            if (document.formulario.hd_editar.value != 2){
                document.formulario.txt_usuarios_lista.value = document.formulario.txt_usuarios_lista.value.replace('-' + codigo + '-','');
            } else {
                document.formulario.txt_usuarios_lista_total.value = document.formulario.txt_usuarios_lista_total.value.replace('-' + codigo + '-','');
            }

        }

        cargar_lista_usuarios();
    }

    function grabar_lista_usuarios(datolista) {
        if (document.formulario.txt_lista_nombre.value == '') {
            alert ('Por favor ingrese el nombre de la lista');
            return;
        }
        document.formulario.btn_aceptar.disabled = true;
        document.formulario.action = 'listas_grabar.php';
        document.formulario.submit();
        if (datolista!='')
            window.close();
    }

    function buscar_depePadre(){
        document.formulario.txt_buscar_dependencia.value = "";
        var cod_inst = document.formulario.txt_buscar_institucion.value;
        if (cod_inst == 0) {
            document.getElementById('tr_dependencia').style.display = 'none';
            document.getElementById('tr_area').style.display = 'none';
            document.getElementById('txt_buscar_dependencia').value = '';
            return;
        }
        document.getElementById('tr_dependencia').style.display = '';
        document.getElementById('tr_area').style.display = '';
        var nomDivPadre = 'mnu_depePadre';
        nuevoAjax('area', 'GET', 'formArea_ajax.php', 'codDepe=');
        if(document.getElementById(nomDivPadre).innerHTML == '')
            nuevoAjax(nomDivPadre, 'GET', 'formDepePadre_ajax.php', 'codInst=' + cod_inst);
        else
            document.getElementById(nomDivPadre).innerHTML = '';
        return;
    }

    function buscar_depeHijo(cod_depe){
        document.formulario.txt_buscar_dependencia.value = cod_depe;
        var nomDivHijo = 'mnu_depeHijo_' + cod_depe;
        nuevoAjax('area', 'GET', 'formArea_ajax.php', 'codDepe=' + cod_depe);
        if(document.getElementById(nomDivHijo).innerHTML == '')
            nuevoAjax(nomDivHijo, 'GET', 'formDepeHijo_ajax.php', 'codDepe=' + cod_depe);
        else
            document.getElementById(nomDivHijo).innerHTML = '';
        return;
    }

    function desplegarContraer(cual,desde) {
        var elElemento=document.getElementById(cual);
        if(elElemento.className == 'elementoVisible') {
            elElemento.className = 'elementoOculto';
            desde.className = 'linkContraido';
        } else {
            elElemento.className = 'elementoVisible';
            desde.className = 'linkExpandido';
        }
    }



</script>
<?php
if ($_GET["lst_codigo"] != '')
    $codigo_lista = $_GET["lst_codigo"];
else
    $codigo_lista= 0;
?>
<!--<body bgcolor="#FFFFFF" onload="crearEditor();">-->
<body bgcolor="#FFFFFF" onload="cargar_datos_lista(<?php echo $codigo_lista; ?>); crearEditor();" >
    <form method="post" name="formulario" id="formulario" action="" >
        <center>

            <div id="div_datos_lista"></div>
            <!--para controles varios -->
            <input type="hidden" id="hd_editar" name="hd_editar" value="0">
            <input type="hidden" id="hd_mail" name="hd_mail" value="0">
            <input type="hidden" id="hd_borrar" name="hd_borrar" value="0">

            <table border=0 width="100%" class="borde_tab" cellpadding="0" cellspacing="5">
                <tr >
                    <td colspan="3" class="titulos4"><div align="center"><strong>Administraci&oacute;n de Notificaciones a Correo </strong></div></td>
                </tr>
            </table>
            <div id="div_busqueda_pagina"  style="display:visible">
                <table border=0 width="100%" class="borde_tab" cellpadding="0" cellspacing="5">
                    <tr>
                        <td width="15%" class="listado5"><font class="tituloListado">Buscar Usuarios: </font></td>
                        <td width="70%" class="listado5" valign="middle">
                            <table>
                                <tr>
                                    <td width="25%" align="right"><span class="listado5">Enviar a Usuarios: </span></td>
                                    <td colspan="3">
                                        <select name='txt_buscar_tipo_usuario' id='txt_buscar_tipo_usuario' class='select' >
                                            <option value="0" selected>Usuarios Conectados</option>
                                            <option value="1">Usuarios Seleccionados</option>

                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%" align="right" class="listado5">Nombre / C.I.:</td>
                                    <td width="25%"><input type=text name="txt_buscar_nombre" id="txt_buscar_nombre" value="" class="tex_area"/></td>
                                    <td width="25%" align="right"><span class="listado5"><?= $descCargo ?>: </span> </td>
                                    <td width="25%"><input type=text name="txt_buscar_cargo" id="txt_buscar_cargo" value="" class="tex_area"></td>
                                </tr>
                                <tr id="tr_institucion" >
                                    <td align="right"><span class="listado5"><?= $descEmpresa ?>: </span></td>
                                    <td colspan="3">
                                        <?
                                        $sql = "select distinct inst_nombre, inst_codi from institucion where inst_estado=1 and inst_codi>0 order by 1";
                                        $rs = $db->conn->query($sql);
                                        echo $rs->GetMenu2("txt_buscar_institucion", "0", "0:&lt;&lt; Todas las Instituciones &gt;&gt;", false, "", "id='txt_buscar_institucion' class='select' onChange='buscar_depePadre()'");
                                        //echo $rs->GetMenu2("txt_buscar_institucion", "0", "Todas las Instituciones", false, "", "id='txt_buscar_institucion' class='select' onChange='buscar_depePadre()'");
                                        ?>
                                    </td>
                                </tr>
                                <tr id="tr_area" style="display: none">
                                    <td  align="right"><span class="listado5">&Aacute;rea Seleccionada: </span></td>
                                    <td colspan="3">
                                        <div id="area" class="divArea">&nbsp;&nbsp;<?= $inputArea ?></div>
                                    </td>
                                </tr>
                                <tr id="tr_dependencia" style="display:none">
                                    <td colspan="4" valign="middle">
                                        <input type="hidden" name="txt_buscar_dependencia" id="txt_buscar_dependencia" value=""/>
                                        <div onclick="desplegarContraer('unaLista',this);" class="linkContraido">Selecionar &Aacute;rea</div>
                                        <ul id="unaLista" class='elementoOculto'>
                                            <div id="wrapper">
                                                <div id="menu" class="menu"><a href="#" onclick="buscar_depePadre();">&Aacute;reas </a>
                                                    <ul id="mnu_depePadre"></ul>
                                                </div>
                                            </div>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="15%" align="center" class="listado5" >
                            <input type="button" name="btn_buscar" value="Buscar" class="botones" title="Buscar Persona" onclick="buscar_usuarios()">
                        </td>
                    </tr>
                </table>
            </div>
            <div id="div_primera_pagina"  style="display:visible">
                <br>
                <table class=borde_tab width="100%" cellpadding="0" cellspacing="4">
                    <tr class=listado2>
                        <td width="100%">
                            <center><b>RESULTADO DE LA B&Uacute;SQUEDA</b></center>
                        </td>
                    </tr>
                </table>
                <div id="div_buscar_usuarios" class="estiloDivPeq"></div>

                <br>
                <table class=borde_tab width="100%" cellpadding="0" cellspacing="4">
                    <tr class=listado2>
                        <td width="100%">
                            <center><b>USUARIOS A LOS QUE SE ENVIARAN CORREOS/NOTIFICACIONES<?php
//                                        if ($lista_codi != '')
//                                            echo strtoupper($descRadicado); else
//                                            echo "LISTADO"; ?>
                                    </b></center>
                            </td>
                        </tr>
                    </table>
                    <div id="div_lista_usuarios" class="estiloDivPeq"></div>
                </div>

                <div id="div_segunda_pagina"  style="display:none">
                    <table  width="100%" borde="1">
                        <tr>
                            <td width="10%" class="titulos5"><font class="tituloListado">Asunto: </font></td>
                            <td width="100%" >
                                <input type=text id="txt_asunto" name="txt_asunto" size="90" maxlength="100" >
                            </td>
                        </tr>
                    </table>

                    <table  width="100%" borde="1">
                        <tr>
                            <td width="100%" align="center">
                                <textarea name="txt_texto_sobre" rows="50" cols="80" style="width: 100%; height: 500px"></textarea>
                            </td>
                        </tr>
                    </table>
                    <table  width="100%" borde="1">
                        <tr>
                        </tr>
                    </table>
                    <div id="div_programar_mail"></div>
                </div>
                <div id="div_tercera_pagina"  style="display:none">
                    <div id="div_editar_notificaciones" class="estiloDivPeq"></div>
                </div>


                <table  width="100%" borde="1">
                    <tr>
                        <td>
                            <input type='button' id="btn_siguiente"  name="btn_siguiente" value='Siguiente' class="botones" onclick='siguiente()' title="Muestra página para el ingreso del texto del mail" >
                        </td>
                        <td>
                            <input type='button' id="btn_listado"  name="btn_listado" value='Listado' class="botones" onclick="window.location='notificaciones_listado.php'" title="Genera un listado de Notificaciones">
                        </td>
                        <td>
                            <input type='button' id ="cancelar" value='Cancelar' class="botones" onclick="window.location='../formAdministracion.php'" title="Regresa al Menú de Administración">
                        </td>



                    </tr>
                </table>
                <div id="div_botones2"  style="display:none"  >
                    <table  width="100%" borde="1">
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <input type='button' id="btn_enviar"  name="btn_enviar" value='Enviar Mail' style='display:none'  class="botones" onclick='enviar_mail_lista_usuarios()' title="Envía mail(s) en este momento">
                            </td>
                        </tr>
                        <tr>

                            <td class="titulos5" width="30%" >
                                Fecha Env&iacute;o (aaaa-mm-dd):
                            <? echo dibujar_calendario("txt_fecha_desde", date('Y-m-d'), "$ruta_raiz") ?>
                                    </td>
                                    <td class="titulos5" align="left" width="20%" >
                                        <!--inicio horas -->
                                        Hora Env&iacute;o:
                                        <select id="hora_desde" name="hora_desde" OnChange="verifica_programacion()">
                                            <option value="<?php echo "0"; ?>"><?php echo "Horas.."; ?></option>
                                            <option value="<?php echo "07:00"; ?>"><?php echo "07:00"; ?></option>
                                            <option value="<?php echo "20:00"; ?>"><?php echo "20:00"; ?></option>
                                        </select>
                                        <!--fin horas -->
                                    </td>
                                    <td>
                                        <input type='button' id="btn_programar" name="btn_programar" value='Programar' style='display:none' class="botones" onClick="programar_envio(<?= $txt_fecha_actual . ",'" . $hora_actual . "'" ?>);" title="Guarda la notificación para su posterior envío">
                        </td>
                    </tr>
                </table>
            </div>
            <table>
                <tr>
                    <td width="30%"></td>
                    <td width="20%"></td>
                    <td></td>
                    <td>
                        <input type='button' id="regresar" name="regresar" value='Regresar' class="botones" style='display:none' onclick='regresar_pagina()' title="Retorna a la página con los usuarios seleccionados">
                    </td>
                </tr>
            </table>
            <br>&nbsp;
        </center>
    </form>

</body>
</html>