<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
 * autor:   teya
 * fecha:   20110610
 * motivo:  para cargar las areas dependiendo de la institucion
*------------------------------------------------------------------------------
**/

session_start();
$ruta_raiz = "../..";
include_once "$ruta_raiz/rec_session.php";
if (isset ($replicacion) && $replicacion && $config_db_replica_not_cargar_combo_area!="") $db = new ConnectionHandler($ruta_raiz,$config_db_replica_not_cargar_combo_area);

$txt_institucion_codi = limpiar_numero($_POST["txt_institucion_codi"]);
$txt_area_codi = limpiar_numero($_POST["txt_area_codi"]);

$where = "inst_codi=$txt_institucion_codi";
if ($txt_institucion_codi==0)
    $where = "inst_codi=".$_SESSION["inst_codi"];

$sql = "select depe_nomb, depe_codi from dependencia where $where order by 1 asc";
$rs = $db->conn->Execute($sql);
if ($rs)
    echo $rs->GetMenu2("txt_area_codi", $txt_area_codi, "0:&lt;&lt; Todas las áreas&gt;&gt;", false,""," id='txt_area_codi' class='select'" );
?>
