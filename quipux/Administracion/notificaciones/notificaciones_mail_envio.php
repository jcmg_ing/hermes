<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * *------------------------------------------------------------------------------
 * autor:   teya
 * fecha:   20110607
 * motivo:  para obtener lista de usuarios para envio de notificaciones programadas
 * ------------------------------------------------------------------------------

 */
$ruta_raiz = "../..";
require_once "$ruta_raiz/funciones.php";
require_once("$ruta_raiz/funciones_interfaz.php");
include_once ("$ruta_raiz/include/db/ConnectionHandler.php");
include_once ("$ruta_raiz/config.php");
$db = new ConnectionHandler("$ruta_raiz");
session_start();

//definir los usuarios a enviar el mail
echo 'INGRESO A OTRA PAG';


//seleccion de datos
$sqlAdmin = "select usua_codi, usua_nomb, inst_nombre, depe_nomb, usua_cargo, usua_email   from usuario where usua_codi in ($whereFiltro)";
//echo $isql;
$rs = $db->conn->Execute($sqlAdmin);

while (!$rs->EOF) {
    $usua_codi = $rs->fields["USUA_CODI"];
    $usua_nombre = $rs->fields["USUA_NOMB"];
    $usua_institucion = $rs->fields["INST_NOMBRE"];
    $usua_area = $rs->fields["DEPE_NOMB"];
    $usua_cargo = $rs->fields["USUA_CARGO"];
    $usua_email = $rs->fields["USUA_EMAIL"];

    // necesito el texto
    // no se si existan los check

    if ($usua_email != '') {
        $mail = "<html><title>Informaci&oacute;n Quipux</title>";
        $mail .= "<body><center><h1>QUIPUX</h1><br /><h2>Sistema de Gesti&oacute;n Documental</h2><br /><br /></center>";
        $mail .= "Estimado(a) $usr_nombre.<br /><br />";
        $mail .= "El Sistema de Gesti&oacute;n Documental Quipux le notifica que desde el día $txt_fecha_desde </b>&quot;";
        $mail .= "hasta el día $txt_fecha_hasta desde las $txt_hora_desde hasta las $txt_hora_hasta<br />
                          El Sistema QUIPUX, entra en Mantenimiento, luego de las fechas y horas indicadas, se continuará con el Servicio.";
        $mail .= "<br /><br />Gracias por su comprensión.<br /><br />";
        $mail .= "<br /><br />Saludos cordiales,<br /><br />Soporte Quipux.";
        $mail .= "<br /><br /><b>Nota: </b>Este mensaje fue enviado autom&aacute;ticamente por el sistema, por favor no lo responda.";
        $mail .= "<br />Si tiene alguna inquietud respecto a este mensaje, comun&iacute;quese con <a href='mailto:$cuenta_mail_soporte'>$cuenta_mail_soporte</a>";
        $mail .= "</body></html>";
        //enviar 5 minutuos antes
        enviarMail($mail, "Quipux: Mantenimiento.", $usua_email, $usua_nomb, $ruta_raiz);
    }

    $rs->MoveNext();
}


//echo $sqlAdmin;
$rsBloqSist = $db->conn->query($sqlAdmin);
while (!$rsBloqSist->EOF) {
    $mail_codi = $rsBloqSist->fields['MAIL_CODI'];
    $fecha_registro = $rsBloqSist->fields['FECHA_REGISTRO'];
    $fecha_envio = $rsBloqSist->fields['FECHA_ENVIO'];
    $usua_remite = $rsBloqSist->fields['USUA_REMITE'];
    $usua_destinatario = $rsBloqSist->fields['USUA_DESTINATARIO'];
    $usua_email = $rsBloqSist->fields['EMAIL'];
    if ($usua_email != '') {
        $mail = "<html><title>Informaci&oacute;n Quipux</title>";
        $mail .= "<body><center><h1>QUIPUX</h1><br /><h2>Sistema de Gesti&oacute;n Documental</h2><br /><br /></center>";
        $mail .= "Estimado(a) $usr_nombre.<br /><br />";
        $mail .= "El Sistema de Gesti&oacute;n Documental Quipux le notifica que desde el día $txt_fecha_desde </b>&quot;";
        $mail .= "hasta el día $txt_fecha_hasta desde las $txt_hora_desde hasta las $txt_hora_hasta<br />
                          El Sistema QUIPUX, entra en Mantenimiento, luego de las fechas y horas indicadas, se continuará con el Servicio.";
        $mail .= "<br /><br />Gracias por su comprensión.<br /><br />";
        $mail .= "<br /><br />Saludos cordiales,<br /><br />Soporte Quipux.";
        $mail .= "<br /><br /><b>Nota: </b>Este mensaje fue enviado autom&aacute;ticamente por el sistema, por favor no lo responda.";
        $mail .= "<br />Si tiene alguna inquietud respecto a este mensaje, comun&iacute;quese con <a href='mailto:$cuenta_mail_soporte'>$cuenta_mail_soporte</a>";
        $mail .= "</body></html>";
        //enviar 5 minutuos antes
        enviarMail($mail, "Quipux: Mantenimiento.", $usua_email, $usua_nomb, $ruta_raiz);
    }
    $rsBloqSist->MoveNext();
}
?>
