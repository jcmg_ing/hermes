<?php
/**  Programa para el manejo de gestion documental, oficios, memorandos, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

$ruta_raiz = "../..";
session_start();
if ($_SESSION["usua_codi"] != 0) die(html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina."));
include_once "$ruta_raiz/rec_session.php";

$accion = 0+$_POST["txt_accion"];
$mensaje = "";
$anio = date("Y");
$dir_bodega = "$ruta_raiz/bodega/$anio";
$i = 0;

// Creamos la carpeta bodega
$carpetas = array ($anio,"tmp","plantillas","logos","plantillas_ciudadanos","respaldos","ciudadanos","validar_bodega");

foreach ($carpetas as $carpeta) {
    $dir_bodega = "$ruta_raiz/bodega/$carpeta";
    if (!is_dir($dir_bodega)) {
        $mensaje .= "\n<tr class='listado".($i%2+1)."'><td>".(++$i)."</td><td>bodega/$carpeta</td><td>General</td><td>General</td><td>";
        if ($accion==1) {
            if (mkdir($dir_bodega, 0755)) {
                grabar_log ("mkdir(\"$dir_bodega\", 0755);", "BODEGA", 0);
                $mensaje .= "<font color='blue'>La carpeta ha sido creada exitosamente.</font>";
            } else {
                $mensaje .= "<font color='red'>Error - No se pudo crear la carpeta</font>";
            }
        } else {
            $mensaje .= "No existe la carpeta";
        }
        $mensaje .= "</td></tr>";
    }
}
$dir_bodega = "$ruta_raiz/bodega/$anio";


// Creamos los archivos de alertas necesarios
$archivos = array ("mensaje_alerta_top.html");

foreach ($archivos as $archivo) {
    $dir_archivo = "$ruta_raiz/bodega/$archivo";
    if (!is_file($dir_archivo)) {
        $mensaje .= "\n<tr class='listado".($i%2+1)."'><td>".(++$i)."</td><td>bodega/$archivo</td><td>General</td><td>General</td><td>";
        if ($accion==1) {
            if (file_put_contents($dir_archivo, " ")) {
                grabar_log ("file_put_contents(\"$dir_archivo\", \" \");", "BODEGA", 0);
                $mensaje .= "<font color='blue'>El archivo ha sido creada exitosamente.</font>";
            } else {
                $mensaje .= "<font color='red'>Error - No se pudo crear el archivo</font>";
            }
        } else {
            $mensaje .= "No existe el archivo";
        }
        $mensaje .= "</td></tr>";
    }
}


// Consulto las areas creadas en el sistema
$rs = $db->query("select depe_codi, depe_nomb, inst_nombre from dependencia d left outer join institucion i on d.inst_codi=i.inst_codi order by inst_nombre, depe_nomb");
// Creamos la estructura de la bodega
while(!$rs->EOF) {
    $carpeta = substr("000000".trim($rs->fields["DEPE_CODI"]),-6);
    $dir_carpeta = "$dir_bodega/$carpeta";

    if (!is_dir($dir_carpeta)) {
        $mensaje .= "\n<tr class='listado".($i%2+1)."'><td>".(++$i)."</td><td>bodega/$anio/$carpeta</td><td>".$rs->fields["INST_NOMBRE"]."</td><td>".$rs->fields["DEPE_NOMB"]."</td><td>";
        if ($accion==1) {
            if (mkdir("$dir_carpeta", 0755)) {
                grabar_log ("mkdir(\"$dir_carpeta\", 0755);", "BODEGA", 0);
                $mensaje .= "<font color='blue'>La carpeta ha sido creada exitosamente.</font>";
            } else {
                $mensaje .= "<font color='red'>Error - No se pudo crear la carpeta</font>";
            }
        } else {
            $mensaje .= "No existe la carpeta";
        }
        $mensaje .= "</td></tr>";
    }

    $dir_carpeta .= "/docs";
    if (!is_dir($dir_carpeta)) {
        $mensaje .= "\n<tr class='listado".($i%2+1)."'><td>".(++$i)."</td><td>bodega/$anio/$carpeta/docs</td><td>".$rs->fields["INST_NOMBRE"]."</td><td>".$rs->fields["DEPE_NOMB"]."</td><td>";
        if ($accion==1) {
            if (mkdir("$dir_carpeta", 0755)) {
                grabar_log ("mkdir(\"$dir_carpeta\", 0755);", "BODEGA", 0);
                $mensaje .= "<font color='blue'>La carpeta ha sido creada exitosamente.</font>";
            } else {
                $mensaje .= "<font color='red'>Error - No se pudo crear la carpeta</font>";
            }
        } else {
            $mensaje .= "No existe la carpeta";
        }
        $mensaje .= "</td></tr>";
    }

    $rs->MoveNext();
}

if ($mensaje == "") {
    $mensaje = "<br>No existieron novedades en la estructura de la bodega.";
} else {
    $mensaje = '
    <table width="100%" border="0" cellpadding="0" cellspacing="3" class="borde_tab">
        <tr>
            <td colspan=5><center><b>Verificar estructura de la bodega</b></center></td>
        </tr>
        <tr>
            <th width="5%">No.</th>
            <th width="15%">Carpeta</th>
            <th width="27%">Instituci&oacute;n</th>
            <th width="27%">&Aacute;rea</th>
            <th width="26%">Mensaje</th>
        </tr>'.$mensaje."\n</table>";

}

if ($accion==1) file_put_contents("$ruta_raiz/bodega/validar_bodega/bodega_".date("Y-m-d_H-i-s").".html", $mensaje);

$mensaje .= "<br><span id='spn_ok' style='display: none;'>OK</span>";

echo $mensaje;


function grabar_log ($sentencia, $tabla, $flag) {
    global $db;
    $flag_log = 1;
    if (!$flag) $flag_log = 0;
    $fecha = $db->conn->sysTimeStamp;
    $sentencia = $db->conn->qstr($sentencia);
    $tabla = $db->conn->qstr($tabla);
    $usr = $_SESSION["usua_codi"];
    $sql = "insert into log (fecha, usua_codi, tabla, sentencia, tipo) values ($fecha,$usr,$tabla,$sentencia,$flag_log)";
    $db->query($sql);
    return;
}

?>