<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**  permite validar la estructura de la bodega y que esten completos los archivos       **
*****************************************************************************************/

$ruta_raiz = "../..";
session_start();
if ($_SESSION["usua_codi"] != 0) die(html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina."));
include_once "$ruta_raiz/rec_session.php";


require_once "$ruta_raiz/funciones.php";
include_once "$ruta_raiz/funciones_interfaz.php";

echo "<html>".html_head(); /*Imprime el head definido para el sistema*/
require_once "$ruta_raiz/js/ajax.js";



?>
<script type="text/javascript">
    var txt_accion = 0;
    var txt_proceso = 0; // Si aprieta el botón de pausar ejecucion
    var timerID = 0;

    function consultar_siguiente_sentencia() {
        nuevoAjax('div_consultar_sentencia', 'POST', 'actualizar_BDD_consultar_sentencia.php', '');
    }

    function ejecutar_proceso() {
        var pagina='';
        switch (txt_proceso) {
            case '1':
                pagina = 'validar_bodega_estructura.php';
                break;
            case '2':
                pagina = 'validar_bodega_documentos.php';
                break;
            case '3':
                pagina = 'validar_bodega_firmados.php';
                break;
            case '4':
                pagina = 'validar_bodega_anexos.php';
                break;
            default:
                alert ('Por favor seleccione el proceso a ejecutar.');
                return;
                break;
        }
        document.getElementById('div_ejecutar_proceso').innerHTML = '';
        document.getElementById('div_esperar').style.display = '';
        document.getElementById('tr_botones').style.display = 'none';

        nuevoAjax('div_ejecutar_proceso', 'POST', pagina, 'txt_accion='+txt_accion+'&txt_fecha='+document.getElementById('txt_fecha').value);
        esperar_respuesta_ejecucion();
        return;
    }

    function esperar_respuesta_ejecucion() {
        try {
            if (trim(document.getElementById('spn_ok').innerHTML)!='') {
                document.getElementById('div_esperar').style.display = 'none';
                document.getElementById('tr_botones').style.display = '';
                clearTimeout(timerID);
            }
        } catch (e) {
            timerID = setTimeout("esperar_respuesta_ejecucion()", 1000);
            return;
        }
    }
</script>

<body>
  <center>
    <table width="90%" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
        <tr>
            <th colspan="4"><center>Proceso a Ejecutar</center></th>
        </tr>
        <tr>
            <td class="listado1" width="10%">&nbsp;</td>
            <td class="listado1" width="40%">
                <input type="radio" name="rad_proceso" id="rad_proceso" value="1" onclick="txt_proceso=this.value">&nbsp;Verificar estructura de la bodega<br>
                <input type="radio" name="rad_proceso" id="rad_proceso" value="2" onclick="txt_proceso=this.value">&nbsp;Verificar existencia archivos generados<br>
                <input type="radio" name="rad_proceso" id="rad_proceso" value="3" onclick="txt_proceso=this.value">&nbsp;Verificar existencia archivos firmados electr&oacute;nicamente<br>
                <input type="radio" name="rad_proceso" id="rad_proceso" value="4" onclick="txt_proceso=this.value">&nbsp;Verificar existencia archivos anexos<br>
                &nbsp;
            </td>
            <td class="listado1" width="20%" valign="middle">
                fecha: &nbsp;&nbsp;<?php echo dibujar_calendario("txt_fecha", date("Y-m-d"), $ruta_raiz, ""); ?>
            </td>
            <td class="listado1" width="30%" valign="middle">
                <input type="radio" name="rad_accion" id="rad_accion" value="0" onclick="txt_accion=this.value" checked>&nbsp;Validar<br>
                <input type="radio" name="rad_accion" id="rad_accion" value="1" onclick="txt_accion=this.value">&nbsp;Reparar<br>
                &nbsp;
            </td>
        </tr>
        <tr id="tr_botones">
            <td colspan="4" class="listado1" align="center" valign="middle">
                <input type="button" name="btn_ejecutar" id="btn_ejecutar" value="Ejecutar Proceso" class="botones_largo" onclick="ejecutar_proceso()" >
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" name="btn_pausar" id="btn_pausar" value="Regresar" class="botones_largo" onclick="history.back()">
                <br>&nbsp;
            </td>
        </tr>
        <tr>
            <th colspan="4"><center>Estado de la Ejecuci&oacute;n</center></th>
        </tr>
        <tr>
            <td colspan="4" class="listado1" align="center">
                <div id="div_esperar" style="width: 98%; text-align: center; display: none;">
                    <br>Por favor espere mientras se procesa su petici&oacute;n.<br>&nbsp;<br>
                    <img src="<?=$ruta_raiz?>/imagenes/progress_bar.gif" alt=""><br>&nbsp;
                </div>
                <div id="div_ejecutar_proceso" style="height: 400px; width: 98%; overflow: auto; text-align: center;"></div>
            </td>
        </tr>
    </table>
    
  </center>
</body>