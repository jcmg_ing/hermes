<?php
/**  Programa para el manejo de gestion documental, oficios, memorandos, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

$ruta_raiz = "../..";
session_start();
if ($_SESSION["usua_codi"] != 0) die(html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina."));
include_once "$ruta_raiz/rec_session.php";
require_once "$ruta_raiz/funciones.php";

$accion = 0+$_POST["txt_accion"];
$fecha = limpiar_sql($_POST["txt_fecha"]);
$anio = date("Y");
$dir_bodega = "$ruta_raiz/bodega/$anio";

$mensaje = "";



$i = 0;

$sql = "select a.*, r.radi_nume_text, u.usua_nombre, u.inst_nombre, u.usua_email, u.depe_nomb
        from (
            select distinct anex_radi_nume, anex_path, anex_fecha, anex_nombre, anex_usua_codi, anex_codigo
            from anexos
            where anex_borrado='N' and anex_fecha::date='$fecha'::date
        ) as a
        left outer join radicado r on r.radi_nume_radi=a.anex_radi_nume
        left outer join datos_usuarios u on u.usua_codi=a.anex_usua_codi
        order by anex_fecha";

$rs = $db->query($sql);

while (!$rs->EOF) {
    $flag_existe = 0;
    // Verificamos si existe el archivo o el tamaño es cero
    if (!is_file("$ruta_raiz/bodega".$rs->fields["ANEX_PATH"])) {
        $flag_existe = 1;
    } else if (filesize ("$ruta_raiz/bodega".$rs->fields["ANEX_PATH"]) == 0) {
        $flag_existe = 2;
    }

    if ($flag_existe > 0) {
        $mensaje .= "\n<tr class='listado".($i%2+1)."'><td>".++$i."</td><td>".$rs->fields["INST_NOMBRE"]."</td><td>".$rs->fields["DEPE_NOMB"]."</td><td>".$rs->fields["USUA_NOMBRE"].
                    "</td><td>".$rs->fields["RADI_NUME_TEXT"]."</td><td>".$rs->fields["ANEX_NOMBRE"].
                    "</td><td style='display:none;'>".$rs->fields["ANEX_PATH"]."</td><td>".substr($rs->fields["ANEX_FECHA"],0,19)."</td><td>";
        if ($flag_existe == 1) {
            $mensaje .= "No se encuentra el Archivo";
            if ($accion==1) {
                $sql = "update anexos set anex_borrado='S' where anex_path='".$rs->fields["ANEX_PATH"]."'; -- Archivo no encontrado";
                $db->query($sql);
                grabar_log ($sql, "BODEGA", 0);
                $mensaje .= "<br> - <font color='blue'>Se inicializ&oacute; path.</font>";
            }
        } else {
            $mensaje .= "Archivo con tama&ntilde;o &quot;0&quot;";
            if ($accion==1) {
                rename("$ruta_raiz/bodega".$rs->fields["ANEX_PATH"], "$ruta_raiz/bodega".$rs->fields["ANEX_PATH"]."0");
                grabar_log ("rename(\"$ruta_raiz/bodega".$rs->fields["ANEX_PATH"]."\", \"$ruta_raiz/bodega".$rs->fields["ANEX_PATH"]."0\");", "BODEGA", 0);
                $sql = "update anexos set anex_borrado='S' where anex_path='".$rs->fields["ANEX_PATH"]."'; -- Archivo tamaño 0";
                $db->query($sql);
                grabar_log ($sql, "BODEGA", 0);
                $mensaje .= "<br> - <font color='blue'>Se inicializ&oacute; path y renombr&oacute; archivo.</font>";
            }
        }

        if ($accion==1) {
            $sql = "select radi_nume_radi from radicado where radi_path='".$rs->fields["ANEX_PATH"]."'";
            $rs_rad = $db->query($sql);
            while ($rs_rad && !$rs_rad->EOF) {
                $sql = "update radicado set radi_path=null where radi_nume_radi=".$rs_rad->fields["RADI_NUME_RADI"]."; -- Anexo no encontrado - ".$rs->fields["ANEX_CODIGO"];
                $db->query($sql);
                grabar_log ($sql, "BODEGA", 0);
                $mensaje .= "<br> - <font color='blue'>Se inicializ&oacute; path del documento No. ".$rs_rad->fields["RADI_NUME_RADI"]."</font>";
                $rs_rad->MoveNext();
            }
        }

        $mensaje .= "</td></tr>";
    }
    $rs->MoveNext();
}

if ($mensaje == "") {
    $mensaje = "<br>No existieron novedades en los anexos.";
} else {
   $mensaje = '
    <table width="100%" border="0" cellpadding="0" cellspacing="3" class="borde_tab">
        <tr>
            <td colspan=9><center><b>Verificar existencia archivos anexos</b></center></td>
        </tr>
        <tr>
            <th width="2%">No.</th>
            <th width="17%">Instituci&oacute;n</th>
            <th width="17%">&Aacute;rea</th>
            <th width="17%">Usuario</th>
            <th width="10%">No. Documento</th>
            <th width="12%">Nombre Anexo</th>
            <th width="0%" style="display:none;">Path</th>
            <th width="10%">Fecha</th>
            <th width="15%">Mensaje</th>
        </tr>'.$mensaje."\n</table>";

}

if ($accion==1) file_put_contents("$ruta_raiz/bodega/validar_bodega/anexos_".date("Y-m-d_H-i-s").".html", $mensaje);

$mensaje .= "<br><span id='spn_ok' style='display: none;'>OK</span>";

echo $mensaje;


function grabar_log ($sentencia, $tabla, $flag) {
    global $db;
    $flag_log = 1;
    if (!$flag) $flag_log = 0;
    $fecha = $db->conn->sysTimeStamp;
    $sentencia = $db->conn->qstr($sentencia);
    $tabla = $db->conn->qstr($tabla);
    $usr = $_SESSION["usua_codi"];
    $sql = "insert into log (fecha, usua_codi, tabla, sentencia, tipo) values ($fecha,$usr,$tabla,$sentencia,$flag_log)";
    $db->query($sql);
    return;
}

?>