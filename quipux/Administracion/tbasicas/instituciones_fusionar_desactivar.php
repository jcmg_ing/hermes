<?php
/**  Programa para el manejo de gestion documental, oficios, memorandos, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";
if($_SESSION["usua_codi"]!=0) die ("Usted no tiene los permisos suficientes para acceder a esta p&aacute;gina.");

$mensaje = "";
$inst_origen = 0 + $_POST["txt_inst_origen"];
$inst_destino = 0 + $_POST["txt_inst_destino"];
$record = array();

// Consulto los usuarioa creados en la institucion origen
$rs = $db->query("select usua_codi, usua_cedula from usuarios where inst_codi=$inst_origen");
// Inicializamos las secuencias
while(!$rs->EOF) {
    unset ($record);
    $record["usua_codi"]    = trim($rs->fields["USUA_CODI"]);
    $record["usua_login"]   = $db->conn->qstr("l" . trim($rs->fields["USUA_CODI"]));
    $record["usua_cedula"]  = $db->conn->qstr(substr(trim($rs->fields["USUA_CEDULA"]),0,10) . "-" . trim($rs->fields["USUA_CODI"]));
    $record["usua_esta"]    = "0";
    $ok = $db->conn->Replace("usuarios", $record, "usua_codi", false,false,true,false);
    if ($ok != 1) $mensaje .= "Error: No se pudo desactivar el usuario ".trim($rs->fields["USUA_CODI"])."<br>";
    $rs->MoveNext();
}

unset ($record);
$record["inst_codi"]    = $inst_origen;
$record["inst_estado"]    = "0";
$ok = $db->conn->Replace("institucion", $record, "inst_codi", false,false,true,false);
if ($ok != 1) $mensaje .= "Error: No se pudo desactivar la institucion ".trim($rs->fields["USUA_CODI"])."<br>";

if ($mensaje == "") $mensaje = "OK";

echo $mensaje;
?>