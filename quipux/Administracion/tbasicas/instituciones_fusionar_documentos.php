<?php
/**  Programa para el manejo de gestion documental, oficios, memorandos, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";
if($_SESSION["usua_codi"]!=0) die ("Usted no tiene los permisos suficientes para acceder a esta p&aacute;gina.");

$mensaje = "";
$inst_origen = 0 + $_POST["txt_inst_origen"];
$inst_destino = 0 + $_POST["txt_inst_destino"];
$record = array();

// Consulto las areas creadas en la institucion origen
$rs = $db->query("select radi_nume_radi from radicado where radi_inst_actu=$inst_origen");
// Inicializamos las secuencias
while(!$rs->EOF) {
    unset ($record);
    $record["radi_nume_radi"] = trim($rs->fields["RADI_NUME_RADI"]);
    $record["radi_inst_actu"] = $inst_destino;
    $ok = $db->conn->Replace("radicado", $record, "radi_nume_radi", false,false,true,false);
    if ($ok != 1) $mensaje .= "Error: No se pudo mover el documento No. ".trim($rs->fields["RADI_NUME_RADI"])."<br>";
    $rs->MoveNext();
}

if ($mensaje == "") $mensaje = "OK";

echo $mensaje;
?>