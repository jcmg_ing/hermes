<?php
/**  Programa para el manejo de gestion documental, oficios, memorandos, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";

if($_SESSION["usua_codi"]!=0) die ("Usted no tiene los permisos suficientes para acceder a esta p&aacute;gina.");

include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();
include_once "$ruta_raiz/js/ajax.js";

$sql = "select inst_nombre, inst_codi from institucion where inst_estado=1 and inst_codi>0 order by 1";
$rs=$db->conn->query($sql);
$menu_inst_origen = $rs->GetMenu2("txt_inst_origen", "0", "0:&lt;&lt seleccione &gt;&gt;", false,"","id='txt_inst_origen' class='select'");
$rs->Move(0);
$menu_inst_destino = $rs->GetMenu2("txt_inst_destino", "0", "0:&lt;&lt seleccione &gt;&gt;", false,"","id='txt_inst_destino' class='select'");


?>
    <script>
        var proceso = new Array();
        proceso[0] = 'Inicializando proceso para unir dos instituciones';
        proceso[1] = 'Mover &aacute;reas a la instituci&oacute;n destino';
        proceso[2] = 'Mover usuarios a la instituci&oacute;n destino';
        proceso[3] = 'Mover documentos a la instituci&oacute;n destino';
        proceso[4] = 'Desactivando instituci&oacute;n origen';

        var pagina = new Array();
        pagina[0] = '';
        pagina[1] = 'instituciones_fusionar_areas.php';
        pagina[2] = 'instituciones_fusionar_usuarios.php';
        pagina[3] = 'instituciones_fusionar_documentos.php';
        pagina[4] = 'instituciones_fusionar_desactivar.php';

        var num_proceso = 0;
        var max_proceso = 4;
        var timerID;
        var parametros;
        
        proceso[max_proceso+1] = '<center><h3><i>Proceso finalizado.</i></h3></center>';

        function fusionar_instituciones() {
            inst_origen = document.getElementById("txt_inst_origen").value || '0';
            inst_destino = document.getElementById("txt_inst_destino").value || '0';
            if (inst_origen == '0') {
                alert ('Por favor seleccione la institución a ser desactivada');
                return;
            }
            if (inst_destino == '0') {
                alert ('Por favor seleccione la institución a la que se moverán las áreas, usuarios y documentos');
                return;
            }
            if (inst_origen == inst_destino) {
                alert ('Por favor seleccione dos instituciones diferentes');
                return;
            }

            if (confirm('¿Desea desctivar la institución ' + document.getElementById("txt_inst_origen").options[document.getElementById("txt_inst_origen").selectedIndex].text
                        + '\n y pasar todos sus datos a la institución ' + document.getElementById("txt_inst_destino").options[document.getElementById("txt_inst_destino").selectedIndex].text + '?')) {
                document.getElementById("btn_aceptar").disabled=true;
                document.getElementById("btn_cancelar").disabled=true;
                parametros = 'txt_inst_origen=' + inst_origen + '&txt_inst_destino=' + inst_destino;
                anadir_log('<br><b>' + proceso[0] + '</b>');
                timer_inicializar_proceso();
            } else {
                return;
            }
        }

        function timer_inicializar_proceso() {
            valor = document.getElementById("div_proceso").innerHTML;
            if (valor=='') {
                anadir_log('Procesando...');
                timerID = setTimeout("timer_inicializar_proceso()", 10000);
                return;
            } else {
                anadir_log(document.getElementById("div_proceso").innerHTML);
            }
            if (valor == 'OK') {
                ++num_proceso;
                mostrar_estado();
                document.getElementById("div_proceso").innerHTML = '';
                if (num_proceso <= max_proceso) {
                    anadir_log('<br><b>' + proceso[num_proceso] + '</b>');
                    nuevoAjax('div_proceso', 'POST', pagina[num_proceso], parametros);
                    timerID = setTimeout("timer_inicializar_proceso()", 10000);
                } else {
                    detener_proceso();
                }
            } else {
                document.getElementById("div_proceso").innerHTML = '';
                anadir_log('<br>Intentando de nuevo ejecutar el proceso ' + proceso[num_proceso]);
                nuevoAjax('div_proceso', 'POST', pagina[num_proceso], parametros);
                timerID = setTimeout("timer_inicializar_proceso()", 10000);
            }
        }

        function detener_proceso() {
            clearTimeout(timerID);
            if (num_proceso > max_proceso) {
                alert('Se han unido las dos instituciones.');
            }
            document.getElementById("btn_cancelar").disabled=false;
            return;
        }

        function anadir_log(mensaje) {
            document.getElementById("div_log").innerHTML += '<br>'+ mensaje;
        }

        function mostrar_estado() {
            document.getElementById("div_estado").innerHTML = '<center><b>' + proceso[num_proceso] + '</b></center>';
            if (num_proceso <= max_proceso) {
                document.getElementById("div_estado").innerHTML += '<center><br><br><img src="<?=$ruta_raiz?>/imagenes/progress_bar.gif"><br>Por favor espere.</center>';
            }
        }

    </script>
<body>
  <center>
    <table width="70%" border="1" cellspacing="0" cellpadding="0" class="borde_tab">
        <tr>
            <th width="100%"><center>Fusionar dos instituciones</center></th>
        </tr>
        <tr>
            <td class="listado1">
                <br>Seleccione la instituci&oacute;n a la que se mover&aacute;n las &aacute;reas, usuarios y documentos
                <br><?=$menu_inst_destino?>
                <br><br>Seleccione la instituci&oacute;n que ser&aacute; desactivada
                <br><?=$menu_inst_origen?>
                <br><br>
                <center>
                    <input  name="btn_aceptar"  id="btn_aceptar"  type="button" class="botones" value="Aceptar" title="Inicia el proceso para fusionar las dos instituciones" onClick="fusionar_instituciones()"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input  name="btn_cancelar" id="btn_cancelar" type="button" class="botones" value="Regresar" title="Regresa al men&uacute; de administraci&oacute;n" onClick="location='../formAdministracion.php'"/>
                </center>
                <br>&nbsp;
            </td>
        </tr>
        <tr>
            <th>Estado del proceso para unir dos instituciones</th>
        </tr>
        <tr>
            <td class="listado1">
                <br>
                <div id="div_estado"><center><h3><i>Pendiente.</i></h3></center></div>
                <div id="div_log"></div>
                <br>
            </td>
        </tr>
    </table>
    <div id="div_proceso" style="display: none;">OK</div>
    <br>
  </center>
</body>
</html>

