<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
$ruta_raiz = "../..";
session_start();
if($_SESSION["usua_admin_sistema"]!=1) die("");
include_once "$ruta_raiz/rec_session.php";

if (!isset($slc_institucion) and $_SESSION["admin_institucion"]==1) $slc_institucion = 0;
if (!isset($slc_institucion)) $slc_institucion = $_SESSION["inst_codi"];
$error = "";
if (isset($_POST['btn_accion'])) {
    $record = array();
    if ($_POST['btn_accion'] == 'Aceptar') {
        $db->conn->BeginTrans();
        if ($slc_institucion==0)
            $txt_id_inst = $db->conn->nextId('sec_institucion');
        else
            $txt_id_inst = $slc_institucion;
        $record['INST_CODI'] = $txt_id_inst;
        $record['INST_RUC'] = $db->conn->qstr(limpiar_sql($_POST['txtRuc']));
        $record['INST_NOMBRE'] = $db->conn->qstr(limpiar_sql($_POST['txtNombre']));
        $record['INST_SIGLA'] = $db->conn->qstr(limpiar_sql(strtoupper($_POST['txtSigla'])));
        $record['INST_ESTADO'] = "1";
        $record['INST_TELEFONO'] = $db->conn->qstr(limpiar_sql(strtoupper($_POST['txtTelefono'])));
        $record['INST_EMAIL'] = $db->conn->qstr(limpiar_sql($_POST['txtCorreo']));//no se guarda con mayusculas
        $record['INST_DESPEDIDA_OFI'] = $db->conn->qstr(limpiar_sql(strtoupper($_POST['txtDespedidaOfi'])));
        $arch_logo = $_FILES["arch_logo"]['tmp_name'];
        if (trim($arch_logo!="")) {
            $path_arch = "/bodega/logos/$txt_id_inst.".limpiar_sql($_POST['txtExt']);
            $record['INST_LOGO'] = $db->conn->qstr($path_arch);
            $ok2 = move_uploaded_file($arch_logo,$ruta_raiz.$path_arch);
            if(!$ok2) $error = "Error al subir el Logo de la Instituci&oacute;n";
        } else
            $ok2 = true;

        $ok1 = $db->conn->Replace("INSTITUCION", $record, "INST_CODI", false,false,true,false);
        // Para eliminar los registros donde la institucion esta como ministerio coordinador de otras
        if(!$ok1) $error = "Error al crear la Instituci&oacute;n";
        if($ok1==2){

        // Creamos el área padre de la institución
        $txt_id_depe = $db->conn->nextId('sec_dependencia');
        unset($record);
        $record['DEPE_CODI']        = $txt_id_depe;
        $record['DEPE_NOMB']        = $db->conn->qstr(limpiar_sql($_POST['txtNombre']));
        $record['DEPE_CODI_PADRE']  = $txt_id_depe;
        $record['DEP_SIGLA']        = $db->conn->qstr(strtoupper(limpiar_sql($_POST['txtSigla'])));
        $record['DEP_CENTRAL']      = $txt_id_depe;
        $record['DEPE_ESTADO']      = "1";
        $record['INST_CODI']        = $txt_id_inst;
        $record['DEPE_PIE1']        = "1";
        $ok3 = $db->conn->Replace("DEPENDENCIA", $record, "", false,false,true,false);
        for ($k=0;$k<=2;$k++) {
            $db->conn->Execute("CREATE SEQUENCE secu"."_".$txt_id_depe."_$k INCREMENT 1 START 1 CACHE 1");
        }
        $dependencia = str_pad($txt_id_depe,6,"0", STR_PAD_LEFT);
            mkdir($ruta_raiz."/bodega/".date('Y')."/".$dependencia."/docs",0755,true);
        } else
            $ok3 = true;

        if ($ok1 && $ok2 && $ok3) {
            $db->conn->CommitTrans();
            $slc_institucion = $txt_id_inst;
            $error = "Los cambios se realizaron exitosamente";
        } else
            $db->conn->RollbackTrans();
    }
}

if ($slc_institucion != 0) {
    $sql = "select * from institucion where inst_codi=$slc_institucion";
    $rs = $db->conn->query($sql);
    $txtRuc = $rs->fields['INST_RUC'];
    $txtNombre = $rs->fields['INST_NOMBRE'];
    $txtSigla = $rs->fields['INST_SIGLA'];
    $txtLogo = $rs->fields['INST_LOGO'];
    $chkCoordinador = $rs->fields['INST_COORDINADOR'];
    $txtTelefono = $rs->fields['INST_TELEFONO'];
    $txtCorreo = $rs->fields['INST_EMAIL'];
    $txtDespedidaOfi = $rs->fields['INST_DESPEDIDA_OFI'];
}

include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();
?>

<!-- Para utilizacion de Ajax -->
<? require_once "$ruta_raiz/js/ajax.js"; ?>
<script type="text/javascript" src="<?=$ruta_raiz?>/js/prototype.js"></script>
<script type="text/javascript" src="<?=$ruta_raiz?>/js/general1.js"></script>
<script type="text/javascript" src="<?=$ruta_raiz?>/js/formchek.js"></script>
<script type="text/javascript">

    function ltrim(s) {
       return s.replace(/^\s+/, "");
    }

    function ValidarInformacion(accion)
    {
        if (ltrim(document.formSeleccion.txtRuc.value) == '')
        {
            alert('Ingrese el Ruc de la Institución');
            document.formSeleccion.txtRuc.focus();
            return false;
        }
        if (ltrim(document.formSeleccion.txtNombre.value) == '')
        {
            alert('Ingrese el Nombre de la Institución');
            document.formSeleccion.txtNombre.focus();
            return false;
        }
        if (ltrim(document.formSeleccion.txtSigla.value) == '')
        {
            alert('Ingrese las Siglas para la Institución');
            document.formSeleccion.txtSigla.focus();
            return false;
        }
        if (ltrim(document.formSeleccion.esEmail.value)==0){
            alert("Correo no Existe o tiene formato no valido")
            return false;
        }
        return true;
    }

    function e(condicion, mensaje) {
        msg = (condicion) ? msg + mensaje + ' \n' : msg;
    }

    function ver_listado()
    {
        window.open('listados.php?var=inst','','scrollbars=yes,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
    }

    function valida_extension() {
        cadena=document.getElementById('arch_logo').value;
        cadena= cadena.substr(-3).toLowerCase();
        if (cadena=='png' || cadena=='jpg' || cadena=='gif') {
            document.getElementById('txtExt').value = cadena;
            return;
        } else {
            alert ("Solo se permite subir archivos con extensiones gif, jpg y png.");
            document.getElementById('arch_logo').value = '';
            document.getElementById('txtExt').value = '';
            return;
        }
    }

    function pulsar(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla==8) return true;
        patron =/\s/;
        te = String.fromCharCode(tecla);
        return !patron.test(te);
    }
</script>
<body>
<center>
<form name="formSeleccion" id="formSeleccion" method='post' ENCTYPE='multipart/form-data' action="<?= $_SERVER['PHP_SELF']?>">
  <table width="80%"class="borde_tab">
    <tr>
      	<td colspan="2" height="40" align="center" class="titulos4"><b>Administrador de Instituciones</b></td>
    </tr>
<? if ($_SESSION["admin_institucion"]==1) { ?>
    <tr>
	<td width="30%" align="left" class="titulos2"><b>Seleccione Instituci&oacute;n (si desea modificarla)</b></td>
	<td width="70%" class="listado2">
<?php
	    $sql = "select inst_nombre, inst_codi from institucion where inst_codi>0 order by inst_nombre";
	    $rs=$db->conn->query($sql);
	    echo $rs->GetMenu2("slc_institucion", $slc_institucion, "0:&lt;&lt Nueva Institución &gt;&gt;", false,"","class='select' Onchange='submit()'");
?>
	</td>
    </tr>
<? } else $slc_institucion = $_SESSION["inst_codi"]; ?>
  </table>

  <br/>
  <table width="80%"class="borde_tab">
    <tr>
        <td align="left" class="titulos2"><b>Nombre:</b></td>
        <td colspan="3" class="listado2">
            <input  name="txtNombre" id="txtNombre" type="text" size="95" maxlength="100" value="<?=$txtNombre?>" <? if($_SESSION["admin_institucion"]!=1) echo 'readonly="true"';?>>
        </td>
    </tr>
    <tr>
	<td align="left" class="titulos2" width="20%"><b>Ruc:</b></td>
        <td class="listado2" width="30%">
	    <input  name="txtRuc" id="txtRuc" type="text" size="13" maxlength="13" value="<?=$txtRuc?>" >
        </td>
        <td class="titulos2" width="20%"><b>Sigla:</b></td>
        <td class="listado2" width="30%"><input name="txtSigla" id="txtSigla" type="text" size="20" maxlength="10" value="<?=$txtSigla ?>" onkeypress = "return pulsar(event)"></td>
    </tr>
    <tr>
        <td align="left" class="titulos2" width="20%"><b>Correo:</b></td>
        <td class="listado2" width="30%">            
	    <input  name="txtCorreo" id="txtCorreo" type="text" size="35" maxlength="50" value="<?=$txtCorreo?>">
        </td>        
        <td align="left" class="titulos2" width="20%"><b>Tel&eacute;fono:</b></td>
        <td class="listado2" width="30%">
	    <input  name="txtTelefono" id="txtTelefono" type="text" size="13" maxlength="13" value="<?=$txtTelefono?>" >
        </td>
    </tr>
    <tr>
        <td align="left" class="titulos2" width="20%"><b>Frase despedida para oficios:</b></td>
        <td class="listado2" colspan="3" >
	    <input  name="txtDespedidaOfi" id="txtDespedidaOfi" type="text" size="35" maxlength="35" value="<?=$txtDespedidaOfi?>" >
        </td>
    </tr>
    <tr>
	<td class="titulos2"><b>Logo:</b></td>
	<td width="35%" class="listado2" colspan="2">
	    <input name="arch_logo" type="file" class="tex_area" onChange="valida_extension();" id="arch_logo" size="0">
	    <input type="hidden" name="txtExt" value="" id="txtExt">
	</td>
        <td width="35%" class="listado2" colspan="2">
	    <? if (trim($txtLogo)!="") {?>
	    	<center><img src="<?=$ruta_raiz.$txtLogo?>" width="180" height="60" border=0 alt="Logo Instituci&oacute;n"></center>
	    <? } ?>
	</td>
    </tr>
<? if ($_SESSION["usua_codi"]==0) { ?>
    <tr>
	<td class="titulos2"><b>Fusionar dos Instituciones:</b></td>
        <td class="listado2" colspan="3">
            <input  name="btn_accion" type="button" class="botones_largo" value="Fusionar Instituciones" title="Mueve &aacute;reas, usuarios y documentos de una instituci&oacute;n a otra" onClick="location='instituciones_fusionar.php'"/>
	</td>
    </tr>
<? } ?>
  </table>
	
<?php
    if ($error != "") 
	echo "<br/><table width='100%'><tr><td align='center'><font color='red' face='Arial' size='3'>$error</font></td></tr></table>";
?>

  <br/>
  <table width="80%"  cellpadding="0" cellspacing="0" >
    <tr>
    	<td width="20%" align="center">
	    <input name="btn_accion" type="button" class="botones_largo" value="Listado de Instituciones" title="Lista todas las instituciones que estan creadas en sistema" onClick="ver_listado();"/>
    	</td>
	<td  width="20%" align="center">
	    <input  name="btn_accion" type="button" class="botones" value="Limpiar"  title="Borrar los datos del formulario" onClick="location='<?= $_SERVER['PHP_SELF']?>'"/>
        </td> 
	<td  width="20%" align="center">
	    <input name="btn_accion" type="submit" class="botones" value="Aceptar" title="Almacena los cambios realizados" onClick="return ValidarInformacion();"/>
	</td>
	<td  width="20%" align="center">
	    <input  name="btn_accion" type="button" class="botones" value="Regresar" title="Regresa a la página anterior, sin guardar los cambios" onClick="location='../formAdministracion.php'"/>
	</td>
    </tr>
  </table>
  <br>
</form>
</center>
</body>
</html>
