<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
/**
*	Autor			Iniciales		Fecha (dd/mm/aaaa)
*
*
*	Modificado por		Iniciales		Fecha (dd/mm/aaaa)
*	Santiago Cordovilla	SC			19-12-2008
*
*	Comentado por		Iniciales		Fecha (dd/mm/aaaa)
*	Santiago Cordovilla	SC			19-12-2008
**/
$ruta_raiz = "../..";
session_start();
if($_SESSION["usua_admin_sistema"]!=1) die("");
include_once "$ruta_raiz/rec_session.php";

$error = "";
//Permite grabar el area en la base de datos con todos sus atributos
if (!isset($txt_depe_codi)) $txt_depe_codi = 0;
if ($txt_ok==1) {
    $record = array();
    $db->conn->BeginTrans();
    if ($txt_depe_codi==0) 
	$txtIdDep = $db->conn->nextId('sec_dependencia');
    else
	$txtIdDep = $txt_depe_codi;
    $record['INST_CODI'] = $_SESSION["inst_codi"];
    $record['DEPE_CODI'] = limpiar_sql($txtIdDep);
    $record['DEPE_NOMB'] = $db->conn->qstr(limpiar_sql($_POST['txt_nombre']));
    $record['DEP_SIGLA'] = $db->conn->qstr(limpiar_sql(strtoupper($_POST['txt_sigla'])));
    if ($_POST['txt_estado']!='')
        $record['DEPE_ESTADO'] = $db->conn->qstr(limpiar_sql($_POST['txt_estado']));
    else
    $record['DEPE_ESTADO'] = "1";	//$_POST['Slc_destado'];
    
    $record['DEPE_PIE1'] = $db->conn->qstr(limpiar_sql($_POST['txt_ciudad'])); //se graba la ciudad del area	
    ($_POST['slc_padre']>0) ? $record['DEPE_CODI_PADRE'] = $_POST['slc_padre'] : $record['DEPE_CODI_PADRE'] = $txtIdDep;
    ($_POST['slc_archivo']>0) ? $record['DEP_CENTRAL'] = $_POST['slc_archivo'] : $record['DEP_CENTRAL'] = $txtIdDep;
    ($_POST['slc_plantilla']>0) ? $record['DEPE_PLANTILLA'] = $_POST['slc_plantilla'] : $record['DEPE_PLANTILLA'] = $txtIdDep;

    $ok1 = $db->conn->Replace("DEPENDENCIA", $record, "DEPE_CODI", false,false,true,false);
    if(!$ok1) $error = "Error al insertar el área";

    $ok3 = true;
    $arch_plantilla = trim($_FILES["arch_plantilla"]['tmp_name']);
    if ($arch_plantilla != "") {
	$path_arch = "$ruta_raiz/bodega/plantillas/$txtIdDep.pdf";
	$ok3 = move_uploaded_file($arch_plantilla, $path_arch);
	if(!$ok3) $error = "Error al subir la plantilla para los documentos del &aacute;rea";
    }

    if ($ok1==2) { //Si se realizó un insert creo secuencias y carpetas
 	for ($k=0;$k<=2;$k++) {
	    $db->conn->Execute("CREATE SEQUENCE secu"."_".$txtIdDep."_$k INCREMENT 1 START 1 CACHE 1");
	}
	$dependencia = str_pad($record['DEPE_CODI'],6,"0", STR_PAD_LEFT);
	if ( is_dir( $ruta_raiz."/bodega/".date('Y')."/".$dependencia."/docs" ) != 1 )
	    $ok2 = mkdir($ruta_raiz."/bodega/".date('Y')."/".$dependencia."/docs",0777,true);
        else {
	    $ok2 = false;
	} 
    } else
	$ok2 = true;
    if (!$ok2)
	$error = "Error al crear ruta en la bodega";
    if ($ok1 && $ok2) {
	$db->conn->CommitTrans();
    } else
	$db->conn->RollbackTrans();
}

if ($accion==1) $mensaje = "El ".$_SESSION["descDependencia"]." $txt_nombre <br/> fue creada correctamente";
if ($accion==2) $mensaje = "Los cambios en el ".$_SESSION["descDependencia"]." $txt_nombre <br/> se realizaron correctamente";
if ($error!="") $mensaje = "Error al crear o modificar el ".$_SESSION["descDependencia"]." $txt_nombre <br/> $error";


include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();
?>

<body>
    <form name="frmConfirmaCreacion" action="adm_dependencias_nuevo.php?accion=2&des_activar=3" method="post">
    <br><br>
    <center>
	<table width="40%" border="2" align="center" class="t_bordeGris">
	    <tr> 
		<td width="100%" height="30" class="listado2">
		    <span class=etexto><center><B><?=$mensaje?></B></center></span> 
		</td> 
	    </tr>
	    <tr>	
		<td height="30" class="listado2">
			<center><input class="botones" type="submit" name="Submit" value="Aceptar"></center>
		</td> 
	    </tr>
	</table>
    </center>
    </form>
</body>
</html>