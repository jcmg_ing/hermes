<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";
require_once "$ruta_raiz/funciones.php";


if (isset($_GET["posponer"])) {
    include_once "$ruta_raiz/funciones_interfaz.php";
    echo "<html>".html_head()."<body>";

    $sql = "update corregir_usuarios set desactivar=0, fecha=null where desactivar=2 and usua_codi_resp=".$_SESSION["usua_codi"];
    $rs = $db->query($sql);
    
    $sql = "select count(id) as total, count(fecha) as num from corregir_usuarios where usua_codi_resp=".$_SESSION["usua_codi"];
    $rs = $db->query($sql);
    
    if (($rs->fields["TOTAL"]-$rs->fields["NUM"]) == 0) {
        $mensaje = "Se han revisado todos los usuarios asignados.";
        $boton = "../formAdministracion.php";
    } else {
        $mensaje = "Faltan por revisar ".($rs->fields["TOTAL"]-$rs->fields["NUM"])." usuarios que fueron pospuestos para el final";
        $boton = "corregir_datos.php";
    }
    echo "<br><center><h6>$mensaje</h6><br>
            <input type='button' name='btn_aceptar' class='botones_largo' value='Aceptar' onclick='window.location=\"$boton\"'>
          </center></body></html>";
    die ("");
}

if (isset ($_POST["txt_usua_codi"])) {
    $sql = "update corregir_usuarios set fecha=current_timestamp, desactivar=2 where usua_codi_resp=".$_SESSION["usua_codi"]." and usua_codi_modi=".$_POST["txt_usua_codi"];
    $rs = $db->query($sql);
    echo "<html><script>window.location = 'corregir_datos.php';</script></html>";
    die ("");
}
?>
