<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";
require_once "$ruta_raiz/funciones.php";

$datos = array();
if (0+$_POST["txt_usua_codi"] == 0) die ("ERROR");

$sql = "select * from titulo where tit_codi=".$_POST["txt_titulo"];
$rs = $db->query($sql);
$titulo = $rs->fields["TIT_NOMBRE"];
$abr_titulo = $rs->fields["TIT_ABREVIATURA"];

if ($_POST["txt_tipo_usuario"] == 1) { //FUNCIONARIO
    $datos["usua_codi"] = $_POST["txt_usua_codi"];
    $datos["usua_nomb"] = "initcap(".$db->conn->qstr(limpiar_sql(trim($_POST["txt_nombre"]))).")";
    $datos["usua_apellido"] = "initcap(".$db->conn->qstr(limpiar_sql(trim($_POST["txt_apellido"]))).")";
    $datos["usua_cargo"] = "initcap(".$db->conn->qstr(limpiar_sql(trim($_POST["txt_puesto"]))).")";
    $datos["usua_email"] = $db->conn->qstr(limpiar_sql(trim($_POST["txt_email"])));
    $datos["usua_telefono"] = $db->conn->qstr(limpiar_sql(trim($_POST["txt_telefono"])));
    $datos["usua_direccion"] = $db->conn->qstr(limpiar_sql(trim($_POST["txt_direccion"])));
    $datos["usua_titulo"] = $db->conn->qstr($titulo);
    $datos["usua_abr_titulo"] = $db->conn->qstr($abr_titulo);
    $db->conn->Replace("usuarios", $datos, "usua_codi", false,false,true,false);
}

if ($_POST["txt_tipo_usuario"] == 2) { //FUNCIONARIO
    $datos["ciu_codigo"] = $_POST["txt_usua_codi"];
    $datos["ciu_nombre"] = "initcap(".$db->conn->qstr(limpiar_sql(trim($_POST["txt_nombre"]))).")";
    $datos["ciu_apellido"] = "initcap(".$db->conn->qstr(limpiar_sql(trim($_POST["txt_apellido"]))).")";
    $datos["ciu_cargo"] = "initcap(".$db->conn->qstr(limpiar_sql(trim($_POST["txt_puesto"]))).")";
    $datos["ciu_empresa"] = "upper(".$db->conn->qstr(limpiar_sql(trim($_POST["txt_institucion"]))).")";
    $datos["ciu_email"] = $db->conn->qstr(limpiar_sql(trim($_POST["txt_email"])));
    $datos["ciu_telefono"] = $db->conn->qstr(limpiar_sql(trim($_POST["txt_telefono"])));
    $datos["ciu_direccion"] = $db->conn->qstr(limpiar_sql(trim($_POST["txt_direccion"])));
    $datos["ciu_titulo"] = $db->conn->qstr($titulo);
    $datos["ciu_abr_titulo"] = $db->conn->qstr($abr_titulo);
    $db->conn->Replace("ciudadano", $datos, "ciu_codigo", false,false,true,false);
}

$tmp_desactivar = "0";
if (isset ($_POST["chk_desactivar"])) $tmp_desactivar = "1";

$sql = "update corregir_usuarios set fecha=current_timestamp, desactivar=$tmp_desactivar where usua_codi_resp=".$_SESSION["usua_codi"]." and usua_codi_modi=".$_POST["txt_usua_codi"];
$rs = $db->query($sql);
//die (var_dump ($_POST));
?>
<html>
    <script>
        window.location = 'corregir_datos.php';
    </script>
</html>
