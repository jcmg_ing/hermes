<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";

require_once "$ruta_raiz/funciones.php";
include_once "$ruta_raiz/obtenerdatos.php";

include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();

$sql = "select usua_codi_modi from corregir_usuarios where id=(select min(id) from corregir_usuarios where usua_codi_resp=".$_SESSION["usua_codi"]." and fecha is null)";
$rs = $db->query($sql);
if (!$rs or $rs->EOF) die ("<html><script>window.location = 'corregir_datos_posponer.php?posponer=si';</script></html>");

$usr = ObtenerDatosUsuario($rs->fields["USUA_CODI_MODI"], $db);

$tmp_titulo = explode(" ", $usr["titulo"]);
$titulo1 = trim($tmp_titulo[0] . " " . $tmp_titulo[1]);
$titulo2 = trim($tmp_titulo[0]);
$titulo3 = trim($tmp_titulo[1]);

$txt_titulo = 0;
$rs=$db->conn->query("select tit_codi from titulo where upper(tit_nombre) = upper('$titulo3')");
if (!$rs->EOF) $txt_titulo = $rs->fields["TIT_CODI"];
$rs=$db->conn->query("select tit_codi from titulo where upper(tit_nombre) = upper('$titulo2')");
if (!$rs->EOF) $txt_titulo = $rs->fields["TIT_CODI"];
$rs=$db->conn->query("select tit_codi from titulo where upper(tit_nombre) = upper('$titulo1')");
if (!$rs->EOF) $txt_titulo = $rs->fields["TIT_CODI"];

$sql = "select count(id) as total, count(fecha) as num from corregir_usuarios where usua_codi_resp=".$_SESSION["usua_codi"];
$rs = $db->query($sql);
$mensaje = "Llevas " . $rs->fields["NUM"] . " de " . $rs->fields["TOTAL"] . " usuarios modificados";


?>
<script>
    function trim(s) {
        return s = s.replace(/^\s+|\s+$/gi, '');
    }

    function validar_envio(tipo) {
        if (tipo == 'P') {
            document.formulario.action = 'corregir_datos_posponer.php';
            document.formulario.submit();
            return;
        }
        if (document.getElementById('txt_titulo').value == '0') {
            alert ('Por favor seleccione el título del usuario.');
            return;
        }
        if (trim(document.getElementById('txt_nombre').value) == '' || trim(document.getElementById('txt_apellido').value) == '') {
            if (!confirm('Grabar sin nombre y/o apellido?'))
                return;
        }
        document.formulario.action = 'corregir_datos_grabar.php';
        document.formulario.submit();
    }

</script>
<body>
  <center>
    <form name='formulario' action="" method="post">
        <br>
        <h6><?=$mensaje?></h6>
        <br>
<?
        if (substr($usr["cedula"],0,2) != "99") {
            include_once "$ruta_raiz/interconexion/validar_datos_ciudadano.php";
            $datos_rc = ws_validar_datos_ciudadano($usr["cedula"]);
?>
        <table width="80%" class="borde_tab" border="0" cellpadding="0" cellspacing="5">
            <tr>
                <th colspan="2">
                    <center>Datos traidos del Registro Civil</center>
                </th>
            </tr>
            <tr>
                <td class="listado2" width="30%">Nombres:</td>
                <td class="listado1" width="70%"><?=$datos_rc["apellidosNombres"]?></td>
            </tr>
            <tr>
                <td class="listado2">Sexo</td>
                <td class="listado1"><?=$datos_rc["sexo"]?></td>
            </tr>
            <tr>
                <td class="listado2">Estado Civil</td>
                <td class="listado1"><?=$datos_rc["estadoCivil"]?></td>
            </tr>
        </table>
        <br>
<? } ?>

        <input type="hidden" name="txt_usua_codi" value="<?=$usr["usua_codi"]?>">
        <input type="hidden" name="txt_tipo_usuario" value="<?=$usr["tipo_usuario"]?>">

        <table width="98%" class="borde_tab" border="0" cellpadding="0" cellspacing="5">
            <tr>
                <th colspan="4">
                    <center>Datos del Usuario <?=$usr["cedula"]?></center>
                </th>
            </tr>
            <tr>
                <td class="listado2" width="15%">Tipo de Usuario:</td>
                <td class="listado1" width="35%"><b><? if ($usr["tipo_usuario"]==1) echo "FUNCIONARIO P&Uacute;BLICO"; else echo "CIUDADANO"; ?></b></td>
                <td class="listado2" width="15%">Instituci&oacute;n:</td>
                <td class="listado1" width="35%"><input type="text" name="txt_institucion" size="40" value="<?=$usr["institucion"]?>"></td>
            </tr>
            <tr>
                <td class="listado2" width="15%"><b>* Nombre:</b></td>
                <td class="listado1" width="35%"><input type="text" name="txt_nombre" id="txt_nombre" size="40" value="<?=$usr["usua_nombre"]?>"></td>
                <td class="listado2" width="15%"><b>* Apellido:</b></td>
                <td class="listado1" width="35%"><input type="text" name="txt_apellido" id="txt_apellido" size="40" value="<?=$usr["usua_apellido"]?>"></td>
            </tr>
            <tr>
                <td class="listado2"><b>* T&iacute;tulo:</b></td>
                <td class="listado1">
<?
                    $sql = "select tit_nombre, tit_codi from titulo order by split_part(tit_nombre, ' ', 1) asc, split_part(tit_nombre, ' ', 2) asc, tit_nombre asc";
                    $rs=$db->conn->query($sql);
                    echo $rs->GetMenu2("txt_titulo", $txt_titulo, "0:&lt;&lt seleccione &gt;&gt;", false,"","class='select' id='txt_titulo'");
?>
                </td>
                <td class="listado2">T&iacute;tulo Anterior:</td>
                <td class="listado1"><?=$usr["titulo"]?></td>
            </tr>
            <tr>
                <td class="listado2">Puesto:</td>
                <td class="listado1"><input type="text" name="txt_puesto" size="40" value="<?=$usr["cargo"]?>"></td>
                <td class="listado2">e-mail:</td>
                <td class="listado1"><input type="text" name="txt_email" size="40" value="<?=$usr["email"]?>"></td>
            </tr>
            <tr>
                <td class="listado2">Direcci&oacute;n:</td>
                <td class="listado1"><input type="text" name="txt_direccion" size="40" value="<?=$usr["direccion"]?>"></td>
                <td class="listado2">Tel&eacute;fono:</td>
                <td class="listado1"><input type="text" name="txt_telefono" size="40" value="<?=$usr["telefono"]?>"></td>
            </tr>
            <tr>
                <td class="listado1" align="right" colspan="4">
                    Desactivar Usuario <input type="checkbox" value="1" name="chk_desactivar">
                </td>
            </tr>
        </table>
        <div style="width: 98%;" align="right">
            <br><input type="button" name="btn_aceptar" class="botones" value="Posponer" onclick="validar_envio('P')">
        </div>
        <br><br>
        <input type="button" name="btn_aceptar" class="botones_largo" value="Aceptar" onclick="validar_envio('A')">
    </form>
  </center>
</body>
</html>