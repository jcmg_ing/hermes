<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
/*************************************************************************************/
/*                                                                                   */
/*************************************************************************************/

$ruta_raiz = "../..";
session_start();
if($_SESSION["usua_admin_sistema"]!=1)
    die(html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina."));
require_once "$ruta_raiz/funciones.php";
require_once "$ruta_raiz/rec_session.php";

include "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();

$accion = 0 + $_GET["accion"];

$paginador = new ADODB_Pager_Ajax($ruta_raiz, "div_buscar_usuarios", "busqueda_paginador_usuarios.php",
                  "txt_nombre,txt_dependencia,txt_permiso,txt_estado");
?>

<script type="text/javascript">
    
   function realizar_busqueda() { 
       paginador_reload_div('');
    }
    function seleccionar_usuario(codigo) {
        window.location='./adm_usuario.php?accion=<?=$accion?>&usr_codigo=' + codigo;
        return true;
    }
</script>

<body>
  <center>
    <table border=0 width="99%" class="borde_tab" cellpadding="0" cellspacing="5">
        <tr>
            <td width="30%" class="titulos5"><font class="tituloListado">Buscar usuario del sistema: </font></td>
            <td width="50%" class="listado5">
                <table width="100%" border="0">
                    <tr>
                        <td class="listado5">Nombre / C.I.</td>
                        <td><input type=text id="txt_nombre" name="txt_nombre" value="" class="tex_area"></td>
                    </tr>
                    <tr>
                        <td class="listado5"><?=$descDependencia ?></td>
                        <td>
<?php
            $sql="select depe_nomb, depe_codi from dependencia where depe_estado=1 and inst_codi=".$_SESSION["inst_codi"]." order by 1 asc";
            $rs=$db->conn->query($sql);
            if($rs) print $rs->GetMenu2("txt_dependencia", "0", "0:&lt;&lt; Seleccione &gt;&gt;", false,"","class='select' id='txt_dependencia'" );
?>
                        </td>
                    </tr>
                    <tr>
                        <td class="listado5">Permiso</td>
                        <td>
<?php
            $sql="select descripcion, id_permiso from permiso where estado=1 order by 1";
            $rs=$db->conn->query($sql);
            if($rs) print $rs->GetMenu2("txt_permiso", "0", "0:&lt;&lt; Seleccione &gt;&gt;", false,"","class='select' id='txt_permiso'" );
?>
                        </td>
                    </tr>
                    <tr>
                        <td class="listado5">Estado</td>
                        <td>
                            <select name="txt_estado" id="txt_estado" class="select">
                                <option value='1' selected>Activos</option>
                                <option value='0'>Inactivos</option>
                                <option value='2'>Todos</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20%" align="center" class="titulos5" >
                <input type="button" name="btn_buscar" value="Buscar" class="botones" onClick="realizar_busqueda();">
            </td>
        </tr>
    </table>
    <br>
    <input name="btn_accion" type="button" class="botones" value="Regresar" onClick="location='./mnuUsuarios.php'"/>
    <br>&nbsp;
    <div id='div_buscar_usuarios' style="width: 99%"></div>
  </center>
</body>
</html>