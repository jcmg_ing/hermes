<?php
/**  Programa para el manejo de gestion documental, oficios, memorandos, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/
$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/funciones_interfaz.php";
if ($_SESSION["usua_admin_sistema"] != 1) {
    echo html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
    die("");
}
require_once("$ruta_raiz/obtenerdatos.php");  //formar la observacion de edicion
include_once "$ruta_raiz/rec_session.php";
include_once "$ruta_raiz/include/tx/Tx.php";
$tx=new Tx($db);
$usuarioSubrEst=usrMensajeSubrogacion($db,$usr_codigo,"perfil");


$recargar=true;
$radicado=array();

$usr_cedula = trim($usr_cedula);

if (!isset($usr_estado) || $usr_estado == null) {
    $usr_estado = 0;
    if($tiene_subrogacion == 1)
        $usr_estado = 1;
}
if ($accion==1) $usr_estado = 1;
if($_REQUEST['usr_area_responsable']==""){
    $usr_area_responsable=0;
    $usr_sumilla=strtolower($usr_sumilla);
}elseif($_REQUEST['usr_area_responsable']==0){
    $sql = "select * from usuarios where depe_codi=$usr_depe and usua_responsable_area=1 ";
    $rs = $db->conn->query($sql);
    //echo $sql;
    if(!$rs->EOF) {
                
                die("<script>alert('Ya existe un responsable de documentación para esta Area'); history.back();</script>");
    }else{
        $usr_area_responsable=1;
        $usr_sumilla=strtoupper($usr_sumilla);
    }
}




if($usr_estado!=0) {
	if ($accion == 2) {
        $sql2 .= " and usua_codi<>$usr_codigo";
    } else {
        $sql2 = "";
    }
	if ($usr_perfil==1) {
	    $sql = "select * from usuario where cargo_tipo=1 and usua_esta=1 and depe_codi=$usr_depe" . $sql2;
	    $rs = $db->conn->query($sql);
	    if(!$rs->EOF) {
	    	include "./adm_usuario.php";
	    	die("<script>alert('Ya existe el usuario \"Jefe\" para esta Area')</script>");
	    }
	}
	if ($usr_perfil==2) {
	    $sql = "select * from usuario where cargo_tipo=2 and usua_esta=1 and depe_codi=$usr_depe" . $sql2;
	    $rs = $db->conn->query($sql);
	    if(!$rs->EOF) {
	    	include "./adm_usuario.php";
	    	die("<script>alert('Ya existe el usuario \"Asistente\" para esta Area')</script>");
	    }
	}
}

$record = array();
$flag_copiar_contrasena = false;
if ($accion == 1) {
    $record["INST_CODI"] = $_SESSION["inst_codi"];
    $usr_codigo = $db->nextId("usuarios_usua_codi_seq");
    $flag_copiar_contrasena = true;
} else {
    $sql = "select usua_cedula from usuarios where usua_codi=$usr_codigo";
    $rs= $db->conn->query($sql);
    if ($rs && !$rs->EOF && $rs->fields['USUA_CEDULA']!=$usr_cedula) {
        $flag_copiar_contrasena = true;
    }
}

if ($flag_copiar_contrasena) {
    $sql = "select usua_pasw from usuarios where usua_nuevo=1 and usua_esta=1 and usua_cedula='$usr_cedula' and usua_codi<>$usr_codigo";
    $rs= $db->conn->query($sql);
    if ($rs && !$rs->EOF) {
        $record["USUA_PASW"] = $db->conn->qstr($rs->fields['USUA_PASW']);
        unset ($usr_nuevo);
        $mensaje = "<b>La contrase&ntilde;a registrada es la que se encuentra definida para las otras cuentas del usuario.</b><br>";
    }
}

$record["USUA_CODI"] = $usr_codigo;

$record["DEPE_CODI"] = $usr_depe;
if ($usr_estado==0) {
    $usr_perfil=0;
    $usr_login = "l$usr_codigo";
    $record["USUA_CEDULA"] = $db->conn->qstr(substr($usr_cedula,0,10)."-$usr_codigo");

//documentos del usuario desactivado
	$sql = "select radi_nume_radi from radicado where esta_codi in (1,2) and radi_usua_actu=$usr_codigo";

	$rs= $db->conn->query($sql);
	unset($radicado);
	while (!$rs->EOF) {
			$radicado[]= $rs->fields['RADI_NUME_RADI'];
			$rs->MoveNext();
		}
	if($usr_destino!=0 and count($radicado)>0) {
		$tx->reasignar( $radicado, $usr_codigo, $usr_destino, 'Reasignado por desactivación de usuario',"",true);
    }
} else {
    $usr_login = "U".substr($usr_cedula,0,10);    
    if ($accion == 2) {
        $sql = "select usua_login,usua_responsable_area from usuarios where usua_codi=$usr_codigo and (usua_login like 'UADMIN%' or usua_login like 'UUSR%')";        
        $rs = $db->conn->query($sql);
        if(!$rs->EOF){ 
            $usr_login = $rs->fields['USUA_LOGIN'];            
        }
    }
    $record["USUA_CEDULA"] = $db->conn->qstr(substr($usr_cedula,0,10));
}
$record["USUA_LOGIN"]       = $db->conn->qstr($usr_login);
$record["USUA_NOMB"]        = $db->conn->qstr(limpiar_sql(trim($usr_nombre)));
$record["USUA_APELLIDO"]    = $db->conn->qstr(limpiar_sql(trim($usr_apellido)));
$record["USUA_TITULO"]      = $db->conn->qstr(limpiar_sql(trim($usr_titulo)));
$record["USUA_ABR_TITULO"]  = $db->conn->qstr(limpiar_sql(trim($usr_abr_titulo)));
$record["USUA_CARGO"]       = $db->conn->qstr(limpiar_sql(trim($usr_cargo)));
$record["USUA_CARGO_CABECERA"]       = $db->conn->qstr(limpiar_sql(trim($usr_cargo_cabecera)));
$record["USUA_SUMILLA"]       = $db->conn->qstr(limpiar_sql(trim($usr_sumilla)));
if ($_SESSION["inst_codi"]==1) $record["INST_NOMBRE"]       = $db->conn->qstr(limpiar_sql(trim($usr_inst_nombre)));

if ($usr_estado==1){  
    if (isset($_POST['usr_area_responsable'])){        
        $record["USUA_RESPONSABLE_AREA"]       = 1;
        $usr_sumilla= strtoupper($usr_sumilla);
        $record["USUA_SUMILLA"]=$db->conn->qstr(limpiar_sql(trim($usr_sumilla)));
        
    }else{ 
        $sql = "select usua_login,usua_responsable_area from usuarios where usua_codi=".$record["USUA_CODI"];
       
        $rs = $db->conn->query($sql);
        if(!$rs->EOF){ 
            $usr_responsable = $rs->fields['USUA_RESPONSABLE_AREA'];            
        }
        
            if($usr_responsable==1){
                
                $usr_sumilla=strtoupper($usr_sumilla);
                $record["USUA_RESPONSABLE_AREA"]       = 1;
                $record["USUA_SUMILLA"]=$db->conn->qstr(limpiar_sql(trim($usr_sumilla)));
            }
            else{
                $record["USUA_RESPONSABLE_AREA"]       = 0;
                $record["USUA_SUMILLA"]=$db->conn->qstr(limpiar_sql(trim($usr_sumilla)));
            }
    }
}else{
    $record["USUA_RESPONSABLE_AREA"]       = 0;
    $record["USUA_SUMILLA"]=$db->conn->qstr(limpiar_sql(trim($usr_sumilla)));
}


if (trim($usr_perfil)){
    $record["CARGO_TIPO"] = limpiar_sql(trim($usr_perfil));
}
else{    
    $subrogacion = usrMensajeSubrogacion($db,$usr_codigo,"perfil");    
    if ($subrogacion==" (Subrogado)")
        $record["CARGO_TIPO"]  = 1;//limpiar_sql(trim($usr_perfil));
    else
        $record["CARGO_TIPO"] = 0;       
}
$record["USUA_EMAIL"]       = $db->conn->qstr(limpiar_sql(trim($usr_email)));
$record["USUA_OBS"]       = $db->conn->qstr(limpiar_sql(trim($usr_obs)));
if($codi_ciudad != "" or $codi_ciudad != 0)
    $record["CIU_CODI"]       = limpiar_sql(trim($codi_ciudad));
$record["USUA_ESTA"]        = limpiar_sql(trim($usr_estado));
$record["USUA_NUEVO"] = 1;

$record["USUA_DIRECCION"]       = $db->conn->qstr(limpiar_sql(trim($usr_direccion)));
$record["USUA_TELEFONO"]       = $db->conn->qstr(limpiar_sql(trim($usr_telefono)));
$record["USUA_CELULAR"]       = $db->conn->qstr(limpiar_sql(trim($usr_celular)));
//Datos del usuario que modifico al funcionario la ultima ves.
$record["USUA_CODI_ACTUALIZA"] = $_SESSION['usua_codi'];
$record["USUA_FECHA_ACTUALIZA"] = "CURRENT_TIMESTAMP";

// Guardar Path de firma digitalizada
if (trim($_FILES["firmaDigitalizada"]['name'])!="")
    $record["USUA_FIRMA_PATH"] = $db->conn->qstr(limpiar_sql('bodega/firmas/'.$usr_codigo.'.'.$_POST["extarch"]));
//file_put_contents("$ruta_raiz/bodega/firmas/".$nomb_arch.'.'.$_POST["extarch"], $$userfile);

$record["USUA_TIPO_CERTIFICADO"] = 0;
if (isset($_POST["usr_permiso_19"])) {
    $record["USUA_TIPO_CERTIFICADO"] = 0 + $_POST["usr_tipo_certificado"];
    if ($record["USUA_TIPO_CERTIFICADO"] == 0) $record["USUA_TIPO_CERTIFICADO"] = 1;
}

//Armar observacion de campos modificados
if($accion == 1)
    $record["USUA_OBS_ACTUALIZA"] =  "'Registro Nuevo'";
else
    $record["USUA_OBS_ACTUALIZA"] = "'".ObtenerObservacionFuncionario($usr_codigo, $record, $db)."'";

$ok1 = $db->conn->Replace("USUARIOS", $record, "USUA_CODI", false,false,true,false);

/************************************* GRABAR FIRMA *************************************/
if($ok1){
    /*$userfile = "firmaDigitalizada";*/
    if (trim($_FILES["firmaDigitalizada"]['name'])!="") {
        $nomb_arch = $_FILES["firmaDigitalizada"]['name'];
        /*$sqlFirma = 'select fir_dig_codi from firma_digitalizada where usua_codi = '.$usr_codigo;
        $rsFirma = $db->conn->query($sqlFirma);*/
        // Si $rsFirma->fields['FIR_DIG_CODI'] es diferente de '' edita el registro
        //$okFrim=GrabarFirma($db, $rsFirma->fields['FIR_DIG_CODI'], $usr_codigo, $$userfile, $nomb_arch, $_POST["extarch"], $ruta_raiz);
        $rutaFirma = "$ruta_raiz/bodega/firmas/".$usr_codigo.'.'.$_POST["extarch"];
        move_uploaded_file($_FILES["firmaDigitalizada"]['tmp_name'],$rutaFirma);
        //move_uploaded_file($$firmaDigitalizada,$rutaFirma);
    }
}


// Añadimos los permisos al usuario
unset($record);
$sql = "select id_permiso from permiso where estado=1";
if ($_SESSION["usua_codi"]!=0) $sql .= " and perfil <> 5";
$rs = $db->conn->query($sql);
$sql = "delete from permiso_usuario where usua_codi=$usr_codigo and id_permiso in ($sql)";
$db->conn->Execute($sql);

while ($rs && !$rs->EOF) { //Cargamos los permisos del usuario
    if (isset($_POST["usr_permiso_".$rs->fields['ID_PERMISO']])) {
        $record["ID_PERMISO"] = $rs->fields['ID_PERMISO'];
        $record["USUA_CODI"] = $usr_codigo;
        if ($rs->fields['ID_PERMISO'] == 26) {
            //Verificamos que solo un usuario de la institución tenga el permiso para saltarse el organico funcional
            $sql = "select usua_codi, usua_nomb, usua_apellido from usuarios where usua_codi in (select usua_codi from permiso_usuario where id_permiso=26) and inst_codi=".$_SESSION["inst_codi"];
            $rs_26 = $db->conn->query($sql);
            if (!$rs_26->EOF) {
                $sql = "delete from permiso_usuario where id_permiso=26 and usua_codi=".$rs_26->fields["USUA_CODI"];
                $db->conn->Execute($sql);

                echo "<script>alert('El permiso para omitir la estructura orgánica funcional\\n".
                     "solo puede ser asignado a un usuario de la institución.\\n".
                     "Se le ha retirado este permiso al usuario \"".$rs_26->fields["USUA_NOMB"] ." " . $rs_26->fields["USUA_APELLIDO"]."\".');</script>";
            }
        }     
        
        $ok1 = $db->conn->Replace("PERMISO_USUARIO", $record, "", false,false,true,false);
    }
    $rs->MoveNext();
}
if ($usuarioSubrEst==" (Subrogante)"){
            $record["USUA_CODI"] = $usr_codigo;
            $record["ID_PERMISO"] = 29;
            $ok1 = $db->conn->Replace("PERMISO_USUARIO", $record, "", false,false,true,false);
        }

$usr_nombre = $usr_nombre . " " . $usr_apellido;
if (isset($usr_nuevo) && $usr_nuevo != null && $usr_estado!=0) {
    $usr_tipo = 1;
    //$usr_login = "U".substr($usr_cedula,0,10);
    include "cambiar_password_mail.php";
    $mensaje = "<b>La contrase&ntilde;a del usuario ha cambiado y se le notific&oacute; a su cuenta de correo electr&oacute;nico.</b><br />";
}
//estado = 1 activo, 0 inactivos
if (trim($usr_estado)==0){//esta inactivo
    if($_POST['ban_compartida'] == 'S'){       
         //elimina la bandeja para usuarios normales
        //si es jefe       
        if (trim($usr_perfil)==0){            
            eliminaBandeja($usr_codigo,$db);
        }
        else{
           
             $eliminaCompartida = 'delete from bandeja_compartida where usua_codi = '.$usr_codigo;
             $db->conn->Execute($eliminaCompartida);
        }
    }
}
if(trim($_POST['eliminar_compartida']=='S')){
     eliminaBandeja($usr_codigo,$db);
}
function eliminaBandeja($usr_codigo,$db){
    $sqlBandeja = 'select * from bandeja_compartida where usua_codi_jefe = '.$usr_codigo;
            $rsBan = $db->conn->query($sqlBandeja);
            while ($rsBan && !$rsBan->EOF) {
                $eliminaCompartida = 'delete from bandeja_compartida where usua_codi_jefe = '.$usr_codigo;
                $db->conn->Execute($eliminaCompartida);
                $rsBan->MoveNext();
            }
}
?>
<html>
    <?echo html_head(); //Imprime el head definido para el sistema?>
<body>
    <form name="frmConfirmaCreacion" action="./cuerpoUsuario.php?accion=2" method="post">
    <center>
        <br /><br />
        <?=$mensaje?><br />
        <table width="40%" border="2" align="center" class="t_bordeGris">
            <tr>
            <td width="100%" height="30" class="listado2">
            <? if ($accion==1) { ?>
                <span class=etexto><center><B>El usuario <?=$usr_nombre?><br/> fue creado correctamente</B></center></span>
            <? } else { ?>
                <span class=etexto><center><B>Los cambios en el usuario <?=$usr_nombre?><br/> se realizaron correctamente</B></center></span>
            <? } ?>
            </td>
            </tr>
            <tr>
            <td height="30" class="listado2">
                <center><input class="botones" type="submit" name="Submit" value="Aceptar"></center>
            </td>
            </tr>
        </table>
    </center>
    </form>
</body>
</html>