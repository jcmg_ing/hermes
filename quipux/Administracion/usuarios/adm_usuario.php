<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/
$ruta_raiz = "../..";
session_start();
if ($_SESSION["usua_admin_sistema"] != 1) {
    die( html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.") );
}
include_once "$ruta_raiz/rec_session.php";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
include_once "$ruta_raiz/funciones_interfaz.php";
include_once "$ruta_raiz/obtenerdatos.php";
p_register_globals(array('accion', 'usr_codigo', 'usr_destino'));


$depe_destino = (isset($depe_destino)) ? $depe_destino : $_SESSION['depe_codi'];

$usuarioSubr = "";
$read = "";
$read2 = "";
$read3 = "readonly";
$read4 = "";
$tiene_subrogacion = 0;

if (!isset($recargar)) {
    if ($accion == 1) { //Nuevo
        $tituloForm = "Creaci&oacute;n de ";
	$usr_nuevo = "checked";
	$usr_estado = "checked";
        
        /*if($_REQUEST['usr_area_responsable']==true )
            $usr_responsable_area=1;
        else
            $usr_responsable_area=0;*/

    } else {
        $sql = "select * from usuarios where usua_codi=$usr_codigo";
        $rs = $db->conn->Execute($sql);
        $usr_depe 	= $rs->fields["DEPE_CODI"];
        $area_inicio    = $rs->fields["DEPE_CODI"];
        $usr_perfil 	= $rs->fields["CARGO_TIPO"];
        $perfil_inicio  = $rs->fields["CARGO_TIPO"];
        $usr_login 	= $rs->fields["USUA_LOGIN"];
        $usr_cedula 	= $rs->fields["USUA_CEDULA"];
        $usr_nombre 	= $rs->fields["USUA_NOMB"];
        $usr_apellido 	= $rs->fields["USUA_APELLIDO"];
        $usr_titulo     = $rs->fields["USUA_TITULO"];
        $usr_abr_titulo = $rs->fields["USUA_ABR_TITULO"];
        $usr_cargo      = $rs->fields["USUA_CARGO"];
        $usr_cargo_cabecera= $rs->fields["USUA_CARGO_CABECERA"];
        $usr_sumilla    = $rs->fields["USUA_SUMILLA"];
        $usr_inst_nombre = $rs->fields["INST_NOMBRE"];
        $usr_responsable_area=($rs->fields["USUA_RESPONSABLE_AREA"] == 1)? "checked" : "";
        //$puesto         = $rs->fields["PUESTO"];
        $cargo_id       = $rs->fields["CARGO_ID"];
        $usr_email      = $rs->fields["USUA_EMAIL"];

        $usr_obs        = $rs->fields["USUA_OBS"];
        $codi_ciudad    = $rs->fields["CIU_CODI"];

        $usr_nuevo      = ($rs->fields["USUA_NUEVO"] == 1) ? "" : "checked";
        $usr_estado     = ($rs->fields["USUA_ESTA"] == 0) ? "" : "checked";

        $usr_firma_path =$rs->fields["USUA_FIRMA_PATH"];

        //direccion y telefono
        $usr_direccion  = $rs->fields["USUA_DIRECCION"];
        $usr_telefono   = $rs->fields["USUA_TELEFONO"];
        //celular
        $usr_celular   = $rs->fields["USUA_CELULAR"];

        //Datos del ultimo usuario que actualizó el registro
        $usr_codi_actualiza     = $rs->fields["USUA_CODI_ACTUALIZA"];
        $usr_fecha_actualiza    = $rs->fields["USUA_FECHA_ACTUALIZA"];
        $usr_obs_actualiza      = $rs->fields["USUA_OBS_ACTUALIZA"];

        $usr_tipo_certificado   = $rs->fields["USUA_TIPO_CERTIFICADO"];

        // Si es Jefe consulta si tiene su bandeja compartida o no
        if($usr_perfil == '1')
        {
            $sqlCompartida = 'select * from bandeja_compartida where usua_codi_jefe = '.$usr_codigo;
            //echo $sqlCompartida;
            $rsComp = $db->conn->Execute($sqlCompartida);
            //var_dump($rsComp->EOF);
            if(!$rsComp->EOF) // Si tiene bandeja compartida
                $ban_compartida = 'S';
            else // No tiene bandeja compartida
                $ban_compartida = 'N';
        }
        else // Si no es Jefe y van a cambiar al usuario de area eliminarlo de la bandeja compartida
        {
            //se verifica primero que no tenga bandeja compartida como jefe ya que hay usuarios que no son jefes y tienen carpeta compartida
            $sqlCompartida = 'select * from bandeja_compartida where usua_codi_jefe = '.$usr_codigo;
            $rsComp = $db->conn->Execute($sqlCompartida);
            if(!$rsComp->EOF)
               $ban_compartida = 'S';
            else
            {
               $sqlCompartida = 'select * from bandeja_compartida where usua_codi = '.$usr_codigo;
               //echo $sqlCompartida;
               $rsComp = $db->conn->Execute($sqlCompartida);
               //var_dump($rsComp->EOF);
               if(!$rsComp->EOF) // Si tiene bandeja compartida
                $ban_compartida = 'S';
               else // No tiene bandeja compartida
                $ban_compartida = 'N';
            }
        }
    }
} else {
        $usr_nuevo = (!isset($_POST["usr_nuevo"])) ? "" : "checked";
        $usr_estado = (!isset($_POST["usr_estado"])) ? "" : "checked";
        //$usr_area_responsable=(!isset($_POST['usr_area_responsable'])) ? "":"checked";
}

    if ($accion == 2) { //Editar
	$tituloForm = "Modificaci&oacute;n de ";
//VERIFICAR SI EL USUARIO ESTA EN SUBROGACION (SUBROGADO O SUBROGANTE)
        $mensajeSubro=usrMensajeSubrogacion($db,$usr_codigo,"estado");
        $usuarioSubrEst=usrMensajeSubrogacion($db,$usr_codigo,"perfil"); 
        
        if (trim($usuarioSubrEst)!=''){
            $read4 = "disabled";
            $tiene_subrogacion =1;
            //$usuarioSubr=str_replace(array("(", ")"),"", $usuarioSubrEst);
            $usuarioSubr=$usuarioSubrEst;
        }else
            $usuarioSubr='';
//
//        $sqlsubrogante = 'select * from usuarios_subrogacion where usua_visible = 1 and usua_subrogante = '.$usr_codigo;
//            //echo $sqlCompartida;
//        $rsSubrgante = $db->conn->Execute($sqlsubrogante);
//            //var_dump($rsComp->EOF);
//        if(!$rsSubrgante->EOF) // Si el usuario es subrogante
//        {
//            $read4 = "disabled";
//            $usuarioSubr = "(Usuario subrogado)";
//            $tiene_subrogacion =1;
//        }
//        $sqlsubrogado = 'select * from usuarios_subrogacion where usua_visible = 1 and usua_subrogado = '.$usr_codigo;
//            //echo $sqlCompartida;
//        $rsSubr = $db->conn->Execute($sqlsubrogado);
//            //var_dump($rsComp->EOF);
//        if(!$rsSubr->EOF) // Si el usuario es subrogado
//        {
//           $read4 = "disabled";
//           $usuarioSubr = "(Usuario subrogado)";
//           $tiene_subrogacion =1;
//        }
    }
    if ($accion == 3) {	//Consultar
    	$read = "readonly";
	$read2 = "disabled";
        $read4 = $read2;
	$tituloForm = "Consulta de ";
    }
    $tituloForm .= ($_SESSION["inst_codi"]>1) ? "Servidores P&uacute;blicos" : "Ciudadanos con Firma Electr&oacute;nica";

    if(trim($usr_codi_actualiza)!='')
    {
        include_once "$ruta_raiz/obtenerdatos.php";
        //Obtener datos del suncionario que actualizo por última ves al ciudadano
        $usua_actualiza = ObtenerDatosUsuario($usr_codi_actualiza, $db);

        $usua_nombre_act = $usua_actualiza['usua_nombre'].' '.$usua_actualiza['usua_apellido'];
        $usua_institucion_act = $usua_actualiza['institucion'];
        $usua_email_act = $usua_actualiza['email'];
    }


echo "<html>".html_head(); /*Imprime el head definido para el sistema*/
require_once "$ruta_raiz/js/ajax.js";
?>
<script type="text/javascript" src="<?=$ruta_raiz?>/js/formchek.js"></script>
<script type="text/javascript" src="<?=$ruta_raiz?>/js/validar_datos_usuarios.js"></script>
<script type="text/javascript">
    function ocultar_combo_usuario_des()
    {
        if (document.forms[0].usr_estado.checked) {
            document.getElementById("tr_estado").style.display="none";
        } else {
            document.getElementById("tr_estado").style.display="";
        }
    }

    function ValidarInformacion()
    {
        //Si es vista de solo consulta, no valida:
        if ('<?php echo $accion; ?>' == '3') {
            return true;
        }

    //Cola de mensajes de error:
        msg = '';

        //Evalúa, en base a la condición, si agrega el mensaje entre los errores o no:
        function e(condicion, mensaje) {
            msg = (condicion) ? msg + mensaje + ' \n' : msg;
        }

        e(trim(document.forms[0].usr_depe.value) == '0', "Seleccione el Area del Usuario.");
        /*e(trim(document.forms[0].usr_login.value) == '', "El campo Login es obligatorio.");*/
        e(trim(document.forms[0].usr_nombre.value) == '', "Ingrese los nombres del usuario.");
        e(trim(document.forms[0].usr_apellido.value) == '', "Ingrese los apellidos del usuario.");

<? if (substr($usr_login,0,4)!="UADM" && substr($usr_login,0,4)!="UUSR") { ?>
        document.forms[0].usr_cedula.value = trim(document.forms[0].usr_cedula.value);
        e((trim(document.forms[0].usr_cedula.value) == "") || document.forms[0].usr_estado.checked
            && !validarCedula(document.getElementById('usr_cedula').value), 'Ingrese una cédula válida.');
<? } ?>

        e(trim(document.forms[0].usr_cargo.value) == '', "Ingrese el puesto del usuario.");
        e(trim(document.forms[0].usr_cargo_cabecera.value) == '', "Ingrese el puesto del usuario a mostrar en la cabecera del documento.");
        e(trim(document.forms[0].usr_sumilla.value) == '' && '<? if ($_SESSION["inst_codi"]>1) echo "1";?>'=='1',
            "Ingrese la sumilla del usuario a mostrar en el pie de página del documento.");
        e(trim(document.forms[0].usr_email.value) == '', "Ingrese el correo electrónico.");
        e(!isEmail(document.forms[0].usr_email.value, true), "El correo electrónico no tiene formato correcto.");
        e(document.forms[0].codi_ciudad.value == 0 || trim(document.forms[0].codi_ciudad.value) == '',
            "Ingrese la ciudad a la que pertence el usuario.");

        //Si hubieron errores, despliega los mensajes:
        if (msg != '') {
            ver_datos();
            alert(msg);
        }

        // Si al Jefe le cambian de perfil de usuario de Jefe a Normal
        if(msg == '' && document.forms[0].ban_compartida.value=='S' && document.forms[0].perfil_inicio.value!=document.forms[0].usr_perfil.value) {
            if(confirm("El usuario tiene Compartida su Bandeja de Documentos Recibidos, si cambia el Perfil se eliminará la configuración de Bandeja Compartida, desea cambiar el Perfil del usuario?") && msg=='') {
                //alert('Eliminar bandejas');
                document.forms[0].eliminar_compartida.value = 'S';
                return true;
            } else {
                //alert('No eliminar bandejas');
                document.forms[0].eliminar_compartida.value = 'N';
                document.forms[0].usr_perfil.value = '1';
                return false;
            }
        }

        // Si al usuario le cambian de área
        if(msg == '' && document.forms[0].ban_compartida.value=='S' && document.forms[0].area_inicio.value!=document.forms[0].usr_depe.value) {
            if(confirm("El usuario tiene Compartida la Bandeja de Documentos Recibidos del Jefe de Área, si se cambia de área se eliminara la configuración de Bandeja Compartida, desea cambiar el área del usuario?") && msg=='')
            {
                //alert('Eliminar bandejas');
                document.forms[0].eliminar_compartida.value = 'S';
                return true;
            }
            else
            {
                //alert('No eliminar bandejas');
                document.forms[0].eliminar_compartida.value = 'N';
                document.forms[0].usr_perfil.value = '1';
                return false;
            }
        }
        if (msg == '' && !validar_datos_registro_civil('usr_nombre','usr_apellido')) return false; // Lanza automaticamente el confirm

        return (msg == '');
    }

function cargarDatosTit(){
    var datosTit = document.frmCrear.cmb_tit.options[document.frmCrear.cmb_tit.selectedIndex].text.split(" - ");
    
    datosCarg = document.frmCrear.cmb_tit.value;
    
    dato1 = datosTit[1]; //abr
    dato2 = datosTit[0]; //tit
    if (typeof dato1=='undefined')
         datosTit[1]='';
      if (typeof dato2=='undefined' || datosCarg==0)
           datosTit[0]='';      
    document.frmCrear.usr_abr_titulo.value = datosTit[1];    
    document.frmCrear.usr_titulo.value = datosTit[0];
   
    /*<?if(trim($usr_titulo)=='') { ?>
    //if(document.frmCrear.usr_codigo.value=='')
        document.frmCrear.usr_titulo.value = datosTit[0];
    <? } ?>*/
}

function cargarCiudad(){
    var area = '';
    var codigo = '';
    area = document.frmCrear.usr_depe.value;
    /*if(document.frmCrear.codi_ciudad.value != '')
        codigo = document.frmCrear.codi_ciudad.value;
    alert(area + "---" + codigo);*/
    if (area != '')
        nuevoAjax('usr_ciu', 'GET', 'ciudad_ajax.php', 'area='+area+'&codigo='+codigo);
    return;
}

nuevoAjax('usr_ciu', 'GET', 'ciudad_ajax.php', 'area=<?=$usr_depe?>&codigo=<?=$codi_ciudad?>');

    // Validar el tipo de archivo que se esta ingresando y copiar su nombre

    var tipo= new Array();
//    tipo[0]='png';
    tipo[0]='gif';
    tipo[1]='jpg';
    tipo[2]='jpeg';

     var marcado = 0,marcadoF="";
    function Obtener_val(formulario){
            marcado=formulario.checked
            
            if(marcado==true)
                document.getElementById("usr_area_responsable").value=1;
            else(marcado==false)
                document.getElementById("usr_area_responsable").value=0;
            //marcadoF=marcado;
            //alert(document.getElementById("usr_area_responsable").value);
        }

    function escogio_archivo()	//ALMACENA EL NOMBRE DEL ARCHIVO Y MUESTA UNA NUEVA FILA
    {
        mensaje = '';
        //var valArchivo = '0';
        arch = document.getElementById('firmaDigitalizada').value.toLowerCase();
        arch = arch.replace(/.p7m/g, "");
        arr_ext = arch.split('.');
        cadena = arr_ext[arr_ext.length-1].toLowerCase();
        flag=true;
        for (j = 0;j < tipo.length; ++j) {
            if (tipo[j]==cadena) {
                flag=false;
                document.getElementById('extarch').value = cadena;
            }
        }
        if (flag) {
            alert ('No está permitido anexar archivos con extensión '+cadena+'.\n'+mensaje+' Consulte con su administrador del sistema.');
            document.getElementById('firmaDigitalizada').value = '';
            document.getElementById('nombarch').value = '';
            return;
        }
        document.getElementById('nombarch').value = document.getElementById('firmaDigitalizada').value;
        return;
    }
    
    function mostrar_div_observacion(accion) {
        if (accion == 1)
            document.getElementById('div_observacion').style.display = '';
        else
            document.getElementById('div_observacion').style.display = 'none';
    }

    function copiar_cargo_cabecera() {
        document.getElementById('usr_cargo_cabecera').value=ulCase(document.getElementById('usr_cargo').value);
    }

    function cambiar_sumilla() {
        var sumilla = trim(document.getElementById('usr_sumilla').value);
        if (sumilla.length<=2) {
            sumilla = trim(document.getElementById('usr_nombre').value).substring(0,1);
            sumilla += trim(document.getElementById('usr_apellido').value).substring(0,1);
            document.getElementById('usr_sumilla').value = sumilla.toLowerCase();
        }
        return;
    }

    function validar_cambio_cedula() {
        cedula = document.getElementById('usr_cedula').value;
        if (trim(cedula)!='') {
            nuevoAjax('div_datos_registro_civil', 'POST', 'validar_datos_registro_civil.php', 'cedula='+cedula);
            nuevoAjax('div_datos_usuario_multiple', 'POST', 'validar_datos_usuario_multiple.php', 'usr_codigo=<?=$usr_codigo?>&cedula='+cedula);
        }
    }
    function verificarResponsable(area,tipo){
        try {
            if (area!=0)
                nuevoAjax('div_responsable_area', 'GET', 'adm_responsable_area.php', 'dependenciaRes='+area);
        } catch (e) {}
    }
    function eliminarResponsable(usuario,area){
        
        if (usuario!=0)
            nuevoAjax('div_responsable_area', 'GET', 'adm_quitar_responsable_area.php', 'usrResponsable='+usuario);
        verificarResponsable(area);
    }


</script>
<body onload="verificarResponsable(<?=0+$area_inicio?>,1)">
  <form name='frmCrear' action="grabar_usuario.php?accion=<?=$accion?>" method="post" ENCTYPE='multipart/form-data'>
    <table width="100%" border="1" align="center" class="t_bordeGris">
  	<tr>
    	    <td class="titulos4">
		<center>
		<p><B><span class=etexto> <?=$tituloForm ?></span></B> </p></center>
	    </td>
	</tr>
    </table>
    <br/>
    <center>
        <div id="div_datos_registro_civil" style="width: 100%;"></div>
        <div id="div_datos_usuario_multiple" style="width: 100%;"></div>
    </center>


    <input type='hidden' name='usr_codigo' value="<?=$usr_codigo?>">
    <input type="hidden" name="usr_login" id="usr_login" value="<?=$usr_login?>">
    <input type="hidden" name="ban_compartida" id="ban_compartida" value="<?=$ban_compartida?>">
    <input type="hidden" name="perfil_inicio" id="perfil_inicio" value="<?=$perfil_inicio?>">
    <input type="hidden" name="area_inicio" id="area_inicio" value="<?=$area_inicio?>">
    <input type="hidden" name="eliminar_compartida" id="eliminar_compartida" value="N">
    <input type="hidden" name="cargo_id" id="cargo_id" value="<?=(0+$cargo_id)?>">
    <input type="hidden" name="tiene_subrogacion" id="tiene_subrogacion" value="<?=$tiene_subrogacion?>">
    <?php 
        
        if ($mensajeSubro!=''){ ?>
        <table width="100%" border="1" align="center" class="borde_tab">
        <tr>
            <th colspan="4">
            <font color="white" size="2"><?php echo $mensajeSubro;?>
            </font></th>
        </tr>
        </table>
        <?php }
    ?>
    <table width="100%" border="1" align="center" class="t_bordeGris" id="usr_datos">
        <tr>
            <td class="titulos2" width="20%">* C&eacute;dula </td>
            <td class="listado2" width="30%">
                <input type="text" name="usr_cedula" id="usr_cedula" value="<?php echo $usr_cedula; ?>" size="20" maxlength="10" <?php echo $read; ?> onchange="validar_cambio_cedula()">
            </td>
            <td class="titulos2" width="20%"> Usuario </td>
            <td class="listado2" width="30%"><?=substr($usr_login,1)?></td>
        </tr>
        <tr>
            <td class="titulos2">* Nombre &nbsp;&nbsp;&nbsp;
                <img src="<?=$ruta_raiz?>/iconos/copy.gif" alt="copiar" title="Copiar datos del Registro Civil" onclick="copiar_datos_registro_civil('nombre', 'usr_nombre')">
            </td>
            <td class="listado2">
                <input type="text" name="usr_nombre" id="usr_nombre" onblur="this.value=ulCase(this.value); cambiar_sumilla();" value="<?php echo $usr_nombre; ?>" size="50" maxlength="100" <?php echo $read; ?>>
            <td class="titulos2">* Apellido &nbsp;&nbsp;&nbsp;
                <img src="<?=$ruta_raiz?>/iconos/copy.gif" alt="copiar" title="Copiar datos del Registro Civil" onclick="copiar_datos_registro_civil('nombre', 'usr_apellido')">
            </td>
            <td class="listado2">
                <input type="text" name="usr_apellido" id="usr_apellido" onblur="this.value=ulCase(this.value); cambiar_sumilla();" value="<?php echo $usr_apellido; ?>" size="50" maxlength="100" <?php echo $read; ?>>
            </td>
        </tr>
        <tr>
            <td class="titulos2" width="20%"> <?php echo ($_SESSION["inst_codi"]==1) ? $descEmpresa : "* $descDependencia";?></td>
            <td class="listado2" width="30%">
            <?
            $sql = "select depe_nomb, depe_codi from dependencia where depe_estado=1 and inst_codi=".$_SESSION["inst_codi"]." order by depe_nomb asc";
            $rs = $db->conn->Execute($sql);
            $mostrar_inst = ($_SESSION["inst_codi"]==1) ? "style='display:none'" : "";
            if ($rs)
                echo $rs->GetMenu2("usr_depe", $usr_depe, "0:&lt;&lt; seleccione &gt;&gt;", false,"","class='select' $read2 onchange='cargarCiudad(); verificarResponsable(this.value,1);' $mostrar_inst");
            $mostrar_inst = ($_SESSION["inst_codi"]==1) ? "" : "style='display:none'";
            ?>
                <input type="text" name="usr_inst_nombre" id="usr_inst_nombre" value="<?php echo $usr_inst_nombre; ?>" size="50" maxlength="200" <?php echo "$read $mostrar_inst"; ?>>
            </td>
            <td class="titulos2"> * Ciudad </td>
            <td class="listado2">
                <div id='usr_ciu'><?=$usr_ciudad?></div>
            </td>
        </tr>
        <tr>
            <td class="titulos2"> Abr. y T&iacute;tulo </td>
            <td class="listado2">
            <?
            $sql_tit = "select tit_nombre || ' - ' || tit_abreviatura, tit_codi from titulo order by split_part(tit_nombre, ' ', 1) asc, split_part(tit_nombre, ' ', 2) asc, tit_nombre asc";
            //$sql_tit = "select tit_nombre, tit_codi from titulo order by split_part(tit_nombre, ' ', 1) asc, split_part(tit_nombre, ' ', 2) asc, tit_nombre asc";

            $rs_tit = $db->conn->Execute($sql_tit);
                echo $rs_tit->GetMenu2("cmb_tit", $cmb_tit, "0:&lt;&lt; seleccione &gt;&gt;", false,"","class='select' $read3 onchange='cargarDatosTit()'");
            ?>
                <br>
                <input type="text" name="usr_abr_titulo" id="usr_abr_titulo" value="<?=$usr_abr_titulo?>" size="4" maxlength="30" <?php echo $read3;?>>
                <input type="text" name="usr_titulo" id="usr_titulo" value="<?php echo $usr_titulo; ?>" size="24" maxlength="100" <?php echo $read3;?>>
            </td>
            <td class="titulos2"> * Correo electr&oacute;nico </td>
            <td class="listado2" colspan="<?=$colspan?>">
                <input type="text" name="usr_email" id="usr_email" value="<?=$usr_email?>" size="50" maxlength="50" <?php echo $read; ?> >
                <!-- onChange="nuevoAjax('div_validar_email', 'POST', 'validar_email.php', 'txt_email='+this.value);" -->
            </td>
        </tr>
        <tr>
            <td class="titulos2"> * Puesto </td>
            <td class="listado2">
                <input type="text" name="usr_cargo" id="usr_cargo" onblur="this.value=ulCase(this.value);" onchange="copiar_cargo_cabecera();" value="<?=$usr_cargo?>" size="50" maxlength="200" title="Nombre del puesto que se visualizará  en el pie de firma del documento" <?php echo $read; ?>>
            </td>
            <td class="titulos2"> * Puesto Cabecera </td>
             <td class="listado2">
                 <input type="text" name="usr_cargo_cabecera" id="usr_cargo_cabecera" onblur="this.value=ulCase(this.value);" value="<?=$usr_cargo_cabecera?>" size="50" maxlength="200"  title="Nombre del puesto que se visualizarà en la cabecera del documento" <?php echo $read; ?>>
            </td>
        </tr>
        <tr>
            <td class="titulos2"> Direcci&oacute;n &nbsp;&nbsp;&nbsp;
                <img src="<?=$ruta_raiz?>/iconos/copy.gif" alt="copiar" title="Copiar datos del Registro Civil" onclick="copiar_datos_registro_civil('direccion', 'usr_direccion')">
            </td>
            <td class="listado2">
                <input type="text" name="usr_direccion" id="usr_direccion" onblur="this.value=ulCase(this.value)"  value="<?=$usr_direccion?>" size="50" maxlength="50" <?php echo $read; ?>>
            </td>
            <td class="titulos2"> Tel&eacute;fono </td>
            <td class="listado2">
                <input type="text" name="usr_telefono" id="usr_telefono" value="<?=$usr_telefono?>" size="50" maxlength="50" <?php echo $read; ?>>
            </td>
        </tr>

        <tr <?php if ($_SESSION["inst_codi"]==1) echo "style='display:none'" ?>>
            <td class="titulos2" width="20%">* Perfil</td>
            <td class="listado2" width="30%">
            <select name=usr_perfil <?php if ($mensajeSubro=='') echo "class='select'";?> <?=$read4?>>
                <option value='0' <?if ($usr_perfil==0) echo "selected"?>> Normal </option>
                <option value='1' <?if ($usr_perfil==1) echo "selected"?>> Jefe </option>
                <option value='2' <?if ($usr_perfil==2) echo "selected"?>> Asistente </option>
            </select>
            </td>
            <td class="titulos2"> * Iniciales Sumilla </td>
            <td class="listado2">
                <input type="text" name="usr_sumilla" id="usr_sumilla" value="<?=$usr_sumilla?>"  <?php echo $read; ?>>
                <div id="div_responsable_area" style="width: 100%;"></div>
<!--                <input type="checkbox" name="usr_area_responsable" id="usr_area_responsable" value="0" onclick="Obtener_val(this)" title="Iniciales del usuario que se visualizará el pie de página (mayùculas) de un documento" <?php echo $usr_responsable_area ."  " .$read; ?>/>Responsable de Área-->
               
            </td>      
        </tr>
        <tr>
             <td class="titulos2" width="20%">Celular</td>
             <td colspan="3" class="listado2" width="30%">
                <input type="text" name="usr_celular" id="usr_celular" value="<?=$usr_celular?>" size="10" maxlength="10" <?php echo $read; ?>/>
             </td>
        </tr>
<?php/* if($_SESSION['usua_codi'] == '0' and $_SESSION["inst_codi"]>1) { ?>
        <tr>
            <td class="titulos2"> Firma digitalizada </td>
            <td class="listado2" >
                <input id="firmaDigitalizada" name="firmaDigitalizada" type="file" class="tex_area" onChange="escogio_archivo();" size="50">
                <input type="hidden" name="nombarch" value="<?=$$nombarch?>" id="nombarch">
                <input type="hidden" name="extarch" value="<?=$$nombarch?>" id="extarch">
            </td>
            <td class="listado2" colspan="2">
                <center>
                <?php if($usr_firma_path!="" and $usr_firma_path!=NULL)  {
                        $urlFirma = $nombre_servidor.'/'.$usr_firma_path;
                ?>
                    <img src="<?=$urlFirma?>" alt="" title="">
                <?php } ?>
                </center>
            </td>
        </tr>
<?php } /**/?>

        <tr>

            <td class="titulos2"> Observaci&oacute;n </td>
            <td class="listado2" colspan="3">
                <textarea name="usr_obs" id="usr_obs" cols="130" rows="3"><?=$usr_obs?></textarea>
            </td>
        </tr>
    </table>
    <br>
        <center>
    <table width="100%"  cellpadding="0" cellspacing="0" >
      <tr>
    	<!--<td width="33%" align="center">
	    <input name="btn_anterior" id="btn_anterior" type="button" class="botones" value="Anterior" onClick="ver_datos();"/>
	    <input name="btn_siguiente" id="btn_siguiente" type="button" class="botones" value="Siguiente" onClick="ver_permisos();"/>
    	</td>-->
	<td  width="50%" align="center">
	    <? if ($accion != 3) { ?>
	    	<input name="btn_aceptar" id="btn_aceptar" type="submit" class="botones" value="Grabar" onClick="return ValidarInformacion();"/>
                <!--<input name="btn_aceptar" id="btn_aceptar" type="submit" class="botones" value="Grabar" onClick="<?= $accion_btn_aceptar ?>"/>-->
	    <? } ?>&nbsp;
	</td>
	<td  width="50%" align="center">
	    <input  name="btn_accion" type="button" class="botones" value="Regresar" onclick="window.location='./cuerpoUsuario.php?accion=2.php'"/> <!--location='./mnuUsuarios.php'-->
	</td>
      </tr>
    </table>
    <?php
    if($usr_codi_actualiza!='')
    {
        if($usr_obs_actualiza!='')
        {
        ?>
        <br>
        <img align="right" src='../../iconos/posit.jpg' title="Ver observaciones de última actualización." onclick="mostrar_div_observacion('1')" alt="">
        <table width="50%" align="right">
            <tr>
                <td>
                <div id="div_observacion" class="cal-TextBox" style="border: thin solid #006699; width: 100%; display: none; text-align: left;">
                    <table border="0">
                        <tr>
                            <td width="98%"><?=$usr_obs_actualiza?></td>
                            <td width="2%" valign="top"><img src="../../iconos/x.gif" alt="Cerrar" title="Cerrar" width="12px" height="12px" onclick="mostrar_div_observacion('0')"></td>
                        </tr>
                    </table>

                </div>
                </td>
            </tr>
        </table>
        <br><? } ?><br>
        <table width="100%" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td class="titulos4" colspan="4">
                    <center>
                        <p><B><span class=etexto><font size="2">&Uacute;ltima actualizaci&oacute;n realizada por:</font></span></B> </p>
                    </center>
                </td>
            </tr>
            <tr>
                <td class="titulos2" width="20%">
                    Usuario:
                </td>
                <td class="listado2" width="30%">
                    &nbsp;&nbsp;<?=$usua_nombre_act?>
                </td>
                <td class="titulos2" width="20%">
                    E-mail:
                </td>
                <td class="listado2" width="30%">
                    &nbsp;&nbsp;<?=$usua_email_act?>
                </td>
            </tr>
            <tr>
                <td class="titulos2">
                    Fecha de Actualizaci&oacute;n:
                </td>
                <td class="listado2" colspan="3">
                    &nbsp;&nbsp;<?=substr($usr_fecha_actualiza,0,19)?>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    
                </td>
            </tr>
        </table>
    <?php
    }
    ?>
    </center>
    <br>
    <table width="100%" border="1" align="center" class="t_bordeGris" id="usr_permisos">
<?
        $sqlp = "select p.descripcion, p.descripcion_larga, p.id_permiso **SELECT**
                from permiso p **FROM**
                where p.estado=1 **WHERE**
                group by perfil, p.descripcion, p.descripcion_larga, p.id_permiso, p.orden order by perfil, p.orden asc";
        if ($usr_codigo=="") { // Si es usuario nuevo
            $sqlp = str_replace ("**SELECT**", ", 0  as permiso **SELECT**", $sqlp);
            $sqlp = str_replace ("**FROM**", "", $sqlp);
        } else {
            $sqlp = str_replace ("**SELECT**", ", count(pc.id_permiso) as permiso **SELECT**", $sqlp);
            $sqlp = str_replace ("**FROM**", " left outer join (select * from permiso_usuario where usua_codi=$usr_codigo) as pc on p.id_permiso=pc.id_permiso ", $sqlp);
        }
        if ($_SESSION["usua_codi"] != 0) // Se muestran permisos especiales para Super Admin
            $sqlp = str_replace ("**WHERE**", " and perfil <> 5 **WHERE**", $sqlp);

        if ($_SESSION["inst_codi"]==1) { // Si es un usuario ciudadano se muestran solo ciertos permisos
            $sqlp = str_replace ("**SELECT**", ", 0 as perfil", $sqlp);
            //$sqlp = str_replace ("**WHERE**", " and p.id_permiso in (19,21,27) **WHERE**", $sqlp);
            //Cambiado por Sylvia Velasco se suprimio la palabra **WHERE** para que el query se ejecute correctamente
            $sqlp = str_replace ("**WHERE**", " and p.id_permiso in (19,21,27) ", $sqlp);
        } else {
            $sqlp = str_replace ("**SELECT**", ", perfil", $sqlp);
            $sqlp = str_replace ("**WHERE**", "", $sqlp);
        }

    	$rs = $db->conn->query($sqlp);
        $perfilAnterior = -1;
        $perfiles = array("Generales","Asistentes ó Secretarias","Jefes","Bandeja de Entrada","Administrador","Usuarios Especiales","R.R.H.H.");
    	while (!$rs->EOF) {
            $numPerfil = 0 + $rs->fields["PERFIL"];
            $id_permiso = $rs->fields["ID_PERMISO"];

            if($perfilAnterior != $numPerfil) {
                echo "<tr><td class='titulos2' colspan='2'>".$perfiles[$numPerfil]."</td></tr>";
                if($numPerfil == 0) { // Si el perfil es general imprime cambio de contraseña y usuario activo
                ?>
                    <tr>
                        <td class="listado2" width="35%">
                            <input type="checkbox" name="usr_nuevo" value="0" <?php echo $usr_nuevo." ".$read2; ?> > Cambio de Contrase&ntilde;a
                        </td>
                        <td class="listado2" width="70%">Se solicita al usuario v&iacute;a e-mail que ingrese una nueva contraseña para ingresar al sistema.</td>
                    </tr>
                    <tr>
                        <td class="listado2">
                            <input type="checkbox" name="usr_estado" value="1" <?php echo $usr_estado." ".$read4; ?> onclick="ocultar_combo_usuario_des()"> Usuario Activo <b><?php                            
                            echo $usuarioSubr; ?></b>
                        </td>
                        <td class="listado2">Activa o desactiva el usuario. Los usuarios desactivados no pueden acceder al sistema.</td>
                    </tr>
                    <?  if (!isset($usr_codigo)) $usr_codigo="";
                    if (!isset($usr_depe)) $usr_depe="";
                    //inicio IF
                    if (trim($usr_codigo) != "" and trim($usr_depe) != "") { ?>
                        <tr id="tr_estado">
                            <td class="listado2">
                            <?
                                $sql = "select usua_nombre, usua_codi from usuario where depe_codi=$usr_depe and usua_esta=1 and usua_codi<>$usr_codigo and usua_login not like 'UADM%' order by 1 asc";
                    //              echo $sql;
                                $rsCmb = $db->conn->Execute($sql);
                                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$rsCmb->GetMenu2("usr_destino", $usr_destino, "0:&lt;&lt; seleccione &gt;&gt;", false,"","class='select' $read2");
                            ?>
                            </td>
                            <td class="listado2">Usuario al que desea reasignar los documentos pertenecientes al usuario desactivado.</td>
                        </tr>
                    <script type="text/javascript">
                        ocultar_combo_usuario_des();
                    </script>
                    <?php
                    }//fin IF
                }
            } // Fin impresion de cabeceras de grupo

            $checked = ($rs->fields["PERMISO"]==1) ? "checked" : "";
            if ($numPerfil==1 and $id_permiso==29){
               echo "<tr>
                        <td class='listado2'>
                            <input type='checkbox' name='usr_permiso_$id_permiso' id='usr_permiso_$id_permiso' value='$id_permiso' onclick='seleccionar_permiso($id_permiso)' $checked $read4 >
                            ".$rs->fields["DESCRIPCION"]."<div id='div_permiso_$id_permiso' style='display:none'></div>
                        </td>
                        <td class='listado2'>".$rs->fields["DESCRIPCION_LARGA"]."</td>
                      </tr>"; 
            }else{
                echo "<tr>
                        <td class='listado2'>
                            <input type='checkbox' name='usr_permiso_$id_permiso' id='usr_permiso_$id_permiso' value='$id_permiso' onclick='seleccionar_permiso($id_permiso)' $checked $read2 >
                            ".$rs->fields["DESCRIPCION"]."<div id='div_permiso_$id_permiso' style='display:none'></div>
                        </td>
                        <td class='listado2'>".$rs->fields["DESCRIPCION_LARGA"]."</td>
                      </tr>";
            }
            $perfilAnterior = $numPerfil;
            $rs->MoveNext();
        }
 ?>

    </table>
  </form>
    <script type="text/javascript">
	function ver_datos() {
	    document.getElementById('usr_datos').style.display='';
	    //document.getElementById('usr_permisos').style.display='none';
        document.getElementById('usr_permisos').style.display='';
	    //document.getElementById('btn_anterior').style.display='none';
	    //document.getElementById('btn_siguiente').style.display='';
	    <? if ($accion != 3) { ?>
	    	document.getElementById('btn_aceptar').style.display='';
	    <? } ?>
	}
	function ver_permisos() {
	    /*if (ValidarInformacion()) {*/
	    	document.getElementById('usr_datos').style.display='none';
	    	document.getElementById('usr_permisos').style.display='';
	    	//document.getElementById('btn_anterior').style.display='';
	    	//document.getElementById('btn_siguiente').style.display='none';
	    	<? if ($accion != 3) { ?>
		    document.getElementById('btn_aceptar').style.display='';
	    	<? } ?>
	    /*}*/
	}
        
        function seleccionar_permiso(permiso) {
            if (document.getElementById('usr_permiso_'+permiso).checked)
                document.getElementById('div_permiso_'+permiso).style.display = '';
            else
                document.getElementById('div_permiso_'+permiso).style.display = 'none';
            return;
        }

<?php
    $sql = "select descripcion, tipo_cert_codi from tipo_certificado where estado=1 and tipo_cert_codi>0 order by 2 asc";
    $rs_tc = $db->conn->Execute($sql);
    $cmb_tipo_certificado = $rs_tc->GetMenu2("usr_tipo_certificado", 0+$usr_tipo_certificado, "", false,"","class='select' $read2");
    $cmb_tipo_certificado = str_replace("\n"," ",str_replace("'",'"',$cmb_tipo_certificado));
?>

        function cargar_combo_firma() {
            if (document.getElementById('usr_permiso_19').checked) {
                document.getElementById('div_permiso_19').style.display = '';
            }
            document.getElementById('div_permiso_19').innerHTML =
                '<table border="0" width="100%"><tr><td>Seleccione el tipo de certificado:</td><td><?=$cmb_tipo_certificado?></td></tr></table>';
        }
        
	ver_datos();
        validar_cambio_cedula();
        cargar_combo_firma();
        
    </script>

</body>
</html>
