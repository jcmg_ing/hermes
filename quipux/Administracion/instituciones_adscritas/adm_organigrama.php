<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/
$ruta_raiz = "../..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
include_once "$ruta_raiz/funciones_interfaz.php";
include_once "$ruta_raiz/obtenerdatos.php";

session_start();


if ($_SESSION["usua_admin_sistema"] != 1) {
    echo html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
    die("");
}

if(isset($_GET)){
   

   
?>

<? require_once "$ruta_raiz/js/ajax.js";?>

<? echo "<html>".html_head(); /*Imprime el head definido para el sistema*/?>
<script type="text/javascript">
function ltrim(s) {
   return s.replace(/^\s+/, "");
}


</script>

<body>
  <form name='frmCrear' action="grabar_usuario_subrogante.php" method="post" ENCTYPE='multipart/form-data'>
   
    <table id="tablaInstituciones" name="tablaInstituciones" width="100%" border="0" align="center" class="listado2">
  	<tr id="celda" class="listado2">
    	    <td class="listado2" colspan="6">
		<center>
		<B><span class=etexto>INSTITUCIÓN PRINCIPAL</span></B></center>
            </td>            
	</tr>        
        <tr class="titulos2" id="elerow1">
        <td width="20%">
            <center><b>Ruc</b></center>
        </td>
        <td width="50%">
            <center><b>Nombre</b></center>
        </td>
        <td width="15%">
            <center><b>Sigla</b></center>
        </td>
        <td width="15%">
            <center><b>Estado</b></center>
        </td>
    </tr>
        <tr>
            <td><center><?php echo $_GET['instRUC'];?></center>		
            </td>           
            <td><center><?php echo $_GET["instNombre"];?></center></td>
            <td><center><?php echo $_GET["instSigla"];?></center></td>
            <td><center><?php echo "Activo";?></center></td>
	</tr>
        
         
    </table>    
  </form>
    

</body>
</html>
<?php } ?>