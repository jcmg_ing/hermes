<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


$ruta_raiz="../..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post


//Archivo que permite buscar el destinatario y remitente para los documentos
session_start();
include_once "$ruta_raiz/rec_session.php";

include_once("$ruta_raiz/obtenerdatos.php");
include_once "$ruta_raiz/funciones.php";
if (isset($_GET)){
    
    $buscar_nom = $_GET["buscar_nom"];
    $buscar_sigla = $_GET["buscar_sigla"];
}
if (!$buscar_nom) $buscar_nom="0";
if (!$buscar_sigla) $buscar_sigla="0";
$buscar_nom = strtoupper(trim(limpiar_sql($buscar_nom)));
    $buscar_sigla = strtoupper(trim(limpiar_sql($buscar_sigla)));
?>

<table class=borde_tab width="100%" cellpadding="0" cellspacing="4">
    <tr class="grisCCCCCC" align="center">
        <td width="10%" class="titulos5">Ruc</td>
        <td width="40%" class="titulos5">Nombre</td>
        <td width="6%" class="titulos5">Sigla</td>        
        <td width="10%" colspan="2" class="titulos5">Definir Como</td>
        <td width="10%"  class="titulos5">Institución Principal</td>
    </tr>
<?

$sql="";

if ($buscar_nom!="0" or $buscar_sigla!="0") {
    
    if ($buscar_nom=="0")
        $glue = "";
    else
        $glue = "or";
    
    $sql = "select *  
            from institucion
            where ";
    if ($buscar_nom != "0") {
        $sql .= " upper(inst_nombre) like ('%$buscar_nom%')";
    }

    if ($buscar_sigla!="0") {
        $sql .= " $glue upper(inst_sigla) like ('%$buscar_sigla%')";
    }
    
    $sql .= " order by 1";    

}else{
     echo "<tr><td colspan=12><font color='red'><center>Ingrese Nombre o Sigla</center></font></td>";
}

if ($sql!="") {
    $rs=$db->query($sql);
    $i=0;
    if ($rs->EOF) {
        if ($lista_usr=="0")
            echo "<tr><td colspan=12><center><span class='titulosError'>No se encontraron Usuarios con ese Nombre o número de CI</span></center></td></tr>";
        else
            echo "<tr><td colspan=12><center><span class='titulosError'>La lista se encuentra vac&iacute;a</span></center></td></tr>";
    }
  
    while(!$rs->EOF)
    {
        $codigoInstitucion = trim($rs->fields["INST_CODI"]);
        $ruc = trim($rs->fields["INST_RUC"]);
        
        $nombreInstitucion = trim ($rs->fields['INST_NOMBRE']);
        $siglaInstitucion = trim ($rs->fields['INST_SIGLA']);
        $estadoInst = trim ($rs->fields['INST_ESTADO']);
        if($estadoInst==1)
            $estnombre="Activo";
        else
            $estnombre="Inactivo";
        
         $matriz_id=esMatriz($db, $codigoInstitucion,1);
         
         
         $datos=array();
         $datos=datosMatriz($db,$matriz_id);
         $matrizHija=esMatriz($db, $matriz_id,1);
    ?>
        <tr onmouseover="this.style.background='#e3e8ec'" onmouseout="this.style.background='white', this.style.color='black'">                     
            <td width="10%"><font size=1><?=substr($rs->fields["INST_RUC"],0,120) ?></font></td>
            <td width="40%"><font size=1><?=substr($rs->fields["INST_NOMBRE"],0,100) ?></font></td>
            <td width="6%"><font size=1><?=substr($rs->fields["INST_SIGLA"],0,70) ?></font></td>            
            <td width="5%"><center><font size=1><input class='botones_azul' title='Institución Principal' type='button' value='Principal' onClick="limpiar_datos(); pasar_principal('<?=$codigoInstitucion?>','<?=$nombreInstitucion?>','<?=$siglaInstitucion?>','<?=$ruc?>','<?=$matriz_id?>');" onblur="copiar_anteriores();"/></font></center></td>
            <td width="5%"><center><font size=1><input class='botones_azul' title='Institución Adscrita' type='button' value='Adscrita' onClick="pasar_adscrita('<?=$codigoInstitucion?>','<?=$nombreInstitucion?>','<?=$siglaInstitucion?>','<?=$ruc?>');"/></font></center></td>
            <td width="10%"><font size=1>
                <a href="javascript:;" onClick="limpiar_datos(); pasar_principal('<?=$matriz_id?>','<?=$datos["1"]?>','<?=$datos["2"]?>','<?=$datos["0"]?>','<?=$matrizHija?>');" class="vinculos"><?=esMatriz($db, $codigoInstitucion,2)?></a>            
            </font></td>
        </tr>
  <?
        
        $rs->MoveNext();
        
    }
}
?>

</table>
<?php
function esMatriz($db,$instCodi,$tipo){
    $sql="select * from institucion_org where org_id = $instCodi";     
    $rsIns=$db->conn->query($sql);
    if ($rsIns->fields["ORG_ID_PADRE"]=='')
        $inst_codi = $rsIns->fields["INST_CODI"];
    else
        $inst_codi = $rsIns->fields["ORG_ID_PADRE"];
    if($tipo==1){
        if ($inst_codi=='')
            return 0;
            else
        return $inst_codi;
    }
    else{
        $sql="select inst_nombre from institucion where inst_codi = $inst_codi";
        $rsIns=$db->conn->query($sql);   
        $inst_nombre = $rsIns->fields["INST_NOMBRE"];
        return $inst_nombre;
    }
}
function datosMatriz($db,$instCodi){
    $datos=array();
    $sql="select * from institucion where inst_codi = $instCodi";
    
    $rsIns=$db->conn->query($sql);
    $datos["0"]=$rsIns->fields["INST_RUC"];
    $datos["1"]=$rsIns->fields["INST_NOMBRE"];
    $datos["2"]=$rsIns->fields["INST_SIGLA"];
    return $datos;
}
?>