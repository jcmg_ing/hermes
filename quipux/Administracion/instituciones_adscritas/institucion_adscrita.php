<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


$ruta_raiz="../..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post


//Archivo que permite buscar el destinatario y remitente para los documentos
session_start();
include_once "$ruta_raiz/rec_session.php";

include_once("$ruta_raiz/obtenerdatos.php");
include_once "$ruta_raiz/funciones.php";
if (isset($_GET)){
    
    $instPadre = 0+$_GET["instCodi"];
    
}
if (!$instPadre) $instPadre="0";
$instPadre=limpiar_sql($instPadre);

?>

<table id="instTabla" name="instTabla" class=borde_tab width="100%" cellpadding="0" cellspacing="4">
    <tr class=listado2 id="elerow1">
        <td colspan="4">
            <center><b>INSTITUCIONES ADSCRITAS</b></center>
        </td>
    </tr>
    <tr class="titulos2" id="elerow1">
        <td width="20%">
            <center><b>Ruc</b></center>
        </td>
        <td width="50%">
            <center><b>Nombre</b></center>
        </td>
        <td width="15%">
            <center><b>Sigla</b></center>
        </td>
        <td width="15%">
            <center><b>Acción</b></center>
        </td>
    </tr>
<?

$sql="";

if ($instPadre!="0") {
    
    $sql = "select *  
            from institucion_org
            where inst_codi=$instPadre and org_id_padre is null order by org_id";    
    

}else{
     echo "<tr><td colspan=12><font color='red'><center>Ingrese Nombre o Sigla</center></font></td>";
}
  $rs=$db->conn->query($sql);
  while(!$rs->EOF)
  {
      $id_inst=trim($rs->fields["ORG_ID"]);
      if ($id_inst!='')
        $anteriorC = $anteriorC.",".$id_inst;
       $rs->MoveNext();
  }
  
  if (trim($anteriorC=='')){
      
      $sql = "select *  
            from institucion_org
            where org_id_padre=$instPadre order by org_id";
       
       }    
  $rs=$db->conn->query($sql);
  while(!$rs->EOF)
  {
        
        $id_institucion = trim($rs->fields["ORG_ID"]);
        if ($id_institucion!='')
        $anteriorIns = $anteriorIns.",".$id_institucion;
        
        $sqlInstitucion="select * from institucion where inst_codi = $id_institucion";
        $rsIns=$db->conn->query($sqlInstitucion);
        
    ?>
        <tr>
            <td><center><?=$rsIns->fields["INST_RUC"]?></center></td>
            <td><center><?=$rsIns->fields["INST_NOMBRE"]?></center></td>
            <td><center><?=$rsIns->fields["INST_SIGLA"]?></center></td>           
            <td><input class='botones_azul' value='Elminar' type=button onclick='remove(this,<?=$id_institucion?>)'></td>
            
        </tr>
  <?        
        $rs->MoveNext();
        
    }    

?>
<input type="hidden" id="txt_anteriores" name="txt_anteriores" value="<?=$anteriorIns?>" class="tex_area"/>
</table>