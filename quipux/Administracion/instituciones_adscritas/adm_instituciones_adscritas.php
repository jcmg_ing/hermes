<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$ruta_raiz = "../../";
session_start();
include_once "$ruta_raiz/rec_session.php";
require_once("$ruta_raiz/funciones.php");
include_once "$ruta_raiz/funciones_interfaz.php";


if($_SESSION["usua_admin_sistema"]!=1 and $_SESSION["usua_perm_ciudadano"]!=1) {
    echo html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
    die("");
}

echo "<html>".html_head();
echo "<script language='JavaScript' src='$ruta_raiz/js/spiffyCal/spiffyCal_v2_1.js'></script>";
include_once "$ruta_raiz/js/ajax.js";
?>
<script type="text/JavaScript">
    var insttemp= '';
    var str = '';
function agregarFila(instTabla,idInst,nombre, sigla,ruc){
    var tbody = document.getElementById
    (instTabla).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR");
    var td1 = document.createElement("TD");
    td1.appendChild(document.createTextNode("columna 1"))
    var td2 = document.createElement("TD");
    td2.appendChild (document.createTextNode("columna 2"))
    var td3 = document.createElement("TD");
    td3.appendChild(document.createTextNode("columna 3"))
    var td4 = document.createElement("TD");
    td4.appendChild (document.createTextNode("columna 4"))
    row.appendChild(td1).innerHTML="<center>"+ruc+"</center>";
    row.appendChild(td3).innerHTML="<center>"+nombre+"</center>";
    row.appendChild(td4).innerHTML="<center>"+sigla+"</center>";
    row.appendChild(td2).innerHTML="<input class='botones_azul' value='Elminar' type=button onclick='remove(this,"+idInst+")'>";
    tbody.appendChild(row);
    instCodi=document.getElementById("instCodi_p").value;
    nombreM=document.getElementById("instNombre_p").value;
    siglaM=document.getElementById("instSigla_p").value;
    rucM=document.getElementById("instRUC_p").value;
    matriz=document.getElementById("instMatriz").value;
    pasar_principal(instCodi,nombreM,siglaM,rucM,matriz)
}
function remove(t,InstCodi)
{    
    document.getElementById('divGuardar').innerHTML='';
    var td = t.parentNode;    
    var tr = td.parentNode;
    var table = tr.parentNode;
    table.removeChild(tr);
    grabar(2,InstCodi);
    inicial = document.getElementById("txt_total_adscritas").value;    
    document.getElementById("txt_total_adscritas").value="";
    codigos = new Array();
    codigos = inicial.split(',');
        for(i=1; i<codigos.length;i++){            
               if (codigos[i]!=InstCodi){
                   document.getElementById("txt_total_adscritas").value += ","+codigos[i];
               }
            }
    document.getElementById("txt_adscritas").value=document.getElementById("txt_total_adscritas").value;
    instCodi=document.getElementById("instCodi_p").value;
    nombre=document.getElementById("instNombre_p").value;
    sigla=document.getElementById("instSigla_p").value;
    ruc=document.getElementById("instRUC_p").value;
    matriz=document.getElementById("instMatriz").value;
    pasar_principal(instCodi,nombre,sigla,ruc,matriz)
        
}
// Coloca un usuario como destinatario o remitente
//instCodi = usuario
//nombre institucion
//sigla institucion
function pasar_principal(instCodi,nombre,sigla,ruc,matriz){   
    document.getElementById("divGuardar").innerHTML="";
    banderaExiste=0;
    document.getElementById("instCodi_p").value=instCodi;
    document.getElementById("instNombre_p").value=nombre;
    document.getElementById("instSigla_p").value=sigla;
    document.getElementById("instRUC_p").value=ruc;    
    document.getElementById("instMatriz").value=matriz;
    inicial = document.getElementById("txt_total_adscritas").value;
    codigos = new Array();
        codigos = inicial.split(',');
        for(i=1; i<codigos.length;i++){    
               if (codigos[i]==instCodi){
                   banderaExiste=1;
                   alert("Esta Institucion ya está Seleccionada")
                   break;
               }
               else
                   banderaExiste=0;
            }
            if(banderaExiste==0){
                 datos="instCodi="+instCodi+"&instNombre="+nombre+"&instSigla="+sigla+"&instRUC="+ruc;
                 nuevoAjax("divOrganigrama", 'GET', 'adm_organigrama.php', datos);
                 datosAds="instCodi="+instCodi;
                 //buscar anteriores
                 nuevoAjax("divAdscritas", "GET", "institucion_adscrita.php", datosAds);
            }
            
}
function pasar_adscrita(instCodi, nombre,sigla,ruc){
   document.getElementById("divGuardar").innerHTML="";
   if (document.getElementById("instCodi_p").value!=''){    
    if (document.getElementById("instCodi_p").value!=instCodi){
        banderaExiste=0;
        document.getElementById("txt_adscritas").value = document.getElementById("txt_total_adscritas").value;
        inicial = document.getElementById("txt_adscritas").value;
        document.getElementById("txt_adscritas").value=document.getElementById("txt_adscritas").value+","+instCodi;

        codigos = new Array();
        codigos = inicial.split(',');
        for(i=1; i<codigos.length;i++){    
               if (codigos[i]==instCodi){
                   banderaExiste=1;
                   alert("Esta Institucion ya está Seleccionada")
                   break;
               }
               else
                   banderaExiste=0;
            }
           if (banderaExiste==0){
               document.getElementById("txt_total_adscritas").value=document.getElementById("txt_total_adscritas").value+","+instCodi;
               agregarFila("instTabla",instCodi,nombre,sigla,ruc);
             }
             grabar(1,instCodi);
    }
    else
      alert("Esta Institución está seleccionada como Principal");
    }else
       alert("Por favor Seleccione Institución Principal");
    
}
function buscar_resultado(){
    
    style_display = '';
    var nomDivResultado = "resultado";
    document.getElementById(nomDivResultado).innerHTML = '<center>Por favor espere mientras se realiza la b&uacute;squeda.<br>&nbsp;<br>' +
                                                           '<img src="<?=$ruta_raiz?>/imagenes/progress_bar.gif"><br>&nbsp;</center>';

    instPrincipal = document.getElementById("instCodi_p").value;
    
    if (instPrincipal=='')
        bandera="No Existe";
    else
        bandera = "Si Existe";
    
    var datos = "buscar_nom=" + document.formu1.buscar_nom.value +
                "&buscar_sigla=" + document.formu1.buscar_sigla.value +
                "&bandera=" + bandera;
            
    nuevoAjax(nomDivResultado, 'GET', 'buscar_institucion.php', datos);
    
    //return;
}
function grabar(tipo,InstCodi){     
     insTHijas=document.getElementById("txt_total_adscritas").value;
     if (tipo==1){//grabar
         if (insTHijas==''){
             alert("Seleccione Instituciones Adscritas")
             if (document.getElementById("txt_anteriores"))
                 document.getElementById("txt_anteriores").value='';
         }
         else{
             txt_total_adscritas= document.getElementById("txt_total_adscritas").value;
             instMatriz= document.getElementById("instMatriz").value;
             instCodi_p = document.getElementById("instCodi_p").value;
             datos = "txt_total_adscritas="+txt_total_adscritas+"&instCodi_p="+instCodi_p+"&instMatriz="+instMatriz;
             nuevoAjax("divGuardar", 'POST', 'adm_grabar_organigrama.php', datos);
         }
      }else{//eliminar          
           txt_total_adscritas= InstCodi;
           instMatriz= document.getElementById("instMatriz").value;
           instCodi_p = document.getElementById("instCodi_p").value;           
           datos = "txt_total_adscritas="+txt_total_adscritas+"&instCodi_p="+instCodi_p+"&accion=2&instMatriz="+instMatriz;
           nuevoAjax("divGuardar", 'POST', 'adm_grabar_organigrama.php', datos);
      }
     buscar_resultado();
         
}
function copiar_anteriores(){
    
    if (document.getElementById("divAdscritas")!=null){
        if (document.getElementById("txt_anteriores").value!=''){    
            document.getElementById("txt_total_adscritas").value=document.getElementById("txt_anteriores").value;
            //document.getElementById("txt_adscritas").value=document.getElementById("txt_anteriores").value;
        }
    }
}
function limpiar_datos(){
    document.getElementById("txt_total_adscritas").value='';
    document.getElementById("txt_adscritas").value='';
}
function validarEv(e){   
  tecla = (document.all) ? e.keyCode : e.which;
  if (tecla==13) buscar_resultado();

}
</script>


<body bgcolor="#FFFFFF">
<form method="post" name="formu1" id="formu1" action="javascript: buscar_resultado();"> 
    <input type="hidden" name="instCodi_p" id="instCodi_p" value="" />
    <input type="hidden" name="instMatriz" id="instMatriz" value="" />
    <input type="hidden" name="instNombre_p" id="instNombre_p" value="" />
    <input type="hidden" name="instSigla_p" id="instSigla_p" value="" />
    <input type="hidden" name="instRUC_p" id="instRUC_p" value="" />
    <br>
    <?php//Adscrita?>
    
    <input type="hidden" name="txt_adscritas" id="txt_adscritas" value="" />
    <input type="hidden" name="txt_total_adscritas" id="txt_total_adscritas" value="" />
   

<table border=0 width="100%" class="borde_tab" cellpadding="0" cellspacing="5">
    <tr id="celda" class="listado2">
    	    <td class="listado2" colspan="4">
		<center>
		<B><span class=etexto>ADMINISTRACIÓN DE ORGANIGRAMA</span></B></center>
            </td>            
	</tr>
  <tr>
    <td width="13%" class="listado5"><font class="tituloListado">BUSCAR INSTTITUCIONES: </font></td>
    <td width="75%" class="listado5" valign="middle">
    <table>
       
        <input type="hidden" name="buscar_tipo" id="buscar_tipo" value="1" />
        <tr>
            <td width="20%" align="right"><span class="listado5"><label id="lbl_datos_usua">Nombre Institución:</label> </span> </td>
            <td width="16%"><input type=text id="buscar_nom" name="buscar_nom" value="<?=$buscar_nom?>" class="tex_area" onkeypress="validarEv(event);"/></td>
            <td width="16%" align="right"><span class="listado5">Sigla: </span> </td>
            <td width="16%"><input type=text id="buscar_sigla" name="buscar_sigla" value="<?=$buscar_sigla?>" class="tex_area" onkeypress="validarEv(event);"/></td>
        </tr>
        
        
<?php if ($_SESSION["inst_codi"] > 1) { // ?>
        <tr id="tr_dependencia" <?php if ($buscar_tipo==2) echo "style='display:none'"?>>
            <td colspan="6" valign="middle"><input type="hidden" name="buscar_depe" id="buscar_depe"/>

            </td>
        </tr>
<? } ?>
      </table>
    </td>
    <td width="12%" align="center" class="listado5" >
        <input type='button' onClick='buscar_resultado();' name='bt_buscar' value='Buscar' class="botones" title="Buscar Instituciones"/>
    </td>
    <td width="12%" align="center" class="listado5" >
    <input  name="btn_accion" type="button" class="botones" value="Regresar" onClick="history.back();"/>
    </td>
  </tr>


</table>
<br>
<table class=borde_tab width="100%" cellpadding="0" cellspacing="1">
    <tr class=listado2>	<td colspan="10">
        <center><b><?php echo "SELECCIONE INSTITUCIÓN PRINCIPAL Y ADSCRITA";?></b></center>
    </td></tr>
</table>
<div id="resultado" class="estiloDivPeq"></div>
<br>
<table class=borde_tab width="100%" cellpadding="0" cellspacing="4">
    <tr class=listado2>
        <td colspan="10">
            <center><b>ESTRUCTURA DEL ORGANIGRAMA SELECCIONADO</b></center>
        </td>
    </tr>
</table>
</form>
 <div id="divOrganigrama" height="60px"><input type='hidden' name='flag_inst' id='flag_inst' value='0'></div>
 <div id="divAdscritas" class="estiloDivPeq">
 
    <table id="instTabla" name="instTabla" class=borde_tab width="100%" cellpadding="0" cellspacing="4">
    <tr class=listado2 id="elerow1">
        <td colspan="4">
            <center><b>INSTITUCIONES ADSCRITAS</b></center>
        </td>
    </tr>
    <tr class="titulos2" id="elerow1">
        <td width="20%">
            <center><b>Ruc</b></center>
        </td>
        <td width="50%">
            <center><b>Nombre</b></center>
        </td>
        <td width="15%">
            <center><b>Sigla</b></center>
        </td>
        <td width="15%">
            <center><b>Acción</b></center>
        </td>
    </tr>
</table>
</div>
 <table width="100%"  cellpadding="0" cellspacing="0" >
     <div id="divGuardar" height="20%"></div>      
</body>
</html>