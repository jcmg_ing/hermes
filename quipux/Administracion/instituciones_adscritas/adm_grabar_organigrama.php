<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$ruta_raiz = "../..";
session_start();
include_once "$ruta_raiz/rec_session.php";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
require_once("$ruta_raiz/obtenerdatos.php");  //formar la observacion de edicion
include_once "$ruta_raiz/funciones_interfaz.php";

if ($_SESSION["usua_admin_sistema"] != 1) {
    echo html_error("Lo sentimos, usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
    die("");
}
$rep = array("E'","'");
if (isset($_POST)){    
    $instiAdscritas = $db->conn->qstr(limpiar_sql(trim($_POST["txt_total_adscritas"])));
    $instiAdscritas = str_replace($rep, '', $instiAdscritas);
    $instPadre = $db->conn->qstr(limpiar_sql(trim($_POST["instCodi_p"])));
    $instPadre = str_replace($rep, '', $instPadre);
    $accion = $_POST["accion"];
    $instMatriz = $db->conn->qstr(limpiar_sql(trim($_POST["instMatriz"])));
    $instMatriz = str_replace($rep, '', $instMatriz);
    $recPadre=array();
    $recPadre["FECHA_REGISTRO"]=$db->conn->sysTimeStamp;
    if ($instPadre!="" and $instiAdscritas!=""){//si esta bien los post            
        if ($accion==2){//eliminar
                if ($instMatriz==0){
                    //si se elimina en desorden
                    $sql="select * from institucion_org 
                    where inst_codi = $instPadre and org_id = $instiAdscritas";                        
                    $rs=$db->conn->query($sql);
                    if ($rs->fields["FECHA_REGISTRO"]=='')
                        $sqlE="delete from institucion_org where org_id_padre = $instPadre and org_id=$instiAdscritas";
                    else
                        $sqlE="delete from institucion_org  where inst_codi = $instPadre and org_id=$instiAdscritas";
                    $db->conn->query($sqlE);
                }else{
                    $sql="delete from institucion_org 
                    where inst_codi = $instMatriz and org_id = $instiAdscritas";                    
                    $db->conn->query($sql);
                }
              $mensajeOrg="Se elimino correctamente la Institucion";
        }//accion
        else{//guardar
            if ($instMatriz!=0){//guarda en nivel 2
                $recPadre['INST_CODI']=$instMatriz;
                $recPadre["ORG_ID_PADRE"]=$instPadre;
                $recPadre["DEPE_CODI"]=encontrarDependencia($db,$instMatriz);
            }
            else{//guarda nivel 1
                 $recPadre['INST_CODI']=$instPadre;
                 $recPadre["DEPE_CODI"]=encontrarDependencia($db,$instPadre);
            }
            
                 //hijas
                 $instHijas = split(",",$instiAdscritas);         
                 for($i=1;$i<sizeof($instHijas);$i++)//instituciones seleccionadas
                 {//for
                     $recPadre["ORG_ID"]=$instHijas[$i];
                     $verSql="select * from institucion_org where org_id=$instHijas[$i] and org_id_padre=$instPadre";
                     $rsVer=$db->conn->query($verSql);
                      if ($rsVer->fields["FECHA_REGISTRO"]=='')
                         //Grabar                         
                         $ok1 = $db->conn->Replace("INSTITUCION_ORG", $recPadre, "", false,false,true,false);
                 }//fin for
                  $mensajeOrg="Se ha guardado Exitosamente Los cambios";
                  
            
        }  
        //matriz origen
    }else//si esta bien los post
    $mensajeOrg="Existío un Problema al Grabar la Institución, Favor comuníquese con el Administrador del Sistema";
    echo "<font color='blue'><center>$mensajeOrg</center></font>";
    
}//fin isset
function encontrarDependencia($db,$instCodi){
    $sql="select min (depe_codi) as depe_min from dependencia where inst_codi = $instCodi";
    $rs=$db->conn->query($sql);
    $dependencia=$rs->fields["DEPE_MIN"];
    return $dependencia;
}
//Guarda nivel 1, quiere decir que guarda solo org_id, inst_codi, fecha_registro y depe_codi
//Guarda nivel 2, quiere decir que guarda org_id, org_id_padre,inst_codi, fecha_registro y depe_codi
?>