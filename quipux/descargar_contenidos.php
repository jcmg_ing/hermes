<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/



session_start();
$ruta_raiz = ".";
include_once "$ruta_raiz/rec_session.php";


function get_mime_tipe($archivo) {

    $tmp = explode(".",$archivo);
    $ext = $tmp[count($tmp)-1];
    switch( $ext ) {
      case "pdf": return "application/pdf"; break;
      case "exe": return "application/octet-stream"; break;
      case "zip": return "application/zip"; break;
      case "doc": return "application/msword"; break;
      case "xls": return "application/vnd.ms-excel"; break;
      case "ppt": return "application/vnd.ms-powerpoint"; break;
      case "gif": return "image/gif"; break;
      case "png": return "image/png"; break;
      case "jpeg":
      case "jpg": return "image/jpg"; break;
      case "mp3": return "audio/mpeg"; break;
      case "wav": return "audio/x-wav"; break;
      case "mpeg":
      case "mpg":
      case "mpe": return "video/mpeg"; break;
      case "mov": return "video/quicktime"; break;
      case "avi": return "video/x-msvideo"; break;
      case "odt": return "application/vnd.oasis.opendocument.text"; break;
      case "php":
      case "htm":
      case "html":
      case "txt": return "text/plain"; break;

      default: return "application/force-download";
    }

}

/**
* Permite descargar los archivos en formato .pdf
**/

$path_arch = $_GET["archivo"];

header("Content-type: application/octet-stream");
    get_mime_tipe($archivo);
    header("Content-Disposition: attachment; filename=\"$path_arch\"\n");
    $fp=fopen("$path_arch", "r");
    fpassthru($fp);



?>
