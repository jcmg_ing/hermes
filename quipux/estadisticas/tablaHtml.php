<?php 
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

$ruta_raiz = "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

require_once($ruta_raiz."/include/myPaginador.inc.php");
  	$paginador=new myPaginador($db,($queryE),$orden);

if(!isset($_GET['genDetalle'])){
        	$orden=isset($orden)?$orden:"";
        	$paginador->setFuncionFilas("pintarEstadistica");
		 } else {
        	$paginador->setFuncionFilas("pintarEstadisticaDetalle");
        }
$paginador->setImagenASC($ruta_raiz."iconos/flechaasc.gif");
$paginador->setImagenDESC($ruta_raiz."iconos/flechadesc.gif");
        	//$paginador->setPie($pie);
echo $paginador->generarPagina($titulos,"titulos3");


if(!isset($_GET['genDetalle'])&& $paginador->getTotal() > 0){
    $total=$paginador->getId()."_total";
	if(!isset($_REQUEST[$total])) {
		$res=$db->query($queryE);
		$datos=0;
		while(!$res->EOF){
			$data1y[]=$res->fields[1];
              		$nombUs[]=$res->fields[0];
            		$res->MoveNext();
		}
		$nombYAxis=substr($titulos[1],strpos($titulos[1],"#")+1);
		$nombXAxis=substr($titulos[2],strpos($titulos[2],"#")+1);
		$nombreGraficaTmp = $ruta_raiz."bodega/tmp/E_$krd.png";
		$rutaImagen = $nombreGraficaTmp;
		if(file_exists($rutaImagen)){
			unlink($rutaImagen);
		}
		$notaSubtitulo = $subtituloE[$tipoEstadistica]."\n";
		$tituloGraph = $tituloE[$tipoEstadistica];
		include "genBarras1.php";
	}

//echo "dependencia".$dependencia_busq;


?>
<script>
function imprimir(){
//	document.getElementById("imprimir").style.display='none';
	window.open ("../reportes/reporte_consulta1.php");
}
function imprimir2() {
    window.open ("../reporte_consulta2.php");
}
function imprimir3(){
    window.open ("../reporte_consulta3.php");
}
function imprimir4(){
//	document.getElementById("imprimir").style.display='none';
	window.open ("../reporte_consulta4.php");
}
function imprimir5(){
    window.open ("../reporte_consulta5.php");
}
function imprimir6(){
    window.open ("../reportes/reporte_consulta6.php");
}
function imprimirDemora(){
    //alert ("aqui");
    window.open ("../reportes/reporte_TiempoDemoraGral.php");
}
function imprimir30(){
    window.open ("../reportes/reporte_documentoExterno.php");
}
function imprimir31() {
    //alert ("ingreso");
    window.open ("../reportes/reporte_documentoEnviado.php");
}
function imprimir32(){
    window.open ("../reportes/reporte_docBandejaImpresion.php");
}
function imprimirDemoraTodos(){
    window.open ("../reportes/reporte_TiempoDemoraGralTodos.php");
}
function imprimir34(){
    window.open ("../reportes/reporte_documentoExternoCiudadano.php");
}
function imprimir35(){
    window.open ("../reportes/reporte_docBandejaImpresionTodos.php");
}
function imprimir36(){
    window.open ("../reportes/reporte_retrasosReasignados.php");
}
function imprimir37(){
    window.open ("../reportes/reporte_docsNoDigitalizados.php");
}
</script>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="listado2">
<center>
    <tr align="center">
    	
	<td align="center">
			<?if($tipoEstadistica==1 && $dependencia_busq != 99999){?>
			<input type=button class="botones_largo" id="imprimir" value="imprimir" onClick="imprimir()"/>
			<?}elseif($tipoEstadistica==2){?>
			<input type=button class="botones_largo" id="imprimir2" value="imprimir" onClick="imprimir2()"/>
			<?}elseif($tipoEstadistica==3){?>
			<input type=button class="botones_largo" id="imprimir3" value="imprimir" onClick="imprimir3()"/>
			<?}elseif($tipoEstadistica==4){?>
			<input type=button class="botones_largo" id="imprimir4" value="imprimir" onClick="imprimir4()"/>
			<?}elseif($tipoEstadistica==5){?>
			<input type=button class="botones_largo" id="imprimir5" value="imprimir" onClick="imprimir5()"/>
			<?}elseif($tipoEstadistica==1 && $dependencia_busq == '99999'){?>
			<input type=button class="botones_largo" align="center" id="imprimir6" value="imprimir" onClick="imprimir6()"/>
            <?}elseif($tipoEstadistica==15 AND $dependencia_busq !='99999'){?>
			<input type=button class="botones_largo" align="center" id="imprimir15" value="imprimir" onClick="imprimirDemora()"/>
            <?}elseif($tipoEstadistica==16 AND $_POST["genero"]=="F"){?>
			<input type=button class="botones_largo" align="center" id="imprimir16" value="imprimir" onClick="imprimir30()"/>
            <?}elseif($tipoEstadistica==17){?>
			<input type="button" class="botones_largo" align="center" id="imprimir17" value="imprimir" onClick="imprimir31();"/>
            <?}elseif($tipoEstadistica==18 AND $dependencia_busq!='99999'){?>
			<input type=button class="botones_largo" align="center" id="imprimir18" value="imprimir" onClick="imprimir32()"/>
            <?}elseif($tipoEstadistica==15 AND $dependencia_busq=='99999'){?>
			<input type=button class="botones_largo" align="center" id="imprimir19" value="imprimir" onClick="imprimirDemoraTodos()"/>
            <?}elseif($tipoEstadistica==16 AND $_POST["genero"]=="C"){?>
			<input type=button class="botones_largo" align="center" id="imprimir120" value="imprimir" onClick="imprimir34()"/>
            <?}elseif($tipoEstadistica==18 AND $dependencia_busq=='99999'){?>
			<input type=button class="botones_largo" align="center" id="imprimir21" value="imprimir" onClick="imprimir35()"/>
            <?}elseif($tipoEstadistica==19 ){?>
			<input type=button class="botones_largo" align="center" id="imprimir22" value="imprimir" onClick="imprimir36()"/>
            <?}elseif($tipoEstadistica==20 ){?>
			<input type=button class="botones_largo" align="center" id="imprimir23" value="imprimir" onClick="imprimir37()"/>
            <?}?>
	</td>
     </tr>
     </center>
     </table>
<?
}
?>



</body>
</html>
