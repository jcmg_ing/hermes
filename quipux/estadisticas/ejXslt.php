<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
$ruta_raiz = "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

include "pagVariables.php";
$proc = new XSLTProcessor;
if (!$proc->hasExsltSupport()) {
   die('EXSLT support not available');
}

error_reporting(7);
// Load the XML source
$xml = new DOMDocument;
$xml->load('usOrfeo.xml');

$xsl = new DOMDocument;
$xsl->load('jh.xsl'); 

// Configure the transformer
if(!$cOrden or !$cOrdenType) 
{
	$cOrden="DEPE_CODI";
	$cOrdenDType="number";
	$cOrdenType="descending";
}
if($cOrdenType=="descending")
{
	$cOrdenType = "ascending";
}else
{
	$cOrdenType = "descending";
}
$proc = new XSLTProcessor;
$proc->setParameter('','cOrdenDType',$cOrdenDType);
$proc->setParameter('','cOrdenType',$cOrdenType);
$proc->setParameter('','cOrden',$cOrden);
$proc->setParameter('','pos1',0);
$proc->setParameter('','pos1',1);
$proc->importStyleSheet($xsl); 
$proc->setParameter('','varDependencia','529');
$proc->setParameter('','paginaActual',$PHP_SELF);
$proc->transformToURI($xml, 'out.html'); 
include "out.html";
?> 
