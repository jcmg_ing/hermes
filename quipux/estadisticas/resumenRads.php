<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

$ruta_raiz = "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

if (!$ruta_raiz)
	$ruta_raiz = "..";
?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="<?=$ruta_raiz?>/js/spiffyCal/spiffyCal_v2_1.css">
<script language="JavaScript" src="<?=$ruta_raiz?>/js/spiffyCal/spiffyCal_v2_1.js"></script>
<link href="../estilos_totales.css" rel="stylesheet" type="text/css">
<div id="spiffycalendar" class="text"></div>
</head>
<body bgcolor="#FFFFFF" text="#000000"> 
<p>
  <script language="javascript">
		var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "busqueda","fechaIni","btnDate1","",scBTNMODE_CUSTOMBLUE);
	 	dateAvailable.date = "2003-08-05";
		dateAvailable.dateFormat="yyyy-MM-dd";
		
		var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "busqueda","fechaFin","btnDate2","",scBTNMODE_CUSTOMBLUE);
	 	dateAvailable2.date = "2003-08-05";
		dateAvailable2.dateFormat="yyyy-MM-dd";
	</script>
</p>
<form name="busqueda" method="post" action="resultResumenRads.php">
<table width="70%" border="0" cellspacing="1" cellpadding="1" class="t_bordeGris">
<tr> 
    <td width="29%" height="15"  align="right" class="grisCCCCCC" > Rango de Fechas 
      de Radicaci&oacute;n<strong><font size="-1" face="Arial, Helvetica, sans-serif" class="style4"> 
      </font></strong></td>
    <td width="19%" height="15"  align="left" class="grisCCCCCC style1" ><font face="Arial, Helvetica, sans-serif" class="etextomenu"> 
      <font size="-1"> 
      <script language="javascript">
	dateAvailable.writeControl();
	dateAvailable.dateFormat="yyyy/MM/dd";
	</script>
      </font></font></td>
    <td height="15" colspan="3" align="left" class="grisCCCCCC style1" > <font face="Arial, Helvetica, sans-serif" class="etextomenu"> 
      <font size="-1"> 
      <script language="javascript">
	dateAvailable2.writeControl();
	dateAvailable2.dateFormat="yyyy/MM/dd";
	</script>
      </font></font></td>
  </tr>
  <tr align="center"> 
    <td height="30" colspan="5" class="grisCCCCCC style1"> 
      <input name="Submit"  type="Submit" class="ebuttons2" value="BUSCAR"  >
     
    </td>
  </tr>
 
  <? 
	//echo"<input type='hidden' name='numdoc' value='$numdoc'>";
	$pnom=$pnomb;
    echo "";
?>
</table>

</form>
<p>&nbsp; </p>
<p>&nbsp;</p>
</body>
</html>
