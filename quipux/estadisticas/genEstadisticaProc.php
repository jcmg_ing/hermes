<? 
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
$ruta_raiz = "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

if(!$db)
{
	
$krdOld = $krd;
$carpetaOld = $carpeta;
$tipoCarpOld = $tipo_carp;
if(!$tipoCarpOld) $tipoCarpOld= $tipo_carpt;
session_start();
if(!$krd) $krd=$krdOsld;
$ruta_raiz = "..";
include "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

require_once("$ruta_raiz/class_control/Mensaje.php");
include("$ruta_raiz/class_control/usuario.php");


//$db->conn->debug = true;
$objUsuario = new Usuario($db);
error_reporting(7);
?>
<br />
<?php 
if( isset($_GET['genDetalle']) && $_GET['dendetalle']=1){
	$tipo_carp=false;
	$usuaDocProc=trim($_GET['usuaDocProc']);
	?>
<html>
<head>
<title>Detalle de las Est&aacute;disticas</title>
<link rel="stylesheet" href="<?=$ruta_raiz?>/estilos/orfeo.css" type="text/css">
</head>
<?php	
}
require_once("$ruta_raiz/envios/paEncabeza.php");

$datosaenviar = "fechaf=$fechaf&genDetalle=$genDetalle&tipoEstadistica=$tipoEstadistica&codus=$codus&krd=$krd&dependencia_busq=$dependencia_busq&ruta_raiz=$ruta_raiz&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&tipoRadicado=$tipoRadicado&tipoDocumento=$tipoDocumento&codUs=$codUs&fecSel=$fecSel&codProc="; 
}
	$where=($_REQUEST['codProceso']=="0") ?"":" AND sExp.SGD_FEXP_CODIGO <> 0 ";
	print $_POST['codProceso'];
	$wheretapaProc=isset($_GET['etapaProc'])?" AND sExp.SGD_FEXP_CODIGO =".$_GET['etapaProc']:"";
	
	if(isset($_REQUEST['codus']) && $_REQUEST['codus']!="0"){
		$whereTipoRadicado.=" AND b.USUA_CODI = ".$_REQUEST['codus'];
	}elseif((!isset($_REQUEST['codus']) && $_REQUEST['codus']!="0") && $usua_perm_estadistica<1){
	    $whereTipoRadicado.=" AND b.USUA_CODI = ".$_SESSION['codusuario'];
	}
	
	if($tipoDocumento and ($_REQUEST['tipoDocumento']!='9999' and $_REQUEST['tipoDocumento']!='9998')){
		$whereTipoRadicado.=" AND t.SGD_TPR_CODIGO = ".$_REQUEST['tipoDocumento'];
	}
	$whereProc=isset($_REQUEST['codProceso']) && $_REQUEST['codProceso'] !="" ?" AND sExp.SGD_PEXP_CODIGO=".$_REQUEST['codProceso']." ":"";
	
	$whereAnoExp =(isset($_REQUEST['codAno']) && $_REQUEST['codAno']!='0')?" AND sExp.SGD_SEXP_ANO = ".$_REQUEST['codAno'] :"";
	$whereDependencia = " AND sExp.DEPE_CODI=".$_REQUEST['dependencia_busq'];
	$whereDependencia.=$where.$whereProc;
	$whereEstadoProc.=$whereProc.$wheretapaProc.$where;
	
	
	
	
switch($tipoEstadistica){
	case "1":
	include "$ruta_raiz/include/query/estadisticas/consultaProc001.php";
	$generar = "ok";
	break;
	case "2":
	include "$ruta_raiz/include/query/estadisticas/consultaProc002.php";
	$generar = "ok";
	break;
}
if($generar == "ok") {
	require_once($ruta_raiz."/include/myPaginador.inc.php");
	//$db->conn->debug=true;
	if($genDetalle==1) $queryE = $queryEDetalle;
	if($genTodosDetalle==1) $queryE = $queryETodosDetalle;
	if ($tipoEstadistica==2) include ("./tablaProcHtml42.php");
    else include ("./tablaProcHtml.php");
 }
?>
