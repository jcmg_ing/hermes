<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
// A medium complex example of JpGraph
// Note: You can create a graph in far fewwr lines of code if you are
// willing to go with the defaults. This is an illustrative example of
// some of the capabilities of JpGraph.

$ruta_raiz = "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

$rutaJpGraph = "$ruta_raiz/jpgraph/src";
error_reporting(7);
include ("$rutaJpGraph/jpgraph.php");
include ("$rutaJpGraph/jpgraph_bar.php");
include ("$rutaJpGraph/jpgraph_line.php");

// Some data
//$databary=array(12,7,16,5,7,14,9,3);
$databary = $data1y;

// New graph with a drop shadow
$graph = new Graph(640,750,'auto');
$graph->SetShadow();

// Use a "text" X-scale
$graph->SetScale("textlin");
$graph->xaxis->SetTickLabels($nombUs);

// Set title and subtitle
$graph->title->Set("Elementary barplot with a text scale");

// Use built in font
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetAngle(90);
$graph->yaxis->SetTitleSide(SIDE_RIGHT);
// Create the bar plot
$b1 = new BarPlot($databary);
$b1->SetLegend("Temperature");
//$b1->SetAbsWidth(6);
//$b1->SetShadow();

// The order the plots are added determines who's ontop
$graph->Add($b1);

// Finally output the  image
$graph->Stroke($nombreGraficaTmp);

?>





