<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

?>
<html>

<head>
<title></title>
<link rel="stylesheet" href="<?=$ruta_raiz?>/estilos/orfeo.css">
<link href="<?=$ruta_raiz?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?=$ruta_raiz?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?=$ruta_raiz?>/estilos/template_css.css" rel="stylesheet" type="text/css">
<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
</head>
<body>
<br /><br />
<?




$dependencia_busqR=$_SESSION['uno'];

$ruta_raiz = "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

require_once($ruta_raiz."/include/myPaginador.inc.php");
$paginador=new myPaginador($db,($isql),$orden);

if(isset($_GET['genDetalle'])){
        	$orden=isset($orden)?$orden:"";

        	$paginador->setFuncionFilas("pintarEstadistica");

		 } else {
        	$paginador->setFuncionFilas("pintarEstadisticaDetalle");

        }
$paginador->setImagenASC($ruta_raiz."iconos/flechaasc.gif");
$paginador->setImagenDESC($ruta_raiz."iconos/flechadesc.gif");

echo $paginador->generarPagina($titulos,"titulos3");


if(isset($_GET['genDetalle'])&& $paginador->getTotal() > 0){
    $total=$paginador->getId()."_total";
	if(isset($_REQUEST[$total])) {
	    $res=$db->query($isql);
		$datos=0;
		while(!$res->EOF){
	        $data1y[]=$res->fields[1];
            $nombUs[]=$res->fields[0];
            $res->MoveNext();
                    //echo "desde aqui==>".$res->fields[0];
		}
		$nombYAxis=substr($titulos[1],strpos($titulos[1],"#")+1);
		$nombXAxis=substr($titulos[2],strpos($titulos[2],"#")+1);
		$nombreGraficaTmp = $ruta_raiz."bodega/tmp/E_$krd.png";
		$rutaImagen = $nombreGraficaTmp;
		if(file_exists($rutaImagen)){
    		unlink($rutaImagen);
		}
		$notaSubtitulo = $subtituloE[$tipoEstadistica]."\n";
		$tituloGraph = $tituloE[$tipoEstadistica];
		include "genBarras1.php";
	}


//Elimino Tabla Temporal
$tabla=$_SESSION['tabla'];
$sqlTbl="drop table ". $tabla;
$queryTbl= $db->query($sqlTbl);


?>

    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="listado2">
    <center>
    <tr align="center">
    <td align="center">
			<?if($tipoEstadistica==15 ){?>
			<input type=button class="botones_largo"  id="imprimir" value="imprimir" onClick="imprimir1()"/>
			<?}?>
	</td>
     </tr>
     </center>
     </table>
<?
}
?>

</body>
</html>

<script language="Javascript">
function imprimir1(){
   window.open ("../reportes/generar_reporte_tiempoDemora_RutaDoc.php");

}
</script>



