<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */ 
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                      */
/*   Hollman Ladino       hladino@gmail.com                Desarrollador             */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*************************************************************************************/



$ruta_raiz = "..";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post

p_register_globals(array());


$krdOld = $krd;
$carpetaOld = $carpeta;
$tipoCarpOld = $tipo_carp;
if(!$tipoCarpOld) $tipoCarpOld= $tipo_carpt;
session_start();
if(!$krd) $krd=$krdOsld;
include "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

if(!$tipoEstadistica) $tipoEstadistica =1;
if(!$dependencia_busq) $dependencia_busq =$dependencia;
$_SESSION["DEPE_CODI"]=$dependencia;
//echo "depe2".$dependencia;
//$_SESSION["DEPE_CODI_ANTE"]=$dependencia;
//echo "CODI".$_SESSION["usua_codi"];

//DEFINE INSTITUCION LOGEADA
$sql="select inst_nombre from DATOS_USUARIOS where usua_codi=".$_SESSION["usua_codi"];
$rs=$db->conn->query($sql);
while(!$rs->EOF){
    $InstLogeada = $rs->fields["INST_NOMBRE"];
    $rs->MoveNext();
}
$sql="";                

//echo "usuario".$_SESSION["usua_codi"];



/** DEFINICION DE VARIABLES ESTADISTICA
	*	var $tituloE String array  Almacena el titulo de la Estadistica Actual
	* var $subtituloE String array  Contiene el subtitulo de la estadistica
	* var $helpE String Almacena array Almacena la descripcion de la Estadistica.
	*/
	$tituloE[1] = "Consulta de registros por estado del documento";
	//$tituloE[2] = "Consulta de registros por tiempo de demora";
	//$tituloE[3] = "ESTADISTICAS DE MEDIO ENVIO FINAL DE DOCUMENTOS";
	$tituloE[4] = "Estadísticas de digitalización de documentos";
	/*$tituloE[5] = "REGISTROS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA";
	$tituloE[6] = "REGISTROS ACTUALES EN EL AREA";
	//$tituloE[7] = "ESTADISTICAS DE NUMERO DE DOCUMENTOS ENVIADOS";	  	  
	$tituloE[8] = "REPORTE DE VENCIMIENTOS";	  	  
	$tituloE[9] = "REPORTE PROCESO REGISTROS DE ENTRADA";
	$tituloE[10] = "REPORTE ASIGNACION REGISTROS";
	$tituloE[11] = "ESTADISTICAS DE DIGITALIZACION";
	//$tituloE[12] = "DOCUMENTOS RETIPIFICADOS POR TRD";
	$tituloE[13] = "EXPEDIENTES POR DEPENDENCIA";
	$tituloE[14] = "REPORTE DE REGISTROS ASIGNADOS DETALLADOS (CARPETAS PERSONALES)";*/
    $tituloE[15] = "Consulta de registros por tiempo de demora";


    $sql="SELECT A.ID_PERMISO FROM PERMISO_USUARIO A , PERMISO B
    WHERE A.ID_PERMISO=B.ID_PERMISO AND A.ID_PERMISO=8  AND B.ESTADO=1 AND A.USUA_CODI=".$_SESSION["usua_codi"];

    $rs1=$db->query($sql);
    while(!$rs1->EOF){
        $tituloE[16] = "Consulta de registros Externos que fueron Ingresados";
        $tituloE[17] = "Consulta de registros Enviados";
        $tituloE[18] = "Consulta de documentos pendientes de Firmar";

        $helpE[16] = "Este reporte genera la cantidad de registros de Tipo Externo que fueron Ingresados." ;
        $helpE[17] = "Este reporte genera la cantidad de registros firmados Manualmente y Electrónicamente." ;
        $helpE[18] = "Este reporte genera la cantidad de registros que se encuentran en la bandeja Por Imprimir y no han sido leidos." ;
        $codigo = $rs1->fields["ID_PERMISO"];
        $rs1->MoveNext();
    };
    $tituloE[19] = "Consulta de retrasos en documentos reasignados";
    $tituloE[20] = "Consulta de documentos sin documentos asociados";

   

	/*$subtituloE[1] = "ORFEO - Generada el: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[2] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[3] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[4] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[5] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[6] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";
	$subtituloE[8] = "ORFEO - Fecha: " . date("Y/m/d H:i:s"). "\n Parametros de Fecha: Entre $fecha_ini y $fecha_fin";  */
	
	$helpE[1] = "Este reporte genera la cantidad de registros por estado del documento." ;
	//$helpE[2] = "Este reporte genera el tiempo de demora de los registros desde su fecha de registro hasta su fecha de envío o finiquito del proceso." ;
	//$helpE[3] = "Este reporte genera la cantidad de registros enviados a su destino final por el &aacute;rea.  " ;
	$helpE[4] = "Este reporte genera la cantidad de registros digitalizados por usuario. Se puede seleccionar el tipo de ingreso." ;
	/*$helpE[5] = "Este reporte genera la cantidad de documentos de entrada registros del &aacute;rea de correspondencia a una dependencia. " ;
	$helpE[6] = "Esta estadistica trae la cantidad de registros \n generados por usuario, se puede discriminar por tipo de Ingreso. " ;
	$helpE[8] = "Este reporte genera la cantidad de registros de entrada cuyo vencimiento esta dentro de las fechas seleccionadas. " ;
	$helpE[9] = "Este reporte muestra el proceso que han tenido los registros tipo 2 que ingresaron durante las fechas seleccionadas. ";
	$helpE[10] = "Este reporte muestra cuantos registros de entrada han sido asignados a cada dependencia. ";
	$helpE[11] = "Muestra la cantidad de registros digitalizados por usuario y el total de hojas digitalizadas. Se puede seleccionar el tipo de ingreso y la fecha de digitalizaci&oacute;n." ;
	//$helpE[12] = "Muestra los registros que ten&iacute;an asignados un tipo documental(TRD) y han sido modificados";
	$helpE[13] = "Muestra todos los expedientes agrupados por dependencia que con el n&uacute;mero de registros totales";
	$helpE[14] = "Muestra el total de registros que tiene un usuario y 
				el detalle del registro con respecto al Remitente(Detalle), 
				Predio(Detalle), ESP(Detalle) ";*/
    $helpE[15] = "Este reporte genera el tiempo de demora de los registros desde su fecha de registro hasta su fecha máxima del trámite." ;
    $helpE[19] = "Este reporte genera el retraso de un documento reasignado desde la fecha máxima de trámite hasta la fecha actual" ;
    $helpE[20] = "Este reporte genera los documentos que  no tienen asociados el documento digitalizado" ;

?>	  
<html>


<head>
<title>principal</title>
<link rel="stylesheet" href="../estilos/orfeo.css">
<link rel="stylesheet" type="text/css" href="../js/spiffyCal/spiffyCal_v2_1.css">
<script>
function adicionarOp (forma,combo,desc,val,posicion){
	o = new Array;
	o[0]=new Option(desc,val);
	eval(forma.elements[combo].options[posicion]=o[0]);
    	//alert ("Adiciona " +val+"-"+desc );
}

</script>
		 <script language="JavaScript" src="../js/spiffyCal/spiffyCal_v2_1.js"></script>
		  
		 <script language="javascript">
		 <!--
			<?
				/*$ano_ini = date("Y");
				$mes_ini = substr("00".(date("m")-1),-2);
               if ($mes_ini==0) {$ano_ini==$ano_ini-1; $mes_ini="12";}
				$dia_ini = date("d");
				if(!$fecha_ini) $fecha_ini = "$ano_ini/$mes_ini/$dia_ini";
					$fecha_busq = date("Y/m/d") ;
				if(!$fecha_fin) $fecha_fin = $fecha_busq;*/

                $ano_ini = date("Y");
                $mes_ini = date("m");
                if($mes_ini == 1)
                {
                    $mes_ini = 12;
                    $ano_ini = $ano_ini - 1;
                }
                else
                    $mes_ini = $mes_ini - 1;
                $dia_ini = date("d");
                if(!$fecha_fin) $fecha_fin = date("Y/m/d");
                if(!$fecha_ini) $fecha_ini = date("Y/m/d", mktime(0, 0, 0, $mes_ini, $dia_ini, $ano_ini));

			?>








   var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "formulario", "fecha_ini","btnDate1","<?=$fecha_ini?>",scBTNMODE_CUSTOMBLUE);
   var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "formulario", "fecha_fin","btnDate2","<?=$fecha_fin?>",scBTNMODE_CUSTOMBLUE);

//--></script>
</head>
<?
//consulta para obtener el nombre de la  institucion.
if(isset($_SESSION['inst_codi'])){
    $queryR= "SELECT inst_nombre FROM institucion where inst_codi=".$_SESSION['inst_codi'];
    $rsE= $db->query($queryR);
    $_SESSION['$NombInsti']=$rsE->fields["INST_NOMBRE"];
    //echo  $_SESSION['$NombInsti'];
}


?>
<table><tr><TD></TD></tr></table>
<?
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include("$ruta_raiz/class_control/usuario.php");
//$db = new ConnectionHandler($ruta_raiz);
//$db->conn->debug=true;
//$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$objUsuario = new Usuario($db);
//comboUsuarioDependencia(document.formulario,document.formulario.elements['dependencia_busq'].value,'codus');

?>



<body bgcolor="#ffffff" onLoad="" topmargin="0">
<div id="spiffycalendar" class="text"></div>
<form name="formulario"  method=post action='./vistaFormConsulta.php?<?=session_name()."=".trim(session_id())."&krd=$krd&fechah=$fechah"?>'>

<table width="100%"  border="0" cellpadding="0" cellspacing="5" class="borde_tab">
  <tr>
    <td colspan="2" class="titulos4">Reportes sistema documental <!-- -  <A href='vistaFormProc.php?<?=session_name()."=".trim(session_id())."&krd=$krd&fechah=$fechah"?>' style="color: #FFFFCC">POR PROCESOS </A>--> </td>
  </tr>
  <tr>
    <td colspan="2" class="titulos3"><span class="cal-TextBox"><?=$helpE[$tipoEstadistica]?></span></td>
  </tr>
  <tr>
    <td width="30%" class="titulos2">Tipo de Consulta / Estadística</td>
    <td class="listado2" align="left">
	   <select name=tipoEstadistica  class="select" onChange="formulario.submit();">
		<?
			foreach($tituloE as $key=>$value)
			{
		?>
	   	<? if($tipoEstadistica==$key) $selectE = " selected "; else $selectE = ""; ?>
			<option value=<?=$key?> <?=$selectE?>><?=$tituloE[$key]?></option>
		<?
		}
		?>
		</select>
	</td>
	</tr>




<!--******CARGA INSTITUCIONES EN CASO DE SUPERUSUARIO-->

        <?
        /**
        * Si el usuario que ingresa al sistema es el usuario super-administrador cargar el combo con la lista de las
        * instituciones.
        **/
        //echo "usuario".$_SESSION["usua_codi"];
        if($tipoEstadistica==15 ){
          if($_SESSION["usua_codi"]==0) {
            if (isset($inst_actu)) {
                if ($inst_actu!=0) {
                    $inst_codi = $inst_actu;
                    $_SESSION["inst_codi"] = $inst_actu;
                }
            } else
            $inst_actu = $_SESSION["inst_codi"];

        ?>
        <form name="formulario" id="formulario" method="post">
            <td width="30%" class="titulos2">Institución</td>
            <td class="listado2">
            <!--<td align="center" class="listado2">-->
            <?
                //$sql = "select INST_NOMBRE, INST_CODI from institucion where inst_estado =1 order by inst_nombre";
                $sql = "select  a.INST_NOMBRE, a.INST_CODI,count(*) from institucion a,DEPENDENCIA b where a.inst_codi=b.inst_codi
                and a.inst_estado=1 and b.depe_estado=1 group by a.INST_NOMBRE, a.INST_CODI having count(*)>=1 order by a.inst_nombre";

                $rs=$db->conn->query($sql);
                echo $rs->GetMenu2("inst_actu", $inst_actu, "0:&lt;&lt seleccione &gt;&gt;", false,"","class='select' Onchange='document.formulario.submit()'");
                
                 //Vuelvo a  cargar el area, debido al cambio de institucion*/
                $isqlDep="select depe_codi from DEPENDENCIA a where  inst_codi=$inst_actu and depe_estado=1";
                $rs=$db->conn->query($isqlDep);
                while(!$rs->EOF){
                    $codigo = $rs->fields["DEPE_CODI"];
                    $rs->MoveNext();
                }

                $dependencia=$codigo;
                if(!$dependencia_busq) $dependencia_busq =$dependencia;
                //echo $dependencia;

            ?>

                </td>
        </form>


        <? }} ?>

<!--FIN CARGA INSTITUCIONES-->




<!--*******CARGA AREA EN CASO DE CARGAR OTRAS INSTITUCIONES PARA TIEMPOS DEMORA******-->
<?
//echo $usua_perm_estadistica;
 if($tipoEstadistica==15 and $_SESSION["usua_codi"]==0 ){
?>

	<tr>
    <td width="30%" class="titulos2">Area</td>
    <td class="listado2">

	<select name=area_demora    class="select"  onChange="formulario.submit();">

    <?

	if($usua_perm_estadistica>1)  {
		if(area_demora==99999)  {
			$datoss= "selected ";
		}
		?>
			<option value=99999  <?=$datoss?>>-- Todas las Areas --</option>
		<?
	}

    $whereAreaSelect="inst_codi=$inst_actu and depe_estado=1";//" DEPE_CODI = $dependencia ";

    $isqlus = "select a.DEPE_CODI,a.DEPE_NOMB,a.DEPE_CODI_PADRE from DEPENDENCIA a
                        where $whereAreaSelect order by a.DEPE_NOMB  ";

    $rs1=$db->query($isqlus);

    echo "<option value='99999'  >Todos</option>";
    do{
        $codigo = $rs1->fields["DEPE_CODI"];
        $vecDeps[]=$codigo;
        $depnombre = $rs1->fields["DEPE_NOMB"];
        $datoss="";
        if($area_demora==$codigo){
            $datoss= " selected ";
        }
        echo "<option value=$codigo  $datoss>$depnombre</option>";
        $rs1->MoveNext();
    }while(!$rs1->EOF);


    ?>
	</select>-
</td>
</tr>

<?

 }
?>
<!--*******FIN CARGA AREAS POR INSTITUCION*****-->



<!--*******CARGA USUARIO "DE" TIEMPO DEMORA SUPER USUARIO********-->
    <?if($_SESSION["usua_codi"]==0 and $tipoEstadistica==15 )
    {
    ?>
    <tr id="de">
        <td width="30%" class="titulos2">De
            <br />
        <?	$datoss = isset($usActivos) && ($usActivos) ?" checked ":"";	?>
        <!--<input name="usActivos" type="checkbox" class="select" <?=$datoss?> onChange="formulario.submit();">
        Incluir Usuarios Inactivos  </td>-->
        <td class="listado2">
        <select name="codus"  class="select"  onChange="formulario.submit();">

        <? 	if ($usua_perm_estadistica > 0) { ?>
                <option value=99999> -- Agrupar por todos los destinatarios --</option>
        <?	}

            $whereUsSelect = (!isset($_POST['usActivos']) )? " u.USUA_ESTA = 1 ":"";

            if (isset($area_demora) && ($area_demora!='99999')){
                $condAreaSU=" and depe_codi=$area_demora ";
            }

            $whereUsSelect=($usua_perm_estadistica < 1)?
                                                      (($whereUsSelect!="")?$whereUsSelect." AND u.USUA_LOGIN='$krd' ":" u.USUA_LOGIN='$krd' "):$whereUsSelect;
                        if($dependencia_busq != 99999)  {
                            $whereUsSelect=($whereUsSelect=="")? substr($whereDependencia,4):$whereUsSelect.$whereDependencia;
                            $isqlusDe = "select u.USUA_NOMB,u.USUA_CODI,u.USUA_ESTA, u.USUA_APELLIDO from USUARIOS u
                                       where  $whereUsSelect and inst_codi=$inst_actu  $condAreaSU
                                       order by u.USUA_NOMB";
                            //if($codusuario!=1) $isqlus .= " and a.usua_codi=$codusuario ";
                            //echo "--->".$isqlus;
                            $rs1=$db->query($isqlusDe);
                            while(!$rs1->EOF)  {
                                $codigo = $rs1->fields["USUA_CODI"];
                                $vecDeps[]=$codigo;
                                $usNombre = $rs1->fields["USUA_NOMB"].' '.$rs1->fields["USUA_APELLIDO"];
                                $datoss=($codus==$codigo)?$datoss= " selected ":"";
                                echo "<option value=$codigo  $datoss>$usNombre</option>";
                                $rs1->MoveNext();
                            }
            }
            ?>
            </select>
        &nbsp;</td>
      </tr>
      <?
      //echo $isqlusDe;
      //echo $condAreaSU;
      }
 //CIERRA BUCLE PARA



//*******CARGA USUARIO "PARA" TIEMPO DEMORA SUPER USUARIO*******
//if($_SESSION["usua_codi"]!=0){
    if( ($_SESSION["usua_codi"]==0) and $tipoEstadistica==15  ){

      ?>
         <tr>
            <td width="30%" class="titulos2">Para</td>
            <td class="listado2">
                <select name="para" id="para" class="select">
                    <?

                    if ($usua_perm_estadistica> 0){	?>
                        <option value=99999> -- Agrupar por todos los remitentes --</option>
                    <?	}
                    //echo '<option value="9999"'.$selected9.'> -- Agrupar por todos los remitentes -- </option>';

                    $whereUsSelect = (!isset($_POST['usActivos']) )? " u.USUA_ESTA = 1 ":"";

                    if (isset($area_demora) && ($area_demora!='99999')){
                        $condAreaSU=" and depe_codi=$area_demora ";
                    }

                    $whereUsSelect=($usua_perm_estadistica < 1)?
                        (($whereUsSelect!="")?$whereUsSelect." AND u.USUA_LOGIN='$krd' ":" u.USUA_LOGIN='$krd' "):$whereUsSelect;	  if($dependencia_busq != 99999)  {

                    $whereUsSelect=($whereUsSelect=="")? substr($whereDependencia,4):$whereUsSelect.$whereDependencia;


                    //Combo para generar tipos de documento.
                        $sql_tipo="select u.USUA_NOMB,u.USUA_CODI,u.USUA_ESTA, u.USUA_APELLIDO from USUARIOS u
                           where  $whereUsSelect $condAreaSU
                           order by u.USUA_NOMB";

                        $rsE = $db->query($sql_tipo);
                        while(!$rsE->EOF)  {
                            $codigo = $rsE->fields["USUA_CODI"];
                    $vecDeps[]=$codigo;
                    $usNombre = $rsE->fields["USUA_NOMB"].' '.$rsE->fields["USUA_APELLIDO"];
                    $datoss=($para==$codigo)?$datoss= " selected ":"";
                    echo "<option value=$codigo  $datoss>$usNombre</option>";
                            $rsE->MoveNext();
                        }
                     }
                    /*echo '<option value="1111"'.$selected1.'>Asignado</option>';// VEJM 06/09/2009(M)Tramitado
                    echo '<option value="11111"'.$selected10.'>Reasignado</option>';// VEJM 06/08/2009(N)
                    echo '<option value="8888"'.$selected8.'>Vencido</option>';*/
                    ?>
                </select>
            </td>
        </tr>
      <?
      }?>
<!--*******CIERRA BUCLE PARA******-->



<!--******CARGA INSTITUCION PARA DOCUMENTOS EXTERNOS, SU ETIQUETA ES "DE"*****-->
<?
        /**
        * Si el usuario que ingresa al sistema es el usuario super-administrador cargar el combo con la lista de las
        * instituciones.
        **/
    //echo $_SESSION["usua_codi"];
        if($tipoEstadistica==16 ){
          //if($_SESSION["usua_codi"]!=0) {
            if (isset($inst_actu)) {
                if ($inst_actu!=0) {
                    $inst_codi = $inst_actu;
                    $_SESSION["inst_codi"] = $inst_actu;
                }
            }
            else
            $inst_actu = $_SESSION["inst_codi"];

            //echo $inst_actu ;
            //$_SESSION["inst_codi_docExt"]=$inst_actu;

        ?>
        <form name="formulario" id="formulario" method="post">
            <td width="30%" class="titulos2">De (Institución - Funcionario):</td>
            <td class="listado2">
            <input type=radio name="genero" value="F" onclick="Obtener_val(this)" echo "checked"> Funcionario
            <input type="hidden" name="genciudadano" id="genciudadano" value="">
            <!--<td align="center" class="listado2">-->
            <?
                //$sql = "select INST_NOMBRE, INST_CODI from institucion where inst_estado =1 order by inst_nombre";
                $sql = "select  a.INST_NOMBRE, a.INST_CODI,count(*) from institucion a,DEPENDENCIA b where a.inst_codi=b.inst_codi
                and a.inst_estado=1 and b.depe_estado=1 group by a.INST_NOMBRE, a.INST_CODI having count(*)>=1 order by a.inst_nombre";

                $rs=$db->conn->query($sql);
                echo $rs->GetMenu2("inst_actu", $inst_actu, "0:&lt;&lt seleccione &gt;&gt;", false,"","class='select' Onchange='document.formulario.submit()'");
                //echo "instit".$inst_actu;

                 
                 //Vuelvo a  cargar el area, debido al cambio de institucion*/
                $isqlDep="select depe_codi from DEPENDENCIA a where  inst_codi=$inst_actu and depe_estado=1";
                $rs=$db->conn->query($isqlDep);
                while(!$rs->EOF){
                        $codigo = $rs->fields["DEPE_CODI"];
                        $rs->MoveNext();
                }

                $dependencia=$codigo;
                if(!$dependencia_busq) $dependencia_busq =$dependencia;
                $_SESSION['inst_codi_Externos']=$inst_actu;
                
            ?>

                </td>
        </form>


        <? } ?>

<!--*******FIN CARGA INSTITUCIONES PARA DOC EXTERNOS*******-->


<!--*****CARGA AREA DE LA INSTITUCION FUNCIONARIO PARA DOCS EXTERNOS*****-->

<?
//echo $usua_perm_estadistica;
 if($tipoEstadistica==16 ){

?>

	<tr>
    <td width="30%" class="titulos2">Area(Institución-Funcionario)</td>

    <td class="listado2">
	<select name=dependencia_busq_ext    class="select"  onChange="formulario.submit();">

    <?

	if($usua_perm_estadistica>1)  {
		if(dependencia_busq_ext==99999)  {
			$datoss= "selected ";
		}
		?>
			<option value=99999  <?=$datoss?>>-- Todas las Areas --</option>
		<?
	}

    $whereAreaSelect="inst_codi=$inst_actu and depe_estado=1";//" DEPE_CODI = $dependencia ";
    //echo "A<<".$whereDepSelect;


    /*if ($usua_perm_estadistica==1){
        if ($_SESSION["usua_codi"]!=0)  {
            $whereDepSelect=" $whereDepSelect or depe_codi_padre = $dependencia ";
        }

    }*/

    $isqlus = "select a.DEPE_CODI,a.DEPE_NOMB,a.DEPE_CODI_PADRE from DEPENDENCIA a
                        where $whereAreaSelect  ";

    $rs1=$db->query($isqlus);

    echo "<option value='99999'  >Todos</option>";
    do{
        $codigo = $rs1->fields["DEPE_CODI"];
        $vecDeps[]=$codigo;
        $depnombre = $rs1->fields["DEPE_NOMB"];
        $datoss="";
        if($dependencia_busq_ext==$codigo){
            $datoss= " selected ";
        }
        echo "<option value=$codigo  $datoss>$depnombre</option>";
        $rs1->MoveNext();
    }while(!$rs1->EOF);
    ?>
	</select>-
</td>
</tr>

<?
//echo $codigo;    //}
 }
?>
<!--*****FIN CARGA AREA INSTITUCIO FUNCIONARIO DOCS EXTERNOS*****-->



<!--***** CARGA INSTITUCION CIUDADANO PARA DOC. EXTERNOS *****-->

<?
         if($tipoEstadistica==16 ){
          //if($_SESSION["usua_codi"]!=0) {
            if (isset($inst_actu)) {
                if ($inst_actu!=0) {
                    $inst_codi = $inst_actu;
                    $_SESSION["inst_codi"] = $inst_actu;
                }
            }
            else
            //$inst_actu = $_SESSION["inst_codi"];

            //echo $inst_actu ;
            //$_SESSION["inst_codi_docExt"]=$inst_actu;

        ?>



        <tr>
		<td width="30%" class="titulos2">De(Institución - Ciudadano)</td>
		<td class="listado2">
            <input type=radio name="genero" value="C"  onclick="Obtener_val(this)" <? if($_POST['genero']=="C") echo 'checked'; ?>> Ciudadano
            <input type="hidden" name="genciudadano" id="genciudadano" value="">



			<select name="cmbCiudadano" id="cmbCiudadano" class="select" >
            <!--onfocus="javascript:this.blur(); return true;-->
				<?
				echo '<option value="99999"'.$selected10.'> << Agrupar por todos las Instituciones >> </option>';

				//Combo para generar tipos de documento.
					 $sql="select
                case
                when a.INST_NOMBRE<>'' then a.INST_NOMBRE   else 'Sin Institución'
                end as INST_NOMBRE, a.INST_CODI,'C' AS tipo_usu,1 as count
                from
                (select distinct INST_NOMBRE,INST_CODI FROM DATOS_USUARIOS where tipo_usuario=2 and inst_estado=1 order by inst_nombre) a";

					$rs = $db->query($sql);
					while(!$rs->EOF)  {
						$codigo = $rs->fields["INST_CODI"];
                        $vecDeps[]=$codigo;
                        $usNombre =$rs->fields["INST_NOMBRE"];
                        $datoss=($cmbCiudadano==$codigo)?$datoss= " selected ":"";
                        echo "<option value='$usNombre'  $datoss>$usNombre</option>";
                        $rs->MoveNext();
                        }
				?>
            </select>
		</td>
	</tr>


        <? }?>

<!--***** FIN CARGA INST CIUDADANO



<!--*******CARGA AREA EN INSTITUCION POR USUARIO LOGEADO*******-->
<?

//$tipoEstadistica==15;
 if( ($tipoEstadistica==1 or $tipoEstadistica==2  or $tipoEstadistica==15 or $tipoEstadistica==17  or $tipoEstadistica==18 or  $tipoEstadistica==16 or  $tipoEstadistica==20  )){
    //if($_SESSION["usua_codi"]!=0){
    $_SESSION['$condiArea']='';

?>

	<tr>
    
    <td width="30%" class="titulos2"><? 
                                                  if($tipoEstadistica==16){
                                                      echo "Para:";
                                                  }elseif($tipoEstadistica!=16  ){
                                                      echo "Areas de la ".$InstLogeada.":";
                                                  }
                                                  ?></td>
    <td class="listado2">
	<select name=dependencia_busq  class="select"  onChange="formulario.submit();">

    <?

	if($usua_perm_estadistica>1)  {
		if($dependencia_busq==99999)  {
			$datoss= " selected ";
		}
		?>
			<option value=99999  <?=$datoss?>>-- Todas las Areas --</option>
		<?
	}
    //Consulta la dependencia en el caso de haber cambiado el area del usuario actual
    $isqlUsu="select depe_codi from usuarios where usua_codi=".$_SESSION["usua_codi"];
    $rs1=$db->query($isqlUsu);

    while(!$rs1->EOF)  {
        $codigo = $rs1->fields["DEPE_CODI"];
        $rs1->MoveNext();
    }

    $dependencia=$codigo;
    $whereDepSelect=" DEPE_CODI = $dependencia ";
    //echo "A<<".$whereDepSelect;


    if ($usua_perm_estadistica==1){
        if ($_SESSION["usua_codi"]!=0)  {
            $whereDepSelect=" $whereDepSelect or depe_codi_padre = $dependencia ";
        }
        
    }

        $isqlus = "select a.DEPE_CODI,a.DEPE_NOMB,a.DEPE_CODI_PADRE from DEPENDENCIA a
                            where $whereDepSelect  ";
    
	//if($codusuario!=1) $isqlus .= " and a.usua_codi=$codusuario "; 

    $rs1=$db->query($isqlus);

    echo "<option value='99999'  >Todos</option>";
    do{
        $codigo = $rs1->fields["DEPE_CODI"];
        $vecDeps[]=$codigo;
        $depnombre = $rs1->fields["DEPE_NOMB"];
        $_SESSION['$condiArea'].=$rs1->fields["DEPE_CODI"].",";
        $datoss="";
        if($dependencia_busq==$codigo){
            $datoss= " selected ";
        }
        echo "<option value=$codigo  $datoss>$depnombre</option>";
        $rs1->MoveNext();
    }while(!$rs1->EOF);
    ?>
	</select>
</td>
</tr>

<?
//echo  $isqlUsu;    //}
 }
?>
<!--CIERRA BUCLE AREA-->



<!--*******CARGA USUARIO ********-->
<?

    if ($dependencia_busq != 99999)  {
        $whereDependencia = " AND DEPE_CODI=$dependencia_busq ";
    }

if($tipoEstadistica==1  or $tipoEstadistica==3 or
	$tipoEstadistica==4 or $tipoEstadistica==5 or $tipoEstadistica==6 or
	$tipoEstadistica==7 or $tipoEstadistica==11 or $tipoEstadistica==12
    )
{
?>
<tr id="cUsuario">
	<td width="30%" class="titulos2">Usuario
		<br />
	<?	$datoss = isset($usActivos) && ($usActivos) ?" checked ":"";	?>
	<!--<input name="usActivos" type="checkbox" class="select" <?=$datoss?> onChange="formulario.submit();">
	Incluir Usuarios Inactivos  </td>-->
	<td class="listado2">
	<select name="codus"  class="select"  onChange="formulario.submit();">
	<? 	if ($usua_perm_estadistica > 0){	?>
			<option value=99999> -- Agrupar por todos los usuarios --</option>
	<?	}
		$whereUsSelect = (!isset($_POST['usActivos']) )? " u.USUA_ESTA = 1 ":"";
		$whereUsSelect=($usua_perm_estadistica < 1)?
					(($whereUsSelect!="")?$whereUsSelect." AND u.USUA_LOGIN='$krd' ":" u.USUA_LOGIN='$krd' "):$whereUsSelect;	  
                    if($dependencia_busq != 99999)  {
                        $whereUsSelect=($whereUsSelect=="")? substr($whereDependencia,4):$whereUsSelect.$whereDependencia;
                        $isqlus = "select u.USUA_NOMB,u.USUA_CODI,u.USUA_ESTA, u.USUA_APELLIDO from USUARIOS u
                                   where  $whereUsSelect
                                   order by u.USUA_NOMB";
                        //if($codusuario!=1) $isqlus .= " and a.usua_codi=$codusuario ";
                        //echo "--->".$isqlus;
                        $rs1=$db->query($isqlus);
                        while(!$rs1->EOF)  {
                            $codigo = $rs1->fields["USUA_CODI"];
                            $vecDeps[]=$codigo;
                            $usNombre = $rs1->fields["USUA_NOMB"].' '.$rs1->fields["USUA_APELLIDO"];
                            $datoss=($codus==$codigo)?$datoss= " selected ":"";
                            echo "<option value=$codigo  $datoss>$usNombre</option>";
                            $rs1->MoveNext();
                        }
                        }
		?>
		</select>
	&nbsp;</td>
  </tr>
  <?
  //echo "--->".$isqlus;
  }//cIERRA BUCLE USUARIO



//*******CARGA USUARIO "DE"********
 //if($_SESSION["usua_codi"]!=0){
    if(($_SESSION["usua_codi"]!=0) and ($tipoEstadistica==15 or $tipoEstadistica==17 ))
    {
    ?>
    <tr id="de">
        <td width="30%" class="titulos2">De
            <br />
        <?	$datoss = isset($usActivos) && ($usActivos) ?" checked ":"";	?>
        <!--<input name="usActivos" type="checkbox" class="select" <?=$datoss?> onChange="formulario.submit();">
        Incluir Usuarios Inactivos  </td>-->
        <td class="listado2">
        <select name="codus"  class="select"  onChange="formulario.submit();">
        <? 	if ($usua_perm_estadistica > 0 and $tipoEstadistica==15){	?>
                <option value=99999> -- Agrupar por todos los destinatarios --</option>
        <? 	}else{	?>
                <option value=99999> -- Agrupar por todos los remitentes --</option>
        <?	}
            $whereUsSelect = (!isset($_POST['usActivos']) )? " u.USUA_ESTA = 1 ":"";
            $whereUsSelect=($usua_perm_estadistica < 1)?
                        (($whereUsSelect!="")?$whereUsSelect." AND u.USUA_LOGIN='$krd' ":" u.USUA_LOGIN='$krd' "):$whereUsSelect;	  if($dependencia_busq != 99999)  {

                $whereUsSelect=($whereUsSelect=="")? substr($whereDependencia,4):$whereUsSelect.$whereDependencia;
                $isqlusDe = "select u.USUA_NOMB,u.USUA_CODI,u.USUA_ESTA, u.USUA_APELLIDO from USUARIOS u
                           where  $whereUsSelect
                           order by u.USUA_NOMB";
                //if($codusuario!=1) $isqlus .= " and a.usua_codi=$codusuario ";
                //echo "--->".$isqlus;
                $rs1=$db->query($isqlusDe);
                while(!$rs1->EOF)  {
                    $codigo = $rs1->fields["USUA_CODI"];
                    $vecDeps[]=$codigo;
                    $usNombre = $rs1->fields["USUA_NOMB"].' '.$rs1->fields["USUA_APELLIDO"];
                    $datoss=($codus==$codigo)?$datoss= " selected ":"";
                    echo "<option value=$codigo  $datoss>$usNombre</option>";
                    $rs1->MoveNext();
                }
            }
            ?>
            </select>
        &nbsp;</td>
      </tr>
      <?
      //echo "--->".codus;
      }
 //CIERRA BUCLE PARA



//*******CARGA USUARIO "PARA"*******
//if($_SESSION["usua_codi"]!=0){
    if( ($_SESSION["usua_codi"]!=0) and $tipoEstadistica==15  ){

      ?>
         <tr>
            <td width="30%" class="titulos2">Para</td>
            <td class="listado2">
                <select name="para" id="para" class="select">
                    <?

                    if ($usua_perm_estadistica> 0){	?>
                        <option value=99999> -- Agrupar por todos los remitentes --</option>
                    <?	}
                    //echo '<option value="9999"'.$selected9.'> -- Agrupar por todos los remitentes -- </option>';

                    $whereUsSelect = (!isset($_POST['usActivos']) )? " u.USUA_ESTA = 1 ":"";
                    $whereUsSelect=($usua_perm_estadistica < 1)?
                        (($whereUsSelect!="")?$whereUsSelect." AND u.USUA_LOGIN='$krd' ":" u.USUA_LOGIN='$krd' "):$whereUsSelect;	  if($dependencia_busq != 99999)  {

                    $whereUsSelect=($whereUsSelect=="")? substr($whereDependencia,4):$whereUsSelect.$whereDependencia;


                    //Combo para generar tipos de documento.
                        $sql_tipo="select u.USUA_NOMB,u.USUA_CODI,u.USUA_ESTA, u.USUA_APELLIDO from USUARIOS u
                           where  $whereUsSelect
                           order by u.USUA_NOMB";

                        $rsE = $db->query($sql_tipo);
                        while(!$rsE->EOF)  {
                            $codigo = $rsE->fields["USUA_CODI"];
                    $vecDeps[]=$codigo;
                    $usNombre = $rsE->fields["USUA_NOMB"].' '.$rsE->fields["USUA_APELLIDO"];
                    $datoss=($para==$codigo)?$datoss= " selected ":"";
                    echo "<option value=$codigo  $datoss>$usNombre</option>";
                            $rsE->MoveNext();
                        }
                     }
                    /*echo '<option value="1111"'.$selected1.'>Asignado</option>';// VEJM 06/09/2009(M)Tramitado
                    echo '<option value="11111"'.$selected10.'>Reasignado</option>';// VEJM 06/08/2009(N)
                    echo '<option value="8888"'.$selected8.'>Vencido</option>';*/
                    ?>
                </select>
            </td>
        </tr>
      <?
      }?>
<!--*******CIERRA BUCLE PARA******-->



<!--******CARGA INSTITUCION  FUNCIONARIO PARA DOCUMENTOS ENVIADOS, SU ETIQUETA ES "PARA"*****-->
<?

        if($tipoEstadistica==17 ){
          //if($_SESSION["usua_codi"]!=0) {
            if (isset($inst_actu)) {
                if ($inst_actu!=0) {
                    $inst_codi = $inst_actu;
                    $_SESSION["inst_codi"] = $inst_actu;
                }
            }
            else
            $inst_actu = $_SESSION["inst_codi"];

            //echo $inst_actu ;
            //$_SESSION["inst_codi_docExt"]=$inst_actu;

        ?>
        <form name="formulario" id="formulario" method="post">

            <td width="30%" class="titulos2">Para (Institución - Funcionario):</td>
            <td class="listado2">
            <input type=radio name="genero" value="F" onclick="Obtener_val(this)" echo "checked"> Funcionario
            <input type="hidden" name="genciudadano" id="genciudadano" value="">
            <!--<td align="center" class="listado2">-->
            <?
                $sql = "select  a.INST_NOMBRE, a.INST_CODI,count(*) from institucion a,DEPENDENCIA b where a.inst_codi=b.inst_codi
                and a.inst_estado=1 and b.depe_estado=1 group by a.INST_NOMBRE, a.INST_CODI having count(*)>=1 order by a.inst_nombre";


                /*$sql="select
                case
                when a.inst_nombre<>'' then (((COALESCE(a.inst_nombre,''::character varying))::text || ' '::text ) ||   ('-Ciudadano'::text))
                else ((('Sin Institución'::character varying) || ' '::text ) ||   ('-Ciudadano'::text))
                end as inst_nombre, a.INST_CODI,'C' AS tipo_usu,1 as count,
                case
                when a.inst_nombre<>'' then a.inst_nombre else 'Sin Institución'
                end as inst_nombre1
                from
                (select distinct inst_nombre,INST_CODI FROM DATOS_USUARIOS where tipo_usuario=2 and inst_estado=1 order by inst_nombre) a
                UNION ALL
                select (((COALESCE(a.inst_nombre,''::character varying))::text || ' '::text ) ||   ('-Funcionario'::text)) as inst_nombre ,
                a.INST_CODI,'F' AS tipo_Usu,count(*) as count,a.inst_nombre as inst_nombre1 from institucion a,DEPENDENCIA b
                where a.inst_codi=b.inst_codi and a.inst_estado=1 and b.depe_estado=1 group by a.INST_NOMBRE, a.INST_CODI having count(*)>=1
                order by tipo_Usu desc";*/

                $rs=$db->conn->query($sql);
                echo $rs->GetMenu2("inst_actu", $inst_actu, "0:&lt;&lt Agrupar por todos las Instituciones &gt;&gt;", false,"","class='select' Onchange='document.formulario.submit()'");
                //echo "instit".$inst_actu;
                $_SESSION['inst_codi_enviados']=$inst_actu;
                //echo $sql;


                 //Vuelvo a  cargar el area, debido al cambio de institucion*/
                $isqlDep="select depe_codi from DEPENDENCIA a where  inst_codi=$inst_actu and depe_estado=1";
                $rs=$db->conn->query($isqlDep);
                while(!$rs->EOF){
                        $codigo = $rs->fields["DEPE_CODI"];
                        $rs->MoveNext();
                }

                $dependencia=$codigo;
                if(!$dependencia_busq) $dependencia_busq =$dependencia;





                
            ?>

                </td>
        </form>


        <? }?>

<!--*******FIN CARGA INSTITUCIONES FUNCIONARIOS PARA DOC EXTERNOS*******-->



<!--*******CARGA AREA EN CASO DE CARGAR OTRAS INSTITUCIONES******-->
<?
//echo $usua_perm_estadistica;
 if($tipoEstadistica==17 ){

?>

	<tr>
    <td width="30%" class="titulos2"><? if ($tipoEstadistica==17){
                                            echo "Area(Institución-Funcionario)";
                                        }else{
                                            echo "Area ".$InstLogeada;
                                        }
                                     ?></td>
    
    <td class="listado2">
    
	<select name=dependencia_busq_docs    class="select"  onChange="formulario.submit();">

    <?

	if($usua_perm_estadistica>1)  {
		if(dependencia_busq_docs==99999)  {
			$datoss= "selected ";
		}
		?>
			<option value=99999  <?=$datoss?>>-- Todas las Areas --</option>
		<?
	}

    $whereAreaSelect="inst_codi=$inst_actu and depe_estado=1";//" DEPE_CODI = $dependencia ";
   
    $isqlus = "select a.DEPE_CODI,a.DEPE_NOMB,a.DEPE_CODI_PADRE from DEPENDENCIA a
                        where $whereAreaSelect  ";

    $rs1=$db->query($isqlus);

    echo "<option value='99999'  >Todos</option>";
    do{
        $codigo = $rs1->fields["DEPE_CODI"];
        $vecDeps[]=$codigo;
        $depnombre = $rs1->fields["DEPE_NOMB"];
        $datoss="";
        if($dependencia_busq_docs==$codigo){
            $datoss= " selected ";
        }
        echo "<option value=$codigo  $datoss>$depnombre</option>";
        $rs1->MoveNext();
    }while(!$rs1->EOF);
    ?>
	</select>-
</td>
</tr>

<?
//echo $isqlus;    //}
 }
?>

<!--*******FIN CARGA AREAS POR INSTITUCION*****-->


<!--******CARGA INSTITUCION CIUDADANOS PARA DOCUMENTOS ENVIADOS, SU ETIQUETA ES "PARA"*****-->


<?

        if($tipoEstadistica==17 ){
          if($_SESSION["usua_codi"]!=0) {
            if (isset($inst_actu)) {
                if ($inst_actu!=0) {
                    $inst_codi = $inst_actu;
                    $_SESSION["inst_codi"] = $inst_actu;
                }
            }
            else
        ?>
        <tr>
		<td width="30%" class="titulos2">Para(Institución - Ciudadano)</td>
		<td class="listado2">
            <input type=radio name="genero" value="C"  onclick="Obtener_val(this)" <? if($_POST['genero']=="C") echo 'checked'; ?>> Ciudadano
            <input type="hidden" name="genciudadano" id="genciudadano" value="">
        	<select name="cmbCiudadano" id="cmbCiudadano" class="select" >
            <!--onfocus="javascript:this.blur(); return true;-->
				<?
				echo '<option value="99999"'.$selected10.'> << Agrupar por todos las Instituciones >> </option>';

				//Combo para generar tipos de documento.
					 $sql="select
                case
                when a.INST_NOMBRE<>'' then a.INST_NOMBRE   else 'Sin Institución'
                end as INST_NOMBRE, a.INST_CODI,'C' AS tipo_usu,1 as count
                from
                (select distinct INST_NOMBRE,INST_CODI FROM DATOS_USUARIOS where tipo_usuario=2 and inst_estado=1 order by inst_nombre) a";

					$rs = $db->query($sql);
					while(!$rs->EOF)  {
						$codigo = $rs->fields["INST_CODI"];
                        $vecDeps[]=$codigo;
                        $usNombre =$rs->fields["INST_NOMBRE"];
                        $datoss=($cmbCiudadano==$codigo)?$datoss= " selected ":"";
                        echo "<option value='$usNombre'  $datoss>$usNombre</option>";
                        $rs->MoveNext();
                        }
				?>
            </select>
		</td>
	</tr>


        <? }}  ?>

<!--*******FIN CARGA INSTITUCIONES CIUDADANOS PARA DOC EXTERNOS*******-->




<?
if($tipoEstadistica==15 ){
  ?>
	 <tr>
		<td width="30%" class="titulos2">Tipo Documento</td>
		<td class="listado2">
			<select name="tipo" id="tipo" class="select">
				<?
				echo '<option value="99999"'.$selected9.'> -- Agrupar por todos los tipos de documentos -- </option>';

				//Combo para generar tipos de documento.
					$sql_tipo="select trad_codigo,trad_descr,trad_doc_sal,trad_carpeta,trad_tipo from tiporad order by 1";

					$rsE = $db->query($sql_tipo);
					while(!$rsE->EOF)  {
                        $codigo = $rsE->fields["TRAD_CODIGO"];
                        $vecDeps[]=$codigo;
                        $usNombre = $rsE->fields["USUA_NOMB"].' '.$rsE->fields["TRAD_DESCR"];
                        $datoss=($tipo==$codigo)?$datoss= " selected ":"";
                        echo "<option value=$codigo  $datoss>$usNombre</option>";
                        $rsE->MoveNext();
					}
				?>
			</select>
		</td>
	</tr>
  <?
  }


  if($tipoEstadistica==1 or $tipoEstadistica==15
  ){

  $selected1='';
  $selected8='';
  $selected9='';
  $selected10='';//VEJM 06/09/2009 (N)
	if(isset($_POST['estado'])){
		if($_POST['estado']=='1111'){
			$selected1='selected';
		}elseif($_POST['estado']=='8888'){
			$selected8='selected';
		}elseif($_POST['estado']=='9999'){
			$selected9='selected';
        }elseif($_POST['estado']=='11111'){//VEJM 06/09/2009
			$selected10='selected';
		}
	}
  ?>
	 <tr>
		<td width="30%" class="titulos2">Estado</td>
		<td class="listado2">
			<select name="estado" id="estado" class="select">
				<?
				echo '<option value="9999"'.$selected9.'> -- Agrupar por todos los estados de los documentos -- </option>';

				//Combo para generar estado del documento.
					$sql_estado="SELECT ESTA_CODI, ESTA_DESC
                        FROM ESTADO
                        WHERE ESTA_CODI not in ('3','4','5','8')
                        ORDER BY 2";

					$rsE = $db->query($sql_estado);
					while(!$rsE->EOF)  {
						$selected='';
						if(isset($_POST['estado']) && $_POST['estado']==$rsE->fields["ESTA_CODI"]){
							$selected='selected';
						}
						echo "<option value='".$rsE->fields["ESTA_CODI"]."'  $selected>".$rsE->fields["ESTA_DESC"]."</option>";
						//$estados[]= $rsE->fields['ESTA_CODI'];
						$rsE->MoveNext();
					}
				echo '<option value="1111"'.$selected1.'>Asignado</option>';// VEJM 06/09/2009(M)Tramitado
                echo '<option value="11111"'.$selected10.'>Reasignado</option>';// VEJM 06/08/2009(N)
				//echo '<option value="8888"'.$selected8.'>Vencido</option>';
				?>
			</select>
		</td>
	</tr>
  <?
  //echo $sql_estado;
  
  }


  if($tipoEstadistica==6 or $tipoEstadistica==10 or
  	$tipoEstadistica==12 or $tipoEstadistica == 14) {
  ?>
  <tr>
    <td width="30%" height="40" class="titulos2">Agrupar por Tipo de Documento </td>
    <td class="listado2">
	<select name=tipoDocumento  class="select" >
        <?
            $isqlTD = "SELECT SGD_TPR_DESCRIP, SGD_TPR_CODIGO
                        from SGD_TPR_TPDCUMENTO
                        WHERE SGD_TPR_CODIGO<>0
                        order by  SGD_TPR_DESCRIP";
            //if($codusuario!=1) $isqlus .= " and a.usua_codi=$codusuario ";
            //echo "--->".$isqlus;
            $rs1=$db->query($isqlTD);
            $datoss = "";

            if($tipoDocumento!='9998'){
                $datoss= " selected ";
                $selecUs = " b.USUA_NOMB USUARIO, ";
                $groupUs = " b.USUA_NOMB, ";
            }

            if($tipoDocumento=='9999'){
                $datoss= " selected ";
            }
		?>
            <option value='9999'  <?=$datoss?>>-- No Agrupar Por Tipo de Documento</option>
		<?   $datoss = "";
            if($tipoDocumento=='9998'){
                $datoss= " selected ";
            }
		?>
		<option value='9998'  <?=$datoss?>>-- Agrupar Por Tipo de Documento</option>
		<?
		$datoss = "";
		if($tipoDocumento=='9997'){
			$datoss= " selected ";
		}
		?>
		<option value='9997'  <?=$datoss?>>-- Tipos Documentales No Definidos</option>
		<?
		do{
			$codigo = $rs1->fields["SGD_TPR_CODIGO"];
			$vecDeps[]=$codigo;
			$selNombre = $rs1->fields["SGD_TPR_DESCRIP"];
			$datoss="";
		if($tipoDocumento==$codigo){
				$datoss= " selected ";
			}
			echo "<option value=$codigo  $datoss>$selNombre</option>";
			$rs1->MoveNext();
		}while(!$rs1->EOF);
		?>
		</select>

	  </td>
  </tr>
  <?
  }
  if($tipoEstadistica==1 or $tipoEstadistica==2 or $tipoEstadistica==3 or
  		$tipoEstadistica==4 or $tipoEstadistica==5 or $tipoEstadistica==7 or
  		$tipoEstadistica==8 or $tipoEstadistica==9 or $tipoEstadistica==10 or
  		$tipoEstadistica==11 or $tipoEstadistica==12 or $tipoEstadistica==14 or $tipoEstadistica==15 or
        $tipoEstadistica==16 or $tipoEstadistica==17 or $tipoEstadistica==18 or $tipoEstadistica==19
        or $tipoEstadistica==20
        )
  {
  ?>
  <tr>
    <td width="30%" class="titulos2">Desde fecha (aaaa/mm/dd) </td>
    <td class="listado2">
	<script language="javascript">
	dateAvailable.writeControl();
	dateAvailable.dateFormat="yyyy/MM/dd";
	</script>
	&nbsp;
  </td>
  </tr>
  <tr>
    <td width="30%" class="titulos2">Hasta  fecha (aaaa/mm/dd) </td>
    <td class="listado2">
	<script language="javascript">
	dateAvailable2.writeControl();
	dateAvailable2.dateFormat="yyyy/MM/dd";
	</script>&nbsp;</td>
  </tr>
    <?
  }
  ?>
  <tr>
    <td colspan="2" class="titulos2">
	<center>
	<input name="Submit" type="submit" class="botones_funcion" value="Limpiar">
	<input type="submit" class="botones_funcion" value="Generar" name="generarOrfeo">
	</center>
	</td>
  </tr>
</table>
</select>
</form>
<?



    $datosaenviar = "fechaf=$fechaf&tipoEstadistica=$tipoEstadistica&codus=$codus&krd=$krd&dependencia_busq=$dependencia_busq&dependencia_busq_ext=dependencia_busq_ext&dependencia_busq_docs=$dependencia_busq_docs&ruta_raiz=$ruta_raiz&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&tipoRadicado=$tipoRadicado&tipoDocumento=$tipoDocumento&id_medio=$id_medio&$para=para&$tipo=tipo&cmbCiudadano=$cmbCiudadano";

    if (isset($generarOrfeo) && $tipoEstadistica == 12) {
        global $orderby;
        $orderby = 'ORDER BY NOMBRE';
        $whereDep = ($dependencia_busq != 99999) ? "AND h.DEPE_CODI = " . $dependencia_busq : '';
        $isqlus = "SELECT u.USUA_NOMB NOMBRE, u.USUA_DOC, d.DEPE_CODI, COUNT(r.RADI_NUME_RADI) TOTAL_MODIFICADOS
                          FROM USUARIO u, RADICADO r, HIST_EVENTOS h, DEPENDENCIA d, SGD_TPR_TPDCUMENTO s
                          WHERE u.USUA_DOC = h.USUA_DOC
                            AND h.SGD_TTR_CODIGO = 32
                            AND h.HIST_OBSE LIKE '*Modificado TRD*%'
                            AND h.DEPE_CODI = d.DEPE_CODI
                            $whereDep
                            AND s.SGD_TPR_CODIGO = r.TDOC_CODI
                            AND r.RADI_NUME_RADI = h.RADI_NUME_RADI
                            AND TO_CHAR(r.RADI_FECH_RADI,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
                          GROUP BY u.USUA_NOMB, u.USUA_DOC, d.DEPE_CODI $orderby";
    //echo $isqlus;

        $rs1 = $db->query($isqlus);
        while(!$rs1->EOF)  {
            $usuadoc[] = $rs1->fields["USUA_DOC"];
            $dependencias[] = $rs1->fields["DEPE_CODI"];
            $rs1->MoveNext();
        }
    }

    //echo $isqlus;

    if($generarOrfeo)
    {
       include "genEstadistica.php";
    }

    //Genera reporte por Tipos de Documento
    if ($_REQUEST[Z]==333)
    {
        include("$ruta_raiz/reportes/generar_reporte_documentos.php");
    }

    //Genera reporte por Tiempos demora
    if ($_REQUEST[Z]==444)
    {
        //echo "paso1";
        include("$ruta_raiz/reportes/generar_reporte_tiempoDemora.php");

       $tipoReporte=9;
        if($tipoReporte==9){
        $generar = "ok";
        }

        if($generar == "ok") {
        //if($genDetalle==1) $queryE = $queryEDetalle;
        include_once ("tablaHtml_TiemposDemora.php");

     }
  

}

?>

<script>
marcado = 0

function Obtener_val(formulario){
    marcado=formulario.value
    document.getElementById("genciudadano").value=marcado;
    
}
</script>






