<?

/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */ 
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                      */
/*   Hollman Ladino       hladino@gmail.com                Desarrollador             */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*************************************************************************************/
$ruta_raiz = "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

//echo "tipo".$tipoDocumento;
if(!$db)
{
$krdOld = $krd;
$carpetaOld = $carpeta;
$tipoCarpOld = $tipo_carp;
if(!$tipoCarpOld) $tipoCarpOld= $tipo_carpt;
session_start();
if(!$krd) $krd=$krdOsld;
$ruta_raiz = "..";
include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

if(isset($_GET['genDetalle'])){
	?>	
<html>
<title>QUIPUX - IMAGEN ESTADISTICAS </title>
		<link rel="stylesheet" href="../estilos/orfeo.css" />
<body>

	<?php
}
//include "$ruta_raiz/envios/paEncabeza.php";
$usua_docu=$_GET['usua_doc'];
$estado1=$_GET['estado1'];

echo $estado1;
//var_dump($usua_doc.'aquiiiii');

?>
<table><tr><TD></TD></tr></table>

<?
//include_once "$ruta_raiz/include/db/ConnectionHandler.php";
require_once("$ruta_raiz/class_control/Mensaje.php");
include("$ruta_raiz/class_control/usuario.php");
//$db = new ConnectionHandler($ruta_raiz);

$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
$objUsuario = new Usuario($db);
if (isset($dependencia_busq)) {
	$dependencia_busq = $HTTP_GET_VARS["dependencia_busq"];
} 
$whereDependencia = " AND DEPE_CODI=$dependencia_busq ";
$datosaenviar = "fechaf=$fechaf&genDetalle=$genDetalle&tipoEstadistica=$tipoEstadistica&codus=$codus&krd=$krd&dependencia_busq=$dependencia_busq&ruta_raiz=$ruta_raiz&fecha_ini=$fecha_ini&fecha_fin=$fecha_fin&tipoRadicado=$tipoRadicado&tipoDocumento=$tipoDocumento&codUs=$codUs&fecSel=$fecSel&usua_docu=$usua_doc&estado1=$estado1"; 
}
$seguridad =",R.SGD_SPUB_CODIGO,B.CODI_NIVEL as USUA_NIVEL";

$whereTipoRadicado = "";


	if($tipoRadicado)
	{
		$whereTipoRadicado=" AND r.RADI_NUME_RADI LIKE '%$tipoRadicado'";
	}
if($tipoRadicado and ($tipoEstadistica==1 or $tipoEstadistica==6))
	{
		$whereTipoRadicado=" AND r.RADI_NUME_RADI LIKE '%$tipoRadicado'";
	}	
	
if($codus)
	{
		$whereTipoRadicado.=" AND b.USUA_CODI = $codus ";
	}elseif(!$codus and $usua_perm_estadistica<1)
	{
	    $whereTipoRadicado.=" AND b.USUA_CODI = $codusuario ";
	}
if($tipoDocumento and ($tipoDocumento!='9999' and $tipoDocumento!='9998' and $tipoDocumento!='9997'))
	{
		$whereTipoRadicado.=" AND t.SGD_TPR_CODIGO = $tipoDocumento ";
	}elseif ($tipoDocumento=="9997")	
	{
		$whereTipoRadicado.=" AND t.SGD_TPR_CODIGO = 0 ";
	}

	include_once($ruta_raiz."/include/query/busqueda/busquedaPiloto1.php");


//Valida año y mes, en caso de nuevo año inicial
$fecha =$fecha_ini;
$separa = explode("/",$fecha);
$mes = $separa[1];
$anio = $separa[0];

$fecha2 =$fecha_fin ;
$separa2 = explode("/",$fecha2);
$anio2 = $separa2[0];
$msj="";

//Valida Fechas para obtener Reporte

if (((strtotime($fecha_ini))<=(strtotime($fecha_fin))) or( ($anio==($anio2-1)) and ($mes==12)) ) {
	switch($tipoEstadistica)
	{
		case "1";
	include "$ruta_raiz/include/query/estadisticas/consulta001.php";
	$generar = "ok";
	break;
		case "2";
	include "$ruta_raiz/include/query/estadisticas/consulta002.php";
	$generar = "ok";
	break;
		case "3";
	include "$ruta_raiz/include/query/estadisticas/consulta003.php";
	$generar = "ok";
	break;
		case "4";
	include "$ruta_raiz/include/query/estadisticas/consulta004.php";
	$generar = "ok";
	break;
		case "5";
	include "$ruta_raiz/include/query/estadisticas/consulta005.php";
	$generar = "ok";
	break;		
		case "6";
	include "$ruta_raiz/include/query/estadisticas/consulta006.php";
	$generar = "ok";
	break;				
		case "7";
	include "$ruta_raiz/include/query/estadisticas/consulta007.php";
	$generar = "ok";
	break;				
		case "8";
	include "$ruta_raiz/include/query/estadisticas/consulta008.php";
	$generar = "ok";
	break;				
		case "9";
	include "$ruta_raiz/include/query/estadisticas/consulta009.php";
	$generar = "ok";
	break;				
		case "10";
	include "$ruta_raiz/include/query/estadisticas/consulta010.php";
	$generar = "ok";
	break;				
		case "11";
	include "$ruta_raiz/include/query/estadisticas/consulta011.php";
	$generar = "ok";
	break;				
		case "12";
	include "$ruta_raiz/include/query/estadisticas/consulta012.php";
	$generar = "ok";
	break;
		case "13";
	include "$ruta_raiz/include/query/estadisticas/consulta013.php";
	$generar = "ok";
	break;
		case "14";
	include "$ruta_raiz/include/query/estadisticas/consulta014.php";
	$generar = "ok";
	break;
        case "15";
	include "$ruta_raiz/include/query/estadisticas/consulta_tiemposDemora.php";
	$generar = "ok";
	break;
         case "16";
    include "$ruta_raiz/include/query/estadisticas/consulta_documentosExternos.php";
    $generar = "ok";
	break;
         case "17";
             
	include "$ruta_raiz/include/query/estadisticas/consulta_documentosEnviados.php";
	$generar = "ok";
	break;
         case "18";
	include "$ruta_raiz/include/query/estadisticas/consulta_documentosSinLeer_BandImpresion.php";
	$generar = "ok";
	break;
         case "19";
	include "$ruta_raiz/include/query/estadisticas/consulta_retrasosReasignados.php";
	$generar = "ok";
	break;
        case "20";
	include "$ruta_raiz/include/query/estadisticas/consulta_docsNoDigitalizados.php";
	$generar = "ok";
	break;
	}
//echo "hola1".$tipoEstadistica;
/*echo '/////////////////////////////////////////////////////////////<br>';
echo '<pre>';
echo $queryE.'/////////';
echo $queryEDetalle.'here';   
echo '</pre>';*/

/*********************************************************************************
Modificado Por: Supersolidaria
Fecha: 15 diciembre de 2006
Descripci�n: Se incluy� el reporte de radicados archivados reporte_archivo.php
**********************************************************************************/
	if($tipoReporte==1)
	{
	//include "$ruta_raiz/include/query/archivo/queryReportePorRadicados.php";
	// Modificado SGD 17-Agosto-2007
	//require_once "$ruta_raiz/archivo/funReporteArchivo.php";
	//$titulos=array("1#".strtoupper($_SESSION["descRadicado"]),"2#FECHA ".strtoupper($_SESSION["descRadicado"]),"3#FECHA ARCHIVO","4#USUARIO","5#".strtoupper($_SESSION["descDependencia"]),"6#NUMERO FOLIOS");
	
	$generar = "ok";
	}

 //$db->conn->debug=true;
//echo "A===>". $queryEDetalle;
//echo "B===>". $queryETodosDetalle;

//echo $generar;

	if($generar == "ok") {
		if($genDetalle==1) $queryE = $queryEDetalle;
        if($genTodosDetalle==1) $queryE = $queryETodosDetalle;
		//$db->conn->debug = true;
		//$rsE = $db->conn->Execute($queryE);
        include ("tablaHtml.php");
	}

//Consulta de Documentos Externos
}
else{
    $msj="Seleccione correctamente el rango de fechas";
    echo "<center><h3>$msj</h3></center>";
}
?>

