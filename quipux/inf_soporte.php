<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
/**
* Pagina que despliega pantalla para envio de mail a soportequipux@informatica.gov.ec ayuda online de Quipux
**/
if (isset($_GET['rsw']))
$rsw=base64_decode($_GET['rsw']);
else
    $rsw = 0;
if ($rsw!=1){
session_start();
$ruta_raiz = ".";
include_once "$ruta_raiz/rec_session.php";

include "./config.php";
$db = new ConnectionHandler("$ruta_raiz");
            $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
}
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../../estilos/orfeo.css">
</head>

<body topmargin="0" leftmargin="0" bgcolor="#ffffff">
<form name='form_cerrar' action="cerrar_session.php?accion=cerrar" target="_parent" method="post">
<br />
<table width="80%" border="0" align="center" bgcolor="#a8bac6">
	<tr>
	<td>
		<center>
		<p><B><span><font size="2" face="Verdana,Arial,Helvetica,sans-serif">SOPORTE A USUARIOS DEL SISTEMA</br>
                            Subsecretaría de Tecnologías de la Información
                            </font></span></B> </p>
	</td>
	</tr>
</table>
<table width="80%" border="0" align="center" bgcolor="#e3e8ec">
	
        <TR>
		<TD>
                    <font size="2" face="Verdana,Arial,Helvetica,sans-serif" color="#086478"><b>Manuales de Usuario:</b></font></br></br>
        <a href="SGDQ_UsuarioFinal_BS.pdf" target="Manual Bandeja de Salida"><font size="2" face="Verdana,Arial,Helvetica,sans-serif">Descargar Manual Bandeja de Salida </font></a>
        </TD>
	</TR>
        <TR>
		<TD>
        <a href="SGDQ_UsuarioFinal_BE.pdf" target="Manual Bandeja de Entrada"><font size="2" face="Verdana,Arial,Helvetica,sans-serif">Descargar Manual Bandeja de Entrada </font></a>
        </TD>
	</TR>
    <TR><TD>&nbsp;</TD></TR>
	<TR>
		<TD><font size="2" face="Verdana,Arial,Helvetica,sans-serif">Para problemas o incidentes del sistema, por favor comunicarse al siguiente correo electrónico:</font></TD>
	</TR>
	<TR><TD>&nbsp;</TD></TR>
	<TR>
            <?php 
            if ($rsw!=1){
            $sql = "select inst_email from institucion where inst_codi = ".$_SESSION["inst_codi"];
            $rs = $db->conn->query($sql);
            $cuenta_mail_soporte = $rs->fields['INST_EMAIL'];          
            ?>            
            <?php if (trim($cuenta_mail_soporte)!=''){?>
            <TD><center><a href="mailto:<?=$cuenta_mail_soporte?>"><?=$cuenta_mail_soporte?></a></center></TD>
            <?php }else{ ?>
            <TD><center><a href="mailto:soporte@informatica.gov.ec">soporte@informatica.gov.ec</a></center></TD>
            <?php }
            }
            else{?>
              <TD><center><a href="mailto:soporte@informatica.gov.ec">soporte@informatica.gov.ec</a></center></TD>   
            <?php }?>
	</TR>
	<TR><TD>&nbsp;</TD></TR>
	<TR>
		<TD><font size="2" face="Verdana,Arial,Helvetica,sans-serif" color="#086478"><b>Con los siguentes datos:</b></font></TD>
	</TR>
	<TR>
		<TD><font size="2" face="Verdana,Arial,Helvetica,sans-serif">- Nombre de la Institución</font></TD>
	</TR>
	<TR>
		<TD><font size="2" face="Verdana,Arial,Helvetica,sans-serif">- Nombre Completo</font></TD>
	</TR>
	<TR>
		<TD><font size="2" face="Verdana,Arial,Helvetica,sans-serif">- Cargo</font></TD>
	</TR>
	<TR>
		<TD><font size="2" face="Verdana,Arial,Helvetica,sans-serif">- Una descripción concisa y precisa sobre el problema (Si es posible enviar como adjunto pantallas que muestren el error)</font></TD>
	</TR>
    <tr>
    <td><br><br>
    <font size="2" face="Verdana,Arial,Helvetica,sans-serif" color="#086478"><b>Requerimientos del Sistema:</b></font>
    <br><br>
    <font size="2" face="Verdana,Arial,Helvetica,sans-serif" >
        Hardware
    <ul>
    
    <li> Procesador: 2000MHz de velocidad por CPU mínimo   </li>
    <li>  Espacio en disco: 600MB libre mínimo, recomendado 1GB   </li>
    <li>  Memoria física (RAM): 1GB mínimo, 2GB recomendado </li>
    <li> Adaptador de video: 256 colores mínimo   </li>
    <li>  Dispositivo apuntador o ratón  </li>
    <li> Enlace de acceso a la red Internet de 64kbps mínimo </li>
    <li> Dispositivo Token USB de firma digital (solo para funcionarios autorizados)  </li>
    <li>Scaner de alta velocidad A4 (para digitalización documentos entrada)</li>

</ul>
Software
<ul>

    <li> Instalación programa navegador Mozilla Firefox vesión 3 o superior.  </li>
    </ul>

    Para Firma Digital:
    <ul>
    <li>  Instalación del programa manejador (driver) del token USB para Microsoft Windows XP o superiormendado </li>
    <li> Sistema operativo: Microsoft Windows XP o superior   </li>
    <li>  Instalación y funcionamiento apropiado del programa Maquina Virtual de Java (JVM) versión 1.5 </li>
    
</ul>
    </font>
    </td>
    </tr>
    <tr>
        <td><font size="2" face="Verdana,Arial,Helvetica,sans-serif" color="#086478"><b>Información General:</b></font>
            <font size="2" face="Verdana,Arial,Helvetica,sans-serif" >
            Sistema de Gestión Documental se desarrolla y mantiene con el personal de la Subsecretaría de Tecnologías de la Información. 
            Inicialmente en el 2007 se basó en el sistema Orfeo, en su primera versión se adaptó las necesidades de las instituciones.
            En el 2008 se inició un desarrollo nuevo para cubrir con las necesidades de los usuarios en relación al ámbito de Gestión de Documentos. 
            Hasta la fecha se han generado 15 revisiones del sistema y se está en continuo cambio.</font>
        </td>        
    </tr>
</table>

</form>
</body>
</html>
