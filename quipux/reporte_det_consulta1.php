<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
session_start();
$ban=$_SESSION['ban'];
$datos1=$_SESSION['d_reporte_det'];
$queryEDetalle=$_SESSION['queryEDetalle'];

/*echo '<pre>';
var_dump($_SESSION['queryE'].'<br>');
var_dump('detalle'.$_SESSION['queryEDetalle']);
echo '</pre>';*/

$ruta_raiz = ".";
include "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

unset($query2);
unset($doc1);
unset($fecha);
unset($asunto);
unset($estado);

$query2= $db->query($queryEDetalle);
while(!$query2->EOF)  {
	$doc1[] = $query2->fields["RADICADO"];
	$fecha[] = $query2->fields["FECHA_RADICADO"];
	$tipo[] = $query2->fields["TIPO_DESC"];
	$dias[] = $query2->fields["DIAS"];
	$asunto[] = $query2->fields["ASUNTO"];
	$estado[] = $query2->fields["ESTADO"];

	$query2->MoveNext();
}

	$long=count($doc1);	

	$inicio = '
	<html>
	<head>
	<title>.: ORFEO - VISTA PREVIA :.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body style="margin: 40 100 40 100;">
	';
	
	$encabezadopdf = '
	<table align="center">
	<tr><th align="center"><font size="4">PRESIDENCIA DE LA REPUBLICA</th></tr>
	<tr><td></td></tr>
	<tr><td></td></tr>
	<tr><td><center><img src="logoEntidad.gif" width=5 height=5 /></center><td></tr>
	</table>
	<br />
	<table align="left">
		<tr><th align="left">Título: Detalle de los estado de documentos por usuario</th></tr>
	</table>
	<br />
	';
	
	$infor='
	<table width="100%">
	<tr>
		<th align="left">Nombre del usuario:</th>
		<td align="left">'.$datos1[4].'</td>
	</tr>
	<tr>
		<th align="left">Nombre del área:</th>
		<td align="left">'.$datos1[1].'</td>
	</tr>
	<tr>
		<td align="left" size="5%"><b>Fecha(desde):</b></td>
		<td align="left">'.$datos1[2].'</td>
		<td align="right"><b>Fecha(hasta):</b></td>
		<td align="right">'.$datos1[3].'</td>
	</tr>
	<br />
	<br />
	<br />
	</table>
	';	

	$cuerpo=
	'<table border="0" width="100%">
	<tr><th align="center"><font size="2"><b>DOCUMENTO</b></font></Th>
	<th align="center"><font size="2"><b>FECHA INGRESO</b></font></Th>
	<th align="center"><font size="2"><b>TIPO</b></font></Th>
	<th align="center"><font size="2"><b>DIAS EN TRAMITE</b></font></Th>
	<th align="center"><font size="2"><b>ASUNTO</b></font></Th>
	<th align="center"><font size="2"><b>ESTADO</b></font></Th></tr>';
	for ($i=0;$i <=$long-1;$i++ ){
	$cuerpo2 .= '<tr><td align="left"><font size="2">'.$doc1[$i].'</font></td>';
	$cuerpo2 .= '<td align="center"><font size="2">'.$fecha[$i].'</font></td>';
	$cuerpo2 .= '<td align="center"><font size="2">'.$tipo[$i].'</font></td>';
	$cuerpo2 .= '<td align="center"><font size="2">'.$dias[$i].'</font></td>';
	$cuerpo2 .= '<td align="left"><font size="2">'.$asunto[$i].'</font></td>';
	$cuerpo2 .= '<td align="center"><font size="2">'.$estado[$i].'</font></td></tr>';
	}
	$cuerpo .= $cuerpo2.'</table>';
	;
	
	$fin = '
	</body>
	</html>
	';
	
	//GENERACION DEL PDF
	$ruta_raiz=".";
	require_once("$ruta_raiz/js/dompdf/dompdf_config.inc.php");
	
	$dompdf = new DOMPDF();
	$dompdf->load_html($inicio.$encabezadopdf.$infor.$cuerpo.$fin);
	$dompdf->set_paper("a4", "landscape");
	$dompdf->set_base_path(getcwd());
	$dompdf->render();
	
	//$nombarch = "/". substr($verrad,0,4)."/".substr($verrad,4,3)."/".$registro["textrad"].".pdf";
	
	//file_put_contents("$ruta_raiz/bodega".$nombarch, $dompdf->output());
	$dompdf->stream("Reporte_DetalleCantidadDocumentosporUsuario.pdf");
	
	//exec ("mv /tmp/$nombarch /var/www/orfeo/bodega/2008/900/$nombarch",$output,$returnS);
	//$sql = "UPDATE RADICADO SET RADI_PATH='$nombarch' where radi_nume_radi=$verrad";
	//$rs = $db->query($sql);	
	//echo "<script>window.close();</script>";
?>
