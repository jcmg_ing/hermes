<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
session_start();
$ban=$_SESSION['ban'];
$datos1=$_SESSION['d_reporte_det'];
$queryEDetalle=$_SESSION['queryEDetalle'];
/*echo '<pre>';
var_dump($_SESSION['queryE'].'<br>');
var_dump('detalle'.$_SESSION['queryEDetalle']);
echo '</pre>';*/ 

$ruta_raiz = ".";
include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

unset($rsE);
unset($doc1);
unset($usua_di);
unset($obs);
unset($fecha_r);
unset($fecha_d);

$rsE = $db->query($queryEDetalle);
while(!$rsE->EOF){
	$doc1[] = $rsE->fields["RADICADO"];
	$usua_di[] = $rsE->fields["USUARIO_DIGITALIZADOR"];
	$obs[] = $rsE->fields["OBSERVACIONES"];
	$fecha_r[] = $rsE->fields["FECHA_RADICACION"];
	$fecha_d[] = $rsE->fields["FECHA_DIGITALIZACION"];
	$_SESSION['doc1']=$doc1;
	$_SESSION['usua_di']=$usua_di;
	$_SESSION['obs']=$obs;
	$_SESSION['fecha_r']=$fecha_r;
	$_SESSION['fecha_d']=$fecha_d;
	$rsE->MoveNext();
}	

	$long=count($doc1);


	//funcion para contar el link de "ver documento" del campo de obs de la bdd.
	foreach($obs as $obs1){
		//echo $obs1;
		$pos=strpos($obs1,'Ver Documento');
		//echo $pos;
		if(!empty($pos)){
			$obs_nueva[]=substr($obs1,0,($pos-1));	
		}else{
			$obs_nueva[]=$obs1;
		}	
	}
	//-------------------------------------	

	$inicio = '
	<html>
	<head>
	<title>.: ORFEO - VISTA PREVIA :.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body style="margin: 40 60 40 60;">
	';
	
	$encabezadopdf = '
	<table align="center">
	<tr><th align="center"><font size="4">PRESIDENCIA DE LA REPUBLICA</th></tr>
	<tr><td></td></tr>
	<tr><td></td></tr>
	<tr><td><center><img src="logoEntidad.gif" width=5 height=5 /></center><td></tr>
	</table>
	<br />
	<table align="left">
		<tr><th align="left">Título: Detalle del número de documentos digitalizados por usuarios</th></tr>
	</table>
	<br />
	';
	
	
	$infor='
	<table width="100%">
	<tr>
		<th align="left">Nombre del usuario:</th>
		<td>'.$datos1[4].'</td>
	</tr>
	<tr>
		<th align="left">Nombre del área:</th>
		<td>'.$datos1[1].'</td>
	</tr>
	<tr>
		<td align="left" size="5%"><b>Fecha(desde):</b></td>
		<td align="left">'.$datos1[2].'</td>
		<td align="right"><b>Fecha(hasta):</b></td>
		<td align="right">'.$datos1[3].'</td>
	</tr>
	<br />
	<br />
	<br />
	</table>
	';	

	$cuerpo=
	'<table border="0" width="100%">
	<tr><th><font size="2">DOCUMENTO</font></Th>
	<th><font size="2">USUARIO DIGITALIZADOR</font></Th>
	<th><font size="2">OBSERVACIONES</font></th>
	<th><font size="2">FECHA DE INGRESO</font></th>
	<th><font size="2">FECHA DE DIGITALIZACION</font></th></tr>';
	for ($i=0;$i <=$long;$i++ ){
	$cuerpo2 .= '<tr><td><font size="2">'.$doc1[$i].'</font></td>';
	$cuerpo2 .= '<td align="center"><font size="2">'.$usua_di[$i].'</font></td>';
	$cuerpo2 .= '<td align="center"><font size="2">'.$obs_nueva[$i].'</font></td>';
	$cuerpo2 .= '<td align="center"><font size="2">'.$fecha_r[$i].'</font></td>';
	$cuerpo2 .= '<td align="center"><font size="2">'.$fecha_d[$i].'</font></td></tr>';
	}
	$cuerpo .= $cuerpo2.'</table>';
	
	//var_dump($cuerpo);
	
	$fin = '
	</body>
	</html>
	';
	
	//GENERACION DEL PDF
	require_once("./js/dompdf/dompdf_config.inc.php");
	
	$dompdf = new DOMPDF();
	$dompdf->load_html($inicio.$encabezadopdf.$infor.$cuerpo.$fin);
	$dompdf->set_paper("a4", "landscape");
	$dompdf->set_base_path(getcwd());
	$dompdf->render();
	
	//$nombarch = "/". substr($verrad,0,4)."/".substr($verrad,4,3)."/".$registro["textrad"].".pdf";
	
	//file_put_contents("$ruta_raiz/bodega".$nombarch, $dompdf->output());
	$dompdf->stream("Documentos_Hojas_digitalizados_por_Usuarios..pdf");
	
	//exec ("mv /tmp/$nombarch /var/www/orfeo/bodega/2008/900/$nombarch",$output,$returnS);
	//$sql = "UPDATE RADICADO SET RADI_PATH='$nombarch' where radi_nume_radi=$verrad";
	//$rs = $db->query($sql);	
	//echo "<script>window.close();</script>";
?>
