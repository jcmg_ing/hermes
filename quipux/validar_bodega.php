<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$ruta_raiz = ".";
include_once ("$ruta_raiz/include/db/ConnectionHandler.php");
include_once ("$ruta_raiz/config.php");
error_reporting(7);
$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

echo "<center><h3>Archivos faltantes en la bodega</h6><center>";
echo "<table width='100%' border '1'>";
echo "<tr><th>No.</th><th>TABLA</th><th>No. Documento</th><th>PATH</th><th>FECHA</th></tr>";
$i = 0;

$sql = "select distinct radi_path, radi_fech_ofic, radi_nume_text from radicado where trim(coalesce(radi_path::text,'')) <>'' and radi_path not like '%docs%' and radi_fech_ofic::date=now()::date";
$rs = $db->query($sql);
while (!$rs->EOF) {
    if (!is_file("$ruta_raiz/bodega".$rs->fields["RADI_PATH"])) {
        echo "<tr><td>".++$i."</td><td>Radicado</td><td>".$rs->fields["RADI_NUME_TEXT"]."</td><td>".$rs->fields["RADI_PATH"]."</td><td>".$rs->fields["RADI_FECH_OFIC"]."</td></tr>";
        $db->query("update radicado set radi_path=null where radi_path='".$rs->fields["RADI_PATH"]."'");
    } else if (filesize ("$ruta_raiz/bodega".$rs->fields["RADI_PATH"]) == 0) {
        rename("$ruta_raiz/bodega".$rs->fields["RADI_PATH"], "$ruta_raiz/bodega".$rs->fields["RADI_PATH"]."0");
        echo "<tr><td>".++$i."</td><td>Radicado Tama&ntilde;o 0</td><td>".$rs->fields["RADI_NUME_TEXT"]."</td><td>".$rs->fields["RADI_PATH"]."0</td><td>".$rs->fields["RADI_FECH_OFIC"]."</td></tr>";
        $db->query("update radicado set radi_path=null where radi_path='".$rs->fields["RADI_PATH"]."'");
    }
    $rs->MoveNext();
}

$sql = "select distinct anex_path, anex_fecha, r.radi_nume_text from anexos a left outer join radicado r on r.radi_nume_radi=a.anex_radi_nume where anex_borrado='N' and anex_fecha::date=now()::date";
$rs = $db->query($sql);
while (!$rs->EOF) {
    if (!is_file("$ruta_raiz/bodega".$rs->fields["ANEX_PATH"]))
        echo "<tr><td>".++$i."</td><td>Anexos</td><td>".$rs->fields["RADI_NUME_TEXT"]."</td><td>".$rs->fields["ANEX_PATH"]."</td><td>".$rs->fields["ANEX_FECHA"]."</td></tr>";
    else if (filesize ("$ruta_raiz/bodega".$rs->fields["ANEX_PATH"]) == 0) {
        rename("$ruta_raiz/bodega".$rs->fields["ANEX_PATH"], "$ruta_raiz/bodega".$rs->fields["ANEX_PATH"]."0");
        echo "<tr><td>".++$i."</td><td>Anexos</td><td>".$rs->fields["RADI_NUME_TEXT"]."</td><td>".$rs->fields["ANEX_PATH"]."0</td><td>".$rs->fields["ANEX_FECHA"]."</td></tr>";
    }
    $rs->MoveNext();
}
echo "</table>";
?>
