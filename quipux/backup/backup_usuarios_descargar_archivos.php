<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

/*****************************************************************************************
**  Muestra la lista de archivos a descargar cuando el zip se parte en varios archivos  **
*****************************************************************************************/

$ruta_raiz = "..";
session_start();
if($_SESSION["usua_perm_backup"]!=1) die("");

$codigo = 0 + $_GET["codigo_backup"];

$flag_multiple_archivo = true;
$archivos = array();
$i = 1;
while ($flag_multiple_archivo and $i<100) {
    $cod_tmp = substr("00$i", -2);
    if (is_file("$ruta_raiz/bodega/respaldos/respaldo_$codigo.z$cod_tmp")) {
        $archivos[] = "$ruta_raiz/bodega/respaldos/respaldo_$codigo.z$cod_tmp";
        ++$i;
    } else {
        $archivos[] = "$ruta_raiz/bodega/respaldos/respaldo_$codigo.zip";
        $flag_multiple_archivo = false;
    }
}

include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();
?>
<body>
    <script type="text/javascript">
        function descargar_archivo(extension) {
            window.location = '<?= "$ruta_raiz/archivo_descargar.php?path_arch=/respaldos/respaldo_$codigo."?>' + extension;
        }
    </script>

    <center>
        <br><br>
        <table width="95%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
            <tr>
                <td class="listado1" colspan="3"><center>El respaldo se obtuvo en <?=$i?> archivos</center></td>
            </tr>
            <tr>
                <th width="33%"><center>No. de Archivo</center></th>
                <th width="33%"><center>Tama&ntilde;o</center></th>
                <th width="33%"><center>Acci&oacute;n</center></th>
            </tr>
<?
    for ($i=0; $i<count($archivos); ++$i) {
        $tamano = filesize($archivos[$i])/1024;
        $tamano_medida = "K";
        if ($tamano>1024) { $tamano=$tamano/1024; $tamano_medida = "M";}
        if ($tamano>1024) { $tamano=$tamano/1024; $tamano_medida = "G";}
        $tamano = round($tamano, 2);
        echo "<tr>
                <td class='listado1'>Parte ".($i+1)."</td>
                <td class='listado1'>$tamano$tamano_medida</td>
                <td class='listado1'><a href='javascript:descargar_archivo(\"".substr($archivos[$i],-3)."\")' class='vinculos' target='_self'>Descargar</a></td>
              </tr>";

    }
?>
            <tr>
                <td class="listado1" colspan="3">
                    <br>
                    <center>Para unir los archivos en uno solo ejecutar: <br><b>&quot;zip -s 0 respaldo_<?=$codigo?>.zip --out respaldo_<?=$codigo?>_unificado.zip&quot;</b></center>
                    <br>
                </td>
            </tr>
        </table>
        <br><br>
        <input class="botones" type="button" name="cerrar" value="Cerrar" onclick="window.close()">
    </center>
</body>