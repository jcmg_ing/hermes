<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------

**************************************************************************************
** Respalda uno por uno los documentos de los usuarios                              **
** Busca los documentos que se deberán respaldar y los respalda uno por uno         **
** llamando a backup_usuarios_respaldar_documentos.php utilizando Ajax              **
**                                                                                  **
** Desarrollado por:                                                                **
**      Mauricio Haro A. - mauricioharo21@gmail.com                                 **
*************************************************************************************/

$ruta_raiz = "..";

include_once "$ruta_raiz/config.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
require_once "$ruta_raiz/funciones.php"; //para traer funciones p_get y p_post
include_once "$ruta_raiz/funciones_interfaz.php";

$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

// Eliminamos los documentos que pudieron ingresarse el paso anterior y por algún error no deben ser ejecutados.
$sql = "select distinct resp_codi from respaldo_usuario
        where coalesce(fecha_inicio::text,'')='' and
        resp_codi in (select distinct resp_codi from respaldo_usuario_radicado)";
$rs = $db->query($sql);
while (!$rs->EOF) {
    $sql = "delete from respaldo_usuario_radicado where resp_codi=".$rs->fields["RESP_CODI"];
    $db->query($sql);
    $rs->MoveNext();
}

echo "<html>".html_head();
include_once "$ruta_raiz/js/ajax.js";

echo "<script>\n";
echo "var radi_nume = new Array();\n";

$sql = "select distinct radi_nume_radi from respaldo_usuario_radicado
        where coalesce(fila,'')='' and num_error<3";
$rs = $db->query($sql);
$i = 0;
while(!$rs->EOF) {
    echo "radi_nume[$i]='" . $rs->fields["RADI_NUME_RADI"] . "';";
    $rs->MoveNext();
    $i++;
}
echo "\n</script>";

?>
<script language="JavaScript" type="text/JavaScript">
    num_respaldo = 0;
    timerID = 0;
    // Ejecuta esta función cada segundo, verifica primero si ya se terminó de ejecutar el respaldo anterior
    function timer_cargar_documentos_respaldo() {
        if (document.getElementById('div_procesar_respaldo').innerHTML!='Procesando...') {
            if (num_respaldo < <?=$i?>) {
                document.getElementById('div_procesar_respaldo').innerHTML='Procesando...';
                nuevoAjax('div_procesar_respaldo', 'POST', 'backup_usuarios_respaldar_documentos.php', 'txt_radi_nume='+radi_nume[num_respaldo]);
                ++num_respaldo;
                timerID = setTimeout("timer_cargar_documentos_respaldo()", 500);
                actualizar_barra();
            } else {
                clearTimeout(timerID);
                window.location = 'backup_usuarios_generar_zip.php';
                return;
            }
        } else {
            timerID = setTimeout("timer_cargar_documentos_respaldo()", 500);
        }
    }

    function actualizar_barra() { // Controla el movimiento de la barra
        try {
            tamano_barra = Math.round(500*num_respaldo/<?=$i?>);
            porcentaje =  Math.round(100*num_respaldo/<?=$i?>);
            document.getElementById('div_barra').style.width=tamano_barra;
            document.getElementById('div_barra_mensaje').innerHTML = '<center>'+ porcentaje + ' %</center>';
            document.getElementById('div_mensaje').innerHTML='<center>Procesando documento No. '+ num_respaldo+' de <?=$i?></center>';
        } catch (e) {
            return;
        }
    }
</script>

<body>
  <center>
    <br><br>
    <table width="90%" align="center" class=borde_tab border="0">
        <tr>
            <td width="100%" class="titulos5" colspan="3">
              <center>
                <br><b>PASO 2:</b> Respaldo de Documentos<br>&nbsp;
              </center>
            </td>
        </tr>
        <tr>
            <td width="20%">&nbsp;</td>
            <td width="60%" align="center">
                <br>Estado del proceso:<br><br>
                <div align='left' style='color: #FFFFFF; height: 26px; width: 506px; border: thin solid #999999; position: relative;'>
                    <div id='div_barra'
                         style='background-color: #a8bac6; color: #FFFFFF; border: thin solid #a8bac6;
                         height:20px; width: 0px; position:absolute; top:2px; left:2px; z-index:1;'></div>
                    <div id='div_barra_mensaje'
                         style='color: #000000; border: none; 
                         height:17px; width: 500px; position:absolute; top:5px; left:2px; z-index:100;'>

                    </div>
                </div><br>
                <div id="div_mensaje"></div><br>

            </td>
            <td width="20%">&nbsp;</td>
        </tr>
    </table>
    <br>


    <div id='div_procesar_respaldo' style="display:none">OK</div>
    <br>
    <input type="button" name="btn_cancelar" value="Regresar" class="botones" 
           onClick="window.location='backup_usuarios_menu.php';">
  </center>

  <script language="JavaScript" type="text/JavaScript">
    timer_cargar_documentos_respaldo();
  </script>
</body>
</html>
