<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------

****************************************************************************************
** Empaqueta uno por uno los respaldos que ya finalizaron                             **
** Busca los backups de los que ya se respaldaron todos los documentos y los comprime **
** Genera los archivos de bandejas, cabeceras, menú, etc. y copia las imágenes        **
** necesarias, limpia los temporales utilizados y genera un archivo ".zip" para       **
** entregarlo al usuario.                                                             **
**                                                                                    **
** Desarrollado por:                                                                  **
**      Mauricio Haro A. - mauricioharo21@gmail.com                                   **
***************************************************************************************/

$ruta_raiz= "..";
include_once "$ruta_raiz/config.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/funciones.php";
include_once "$ruta_raiz/obtenerdatos.php";
include_once "$ruta_raiz/backup/backup_usuarios_generar_zip_html.php";

$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

try {
    $resp_codi=limpiar_sql($_POST["txt_resp_codi"]);

    $sql = "select count(resp_codi) as \"num\" from respaldo_usuario_radicado where fila is null and resp_codi=$resp_codi";
    $rs = $db->query($sql);
    if ($rs->fields["NUM"] > 0) die ("OK");

    $path = "$ruta_raiz/bodega/respaldos/respaldo_$resp_codi";

    // Copiamos las imágenes y creamos los archivos index, menú, bandejas, etc.
    copy ("$ruta_raiz/img/content/down_icon.png" , "$path/archivos/descargar.png");
    copy ("$ruta_raiz/img/content/regresar.png" , "$path/archivos/regresar.png");
    copy ("$ruta_raiz/quipux-logo.png" , "$path/archivos/logo.png");
    $html = cargar_estilos();
    file_put_contents ("$path/documentos/estilos.css", $html);
    $html = cargar_index();
    file_put_contents ("$path/index.html", $html);
    $html = cargar_top ($resp_codi);
    file_put_contents ("$path/documentos/top.html", $html);
    $html = cargar_menu ();
    file_put_contents ("$path/documentos/menu.html", $html);
    $html = cargar_informacion($resp_codi);
    file_put_contents ("$path/documentos/informacion.html", $html);
    $html = cargar_bandejas ($resp_codi, 2);
    file_put_contents ("$path/documentos/recibidos.html", $html);
    $html = cargar_bandejas ($resp_codi, 1);
    file_put_contents ("$path/documentos/enviados.html", $html);

    $path_actual = exec("pwd");

    chdir($path);
    shell_exec("zip -s 4608m -r ../respaldo_$resp_codi.zip *");
    chdir($path_actual);
//    Se cambiaron estas lineas a backup_usuarios_eliminar
//    shell_exec("rm -R $path");
//
//    $sql = "delete from respaldo_usuario_radicado where resp_codi=$resp_codi";
//    $db->query($sql);
    $sql = "update respaldo_usuario set fecha_fin=".$db->conn->sysTimeStamp." where resp_codi=$resp_codi";
    $db->query($sql);

    $rs->MoveNext();
} catch (Exception $e) {
    die ("OK");
}

die ("OK");

?>