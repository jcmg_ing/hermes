<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

$ruta_raiz = "..";
include_once "$ruta_raiz/config.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/funciones_interfaz.php";
include_once "$ruta_raiz/funciones.php";

$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

$cmb_institucion = trim(limpiar_sql($_GET["cmb_institucion"]));

echo "<html>".html_head();

if($orden_cambio==1) {
    if(strtolower($orderTipo)=="desc")
	$orderTipo="asc";
    else
        $orderTipo="desc";
}
    if (!$orderTipo) $orderTipo="desc";
?>
  <body>
    <br>
<?

    $isql = "select substr(r.fecha_solicita::text,1,19) as \"Fecha Solicitud\",
            u.usua_nombre as \"Usuario\",
            u.inst_nombre as \"Institución\",
            case when fecha_eliminado is not null then 'Eliminado' else case when fecha_fin is not null then 'Finalizado' else 'Pendiente' end end as \"Estado\",
            case when fecha_eliminado is null and fecha_fin is not null then 'Descargar' else '' end as \"SCR_Descargar\",
            'descargar_respaldo(\"' || r.resp_codi || '\");' as \"HID_FUNCION\",
            case when fecha_eliminado is null then 'Eliminar' else '' end as \"SCR_Eliminar\",
            'eliminar_respaldo(\"' || r.resp_codi || '\");' as \"HID_FUNCION_ELIMINAR\"
            from respaldo_usuario r left outer join usuario u on r.usua_codi=u.usua_codi";
    if ($cmb_institucion != "0") $isql .= " where u.inst_codi=$cmb_institucion ";
    $isql .= " order by ".($orderNo+1)." $orderTipo";

//echo "$isql";
    $db->query('set enable_nestloop = off');
	$pager = new ADODB_Pager($db,$isql,'adodb', true,$orderNo,$orderTipo,true);
	$pager->checkAll = false;
	$pager->checkTitulo = true;
	$pager->toRefLinks = $linkPagina;
	$pager->toRefVars = $encabezado;
	$pager->descCarpetasGen=$descCarpetasGen;
	$pager->descCarpetasPer=$descCarpetasPer;
	$pager->Render($rows_per_page=20,$linkPagina,$checkbox=chkAnulados);
    $db->query('set enable_nestloop = on');
    
?>

  </body>
</html>

