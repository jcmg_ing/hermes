<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

/*****************************************************************************************
**											**
*****************************************************************************************/

$ruta_raiz = "..";
session_start();
if($_SESSION["usua_perm_backup"]!=1) die("");

include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();
?>
<body>
<center>
<form name='frmMnuUsuarios' action='../Administracion/usuarios/mnuUsuarios.php' method="post">
  <br><br>
  <table width="32%" align="center" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
  <tr>
    <td class="listado2" width="98%"><a href="backup_usuarios_solicitar.php" class="vinculos" target='mainFrame'>1. Solicitar respaldos de documentos de usuarios</a></td>
  </tr>
  <tr>
    <td class="listado2" width="98%"><a href="backup_usuarios_estado.php" class="vinculos" target='mainFrame'>2. Verificar estado de los respaldos</a></td>
  </tr>
  <tr>
    <td class="listado2" width="98%"><a href="backup_usuarios_ejecutar.php" class="vinculos" target='mainFrame'>3. Ejecutar proceso para respaldar documentos</a></td>
  </tr>
  <tr>
    <td align="center" class="listado2">
      <center><input align="middle" class="botones" type="submit" name="Submit" value="Regresar"></center>
    </td>
  </tr>
</table>
</form>
</center>
</body>
</html>
