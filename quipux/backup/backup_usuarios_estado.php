<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------

**************************************************************************************
** Verifica el estado de los respaldos                                              **
** Muestra un informe del estado de los respaldos, permite descargar el respaldo    **
** si es que ya finalizó y eliminarlos.                                             **
**                                                                                  **
** Desarrollado por:                                                                **
**      Mauricio Haro A. - mauricioharo21@gmail.com                                 **
*************************************************************************************/

$ruta_raiz = "..";
include_once "$ruta_raiz/config.php";
include_once "$ruta_raiz/include/db/ConnectionHandler.php";
include_once "$ruta_raiz/funciones_interfaz.php";

$db = new ConnectionHandler("$ruta_raiz");
$db->conn->SetFetchMode(ADODB_FETCH_ASSOC);

echo "<html>".html_head();
include_once "$ruta_raiz/js/ajax.js";
$paginador = new ADODB_Pager_Ajax($ruta_raiz, "div_buscar_respaldos", "backup_usuarios_estado_buscar.php", "cmb_institucion","");

?>
<script language="JavaScript" type="text/JavaScript">
    function eliminar_respaldo(codigo) {
        if (confirm ('¿Desea eliminar el respaldo?')) {
            nuevoAjax('div_eliminar_respaldo', 'POST', 'backup_usuarios_eliminar.php', 'txt_resp_codi=' + codigo);
        }
        buscar_respaldos();
    }
    function buscar_respaldos() {
        cmb_institucion = document.getElementById('cmb_institucion').value;
        nuevoAjax('div_buscar_respaldos', 'GET', 'backup_usuarios_estado_buscar.php', 'cmb_institucion=' + cmb_institucion);
    }

    function descargar_respaldo(codigo) {
        windowprops = "top=50,left=50,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=700,height=500";
        url = 'backup_usuarios_descargar_archivos.php?codigo_backup=' + codigo;
        ventana = window.open(url , "descargar_respaldo_" + codigo, windowprops);
        ventana.focus();
    }

</script>
<body>
  <center>
    <form name="formulario" action="" method="post">
        <table width="100%" align="center" class=borde_tab border="0">
            <tr>
                <td width="100%" class="titulos5">
                  <center>
                    <br>Estado de los respaldos<br>&nbsp;
                  </center>
                </td>
            </tr>
        </table>
        <br>
        <table width="100%" align="center" class=borde_tab border="0">
            <tr>
                <td width="25%" class="titulos5">
                    <br>Instituci&oacute;n:<br>&nbsp;
                </td>
                <td width="50%" class="listado5" valign="middle">
<?
                $sql="select inst_nombre, inst_codi from institucion where inst_codi<>0 order by 1 asc";
                $rs=$db->conn->query($sql);
                if($rs) print $rs->GetMenu2("cmb_institucion", 0, "0:&lt;&lt; Todas las instituciones &gt;&gt;", false,"","class='select' id='cmb_institucion'" );
?>
                </td>
                <td width="25%" class="titulos5" valign="middle">
                    <center><input type='button' value='Buscar' name='btn_buscar' class='botones' onClick='buscar_respaldos();'></center>
                </td>
            </tr>
        </table>
        <div id='div_eliminar_respaldo'></div>
        <div id='div_buscar_respaldos'></div>
        <div id='div_descargar_respaldo'></div>
        <br>
        <input type="button" name="btn_cancelar" value="Regresar" class="botones" 
               onClick="window.location='backup_usuarios_menu.php';">
      </form>
      <script  language="JavaScript" type="text/JavaScript">buscar_respaldos();</script>
    </center>
  </body>
</html>
