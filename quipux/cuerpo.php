<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


$ruta_raiz = ".";
session_start();
include_once "$ruta_raiz/rec_session.php";

require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());


include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();

if ($_SESSION["depe_codi"]==0 and $_SESSION["tipo_usuario"]==1) {
    die ("<br/><center><font size='3' color='blue'><b>Su usuario no tiene definida un &Aacute;rea.<br />Por favor comun&iacute;quese con el administrador del sistema. </b></font>");
}

if ($version_light && $carpeta==12) {
//    die ("<br/><center><font size='3' color='blue'><b>Lo sentimos, esta funcionalidad se encuentra actualmente en mantenimiento.<br>Por favor vuelva a intentarlo m&aacute;s tarde. </b></font>");
}

$carpeta = 0 + $_GET["carpeta"];

$nomcarpeta =$_GET['nomcarpeta'];
//$slc_tipo_fecha = 0 +$_GET['slc_tipo_fecha'];
$noLeidos=0+$_GET['noLeidos'];

if ($carpeta==0) $nomcarpeta="Nuevo";
if ($_SESSION["tipo_usuario"]==2 && $carpeta == 0) {
    $carpeta = 80;
    $nomcarpeta = "Documentos Enviados";
}

//paginador
$paginador = new ADODB_Pager_Ajax($ruta_raiz, "div_cuerpo", "cuerpo_paginador.php",
                                  "txt_fecha_desde,txt_fecha_hasta,estado,busqRadicados,tipoLectura,slc_tarea_estado","carpeta=$carpeta");

?>
<script type="text/javascript">

   /*function vista_previa1(radicado,texto) {
            windowprops = "top=100,left=100,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=500,height=300";
            url = '<?=$ruta_raiz?>/VistaPrevia.php?verrad='+radicado+'&archivo=&textrad='+texto;
            window.open(url , "Vista_Previa_"+texto, windowprops);
            return;
   }*/

    function vista_previa1(radicado, path, texto) {
        if(path!="")
        {
            var nomArchivo = path.split(".");
            var pathPDF = nomArchivo[0] + "." + nomArchivo[1];
        }
        else
            pathPDF = "";
        //window.open('<?=str_replace('.p7m','',$path_descarga)?>','_self','');
        var x = (screen.width - 10) / 2;
        var y = (screen.height - 10) / 2;
        windowprops = "top=100,left=100,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=10,height=10";
        URL = '<?=$ruta_raiz?>/VistaPrevia.php?verrad=' + radicado + '&archivo=' + pathPDF + '&textrad=' + texto;
        //alert(URL);
        ventana = window.open(URL , "Vista_Previa_" + radicado, windowprops);
        ventana.moveTo(x, y);
        ventana.focus();
    }

    function cambiar_contador() {
        try {
            contador = document.getElementById('txt_contador').value;
            noleidos = document.getElementById('txt_noleidos').value;
            if (noleidos!='')
            parent.leftFrame.cambiar_contador('<?=$carpeta?>',contador,noleidos);
        else
             parent.leftFrame.cambiar_contador('<?=$carpeta?>',contador,'<?=$noLeidos?>');
        } catch (e) {
            timerID = setTimeout("cambiar_contador()", 1000);
        }
    }
    function fecha_ver(){
    if (document.form1.txt_fecha_hasta.value!=''){
        if (document.form1.txt_fecha_hasta.value!=''){           
           if(compare_dates(document.form1.txt_fecha_desde.value, document.form1.txt_fecha_hasta.value) == true){
                alert("Fecha "+document.form1.txt_fecha_hasta.value+" no puede ser menor a "+document.form1.txt_fecha_desde.value)
                document.form1.txt_fecha_hasta.value='';
            }
            else{
               numeroCaracteres=<?=$numeroCaracteresTexto?>;
        
                if (document.getElementById('txt_nombre_texto_error').value=='')
                   paginador_reload_div('');
                else
                  alert("Se requiere más información en los campos ingresados, debe ser al menos "+numeroCaracteres+ " caracteres");
                }
            }
       
    }//si tiene fechas
    else{
        alert("Ingresa Fechas")
        }
    }
   
    function compare_dates(fecha, fecha2)
  {
    var xMonth=fecha.substring(5, 7);//primer mes
    var xDay=fecha.substring(8, 10);
    var xYear=fecha.substring(0,4);//primer año
    var yMonth=fecha2.substring(5, 7);
    var yDay=fecha2.substring(8, 10);
    var yYear=fecha2.substring(0,4);//segundo año
   
            
    if (xYear> yYear)
    {
        
        return(true)
    }
    else
    {
      if (xYear == yYear)
      {
        if (xMonth> yMonth)
        {            
            return(true)
        }
        else
        {
          if (xMonth == yMonth)
          {
            if (xDay> yDay)
              return(true);
            else
              return(false);
          }
          else{
            return(false);
          }
        }
      }
      else
        return(false);
    }
}
function pulsar(e) {
  tecla = (document.all) ? e.keyCode : e.which;
  if (tecla==13) {
    fecha_ver();
    // aquí el código que quieras ejecutar
  }
}

/*if(compareDate(document.form1.txt_fecha_desde.value, document.form1.txt_fecha_hasta.value) == -1)
            {
                alert("La fecha fin del hito no puede ser mayor a la fecha termino del proyecto");
                document.form1.txt_fecha_hasta.style.borderColor='#FF0000';
                document.form1.txt_fecha_hasta.style.backgroundColor='#FFFccc';
                document.form1.txt_fecha_hasta.focus();
                return false;
            }
            else
        paginador_reload_div('');
        */
    //}
<?
 $sql ="select * from carpeta order by carp_orden asc";
	$rs = $db->query($sql);

require_once("$ruta_raiz/pestanas.js");


?>

function llamarListado(nombreCarpeta, codigoCarpeta){
     location.href= 'cuerpo.php?nomcarpeta='+nombreCarpeta+'&carpeta='+codigoCarpeta+'&adodb_next_page=1';
     document.getElementById('btn_Buscar').focus();
}

function init() {

    var nomCarpeta = ""; //Nombre de la bandeja que esta en la base de datos
    var codCarpeta = ""; //Codigo de la bandeja que esta en la base de datos (Primary Key)
    shortcut.add("Alt+b", function() {
        nomCarpeta = "En Elaboración";
        codCarpeta = "1";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+r", function() {
        nomCarpeta = "Recibidos";
        codCarpeta = "2";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+c", function() {
        nomCarpeta = "Eliminados";
        codCarpeta = "6";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+n", function() {
        nomCarpeta = "No Enviados";
        codCarpeta = "7";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+e", function() {
        nomCarpeta = "Enviados";
        codCarpeta = "8";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+p", function() {
        nomCarpeta = "Reasignados";
        codCarpeta = "12";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+a", function() {
        nomCarpeta = "Archivados";
        codCarpeta = "10";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+i", function() {
        nomCarpeta = "Informados";
        codCarpeta = "13";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+t", function() {
        nomCarpeta = "Tareas Recibidas";
        codCarpeta = "15";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    shortcut.add("Alt+s", function() {
        nomCarpeta = "Tareas Enviadas";
        codCarpeta = "16";
        llamarListado(nomCarpeta, codCarpeta)
        window.top.window.leftFrame.cambioMenuAsociado(nomCarpeta);
    });
    
    //Acciones

    shortcut.add("Ctrl+Shift+c", function() {//eliminar
                changedepesel(2);
    });
    shortcut.add("Ctrl+Shift+p", function() {//reasignar
                changedepesel(9);
    });
    shortcut.add("Ctrl+Shift+i", function() {//informar
                changedepesel(8);
    });
    shortcut.add("Ctrl+Shift+e", function() {//firmar/enviar
                changedepesel(11);
    });
    shortcut.add("Ctrl+Shift+m", function() {//comentar
                changedepesel(18);
    });
    //shortcut.add("Ctrl+Shift+f", function() {//dar fisico
      //          changedepesel(69);
    //});
    shortcut.add("Ctrl+Shift+r", function() {//restaurar
                changedepesel(6);
    });
    shortcut.add("Ctrl+Shift+b", function() {//editar
                changedepesel(16);
    });
    shortcut.add("Ctrl+Shift+v", function() {//regresar
                javascript:history.back();
    });
    shortcut.add("Ctrl+Shift+a", function() {//archivar
                changedepesel(13);
    });
    shortcut.add("Ctrl+Shift+t", function() {//nueva tarea
                changedepesel(30);
    });
    shortcut.add("Ctrl+Shift+f", function() {//envio fisico
                changedepesel(69);
    });
    shortcut.add("Ctrl+Shift+d", function() {//envio digital
                changedepesel(4);
    });
    shortcut.add("Ctrl+Shift+s", function() {//sobre
                changedepesel(70);
    });
}
window.onload=init();

</script>

<script type='text/JavaScript' src='<?=$ruta_raiz?>/js/shortcut.js'></script>

</head>
<style type="text/css">

/*Tool Tip*/
a.Ntooltip {
position: relative; /* es la posición normal */
text-decoration: none !important; /* forzar sin subrayado */
color:#0080C0 !important; /* forzar color del texto */
font-weight:bold !important; /* forzar negritas */
}

a.Ntooltip:hover {
z-index:999; /* va a estar por encima de todo */
background-color:#000000; /* DEBE haber un color de fondo */
}

a.Ntooltip span {
display: none; /* el elemento va a estar oculto */
}

a.Ntooltip:hover span {
display: block; /* se fuerza a mostrar el bloque */
position: absolute; /* se fuerza a que se ubique en un lugar de la pantalla */
top:1em; left:1em; /* donde va a estar */
width:70px; /* el ancho por defecto que va a tener */
padding:5px; /* la separación entre el contenido y los bordes */
background-color: #E0ECF8; /* el color de fondo por defecto */
color: #000000; /* el color de los textos por defecto */
}
/*Tool Tip*/
</style>
<?php
$txt_fecha_desde = date("Y-m-d", strtotime(date("Y-m-d")." - 30 day"));
$txt_fecha_hasta = date("Y-m-d");

?>
<script language="JavaScript" type="" >
    <? include_once "$ruta_raiz/js/spiffyCal/spiffyCal_v2_2.js";?>
    var dateAvailable1 = new ctlSpiffyCalendarBox("dateAvailable1", "form1", "txt_fecha_desde","btnDate1","<?=$txt_fecha_desde?>",scBTNMODE_CUSTOMBLUE);
    var dateAvailable2 = new ctlSpiffyCalendarBox("dateAvailable2", "form1", "txt_fecha_hasta","btnDate2","<?=$txt_fecha_hasta?>",scBTNMODE_CUSTOMBLUE);
    dateAvailable1.dateFormat="yyyy-MM-dd";
    dateAvailable2.dateFormat="yyyy-MM-dd";
</script>


<body  topmargin="0" onLoad="window_onload(); paginador_reload_div('');cambiar_contador();">
     <div id="spiffycalendar" class="text"></div>
<?
   include_once "./obtenerdatos.php";
//   include "./envios/paEncabeza.php";
   include_once "$ruta_raiz/funciones.php";


    
    ?>
<form name="form1" id="form1" action="./tx/formEnvio.php?<?=$encabezado?>" method="POST">
    <table width="100%" class="borde_tab" border="0" >
        <input type="hidden" name="txt_nombre_texto_error" id="txt_nombre_texto_error" class="tex_area" value=""/>
        <tr <?php if($carpeta != 12) echo "style='display:none'"?>>
           <?php
           /*<td class="listado2">Fecha de:
                <select name="slc_tipo_fecha" id="slc_tipo_fecha" class="select">
                    <option value="0">Documento</option>
                    <option value="1" selected>Reasignación</option>
                    <option value="2">Respuesta</option>
                </select>
            </td>*/
           ?>
            <td width="15%" class="titulos2">Desde Fecha (yyyy/mm/dd):</td>            
            <td width="20%" class="listado2">
                <script type="text/javascript">
                    dateAvailable1.writeControl();
                </script>
            </td>
            <td width="15%" class="titulos2">Hasta Fecha (yyyy/mm/dd):</td>
            <td width="20%" class="listado2">
                <script type="text/javascript">
                    dateAvailable2.writeControl();
                </script>
            </td>
            <td width="10%" class="titulos2">Estado</td>
            <td width="20%" class="listado2">
		<?php
                //Combo para generar estado del documento.
                $sql_estado="SELECT ESTA_DESC, ESTA_CODI FROM ESTADO ORDER BY 1";                
                $rsE = $db->query($sql_estado);
                echo $rsE->GetMenu2("estado", -1, "-1:&lt;&lt Todos &gt;&gt;", false,""," id='estado' class='select'" );
               // echo $rs->GetMenu2("txt_tipo_documento", "$txt_tipo_documento", "0:&lt;&lt Todos &gt;&gt;", false,"","class='select' id ='txt_tipo_documento'");
                ?>
            </td>
            <td rowspan="2">
                <input type=button value='Buscar' name=Buscar class='botones' title="Busca el texto ingresado en: Numero Documento, Asunto, No. Referencia y Fecha" onclick="fecha_ver();">
            </td>
        </tr>
       
        <tr <?php         
        if($carpeta != 15 and $carpeta != 16) echo "style='display:none'"?>>            
            <td class="titulos2" width="15%">Estado</td>
            <td class="listado2" width="20%" colspan="4">
                <select name="slc_tipo_tarea" id="slc_tarea_estado" class="select">
                    <option value="0" <? if ($carpeta==15 or $carpeta==16) echo "selected"; ?>>Todos</option>
                    <option value="1" >Pendiente</option>
                    <option value="2">Finalizado</option>
                    <option value="3">Cancelado</option>
                </select>
            </td>        
            <td></td>
            
        </tr>
        <tr>
            <td width="15%" class="titulos2">Texto a Buscar</td>
            <?php if ($carpeta==14){?>
            <td width="50%" colspan="4">
            <?php } else{?>
                <td width="80%" colspan="4">
            <?php }?>
                <!---    Buscar <?=$_SESSION["descRadicado"]?>(s) (Separados por coma) -->                
<!--                <input name="busqRadicados" id="busqRadicados" type="text" size="40" class="tex_area" value=""  onkeypress = "pulsar(event)">-->
                    <?php echo cajaTextoValida('busqRadicados','',$numeroCaracteresTexto,"onkeypress=evento_ver(event,this,$numeroCaracteresTexto,'busqRadicados',1)")?>
            Asunto, Número de Documento, Número de Referencia</td>
            <?php if ($carpeta==12){?>
            <td></td>
            
            <?php } ?>
            <?php if ($carpeta!=12){?>
            <td></td>
            
            <?php }?>
            
    <?if ($carpeta == 14) //Carpeta compartida
    {
        $datosJefe = ObtenerDatosUsuario($_SESSION['usua_codi_jefe'],$db);
        echo "<td width='60%'><table border=0 width='100%' class='borde_tab' cellspacing='0'>
                <tr>
                    <td align='center' class='listado2'>
                        <font size='2'> Bandeja de Documentos Recibidos de ". $datosJefe['nombre']."</font>
                    </td>
                </tr>
              </table></td>";
    }
    ?>
         <?php if($carpeta != 12){ ?>
            <td rowspan="2">
                <input id="btn_Buscar" type=button value='Buscar' name=Buscar valign='middle' class='botones' title="Busca el texto ingresado en: Numero Documento, Asunto, No. Referencia y Fecha" onclick="fecha_ver();">
            </td>
            <?php } ?>
        </tr>
    </table>
    <?php //antes de link javascript
    /*if ($_SESSION["tipo_usuario"]!=2){ ?>
<a target='mainFrame'  onclick="cambioMenu(<?=$num?>);"
   href="busqueda/busqueda.php" class="aqui">B&uacute;squeda Avanzada</a>
   <?php } */
    ?>
    <?php if ($_SESSION["tipo_usuario"]!=2){ 
        $parametros="'busqueda/busqueda.php'";
        ?>
<a target='mainFrame'  onclick="llamaCuerpo(<?=$parametros?>)"
   href="javascript:;" class="aqui">B&uacute;squeda Avanzada</a>
   <?php } ?>
                        
<?

        if(!$tipo_carp) $tipo_carp = "0";

        $controlAgenda=1;
        $estado=0;
        if ($carpeta==99) $estado=5;
        if ($carpeta==80) $estado=100;
        if ($carpeta==81) $estado=100;
        if ($carpeta==1) $estado=1;
        if ($carpeta==2) $estado=2;
        if ($carpeta==6) $estado=7;
        if ($carpeta==8) $estado=6;
        if ($carpeta==7) $estado=3;
        if ($carpeta==12) $estado=12;
        if ($carpeta==13) $estado=13;
        if ($carpeta==14) $estado=14;
        if ($carpeta==15) $estado=15;
        if ($carpeta==16) $estado=16;
        if ($carpeta==82) $estado=82;
        if ($carpeta==83) $estado=83;
        if ($carpeta==84) $estado=84;
        if ($carpeta==85) $estado=85;
        if ($carpeta==86) $estado=86;
        if ($carpeta==87) $estado=87;
        if ($carpeta==90) $estado=90;

        // Parte LISTAR POR
        include "./tx/txOrfeo.php";


?>
    <center>
    <div id="div_cuerpo"></div>
    </center>
</form>
</body>
</html>
<script type="text/javascript">
function llamaCuerpo(parametros){    
    top.frames['mainFrame'].location.href=parametros;

}
</script>