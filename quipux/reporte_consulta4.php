<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
session_start();
$datos=$_SESSION['d_reporte'];
$ban=$_SESSION['ban'];
$queryE=$_SESSION['queryE'];
/*echo '<pre>';
var_dump($_SESSION['queryE'].'<br>');
var_dump('detalle'.$_SESSION['queryEDetalle']);
echo '</pre>';*/

$ruta_raiz = ".";
include "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

unset($rs1);
unset($usuadoc);
unset($doc);
unset($hojas);

$rs1 = $db->query($queryE);
while(!$rs1->EOF)  {
	$usuadoc[] = $rs1->fields["USUARIO"]; 
	$doc[] = $rs1->fields["RADI_REGI"]; 
	$hojas[] = $rs1->fields["RADI_DIGI"];
	$_SESSION['usuadoc']=$usuadoc;
	$_SESSION['doc']=$doc;
	$_SESSION['hojas']=$hojas;
	$rs1->MoveNext();
}

	$lon=count($usuadoc);
	
	$inicio = '
	<html>
	<head>
	<title>.: ORFEO - VISTA PREVIA :.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body style="margin: 60 60 60 60;">
	';
	
	$encabezadopdf = '
	<table align="center">
	<tr><th align="center"><font size="4">PRESIDENCIA DE LA REPUBLICA</th></tr>
	<tr><td></td></tr>
	<tr><td></td></tr>
	<tr><td></td></tr>
	<tr><td><center><!--img src="logoEntidad.gif" width=5 height=5 /--></center><td></tr>
	</table>
	<br />
	<br />
	<br />
	<table align="left">
		<tr><th align="left">Título: Número de documentos digitalizados por usuario</th></tr>
	</table>
	<br />
	';
	
	if(empty($datos[4])){
		$infor='
		<table width="100%">
		<tr>
			<th align="left">Nombre del área:</th>
			<td>'.$datos[1].'</td>
		</tr>
		<tr>
			<td align="left" size="5%"><b>Fecha(desde):</b></td>
			<td align="left">'.$datos[2].'</td>
			<td align="right"><b>Fecha(hasta):</b></td>
			<td align="right">'.$datos[3].'</td>
		</tr>
		<br />
		<br />
		<br />
		</table>
		';
		$cuerpo=
		'<table border="0" width="100%">
		<tr><th><font size="2">USUARIO</font></Th>
		<th><font size="2">DOC. REGISTRADOS</font></Th>
		<th><font size="2">DOC. DIGITALIZADOS</font></Th></tr>';
		for ($i=0;$i <=$lon;$i++ ){
		$cuerpo2 .= '<tr><td><font size="2">'.$usuadoc[$i].'</font></td>';
		$cuerpo2 .= '<td align="center"><font size="2">'.$doc[$i].'</font></td>';
		$cuerpo2 .= '<td align="center"><font size="2">'.$hojas[$i].'</font></td></tr>';
		}
		$cuerpo .= $cuerpo2.'</table>';
		;
		

		}elseif(!empty($datos[1]) && !empty($datos[4])){
			$infor='
			<table align="left" width="100%">
			<tr>
				<th align="left">Nombre del usuario:</th>
				<td>'.$datos[4].'</td>
			</tr>
			<tr>
				<th align="left">Nombre del área:</th>
				<td>'.$datos[1].'</td>
			</tr>
			<tr>
				<td align="left" size="5%"><b>Fecha(desde):<b></td>
				<td align="left">'.$datos[2].'</td>
				<td align="right"><b>Fecha(hasta):<b></td>
				<td align="right">'.$datos[3].'</td>
			</tr>
			<br />
			<br />
			<br />
			</table>
			';
	
			$cuerpo=
			'<table border="0" width="100%">
			<tr><th><font size="2">DOC. REGISTRADOS</font></Th>
			<th><font size="2">DOC. DIGITALIZADOS</font></Th></tr>';
			for ($i=0;$i <=$lon;$i++ ){
			$cuerpo2 .= '<tr><td align="center"><font size="2">'.$doc[$i].'</font></td>';
			$cuerpo2 .= '<td align="center"><font size="2">'.$hojas[$i].'</font></td></tr>';
			}
			$cuerpo .= $cuerpo2.'</table>';
			;

		
		}

	
	$fin = '
	</body>
	</html>
	';
	

//echo $inicio.$encabezadopdf.$infor.$cuerpo.$fin;

	//GENERACION DEL PDF
	require_once("./js/dompdf/dompdf_config.inc.php");
	
	$dompdf = new DOMPDF();
	$dompdf->load_html($inicio.$encabezadopdf.$infor.$cuerpo.$fin);
	$dompdf->set_paper("a4", "portrait");
	$dompdf->set_base_path(getcwd());
	$dompdf->render();
	
	//$nombarch = "/". substr($verrad,0,4)."/".substr($verrad,4,3)."/".$registro["textrad"].".pdf";
	
	//file_put_contents("$ruta_raiz/bodega".$nombarch, $dompdf->output());
	$dompdf->stream("Documentos_digitalizados_por_Usuarios.pdf");
	
	//exec ("mv /tmp/$nombarch /var/www/orfeo/bodega/2008/900/$nombarch",$output,$returnS);
	//$sql = "UPDATE RADICADO SET RADI_PATH='$nombarch' where radi_nume_radi=$verrad";
	//$rs = $db->query($sql);	
	//echo "<script>window.close();</script>";
?>