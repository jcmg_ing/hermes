<?php

/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


session_start();
if(empty($ruta_raiz)) $ruta_raiz = "..";

?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?

unset($query1);
unset($instit);
unset($de);
unset($para);

unset($nomareaP);
unset($asunto);

unset($fecha);


unset($numdoc);
unset($referencia);
unset($areaAnte);
//unset($estado);
//unset($tipodoc);

$queryE=$_SESSION['queryE'];
$titulos=$_SESSION['$titulos'];


$datos=$_SESSION['d_reporte'];
$area=$_SESSION['$area'];
$nomInst=$_SESSION['$NombInsti'];

include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

$query1= $db->query($queryE);
$CONTCOL=0;





while(!$query1->EOF)  {

    $instit[] = $query1->fields["INSTITUTO"];
    $de[] = $query1->fields["DE"];
    $nomareaP[] = $query1->fields["NOMBREAREA_PARA"];
    $para[] = $query1->fields["PARA"];
    $asunto[] = $query1->fields["ASUNTO"];
    $fecha[] = $query1->fields["DAT_FECHA_DOCUMENTO"];
    $numdoc[] = $query1->fields["NUMERO_DOCUMENTO"];
    $referencia[] = $query1->fields["NO_REFERENCIA"];
    //$tipodoc[] = $query1->fields["TIPO_DOCUMENTO"];
    $areaAnte = $query1->fields["NOMAREA"];
    $query1->MoveNext();
    $CONTCOL+=1;
}

	$inicio = '
	<html>
	<head>
        <title>.: QUIPUX - VISTA PREVIA :.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body style="margin: 30 20 50 20;">
	';


	$encabezadopdf = '
	<table align="center">

	<tr><td></td></tr>
	<tr><td><center><img src="logoEntidad.gif" width=5 height=5 /></center><td></tr>
	</table>
	<br />
	<table align="center">
		<tr><th align="center">Documentos Externos Ciudadano</th></tr>
	</table>
	<br />
	';


	if(!empty($CONTCOL)){
		$infor='
		<table width="100%">
		<tr>
			<th align="left">Institución:</th>
			<td>'.$nomInst.'</td>
        </tr>
		<tr>
			<td align="left" size="5%"><b>Fecha Desde:</b></td>
			<td align="left">'.$datos[5].'</td>
			<td align="right"><b>Hasta:</b></td>
			<td align="right">'.$datos[6].'</td>
		</tr>
		<br />
		</table>';


		$cuerpo1='<HR><table border="0" width="100%" align="center"><tr>';
        for ($j=1;$j<=count($_SESSION['$titulos']);$j++){
                $cuerpo1.='<th><font size="1"> '.substr($titulos[$j],2).'</font></Th>';
        }

        $cuerpo1.='</tr>';
        $cuerpo=$cuerpo1;


		for ($i=0;$i <=$CONTCOL;$i++ ){
            $cuerpo2 .= '<tr><td><font size="1">'.$instit[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$de[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$nomareaP[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$para[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$asunto[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$fecha[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$numdoc[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$referencia[$i].'</font></td>
            </tr>';
		}
 $cuerpo .= $cuerpo2.'</table>';
}

$fin = '
</body>
</html>';

//ECHO $cuerpo;



//GENERACION DEL PDF
include "$ruta_raiz/config.php";
require_once("$ruta_raiz/interconexion/generar_pdf.php");
$plantilla = "";
$plantilla = "../bodega/plantillas/".$_SESSION['uno'].".pdf";
$doc_pdf=$inicio.$encabezadopdf.$infor.$cuerpo.$fin;
$pdf = ws_generar_pdf($doc_pdf, $plantilla, $servidor_pdf);
$nomArch="Reporte_DocumentoExterno_Ciudadano.pdf";
header( "Content-Disposition: attachment; filename=$nomArch");
header("Content-Type:application/pdf");//.application/pdf
header("Content-Transfer-Encoding: binary");
echo  $pdf;




/*
//GENERACION DEL PDF
//	$ruta_raiz=".";
	require_once("./js/dompdf/dompdf_config.inc.php");

	$dompdf = new DOMPDF();
	$dompdf->load_html($inicio.$encabezadopdf.$infor.$cuerpo.$fin.$piePagina);
	$dompdf->set_paper("a4", "portrait");
	$dompdf->set_base_path(getcwd());
	$dompdf->render();

	//$nombarch = "/". substr($verrad,0,4)."/".substr($verrad,4,3)."/".$registro["textrad"].".pdf";

	//file_put_contents("$ruta_raiz/bodega".$nombarch, $dompdf->output());
	$dompdf->stream("Reporte_DocumentosIngresadosporUsuario.pdf");

	//exec ("mv /tmp/$nombarch /var/www/orfeo/bodega/2008/900/$nombarch",$output,$returnS);
	//$sql = "UPDATE RADICADO SET RADI_PATH='$nombarch' where radi_nume_radi=$verrad";
	//$rs = $db->query($sql);
	//echo "<script>window.close();</script>";

*/
?>

