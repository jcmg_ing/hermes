<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
$ruta_raiz = "..";

session_start();
include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

include_once "$ruta_raiz/obtenerdatos.php";
$area = ObtenerDatosDependencia($_SESSION["depe_codi"],$db);

if (ISSET($_GET['tipoenvio']) && ($_GET['tipoenvio']==88)) {
    $radi_nume=$_GET['verrad'];
    $depe_nombInst=$_GET['area'];
    
    $responsable1=$_GET['responsable'];
    $fechaReg1=$_GET['FechaReg'];
    $fechaReg=date('Y-m-d H:i:s', strtotime($fechaReg1));
    $observacion=$_GET['comentario'];
    $usCodDestino=$_GET['usuaDest'];
    $estado=$_GET['estado'];
    $enviado_Por=$_GET['usua_enviaPor'];



    $isql1 = "select radi_asunto,ver_usuarios(radi_usua_rem,',') as Remitente,
             radi_usua_radi as CodRegistradoPor,radi_nume_text,radi_cuentai,radi_fech_radi,radi_fech_ofic
             from radicado where radi_nume_radi='$radi_nume'" ;
    $query1= $db->query($isql1);

    while(!$query1->EOF)  {
        $radi_asunto=$query1->fields["RADI_ASUNTO"];
        $radi_remitente=$query1->fields["REMITENTE"];
        $codRegistrPor=$query1->fields["CODREGISTRADOPOR"];
        $radi_nume_text=$query1->fields["RADI_NUME_TEXT"];
        $referencia=$query1->fields["RADI_CUENTAI"];
        $fechacreacion=$query1->fields["RADI_FECH_RADI"];
        $fechaenvio=$query1->fields["RADI_FECH_OFIC"];
        $query1->MoveNext();
    }

    $isql ="select usua_nombre from usuario where usua_codi=$codRegistrPor";
    $query1= $db->query($isql);
    $registrPor=$query1->fields["USUA_NOMBRE"];

    //Area
    $usCodDestino1=ltrim($usCodDestino);
    $isql ="select depe_nomb from usuario where usua_nombre like '$usCodDestino1' and usua_esta=1 and tipo_usuario=1 and inst_codi=". $_SESSION[inst_codi];
    $query1= $db->query($isql);
    $depe_nomb=$query1->fields["DEPE_NOMB"];

    
     //Area "Enviado por"
    $isql="select depe_nomb from usuario where usua_codi=".$_SESSION["usua_codi"]." and usua_esta=1 and tipo_usuario=1 and inst_codi=". $_SESSION[inst_codi];
    
    $query1= $db->query($isql);
    $depe_nombInst=$query1->fields["DEPE_NOMB"];
    


    $isql ="select usua_nombre from usuario where usua_codi=$enviado_Por";
    $query1= $db->query($isql);
    $enviadoPor=$query1->fields["USUA_NOMBRE"];

    $responsable=strtolower($responsable1);
    $responsable=ucwords($responsable);

}ELSE{    
    $fechaReg=date("Y-m-d H:i:s");
    $usCodDestino=$_SESSION['$usrEnviadoa'];

    $responsable1=$_GET['responsable'];
    $observacion=$_GET['comentario'];

    //Obtengo el nombre del usuario enviado por
    
    $queryE="select  usua_nomb||' '||usua_apellido as \"enviadoPor\"  from usuarios where usua_codi=".$_SESSION["usua_codi"];
    $query1= $db->query($queryE);
    $enviadoPor=$query1->fields["ENVIADOPOR"];


    //Obtengo el nombre del usuario registrado por
    //foreach($_SESSION['$radicadosUserActu'] as $radi_usua_actu) {
    //    $queryE="select  usua_nomb||' '||usua_apellido as \"registradoPor\"  from usuarios where usua_codi=".$radi_usua_actu;
    //    $query1= $db->query($queryE);
     //   $registrPor=$query1->fields["REGISTRADOPOR"];
        
   // }

     //Area "Recibido por"
    $usCodDestino1=ltrim($usCodDestino);
    $isql ="select depe_nomb from usuario where usua_nombre like '$usCodDestino1' and usua_esta=1 and tipo_usuario=1 and inst_codi=". $_SESSION[inst_codi];
    $query1= $db->query($isql);
    $depe_nomb=$query1->fields["DEPE_NOMB"];


    //Area "Enviado por"
    $isql="select depe_nomb from usuario where usua_codi=".$_SESSION["usua_codi"]." and usua_esta=1 and tipo_usuario=1 and inst_codi=". $_SESSION[inst_codi];
    $query1= $db->query($isql);
    $depe_nombInst=$query1->fields["DEPE_NOMB"];
    

    //Armo estado del documento
    if ($_SESSION['$estado']=='B'){
     $estado="Bueno";
    }
    elseif ($_SESSION['$estado']=='M'){
     $estado="Malo";
    }
    elseif ($_SESSION['$estado']=='R'){
     $estado="Regular";
    }

   

    //Asigno Variables
    //$responsable1=$_SESSION['$responsable'];//$_POST['nombre'];
    $responsable=strtolower($responsable1);
    $responsable=ucwords($responsable);

    //$observacion=$_SESSION['$observacion'];

}

    if(strlen($observacion) > 70) {  // comprobamos que el texto tiene mas de 70 caracteres
        $pos = strpos($observacion, "/");
        $observacion = substr($observacion,0,$pos);
        $observacionJustificado = wordwrap($observacion,70,"<br />\n",true);  }
    else{
        $pos = strpos($observacion, "/");
        $observacion = substr($observacion,0,$pos);       
        $observacionJustificado=$observacion;
    }

if($referencia==""){
    $referencia="-------";
}


//Armo HTML
$inicio = '
<html>
<head>
<title>.: QUIPUX - VISTA PREVIA :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="margin: 5 20 30 5;">
';



$encabezadopdf = '<br>
	<table align="center">
		<b><tr><th align="center"><font size="6">Traspaso de documentos físicos</th></tr></b>
	</table>
	<br />
	';

$titulo='<table width="100%" >
 <tr>
    <td>Información del documento</td>
</tr>
</table>';

if (ISSET($_GET['tipoenvio']) && ($_GET['tipoenvio']==88)) {
    $cuerpo1='<table border="1" width="100%" >
    <tr>
    <td>No.documento:</td>
    <td>'.$radi_nume_text.'</td>
    <td>Referencia:</td>
    <td>'.$referencia.'</td>
    </tr>
    <tr>
    <td colspan="1">Remitente:</td>
    <td colspan="3">'.$radi_remitente.'</td>
    </tr>

    <tr>
    <td colspan="1">Asunto:</td>
    <td colspan="3">'.$radi_asunto.'</td>
    </tr>

    <tr>
    <td colspan="1">Registrado por:</td>
    <td colspan="3">'.$registrPor.'</td>
    </tr>

    <tr>
    <td colspan="1">Fecha de Creación:</td>
    <td colspan="3">'.substr($fechacreacion,0,16).'</td>
    </tr>

    <tr>
    <td colspan="1">Fecha de Envío:</td>
    <td colspan="3">'.substr($fechaenvio,0,16).'</td>
    </tr>
    </table>';

}ELSE{
    if ($_GET['verrad']!='')
        $sql = "select radi_fech_radi, radi_fech_ofic, radi_tipo,radi_usua_radi  from radicado where radi_nume_radi =".$_GET['verrad'];
    $rsql= $db->query($sql);
    $fecha_Ofic=$rsql->fields["RADI_FECH_OFIC"];
    $fecha_registro=$rsql->fields["RADI_FECH_RADI"];
    $radi_tipo_doc=$rsql->fields["RADI_TIPO"];
    $codRegistrPor = $rsql->fields["RADI_USUA_RADI"];
    $isql ="select usua_nombre from usuario where usua_codi=$codRegistrPor";
    $query1= $db->query($isql);
    $registrPor=$query1->fields["USUA_NOMBRE"];
    foreach($_SESSION['$radicado'] as $radi_nume) {
    foreach($_SESSION['$radicadoText'] as $radi_nume_text) {
    foreach($_SESSION['$radicadosRemitente'] as $radi_remitente) {
    foreach($_SESSION['$asunto']  as $radi_asunto) {
    foreach($_SESSION['$referencia'] as $referencia) {
    if ($radi_tipo_doc == 2){//externo
        $labelFecha = 'Fecha de Registro:';
        $labelFecha1 = 'Fecha del Documento:';
    }
    else{
        $labelFecha = 'Fecha de Creación:';
        $labelFecha1 = 'Fecha de Envío';
    }
    if($referencia==""){
    $referencia="-------";}


    $cuerpo1='<table  border="1" width="100%" >
    <tr>
    <td width="25%">No.documento:</td>
    <td width="25%">'.$radi_nume_text.'</td>
    <td width="25%">Referencia:</td>
    <td width="25%">'.$referencia.'</td>
    </tr>
    <tr>
    <td colspan="1" width="50%">Remitente:</td>
    <td colspan="3" width="50%">'.$radi_remitente.'</td>
    </tr>

    <tr>
    <td colspan="1" width="50%">Asunto:</td>
    <td colspan="3" width="50%">'.$radi_asunto.'</td>
    </tr>

    <tr>
    <td colspan="1" width="50%">Registrado por:</td>
    <td colspan="3" width="50%">'.$registrPor.'</td>
    </tr>';    
    $cuerpo1.='<tr>
    <td colspan="1" width="50%">'.$labelFecha.'</td>
    <td colspan="3" width="50%">'.substr($fecha_registro,0,16).'</tr>';   
    $cuerpo1.='<tr>
    <td colspan="1" width="50%">'.$labelFecha1.':</td>
    <td colspan="3" width="50%">';
    if ($radi_tipo_doc == 2) 
       $cuerpo1.=substr($fecha_Ofic,0,10);
    else 
       $cuerpo1.=substr($fecha_Ofic,0,16);

            $cuerpo1.='</td>
    </tr>';   
    $cuerpo1.='</table>';
                    }
                }
            }
        }
    }
}


$titulo1='<table width="100%" >
 <tr>
    <td>Información del traspaso</td>
</tr>
</table>';

$cuerpo2='<table  border="1" width="100%">
<tr>
<td>Enviado a:</td>
    <td>'.$depe_nombInst.'</td>
<td>Recibido por:</td>
    <td>'.$usCodDestino.'</td>
</tr>
<tr>
<td colspan="1">Fecha entrega:</td>
<td colspan="3">'.$fechaReg.'</td>
</tr>
<tr>
<td colspan="1">Enviado por:</td>
<td colspan="3">'.$enviadoPor.'</td>
</tr>
<tr>
<td colspan="1">Responsable traslado:</td>
<td colspan="3">'.$responsable.'</td>
</tr>
<tr>
<td colspan="1">Comentario:</td>
<td colspan="3">'. $observacionJustificado.'</td>
</tr>
<tr>
<td colspan="1">Estado:</td>
<td colspan="3">'. $estado.'</td>
</tr>
</table>';


$cuerpo3='<table align="center" border="0" font color="#F5EFFB">
<tr><td>. </td></tr>

<tr><td> </td></tr> </table>';


/*$cuerpo4='<table align="center" border="0">
<tr>
<td>------------------------------------</td>
<td>------------------------------------</td>
<td>------------------------------------</td>
</tr>
<tr>
<td align="center" >Recibido por</td>
<td align="center">Enviado por</td>
<td align="center">Responsable traslado</td>
</tr>
<tr>
<td align="center" >'.$usCodDestino.'</td>
<td align="center">'.$enviadoPor.'</td>
<td align="center">'.$responsable.'</td>
</tr>

</table>';*/


$cuerpo4='<table align="center" border="0">
<tr>
<td align="center" >Recibido por</td>
<td align="center">Enviado por</td>
<td align="center">Responsable traslado</td>
</tr>
<tr></tr><tr></tr>
<tr>
<td>------------------------------------</td>
<td>------------------------------------</td>
<td>------------------------------------</td>
</tr>
<tr>
<td align="center" >'.$usCodDestino.'</td>
<td align="center">'.$enviadoPor.'</td>
<td align="center">'.$responsable.'</td>
</tr>
<tr>
<td align="center" >'.$depe_nomb.'</td>
<td align="center">'.$depe_nombInst.'</td>
</tr>

</table>
</body>
</html>';




//echo $inicio.$encabezadopdf.$cuerpo1.$cuerpo2.$cuerpo3.$cuerpo4;





//GENERACION DEL PDF
//include "$ruta_raiz/config.php";
require_once("$ruta_raiz/interconexion/generar_pdf.php");
$plantilla = "$ruta_raiz/bodega/plantillas/".$area["plantilla"].".pdf";
$doc_pdf=$inicio.$encabezadopdf.$titulo.$cuerpo1.$titulo1.$cuerpo2.$cuerpo3.$cuerpo4;
//var_dump($doc_pdf);die();
$pdf = ws_generar_pdf($doc_pdf, $plantilla, $servidor_pdf,"","","",90,"R");
$nomArch="Reporte_TraspasoDocumentosF.pdf";
header( "Content-Disposition: attachment; filename=$nomArch");
header("Content-Type:application/pdf");//.application/pdf
header("Content-Transfer-Encoding: binary");
echo  $pdf;

?>

