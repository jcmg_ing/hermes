<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

session_start();
if(empty($ruta_raiz)) $ruta_raiz = "..";


unset($query1);
unset($usuadoc);
unset($doc);
unset($estado);




$datos=$_SESSION['d_reporte'];
$ban=$_SESSION['ban'];
$queryE=$_SESSION['queryE'];
$nomInst=$_SESSION['$NombInsti'];


include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

//include_once "../include/db/ConnectionHandler.php";
//if(!$db)  { $db = new ConnectionHandler('.');}

$query1= $db->query($queryE);

//echo $query1;
$CONTCOL=0;



while(!$query1->EOF)  {
    //$query1->fields["USUARIO"];
    if ($CONTCOL==0){
    $usuadoc[] = $query1->fields["USUARIO"];
	$estado[] = $query1->fields["ESTA_DESC"];
	$doc[] = $query1->fields["RADICADOS"];
    $usuadocAnte = $query1->fields["USUARIO"];
    }
    else {
        if ($usuadocAnte==$query1->fields["USUARIO"]){
            $usuadoc[] = '';
        }
        else{
            $usuadoc[] = $query1->fields["USUARIO"];
        }
        $estado[] = $query1->fields["ESTA_DESC"];
        $doc[] = $query1->fields["RADICADOS"];
        $usuadocAnte = $query1->fields["USUARIO"];
    }
	
	$query1->MoveNext();
    $CONTCOL+=1;
}

	$lon=count($usuadoc);

	$inicio = '
	<html>
	<head>
        <title>.: QUIPUX - VISTA PREVIA :.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body style="margin: 30 20 50 20;">
	';

    /*<tr><th align="center"><font size="4">Gobierno Nacional De La República Del Ecuador</th></tr>
	<tr><td></td></tr>
	<tr><td></td></tr>*/

	$encabezadopdf = '
	<table align="center">
	
	<tr><td></td></tr>
	<tr><td><center><img src="logoEntidad.gif" width=5 height=5 /></center><td></tr>
	</table>
	<br />
	<table align="center">
		<tr><th align="center">Estado De Documentos Por Usuario</th></tr>
	</table>
	<br />
	';

	if(empty($datos[4])){
		$infor='
		<table width="100%">
		<tr>
			<td align="left" size="5%"><b>Institución:</b></td>
			<td align="left">'.$nomInst.'</td></tr>
			<td align="left"  size="5%"<b>>Area:</th>
			<td>'.$datos[1].'</td>
		</tr>
		<tr>
			<td align="left" size="5%"><b>Fecha Desde:</b></td>
			<td align="left">'.$datos[2].'</td>
			<td align="right"><b>Hasta:</b></td>
			<td align="right">'.$datos[3].'</td>
		</tr>
		<br />
		</table>
		';
    //
		$cuerpo=
		'<hr><table border="0" width="90%" align="center">
		<tr><th><font size="2">Usuario</font></Th>
		<th><font size="2">Estado Del Documento</font></Th>
		<th><font size="2">Num. De Documentos</font></Th></tr>';
		for ($i=0;$i <=$lon;$i++ ){
		$cuerpo2 .= '<tr><td><font size="2">'.$usuadoc[$i].'</font></td>';
		$cuerpo2 .= '<td><font size="2">'.$estado[$i].'</font></td>';
		$cuerpo2 .= '<td align="center"><font size="2">'.$doc[$i].'</font></td></tr>';
		}
		$cuerpo .= $cuerpo2.'</table>';
		;

		}elseif(!empty($datos[1]) && !empty($datos[4])){
			$infor='
			<table align="left" width="100%">
			<tr>
				<th align="left">Nombre del usuario:</th>
				<td>'.$datos[4].'</td>
			</tr>
			<tr>
				<th align="left">Nombre del área:</th>
				<td>'.$datos[1].'</td>
			</tr>
			<tr>
				<td align="left" size="5%"><b>Fecha(desde):<b></td>
				<td align="left">'.$datos[2].'</td>
				<td align="right"><b>Fecha(hasta):<b></td>
				<td align="right">'.$datos[3].'</td>
			</tr>
			<br />
			<br />
			<br />
			</table>
			';
	
			$cuerpo=
			'<table border="0" width="80%" align="center">
			<tr><th><font size="2">ESTADO DEL DOCUMENTO</font></Th>
			<th><font size="2">NUM. DE DOCUMENTOS</font></Th></tr>';
			for ($i=0;$i <=$lon;$i++ ){

                $cuerpo2 .= '<tr><td><font size="2">'.$estado[$i].'</font></td>';
                $cuerpo2 .= '<td align="center"><font size="2">'.$doc[$i].'</font></td></tr>';

			}
			$cuerpo .= $cuerpo2.'</table>';
			;
		}

	$fin = '
	</body>
	</html>
	';

//echo $inicio.$encabezadopdf.$infor.$cuerpo.$fin;



//GENERACION DEL PDF
include "$ruta_raiz/config.php";
require_once("$ruta_raiz/interconexion/generar_pdf.php");
$plantilla = "";
$plantilla = "../bodega/plantillas/".$_SESSION["DEPE_CODI"].".pdf";
$doc_pdf=$inicio.$encabezadopdf.$infor.$cuerpo.$fin;
$pdf = ws_generar_pdf($doc_pdf, $plantilla, $servidor_pdf);
$nomArch="Reporte_PorEstado.pdf";
header( "Content-Disposition: attachment; filename=$nomArch");
//header( "Content-Length: ".filesize($path_arch));
header("Content-Type:application/pdf");//.application/pdf
header("Content-Transfer-Encoding: binary");
echo  $pdf;



/*
//GENERACION DEL PDF
//	$ruta_raiz=".";
	require_once("./js/dompdf/dompdf_config.inc.php");
	
	$dompdf = new DOMPDF();
	$dompdf->load_html($inicio.$encabezadopdf.$infor.$cuerpo.$fin.$piePagina);
	$dompdf->set_paper("a4", "portrait");
	$dompdf->set_base_path(getcwd());
	$dompdf->render();
	
	//$nombarch = "/". substr($verrad,0,4)."/".substr($verrad,4,3)."/".$registro["textrad"].".pdf";
	
	//file_put_contents("$ruta_raiz/bodega".$nombarch, $dompdf->output());
	$dompdf->stream("Reporte_DocumentosIngresadosporUsuario.pdf");
	
	//exec ("mv /tmp/$nombarch /var/www/orfeo/bodega/2008/900/$nombarch",$output,$returnS);
	//$sql = "UPDATE RADICADO SET RADI_PATH='$nombarch' where radi_nume_radi=$verrad";
	//$rs = $db->query($sql);	
	//echo "<script>window.close();</script>";

*/
?>