<?

/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                      */
/*   Hollman Ladino       hladino@gmail.com                Desarrollador             */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion                           */
/*************************************************************************************/


//ECHO "HOLA";
$ruta_raiz = "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

//ECHO $_SESSION['queryE'];
//echo "estado=>".$_REQUEST['estado1'];

$krdOld = $krd;
$carpetaOld = $carpeta;
$tipoCarpOld = $tipo_carp;
if(!$tipoCarpOld) $tipoCarpOld= $tipo_carpt;
session_start();
if(!$krd) $krd=$krdOsld;
include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

//echo $_REQUEST['estado1'];

//Armo las consultas por estado
if (isset($_REQUEST['estado1']) )
{
        switch ($_REQUEST['estado1']){
             case 0://Archivados

                include_once "$ruta_raiz/include/query/estadisticas/consultaPorTipoBandeja.php";
                break;
            case 1://Edición
                
                include_once "$ruta_raiz/include/query/estadisticas/consultaPorTipoBandeja.php";
                break;
            case 2: //Recibidos o en Tràmite
                
                include_once "$ruta_raiz/include/query/estadisticas/consultaPorTipoBandeja.php";
                break;
            case 3||4||5: //No enviado(originalmente),(electrónicamente).(manualmente)
                
                include_once "$ruta_raiz/include/query/estadisticas/consultaPorTipoBandeja.php";
                break;
            case 6: //Enviados
                
                include_once "$ruta_raiz/include/query/estadisticas/consultaPorTipoBandeja.php";
                break;
            case 9: //Reasignados
                
                include_once "$ruta_raiz/include/query/estadisticas/consultaPorTipoBandeja.php";

                break;
            case 1111: //Asignados
                
                include_once "$ruta_raiz/include/query/estadisticas/consultaPorTipoBandeja.php";
                break;
            case 8888: //Vencidos
                
                include_once "$ruta_raiz/include/query/estadisticas/consultaPorTipoBandeja.php";
                break;
        }
    //return ;
}

$tipoReporte=7;
if($tipoReporte==7){
	$generar = "ok";
}
//ECHO $generar.$genDetalle;
if($generar == "ok") {
    if($genDetalle==1) $queryE = $queryEDetalle;
    include_once ("tablaHtml_porDocumentos.php");
}
    




