<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

//////CODIGO REPORTES
session_start();
unset($_SESSION['queryE']);

$ruta_raiz = "..";
$_SESSION['ban']=0;
$cod_usuario=$_POST['codus'];

///DEPENDENCIA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$_SESSION['uno'];
}
///USUARIO
if(!empty($_POST["codus"]))
{
	$cod_u=$_POST["codus"];
	unset($_SESSION['cod_u']);
	$_SESSION['cod_u']=$cod_u;
}

//consulta para obtener el nombre del departamento.
if(isset($_SESSION['uno'])){
    $dependencia_busq_rep=$_SESSION['uno'];
    if($dependencia_busq_rep!='99999'){

        $queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq_rep;
        $rsE= $db->query($queryR);
        $area=$rsE->fields["DEPE_NOMB"];
    }
}

$_SESSION['$area']=$area;


switch($db->driver)
{
	case 'postgres':
	{

        $titulos=array("#","1#De","2#Fecha/Hora","3#Acción", "4#Para", "5#Número Días", "6#Comentario");
        $_SESSION['$titulos']=$titulos;



        $isql="select TO_CHAR(h.HIST_FECH,'DD-MM-YYYY HH24:MI AM') AS HIST_FECH1 ,
        uo.usua_nombre as USUA_ORI,
        uo.DEPE_NOMB,
        ud.usua_nombre as USUA_DEST,
        t.sgd_ttr_codigo ,
        t.SGD_TTR_DESCRIP,
        h.HIST_OBSE,
        f.tot-(to_date(CURRENT_DATE,'yyyy-mm-dd') - to_date(h.hist_fech,'yyyy-mm-dd')) as TOT_DIAS
        from hist_eventos h
        left outer join datos_usuarios uo on uo.usua_codi=h.usua_codi_ori
        left outer join datos_usuarios ud on ud.usua_codi=h.usua_codi_dest
        left outer join sgd_ttr_transaccion t on t.sgd_ttr_codigo=h.sgd_ttr_codigo ,
        (select max(to_date(CURRENT_DATE,'yyyy-mm-dd') - to_date(hist_fech,'yyyy-mm-dd')) as tot
        from hist_eventos
        where radi_nume_radi=".$_REQUEST['numdoc'].") as f
        where h.radi_nume_radi =".$_REQUEST['numdoc']."
        order by hist_fech desc";
         break;
    }
 }

//echo $isql;

//Asigno Variables  para manejo en PDF
$_SESSION['$isql']=$isql;
$_SESSION['$numdoc']=$_REQUEST['numdoc'];
$_SESSION['$documento']=$_REQUEST['documento'];
$TituloFrame="Recorrido Del Documento  --Tiempo Demora--";//$_SESSION['$descEstado'];


//Encabezados para reporte en PDF
$_SESSION['d_reporte']=NULL;
$datos =array("1"=>$_REQUEST['de'], "2"=>$fecha_ini, "3"=>$fecha_fin,"5"=>$_REQUEST['numdoc'],"6"=>$_REQUEST['fechaReg']);
$_SESSION['d_reporte']=$datos;



//Pone encabezados en los reportes
$encabezadopdf='
<table width="100%"  border="0" cellpadding="5" cellspacing="2" class="borde_tab">
<tr><th colspan=4 class="titulos4">'.$TituloFrame.'</tr>
<tr><th class="titulos4">De<th class="titulos4">Para<th class="titulos4">Documento
<tr BGCOLOR="#DFDFDF"><td align="center" class="listado2"><font SIZE=2>'.$_REQUEST['de'].'</font></td><td align="center" class="listado2"><font SIZE=2>'.$_REQUEST['para'].'</font></td><td align="center" class="listado2"><font SIZE=2>'.$_REQUEST['documento'].'</font>
</td><tr></tr>
</table>
<table align="center">
<tr><td></td><td>
</td>
</tr>
</table></ br>';

echo $encabezadopdf;
echo (
"<table class='borde_tab' width=100% >
<b>
<center>
<tr class=listado1>
  <td >
  </td>
   <td align=center>
     <A  class=botones class=vinculos HREF='javascript:history.go(-1);'>Regresar</A>
   </td>
</tr>
</center>
</b>
</table>");

function pintarEstadistica($fila,$indice,$numColumna)
{
    global $ruta_raiz,$_POST,$_GET,$Estado;
    $salida="";
    $m=0;

    
            switch ($numColumna){
            case  0:
                $salida=$indice;
                break;
            case 1:
                $salida=$fila['USUA_ORI'];
            break;
            case 2:
               $salida=$fila['HIST_FECH1'];
            break;
            case 3:
                $salida=$fila['SGD_TTR_DESCRIP'];
                break;
            case  4:
                $salida=$fila['USUA_DEST'];
                break;
            case 5:
                $salida=$fila['TOT_DIAS'];
            break;
            case 6:
               $salida=$fila['HIST_OBSE'];
            break;
            default: $salida=false;
        }
        return $salida;
    
}

?>

