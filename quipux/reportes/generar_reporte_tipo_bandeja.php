<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

if(empty($ruta_raiz)) $ruta_raiz = "..";

?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?

unset($query1);
unset($DE);
unset($PARA);
unset($ASUNTO);
unset($FECHA);
unset($NUMDOC);
unset($NUMREFE);
unset($TIPODOC);
unset($ESTADO);
unset($FIRMA);
unset($OBSERVACION);
unset($NUMROWAGRUPADA);


session_start();

$datos=$_SESSION['d_reporte'];
$ban=$_SESSION['ban'];
$estadoR=$_SESSION['$estado'];
$titulos=$_SESSION['$titulos'];
$usua_docR=$_SESSION['$usua_doc'];
$descEstadoR=$_SESSION['$descEstado'];
$nomInst=$_SESSION['$NombInsti'];
$ordenR=$_SESSION['$ORDEN'];
$queryR=$_SESSION['$isql'];
$queryE="$queryR $ordenR";
$nomArch=$descEstadoR;


echo $estadoR;
echo  $usua_docR;

include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

$query1= $db->query($queryE);
$CONTCOL=0;

while(!$query1->EOF)  {
    
    IF ($CONTCOL==0){

          IF ($estadoR==0){
            $DE[]= $query1->fields["DE"];
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $DE_ANTE=$query1->fields["DE"];
        }
        IF ($estadoR==1){
            $DE[]= $query1->fields["DE"];
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $TIPODOC[]= $query1->fields["TIPO_DOCUMENTO"];
            $DE_ANTE=$query1->fields["DE"];
        }
        IF ($estadoR==2){
            $DE[]= $query1->fields["DE"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $ESTADO[]= $query1->fields["ESTADO"];
            $FIRMA[]= $query1->fields["FIRMA_DIGITAL"];
            $DE_ANTE=$query1->fields["DE"];
        }
        IF ($estadoR==3||$estadoR==4||$estadoR==5){
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $PARA_ANTE=$query1->fields["PARA"];
        }
        IF ($estadoR==6){
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $FIRMA[]= $query1->fields["FIRMA_DIGITAL"];
            $PARA_ANTE=$query1->fields["PARA"];
        }
         IF ($estadoR==7){
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
        }
        IF ($estadoR=='1111'){
            $PARA[]= $query1->fields["USUARIO_ORIGEN"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            
        }

        IF ($estadoR==9 ){ //Reasignados &&  $usua_docR=='11111'
            $DE[]= $query1->fields["DE"];
            ECHO $query1->fields["DE"];
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERNCIA"];
            $DE_ANTE=$query1->fields["DE"];
        }
        IF ($estadoR==8888){
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $PARA_ANTE=$query1->fields["PARA"];
        }
    }
    ELSEIF ($CONTCOL>=1){

        IF ($estadoR==0){
            IF ($DE_ANTE==$query1->fields["DE"]){
                $DE[]='';
            }
            ELSE{
                $DE[]= $query1->fields["DE"];
            }
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $DE_ANTE=$query1->fields["DE"];
        }

        IF ($estadoR==1){
            IF ($DE_ANTE==$query1->fields["DE"]){
                $DE[]='';
            }
            ELSE{
                $DE[]= $query1->fields["DE"];
            }
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $TIPODOC[]= $query1->fields["TIPO_DOCUMENTO"];
            $DE_ANTE=$query1->fields["DE"];
        }

       IF ($estadoR==2){
            IF ($DE_ANTE==$query1->fields["DE"]){
                $DE[]='';
            }
            ELSE{
                $DE[]= $query1->fields["DE"];
            }
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $ESTADO[]= $query1->fields["ESTADO"];
            $FIRMA[]= $query1->fields["FIRMA_DIGITAL"];
            $DE_ANTE=$query1->fields["DE"];
        }

        IF ($estadoR==3||$estadoR==4||$estadoR==5){
            IF ($PARA_ANTE==$query1->fields["PARA"]){
                $PARA[]='';
            }
            ELSE{
                $PARA[]= $query1->fields["PARA"];
            }
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $PARA_ANTE=$query1->fields["PARA"];
        }
        IF ($estadoR==6){
            IF ($PARA_ANTE==$query1->fields["PARA"]){
                $PARA[]='';
            }
            ELSE{
                $PARA[]= $query1->fields["PARA"];
            }
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERENCIA"];
            $FIRMA[]= $query1->fields["FIRMA_DIGITAL"];
            $PARA_ANTE=$query1->fields["PARA"];
        }
         IF ($estadoR==7){
            IF ($PARA_ANTE==$query1->fields["PARA"]){
                $PARA[]='';
            }
            ELSE{
                $PARA[]= $query1->fields["PARA"];
            }
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $PARA_ANTE=$query1->fields["PARA"];
        }
        IF ($estadoR=='1111'){
            IF ($PARA_ANTE==$query1->fields["USUARIO_ORIGEN"]){
                $PARA[]='';
 
            }
            ELSE{
                $PARA[]= $query1->fields["USUARIO_ORIGEN"];
            }
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $PARA_ANTE=$query1->fields["USUARIO_ORIGEN"];
 
        }
        IF ($estadoR==9 ){ // && $usua_docR=='11111'
            IF ($DE_ANTE==$query1->fields["DE"]){
                $DE[]='';
            }
            ELSE{
                $DE[]= $query1->fields["DE"];
            }
            $PARA[]= $query1->fields["PARA"];
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $NUMREFE[]= $query1->fields["NO_REFERNCIA"];
            $DE_ANTE=$query1->fields["DE"];
        }
        IF ($estadoR==8888){
            IF ($PARA_ANTE==$query1->fields["PARA"]){
                $PARA[]='';
            }
            ELSE{
                $PARA[]= $query1->fields["PARA"];
            }
            $ASUNTO[]= $query1->fields["ASUNTO"];
            $FECHA[]= $query1->fields["DAT_FECHA_DOCUMENTO"];
            $NUMDOC[]= $query1->fields["NUMERO_DOCUMENTO"];
            $PARA_ANTE=$query1->fields["PARA"];
        }

    }

$CONTCOL+=1;
$query1->MoveNext();
$a+=1;
}

$lon=count($usuadoc);

	$inicio = '
	<html>
	<head>
	<title>.: QUIPUX - VISTA PREVIA :.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body style="margin: 30 20 50 20;">
	';

	$encabezadopdf = '
	<table align="center" border="0">
	<tr><td></td></tr>
	<tr><td></td></tr>
	<tr><td><center><img src="logoEntidad.gif" width=5 height=5 /></center><td></tr>
	</table>
	<br />
	<table align="center">
		<tr align="center"><th>Documentos Con Estado  "'.$nomArch.'"</th></tr>
	</table>
	<br />
	';


//$descEstadoR
$linea='
<table align="center" border="1"></table>
';


	if(!empty($datos[1]) ){

        $infor0='
		<table width="100%">
        <tr>
            <td algin="left" size="5"><b>Institución:</b></td>
			<td algin="left" size="5">'.$nomInst.'</td>
        </tr>
        </table>
		';



		$infor=$infor0.'
		<table width="100%">
        <tr>
            <td algin="left" size="1"><b>Usuario:</b></td>
			<td algin="left">'.$datos[5].'</td>
			<td algin="left"><b>Area:</b></td>
			<td algin="left">'.$datos[1].'</td>
			<td algin="left"><b>Estado:</b></td>
			<td algin="left">'.$datos[6].'</td>
            <td algin="left"><b>#Doc:</b></td>
			<td algin="left">'.$datos[7].'</td>
        </tr>
		<br />
		</table>
		';

$infor1='<table width="100%">

		<tr>
			<td align="left" size="2%"><b>Fecha Desde:</b></td>
			<td align="left">'.$datos[2].'</td>
			<td align="right"><b>Hasta:</b></td>
			<td align="right">'.$datos[3] .'</td>
		</tr>
		<br />
        <br />
		</table>';

$infor.=$infor1;

        //Armo títulos del reporte para PDF
        $cuerpo1='<HR><table border="0" width="100%" align="center"><tr>';
        for ($j=1;$j<=count($_SESSION['$titulos']);$j++){
                $cuerpo1.='<th><font size="1"> '.substr($titulos[$j],2).'</font></Th>';
        }

        $cuerpo1.='</tr>';
        $cuerpo=$cuerpo1;


        //Cargo detalle del reporte para PDF
        for ($i=0;$i<=$a-1;$i++ ){

        switch ($estadoR){

                case 0:
                    $cuerpo2 .= '<tr><td><font size="1">'.$DE[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$PARA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMDOC[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$NUMREFE[$i].'</font></td></tr>';
                    break;
                case 1:
                    $cuerpo2 .= '<tr><td><font size="1">'.$DE[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$PARA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMDOC[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMREFE[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$TIPODOC[$i].'</font></td></tr>';
                    break;
                case 2:
                    $cuerpo2 .= '<tr><td><font size="1">'.$DE[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMDOC[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMREFE[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ESTADO[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$FIRMA[$i].'</font></td></tr>';
                   break;
                case  ($estadoR==3||$estadoR==4||$estadoR==5):
                    $cuerpo2 .= '<tr><td><font size="1">'.$PARA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMDOC[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$NUMREFE[$i].'</font></td></tr>';
                    break;

                case 6:
                    $cuerpo2 .= '<tr><td><font size="1">'.$PARA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMDOC[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMREFE[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$FIRMA[$i].'</font></td></tr>';
                    break;
                case 7:
                    $cuerpo2 .= '<tr><td><font size="1">'.$PARA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$NUMDOC[$i].'</font></td></tr>';
                    break;
                case ($estadoR==1111): //Asignados
                    $cuerpo2 .= '<tr><td><font size="1">'.$PARA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$NUMDOC[$i].'</font></td></tr>';
                    break;
                case ($estadoR==9 ): //Reasignados && $usua_docR=='11111'
                    $cuerpo2 .= '<tr><td><font size="1">'.$DE[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$PARA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$NUMDOC[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$NUMREFE[$i].'</font></td></tr>';
                    break;
                case 8888:
                    $cuerpo2 .= '<tr><td><font size="1">'.$PARA[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$ASUNTO[$i].'</font></td>';
                    $cuerpo2 .= '<td><font size="1">'.$FECHA[$i].'</font></td>';
                    $cuerpo2 .= '<td align="center"><font size="1">'.$NUMDOC[$i].'</font></td></tr>';
                    break;
        }
		}
		$cuerpo .= $cuerpo2.'</table>';
		;
    }elseif(!empty($datos[1]) && !empty($datos[4])){

			/*$infor='
			<table align="left" width="100%">
			<tr>
				<th align="left">Nombre del usuario:</th>
			<td>'.$datos[4].'</td>
			</tr>
			<tr>
				<th align="left">Nombre del área:</th>
				<td>'.$datos[1].'</td>
			</tr>
			<tr>
				<td align="left" size="5%"><b>Fecha(desde):<b></td>
				<td align="left">'.$datos[2].'</td>
				<td align="right"><b>Fecha(hasta):<b></td>
				<td align="right">'.$datos[3].'</td>
			</tr>
			<br />
			<br />
			<br />
			</table>
			';

			$cuerpo=
			'<table border="0" width="80%" align="center">
			<tr><th><font size="2">ESTADO DEL DOCUMENTO</font></Th>
			<th><font size="2">NUM. DE DOCUMENTOS</font></Th></tr>';
			for ($i=0;$i <=$lon;$i++ ){
			$cuerpo2 .= '<tr><td><font size="2">'.$estado[$i].'</font></td>';
			$cuerpo2 .= '<td align="center"><font size="2">'.$doc[$i].'</font></td></tr>';
			}
			$cuerpo .= $cuerpo2.'</table>';
			;*/
		}

	$fin = '
	</body>
	</html>
	';
//    ECHO $cuerpo;


//GENERACION DEL PDF
include "$ruta_raiz/config.php";
require_once("$ruta_raiz/interconexion/generar_pdf.php");
$plantilla = "";
$plantilla = "$ruta_raiz/bodega/plantillas/".$_SESSION["depe_codi"].".pdf";
$doc_pdf=$inicio.$encabezadopdf.$infor.$cuerpo.$fin;
$pdf = ws_generar_pdf($doc_pdf, $plantilla, $servidor_pdf);
$nomArch="Reporte_PorEstado$descEstadoR.pdf";
header( "Content-Disposition: attachment; filename=$nomArch");
header("Content-Type:application/pdf");//.application/pdf
header("Content-Transfer-Encoding: binary");
echo  $pdf;




/*
require_once("../js/dompdf/dompdf_config.inc.php");
$dompdf = new DOMPDF();
//$dompdf->load_html($html);
$dompdf->load_html($inicio.$encabezadopdf.$infor.$cuerpo.$fin);//.$piePagina
$dompdf->set_paper("a4", "portrait");
$dompdf->set_base_path(getcwd());
$dompdf->render();
//$nombarch = "/". substr($verrad,0,4)."/".substr($verrad,4,3)."/".$registro["textrad"].".pdf";
//file_put_contents("$ruta_raiz/bodega".$nombarch, $dompdf->output());
$a="Reporte_DocumentosPorEstado$descEstadoR.pdf";
$dompdf->stream($a);
//exec ("mv /tmp/$nombarch /var/www/orfeo/bodega/2008/900/$nombarch",$output,$returnS);
//$sql = "UPDATE RADICADO SET RADI_PATH='$nombarch' where radi_nume_radi=$verrad";
//$rs = $db->query($sql);
//echo "<script>window.close();</script>";
*/
?>
