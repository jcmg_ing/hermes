<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

session_start();
$ruta_raiz = isset($ruta_raiz) ? $ruta_raiz : "..";

//Limpia parametros
unset($query1);
unset($numdoc);
unset($docRefe);
unset($de);
unset($para);
unset($asunto);
unset($descAnex);
unset($fechDocu);
unset($fechReg);
unset($deRecorr);
unset($fechaRecorr);
unset($accionRecorr);
unset($paraRecorr);
unset($tot_dias);
unset($comentarioRecorr);
$TipoFirma=0;
$EsModi=true;

//Datos para reporte-Recorrido
include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

// Obtener plantilla del area del usuario actual
include_once "$ruta_raiz/obtenerdatos.php";
$area = ObtenerDatosDependencia($_SESSION["depe_codi"],$db);

//Valido permisos de bandeja de entrada
$sql="select usua_codi from permiso_usuario where id_permiso=5 and usua_codi=".$_SESSION['usua_codi'];
$rs = $db->query($sql);
$usua=$rs->fields["USUA_CODI"];



//query
$sqlFecha = $db->conn->SQLDate("Y-m-d H:i A","h.hist_fech");
$datosrad = ObtenerDatosRadicado($verrad,$db);
$cont=0;

$isql = "select $sqlFecha as hist_fech1
        , uo.usua_nombre as usua_ori
        , uo.depe_nomb
        , ud.usua_nombre as usua_dest
        , t.sgd_ttr_codigo
        , t.sgd_ttr_descrip
        , h.hist_obse
        , h.hist_referencia
        , h.hist_codi
        ,(h.hist_fech::date - '".$datosrad["fecha_radicado"]."'::date) as TOT_DIAS
        ,h.usua_codi_ori
            from hist_eventos h
                    left outer join datos_usuarios uo on uo.usua_codi=h.usua_codi_ori
                    left outer join datos_usuarios ud on ud.usua_codi=h.usua_codi_dest
                    left outer join sgd_ttr_transaccion t on t.sgd_ttr_codigo=h.sgd_ttr_codigo
            where
                    h.radi_nume_radi=$verrad
                    order by hist_codi desc ";


$query1= $db->query($isql);

//Datos Reporte Recorrido
while(!$query1->EOF)  {

    if((isset($query1->fields["SGD_TTR_CODIGO"]) && $query1->fields["SGD_TTR_CODIGO"]!=11)){
        if (!isset($query1->fields["USUA_ORI"])|| $query1->fields["USUA_ORI"]=='' ){
            $deRecorr[]="--";
        }
        else{
            $deRecorr[]= $query1->fields["USUA_ORI"];
        }

        $fechaRecorr[]= $query1->fields["HIST_FECH1"];
        $accionRecorr[]= $query1->fields["SGD_TTR_DESCRIP"];
        $paraRecorr[]= $query1->fields["USUA_DEST"];

        if (!isset($query1->fields["HIST_OBSE"])|| $query1->fields["HIST_OBSE"]==''){
            $comentarioRecorr[]= "--";
        }
        else{
            $comentarioRecorr[]= $query1->fields["HIST_OBSE"];
        }
        $tot_dias[]= $query1->fields["TOT_DIAS"];
        $cont+=1;
        $EsModi=true;

 } elseif((isset($query1->fields["SGD_TTR_CODIGO"]) && ($query1->fields["SGD_TTR_CODIGO"]==11 && $EsModi==true))){

        $EsModi=false;
        if (!isset($query1->fields["USUA_ORI"])|| $query1->fields["USUA_ORI"]=='' ){
            $deRecorr[]="--";
        }
        else{
            $deRecorr[]= $query1->fields["USUA_ORI"];
        }

        $fechaRecorr[]= $query1->fields["HIST_FECH1"];
        $accionRecorr[]= $query1->fields["SGD_TTR_DESCRIP"];
        $paraRecorr[]= $query1->fields["USUA_DEST"];

        if (!isset($query1->fields["HIST_OBSE"])|| $query1->fields["HIST_OBSE"]==''){
            $comentarioRecorr[]= "--";
        }
        else{
            $comentarioRecorr[]= $query1->fields["HIST_OBSE"];
        }
        $tot_dias[]= $query1->fields["TOT_DIAS"];
        $cont+=1;

 }
$query1->MoveNext();
}

//Datos Cabecera

$cod_estado=$_SESSION["depe_codi"];

//Datos para cabecera del reporte
$numdoc=$datosrad["radi_nume_text"];//;$_SESSION['$numdoc'];

//Inicializo Variables
if (!isset($datosrad["radi_referencia"]) || $datosrad["radi_referencia"]=='' ){
    $docRefe="--";
}
else{
    $docRefe=$datosrad["radi_referencia"];
}


$de1=lista_general_lista_usuarios($datosrad["radi_nume_temp"], $datosrad["usua_rem"], $datosrad["estado"], 1, $db);
if (!isset($de1) || $de1==''){
    $de="--";
}
else{
    $de=substr($de1,0,60);
}

$para1=lista_general_lista_usuarios($datosrad["radi_nume_temp"], $datosrad["usua_dest"], $datosrad["estado"], 2, $db);
if (!isset($para1) || $para1==''){
    $para="--";
}
else{
    $para=$para1;//substr($_SESSION['$para'],0,500);
}


if (!isset($datosrad["radi_asunto"]) || $datosrad["radi_asunto"]=='' ){
    $asunto="--";
}
else{
    $asunto=substr($datosrad["radi_asunto"],0,70);
}

if (!isset($datosrad["radi_desc_anexos"]) || $datosrad["radi_desc_anexos"]=='' ){
    $descAnex="--";
}
else{$inicio.$encabezadopdf.$cuerpo5;
    $descAnex=substr($datosrad["radi_desc_anexos"],0,70);
}

if (!isset($datosrad["fecha_firma"]) || $datosrad["fecha_firma"]==''){ //DOCUMENTO FIRMADO ELECTRONICAMENTE
    $fechDocu="--";
}
else{
    $fechDocu=substr($datosrad["fecha_firma"],0,10);
}


if ($fechDocu=='' || $datosrad["fecha_firma"]=='' ){//DOCUMENTO FIRMADO MANUALMENTE*******->OJO
    if (!isset($datosrad["radi_fecha"]) || $datosrad["radi_fecha"]==''){
        $fechDocu="--";
    }
    else{
        $fechDocu=substr($datosrad["radi_fecha"],0,10);
}
}


if (!isset($datosrad["fecha_radicado"]) || $datosrad["fecha_radicado"]==''){//FECHA DEL REGISTRO
    $fechReg="--";
}
else{
    $fechReg=substr($datosrad["fecha_radicado"],0,10);
}

$fecha1 = time();
$fecha=date("Y-m-d",$fecha1 )." / ".date("h:i:s",$fecha1 );

//Armo HTML
$inicio= '
<html>
<head>
<title>.: QUIPUX - VISTA PREVIA :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
';

$encabezadopdf1= '<table align="center" border="0">
<tr><th align="center"><font size="5">Hoja de Ruta</th></tr>
</table>';

$encabezadopdf='
<table border="0" align="left">
<tr><td align="left"><font size=2>Fecha y hora generación:</font></td><td align="left" ><font size=2>'.$fecha.'</font></td></tr>
<tr><td align="left"><font size=2>Generado por:</font></td><td align="left" ><font size=2>'.$_SESSION['usua_nomb'].'</font></td></tr>
</table>';

$cuerpo5='
<table width="100%" align="center" cellspacing="1" cellpadding="4" border="1"  >
<tr>
    <td >
	<table width="100%" align="center" cellspacing="1" cellpadding="2" border="1">
            <tr><td colspan="4" align="left">Información del Documento</td>
		<tr>
                    <td align="left">No.Documento:</td>
		    <td align="left">'.$numdoc.'</td>
                    <td align="left">Doc.Referencia:</td>
                    <td align="left">'.$docRefe.'</td>
                </tr>
		<tr>
                    <td align="left">De:</td>
                    <td align="left">'.$de.'</td>
                    <td align="left">Para:</td>
                    <td align="left">'.$para.'</td>
                </tr>
		<tr>
		    <td align="left">Asunto:</td>
                    <td align="left">'.$asunto.'</td>
                    <td align="left">Descripción Anexos:</td>
                    <td align="left">'.$descAnex.'</td>
		</tr>
		<tr>
		    <td align="left">Fecha Documento:</td>
                    <td align="left">'.$fechDocu.'</td>
                    <td align="left">Fecha Registro:</td>
                    <td align="left">'.$fechReg.'</td>
		</tr>
	</table>
    </td>
</tr >
</table>';

/*<!-- Reporte Recorrido  -->*/
$cuerpo6='<table width="100%" align="center" cellspacing="2" cellpadding="2" border="1">
            <tr><td colspan="5" align="left">Ruta del documento</td>
            <tr>
                <td align="center">De</td>
                <td align="center">Fecha/Hora</td>
                <td align="center">Acción</td>
                <td align="center">Para</td>
                <td align="center">No.Días</td>';

if(0+$_GET['bus_avan']<2)
    $cuerpo6.='</tr>';
else{
    if($usua=="")
        $cuerpo6.='<td align="center">Comentario</td></tr>';
    else
        $cuerpo6.='</tr>';
}
    


for ($i=0;$i<=$cont-1;$i++ ){
    $cuerpo61 .= '<tr><td>'.$deRecorr[$i].'</td>';
    $cuerpo61 .= '<td>'.$fechaRecorr[$i].'</td>';
    $cuerpo61 .= '<td>'.$accionRecorr[$i].'</td>';

    if ($deRecorr[$i] == $paraRecorr[$i]){
        $cuerpo61 .= '<td></td>';
    }else{
        $cuerpo61 .= '<td>'.$paraRecorr[$i].'</td>';
    }
    $cuerpo61 .= '<td>'.$tot_dias[$i].'</td>';


    if(0+$_GET['bus_avan']<2)
        $cuerpo61 .= '</tr>';
    else{
        if($usua=="")
            $cuerpo61 .= '<td>'.$comentarioRecorr[$i].'</td></tr>';
        else
            $cuerpo61 .= '</tr>';
    }
}

$cuerpo61.='</table></body></html>';




//GENERACION DEL PDF
include "$ruta_raiz/config.php";
require_once("$ruta_raiz/interconexion/generar_pdf.php");
$plantilla = "";
$plantilla = "$ruta_raiz/bodega/plantillas/".$area["plantilla"].".pdf";
$doc_pdf=$inicio.$encabezadopdf1.$encabezadopdf.$cuerpo5.$cuerpo6.$cuerpo61;
$pdf = ws_generar_pdf($doc_pdf, $plantilla, $servidor_pdf);
$nomArch="Reporte_DeRuta.pdf";
header( "Content-Disposition: attachment; filename=$nomArch");
header("Content-Type:application/pdf");//.application/pdf
header("Content-Transfer-Encoding: binary");
echo  $pdf;
?>


<?
function lista_general_lista_usuarios($radicado, $usuario, $estado, $tipo, $db)
{
    $cadena = "";
    if ($estado==1 or $estado==7 or $estado==8) {//borradores o eliminados
        foreach (explode('-',$usuario) as $usua_codi) {
	    if (trim($usua_codi!="")) {
	        $usr = ObtenerDatosUsuario($usua_codi,$db);
	        $cadena .= $usr["abr_titulo"]." ".$usr["nombre"].", ".$usr["cargo"].", ".$usr["institucion"]."<br/>";
	    }
        }
    } else {
	$sql = "select usua_nombre, usua_apellido, usua_abr_titulo, usua_cargo, usua_institucion
		from usuarios_radicado where radi_nume_radi=$radicado and radi_usua_tipo=$tipo";

	$rs=$db->conn->query($sql);
    	while(!$rs->EOF)
    	{
	    $cadena .= $rs->fields["USUA_ABR_TITULO"]." ".$rs->fields["USUA_NOMBRE"]." ".$rs->fields["USUA_APELLIDO"];
	    $cadena .= ", ".$rs->fields["USUA_CARGO"].", ".$rs->fields["USUA_INSTITUCION"]."<br/>";
	    $rs->MoveNext();
	}
    }
    return $cadena;
}

?>
