<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

session_start();
if(empty($ruta_raiz)) $ruta_raiz = "..";

?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?

unset($query1);
unset($numdoc);
unset($userActu);
unset($dias);
unset($de);
unset($para);
unset($asunto);
unset($docrefe);
unset($fechaReg);
unset($fechaDoc);
unset($estado);
unset($tipodoc);

$datos=$_SESSION['d_reporte'];
$area=$_SESSION['$area'];
$ban=$_SESSION['ban'];
$queryE=$_SESSION['queryE'];
$titulos=$_SESSION['$titulos'];
$tabla=$_SESSION['tabla'];
$nomInst=$_SESSION['$NombInsti'];



include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

$query1= $db->query($queryE);
$CONTCOL=0;

while(!$query1->EOF)  {

     if ($CONTCOL==0){

         $numdoc[] = $query1->fields["DOCUMENTO"];
         $userActu[] = $query1->fields["USUA_ORI"];
         $dias[] = $query1->fields["TOTAL"];
         $asunto[] = $query1->fields["ASUNTO"];
         $fechaReg[] = $query1->fields["FECHA_REGISTRO"];
         $estado[] = $query1->fields["ESTADO"];
         $tipodoc[] = $query1->fields["TIPODOC"];
         $tipodocAnte = $query1->fields["TIPODOC"];
         $userActuAnte = $query1->fields["USUA_ORI"];

    }
    else {
        //Tipo Documento
        if ($tipodocAnte==$query1->fields["TIPODOC"]){
            $tipodoc[]= '';
        }
        else{
            $tipodoc[] = $query1->fields["TIPODOC"];
        }
        //Usuario Actual
        if ($userActuAnte==$query1->fields["USUA_ORI"]){
            $userActu[]= '';
        }
        else{
            $userActu[] = $query1->fields["USUA_ORI"];
        }
         $numdoc[] = $query1->fields["DOCUMENTO"];
         $dias[] = $query1->fields["TOTAL"];
         $asunto[] = $query1->fields["ASUNTO"];
         $fechaReg[] = $query1->fields["FECHA_REGISTRO"];
         $estado[] = $query1->fields["ESTADO"];
         $tipodocAnte = $query1->fields["TIPODOC"];
         $userActuAnte = $query1->fields["USUA_ORI"];
   }
	$query1->MoveNext();
    $CONTCOL+=1;
}

	$inicio = '
	<html>
	<head>
        <title>.: QUIPUX - VISTA PREVIA :.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body style="margin: 30 20 50 20;">
	';


	$encabezadopdf = '
	<table align="center">

	<tr><td></td></tr>
	<tr><td><center><img src="logoEntidad.gif" width=5 height=5 /></center><td></tr>
	</table>
	<br />
	<table align="center">
		<tr><th align="center">Tiempo Demora En Finiquitar Un Documento</th></tr>
	</table>
	<br />
	';


	if(!empty($CONTCOL)){
		$infor='
		<table width="100%">
		<tr>
			<th align="left">Institución:</th>
			<td>'.$nomInst.'</td>
			<th align="left">Area:</th>
			<td>'.$area.'</td>
		</tr>
		<tr>
			<td align="left" size="5%"><b>Fecha Desde:</b></td>
			<td align="left">'.$datos[5].'</td>
			<td align="right"><b>Hasta:</b></td>
			<td align="right">'.$datos[6].'</td>
		</tr>
		<br />
		</table>';

   
		$cuerpo1='<HR><table border="0" width="100%" align="center"><tr>';
        for ($j=1;$j<=count($_SESSION['$titulos']);$j++){
                $cuerpo1.='<th><font size="1"> '.substr($titulos[$j],2).'</font></Th>';
        }

        $cuerpo1.='</tr>';
        $cuerpo=$cuerpo1;

		for ($i=0;$i <=$CONTCOL;$i++ ){
            $cuerpo2 .= '<tr><td><font size="1">'.$tipodoc[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$userActu[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$dias[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$asunto[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$fechaReg[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$estado[$i].'</font></td>';
            $cuerpo2 .= '<td ><font size="1">'.$numdoc[$i].'</font></td>
            </tr>';
		}
 $cuerpo .= $cuerpo2.'</table>';
}

$fin = '
</body>
</html>';




//GENERACION DEL PDF
include "$ruta_raiz/config.php";
require_once("$ruta_raiz/interconexion/generar_pdf.php");
$plantilla = "";
$plantilla = "../bodega/plantillas/".$_SESSION['uno'].".pdf";
$doc_pdf=$inicio.$encabezadopdf.$infor.$cuerpo.$fin;
$pdf = ws_generar_pdf($doc_pdf, $plantilla, $servidor_pdf);
//$nomArch="Reporte_PorEstado.pdf";
$nomArch="Reporte_PorDemoraFiniquitar.pdf";
header( "Content-Disposition: attachment; filename=$nomArch");
header("Content-Type:application/pdf");//.application/pdf
header("Content-Transfer-Encoding: binary");
echo  $pdf;




/*
//GENERACION DEL PDF
//	$ruta_raiz=".";
	require_once("./js/dompdf/dompdf_config.inc.php");

	$dompdf = new DOMPDF();
	$dompdf->load_html($inicio.$encabezadopdf.$infor.$cuerpo.$fin.$piePagina);
	$dompdf->set_paper("a4", "portrait");
	$dompdf->set_base_path(getcwd());
	$dompdf->render();

	//$nombarch = "/". substr($verrad,0,4)."/".substr($verrad,4,3)."/".$registro["textrad"].".pdf";

	//file_put_contents("$ruta_raiz/bodega".$nombarch, $dompdf->output());
	$dompdf->stream("Reporte_DocumentosIngresadosporUsuario.pdf");

	//exec ("mv /tmp/$nombarch /var/www/orfeo/bodega/2008/900/$nombarch",$output,$returnS);
	//$sql = "UPDATE RADICADO SET RADI_PATH='$nombarch' where radi_nume_radi=$verrad";
	//$rs = $db->query($sql);
	//echo "<script>window.close();</script>";

*/
?>
