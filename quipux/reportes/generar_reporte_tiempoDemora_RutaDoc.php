<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?=$ruta_raiz?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?=$ruta_raiz?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?=$ruta_raiz?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?


session_start();
if(empty($ruta_raiz)) $ruta_raiz = "..";

//Limpia parametros
unset($query1);
unset($deRecorr);
unset($fechaRecorr);
unset($accionRecorr);
unset($paraRecorr);
unset($tot_dias);
unset($comentarioRecorr);



$cod_estado=$_SESSION["depe_codi"];
$titulos=$_SESSION['$titulos'];
$numdoc=$_SESSION['$documento'];
$datos=$_SESSION['d_reporte'];
$area=$_SESSION['$area'];


//Datos para reporte-Recorrido
$queryR=$_SESSION['$isql'];

//ECHO "sql".$_SESSION['$isql'];
include_once "$ruta_raiz/rec_session.php";
$db = new ConnectionHandler("$ruta_raiz","reportes");

$query1= $db->query($queryR);


//ECHO "ASAS".COUNT($_SESSION['$titulos']);


//Datos Reporte Recorrido
while(!$query1->EOF)  {

    if (!isset($query1->fields["USUA_ORI"])|| $query1->fields["USUA_ORI"]=='' ){
        $deRecorr[]="--";
    }
    else{
        $deRecorr[]= $query1->fields["USUA_ORI"];
    }

    $fechaRecorr[]= $query1->fields["HIST_FECH1"];
    $accionRecorr[]= $query1->fields["SGD_TTR_DESCRIP"];
    $paraRecorr[]= $query1->fields["USUA_DEST"];

    if (!isset($query1->fields["HIST_OBSE"])|| $query1->fields["HIST_OBSE"]==''){
        $comentarioRecorr[]= "--";
    }
    else{
        $comentarioRecorr[]= $query1->fields["HIST_OBSE"];
    }
    $tot_dias[]= $query1->fields["TOT_DIAS"];

    $query1->MoveNext();
    $cont+=1;
}


//Armo HTML
$inicio = '
<html>
<head>
<title>.: QUIPUX - VISTA PREVIA :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="margin: 5 20 30 5;">
';
//<tr><td></td></tr>
//<tr><td><center><img src="logoEntidad.gif" width=5 height=5 /></center><td></tr>
//<tr><th align="center"><font size="2">Gobierno Nacional De La República Del Ecuador</th></tr>
$encabezadopdf = '
<table align="center" border="0">
<tr><th align="center"><font size="3">Hoja de Ruta</th></tr>
</table>
';


/*<tr>
    <td  colspan="1"  align="center"><font><strong>Reporte Recorrido </strong></font></td>
</tr>
*/

	if(!empty($cont)){
		$infor='
		<table width="100%">
		<tr>
			<th align="left">Area:</th>
			<td>'.$area.'</td>
            <th align="left">Documento:</th>
			<td>'.$numdoc.'</td>
		</tr>
		<tr>
			<td align="left" size="5%"><b>Fecha Desde:</b></td>
			<td align="left">'.$datos[2].'</td>
			<td align="right"><b>Hasta:</b></td>
			<td align="right">'.$datos[3].'</td>
		</tr>
		<br />
		</table>';

        $cuerpo1='<HR><table border="0" width="100%" align="center"><tr>';
        for ($j=1;$j<=count( $_SESSION['$titulos']);$j++){
                $cuerpo1.='<th><font size="1"> '.substr($titulos[$j],2).'</font></Th>';
        }

        $cuerpo1.='</tr>';
        $cuerpo=$cuerpo1;

		for ($i=0;$i<=$cont;$i++ ){
            $cuerpo2 .= '<tr><td><font size="1">'.$deRecorr[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$fechaRecorr[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$accionRecorr[$i].'</font></td>';
            if ($deRecorr[$i] == $paraRecorr[$i]){
                $cuerpo2 .= '<td></td>';
            }else{
                $cuerpo2 .= '<td>'.$paraRecorr[$i].'</td>';
            }
            $cuerpo2 .= '<td><font size="1">'.$tot_dias[$i].'</font></td>';
            $cuerpo2 .= '<td><font size="1">'.$comentarioRecorr[$i].'</font></td>;
            </tr>';
		}
 $cuerpo .= $cuerpo2.'</table>';
}

$fin = '
</body>
</html>';


//echo $inicio.$encabezadopdf.$infor.$cuerpo;//$cuerpo2;


//GENERACION DEL PDF
include "$ruta_raiz/config.php";
require_once("$ruta_raiz/interconexion/generar_pdf.php");
$plantilla = "";
$plantilla = "$ruta_raiz/bodega/plantillas/".$cod_estado.".pdf";
$doc_pdf=$inicio.$encabezadopdf.$infor.$cuerpo;
$pdf = ws_generar_pdf($doc_pdf, $plantilla, $servidor_pdf);
$nomArch="ReporteRuta_TiempoDemora.pdf";
header( "Content-Disposition: attachment; filename=$nomArch");
header("Content-Type:application/pdf");//.application/pdf
header("Content-Transfer-Encoding: binary");
echo  $pdf;



?>
