<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
//////////////////////////////////////////////////////////////////////
// index.php
//--------------------------------------------------------------------
//
// Holding Constant
//
//--------------------------------------------------------------------
// Revision History
// V1.02	8  mar	2005	Jean-Sebastien Goupil	Spreading all Classes in single file
// V1.00	17 jun	2004	Jean-Sebastien Goupil
//--------------------------------------------------------------------
// Copyright (C) Jean-Sebastien Goupil
// http://other.lookstrike.com/barcode/
//--------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////
if(!defined('IN_CB'))die('You are not allowed to access to this page.');

//////////////////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////////////////
define('IMG_FORMAT_PNG',	1);
define('IMG_FORMAT_JPEG',	2);
define('IMG_FORMAT_WBMP',	4);
define('IMG_FORMAT_GIF',	8);
?>