<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
class tpl_LS {
	function __header ( $width, $title ) {
		echo '<table width="'.$width.'" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#004163" class="tableline"><tr><th><u>'.$title.'</u>'."\n".'</th></tr><tr><td>'."\n".'<table width="100%" border="0" cellpadding="4" cellspacing="1" align="center">';
	}

	function __footer ( $width, $title ) {
		echo '</table>'."\n".'</td></tr></table>'."\n";
	}

	function __row_start ( $attributes ) {
		$code = '';
		$found_class = false;
		reset( $attributes );
		while( list( $key, $value ) = each( $attributes ) ) {
			$code .= ' '.$key.'="'.$value.'"';
			if( $key == 'class' )
				$found_class = true;
		}
		if( $found_class=== false )
			$code .= ' class="'.next_color().'"';
		echo '<tr'.$code.'>';
	}

	function __row_stop ( $attributes ) {
		echo '</tr>';
	}

	function __cell_start ( $text, $attributes ) {
		$code = '';
		reset( $attributes );
		while( list( $key, $value ) = each( $attributes ) )
			$code .= ' '.$key.'="'.$value.'"';
		echo '<td'.$code.'>'.$text;
	}

	function __cell_stop ( $text, $attributes ) {
		echo '</td>';
	}
}

class tpl_BLANK {
	function __header ( $width, $title ) {
		echo '<table width="'.$width.'" border="0" cellpadding="0" cellspacing="0" align="center">';
	}

	function __footer ( $width, $title ) {
		echo '</table>';
	}

	function __row_start ( $attributes ) {
		echo '<tr>';
	}

	function __row_stop ( $attributes ) {
		echo '</tr>';
	}

	function __cell_start ( $text, $attributes ) {
		$code = '';
		reset( $attributes );
		while( list( $key, $value ) = each( $attributes ) )
			$code .= ' '.$key.'="'.$value.'"';
		echo '<td'.$code.'>'.$text;
	}

	function __cell_stop ( $text, $attributes ) {
		echo '</td>';
	}
}
?>