<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

if(!defined('IN_CB'))die('You are not allowed to access to this page.');

/**
 * Holds Color in RGB Format.
 */
class FColor {
	protected $r,$g,$b;	// int Hexadecimal Value

	/**
	 * Save RGB value into the classes
	 *
	 * @param int $r
	 * @param int $g
	 * @param int $b
	 */
	public function __construct($r,$g,$b){
		$this->r = $r;
		$this->g = $g;
		$this->b = $b;
	}

	/**
	 * Returns Red Color
	 *
	 * @return int
	 */
	public function r(){
		return $this->r;
	}

	/**
	 * Returns Green Color
	 *
	 * @return int
	 */
	public function g(){
		return $this->g;
	}

	/**
	 * Returns Blue Color
	 *
	 * @return int
	 */
	public function b(){
		return $this->b;
	}

	/**
	 * Returns the int value for PHP color
	 *
	 * @return int
	 */
	public function allocate($im) {
		return imagecolorallocate($im,$this->r,$this->g,$this->b);
	}
};
?>