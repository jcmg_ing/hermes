<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

define('IN_CB',true);
include('header.php');

$n = $table->numRows();
$table->insertRows($n, 3);
$table->addRowAttribute($n,'class','table_title');
$table->addCellAttribute($n,0,'align','center');
$table->addCellAttribute($n,0,'colspan','2');
$table->setText($n,0,'<font color="#ffffff"><b>Specifics Configs</b></font>');
$table->setText($n+1,0,'Text Label');
$table->setText($n+1,1,'<input type="text" name="a1" value="'.$a1.'" />');
$table->setText($n+2,0,'Explanation');
$table->setText($n+2,1,'<ul style="margin: 0px; padding-left: 25px;"><li>Enter width of each bars with one characters. Begin by a bar.</li><li>10523 : Will do 2px bar, 1px space, 6px bar, 3px space, 4px bar.</li></ul>');
$table->draw();

echo '</form>';

include('footer.php');
?>
