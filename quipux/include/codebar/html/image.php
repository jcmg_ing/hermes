<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

if(isset($_GET['code']) && isset($_GET['t']) && isset($_GET['r']) && isset($_GET['text']) && isset($_GET['f']) && isset($_GET['o']) && isset($_GET['a1']) && isset($_GET['a2'])){
	define('IN_CB',true);
	require('../class/index.php');
	require('../class/FColor.php');
	require('../class/BarCode.php');
	require('../class/FDrawing.php');
	if(include('../class/'.$_GET['code'].'.barcode.php')){
		$color_black = new FColor(0,0,0);
		$color_white = new FColor(255,255,255);
		if(!empty($_GET['a2']))
			$code_generated = new $_GET['code']($_GET['t'],$color_black,$color_white,$_GET['r'],$_GET['text'],$_GET['f'],$_GET['a1'],$_GET['a2']);
		elseif(!empty($_GET['a1']))
			$code_generated = new $_GET['code']($_GET['t'],$color_black,$color_white,$_GET['r'],$_GET['text'],$_GET['f'],$_GET['a1']);
		else
			$code_generated = new $_GET['code']($_GET['t'],$color_black,$color_white,$_GET['r'],$_GET['text'],$_GET['f']);
		$drawing = new FDrawing(1024,1024,'',$color_white);
		$drawing->init();
		$drawing->add_barcode($code_generated);
		$drawing->draw_all();
		$im = $drawing->get_im();
		$im2 = imagecreate($code_generated->lastX,$code_generated->lastY);
		imagecopyresized($im2, $im, 0, 0, 0, 0, $code_generated->lastX, $code_generated->lastY, $code_generated->lastX, $code_generated->lastY);
		$drawing->set_im($im2);
		$drawing->finish($_GET['o']);
	}
	else{
		header('Content: image/png');
		readfile('error.png');
	}
}
else{
	header('Content: image/png');
	readfile('error.png');
}
?>