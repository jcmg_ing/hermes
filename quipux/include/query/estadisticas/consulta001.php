<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/


?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--link rel="stylesheet" href="../estilos/orfeo.css"-->
<link href="<?= $ruta_raiz ?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?


//////CODIGO REPORTES
session_start();
unset($_SESSION['queryE']);

$ruta_raiz = "..";
//unset($_SESSION['queryEDetalle']);
//$_SESSION['queryE']=null;
$_SESSION['ban']=0;
$cod_usuario=$_POST['codus'];
/////////////
///DEPENDENCIA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$_SESSION['uno'];
}
///USUARIO
if(!empty($_POST["codus"]))
{
	$cod_u=$_POST["codus"];
	unset($_SESSION['cod_u']);
	$_SESSION['cod_u']=$cod_u;
}


if($estado!='9999'){
	
    if ($estado== 6){
        $condicionL=" AND e.ESTA_CODI in ('6') ";
    }else{
        $condicionL=" AND e.ESTA_CODI= $estado";
    }
}
//
////////
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';
if(!$orno) $orno=2;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */
switch($db->driver)
	{
	case 'postgres':
	{		
		//if($tipoDocumento=='9999')
		//{
			if($dependencia_busq!='99999'){
				if(isset($_SESSION['uno'])){
					$dependencia_busq=$_SESSION['uno'];
					if($dependencia_busq!='99999'){
						$condicionE = "	AND b.depe_codi = $dependencia_busq";
                        $_SESSION['CondAsignado']=$condicionE;
                        $condicionE1 = "	where depe_codi = $dependencia_busq";
                        $condicionE2 = "	and  bb.depe_codi = $dependencia_busq";
                        $condicionE3 = "	AND a.depe_codi = $dependencia_busq";
					}
				}
			}


        	if(isset($_SESSION['cod_u'])){
				$cod_usuario=$_SESSION['cod_u'];
				if($cod_usuario!='99999'){
					$whereTipoRadi1 =" AND b.USUA_CODI= $cod_usuario";
                    $_SESSION['CondAsignadoUs']=$whereTipoRadi1;
                    $whereTipoRadi2 =" AND b.USUA_CODI_ORI= $cod_usuario"; //VEJM 06/10/2009(N)
                    $whereTipoRadi3 =" AND bb.USUA_CODI= $cod_usuario";
                    $whereTipoRadi4 =" AND H2.usua_codi_ori=$cod_usuario";
				}
			}
			/// el orden en el que se deben ordenar los datos.
			if(isset($_POST['adodb_orden_por'])){
				$ordenar_por=$_POST['adodb_orden_por'];
			}elseif($condicionE!=''){
				$ordenar_por=' 1';
            }elseif($condicionE==''){
				$ordenar_por='8';
            }

			if(isset($_POST['adodb_tipo_orden'])){
				$tipo_ordenar=' '.$_POST['adodb_tipo_orden'];
			}else{
				$tipo_ordenar=' ASC';
			}

//echo $ordenar_por;

			/*echo '<pre>';
			var_dump('dep_bus'.$dependencia_busq);
			var_dump('cod_us'.$cod_usuario);
			var_dump('condiE'.$condicionE);
			var_dump('wheretipo'.$whereTipoRadi1);
			where ((h.usua_codi<>h.usua_codi_dest or h.depe_codi<>h.depe_codi_dest) and h.SGD_TTR_CODIGO<>13 and (h.usua_codi<>r.radi_usua_actu or h.depe_codi<>r.radi_depe_actu))
			echo '</pre>';*/

            //echo "estado".$estado;

			if($estado!='9999'){
               
				if($estado=='1111'){
				//listado Asignados  VEJM 06/09/2009 (M)Tramitado

                    $queryE= "select count(p.radi_nume_radi) as RADICADOS, p.USUARIO as USUARIO,'Asignado' as ESTA_DESC,'1111' as esta_codi
                    ,P.depe_nomb AS NOMB_HID_DEPE_USUA
                    from (
                        select distinct
                        r.radi_nume_radi,
                        b.USUA_NOMBre as USUARIO,b.depe_nomb
                        from
                            (select radi_usua_actu,radi_nume_radi from radicado where  esta_codi=2  and radi_inst_actu=".$_SESSION['inst_codi']."
                            and  ".$db->conn->SQLDate('Y/m/d', 'radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                            )r
                        left outer join datos_usuarios b on r.radi_usua_actu=b.usua_codi
                        left outer join hist_eventos h on h.radi_nume_radi=r.radi_nume_radi
                    where
                        h.sgd_ttr_codigo=9
                        $condicionE
                        $whereTipoRadi1
                        )P

                    group by USUARIO,ESTA_DESC,P.depe_nomb";

                    //echo $queryE;
                }

                ELSEif($estado=='11111'){
				//listado Reasignados  VEJM 06/16/2009 (N)
                   $queryE="SELECT distinct
                    a.usua_nombre as usuario,
                    (select sgd_ttr_descrip from sgd_ttr_transaccion where sgd_ttr_codigo=9 ) as ESTA_DESC,
                    count (*) as RADICADOS,
                    a.usua_cedula,
                    9 as esta_codi,
                    a.usua_codi as hid_cod_usuario,
                    a.depe_codi as HID_DEPE_USUA,
                    a.depe_nomb AS NOMB_HID_DEPE_USUA,
                    
                    a.depe_codi
                    FROM
                    (select distinct  radi_nume_radi, hist_codi,usua_codi_ori from hist_eventos b
                    where b.sgd_ttr_codigo=9 and ".$db->conn->SQLDate('Y/m/d', 'b.hist_fech')." BETWEEN '$fecha_ini' AND '$fecha_fin' ) AS H2
                    left outer join DATOS_USUARIOS A ON a.usua_codi=h2.usua_codi_ori
                    left outer join radicado b ON h2.RADI_NUME_RADI=b.RADI_NUME_RADI
                    where a.inst_codi=".$_SESSION['inst_codi']." $condicionE3
                    $whereTipoRadi4
                    group by a.usua_nombre,a.usua_cedula,a.usua_codi,a.depe_codi,a.depe_nomb";
                 }
                 ELSEif($estado=='8888'){ //else
					//listado vencidos
					$queryE= "select b.USUA_NOMB||' '||b.USUA_APELLIDO as USUARIO, 'Vencido' as ESTA_DESC, count(*) as RADICADOS, b.USUA_CEDULA,
                    8888 as esta_codi, MIN(USUA_CODI) as HID_COD_USUARIO, MIN(b.depe_codi) as HID_DEPE_USUA,
                    (select depe_nomb from dependencia where depe_codi=MIN(b.depe_codi)) AS NOMB_HID_DEPE_USUA
					from usuarios b, radicado r 
					where ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
						AND r.radi_usua_actu=b.usua_codi
						AND r.radi_inst_actu=".$_SESSION['inst_codi']."
						and r.radi_fech_asig <= current_date
					$condicionE $whereTipoRadi1
					group by B.USUA_CEDULA, b.USUA_NOMB, b.USUA_APELLIDO
					order by ".$ordenar_por.$tipo_ordenar;
                    
                 }
                 //TRAMITE O RECIBIDO
                 ELSEif($estado==2){  
					
					$queryE= "SELECT 
                    b.USUA_NOMB||' '||b.USUA_APELLIDO as USUARIO, e.ESTA_DESC as ESTA_DESC,count(*) as RADICADOS,
                    b.USUA_CEDULA,e.esta_codi as ESTA_CODI,min(b.USUA_CODI) as HID_COD_USUARIO,
                    min(b.depe_codi) as HID_DEPE_USUA,
                    (select d.depe_nomb from dependencia d where d.depe_codi=min(b.depe_codi)) AS NOMB_HID_DEPE_USUA
                    FROM radicado r
                      left outer join usuarios b on r.radi_usua_actu=b.usua_codi
                      left outer join estado e on r.esta_codi=e.esta_codi
                    WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                    AND r.radi_fech_agend is null AND 
                    e.ESTA_CODI= 2  and 
                    r.radi_inst_actu=".$_SESSION['inst_codi']."
                    $condicionE
                    $whereTipoRadi1 AND
                    r.radi_nume_radi not in(
                        select
                        (p.radi_nume_radi) as RADICADOS
                        from ( select distinct r.radi_nume_radi, bb.USUA_NOMBre as USUARIO 
                            from 
                            (select radi_usua_actu,radi_nume_radi from radicado where 
                             esta_codi=2 and 
                             radi_inst_actu=".$_SESSION['inst_codi']."  and
                             ".$db->conn->SQLDate('Y/m/d', 'radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' )r

                        left outer join datos_usuarios bb on r.radi_usua_actu=bb.usua_codi 
                        left outer join hist_eventos h on h.radi_nume_radi=r.radi_nume_radi 
                        where h.sgd_ttr_codigo=9 
                        $condicionE2  $whereTipoRadi3 )P )
                     GROUP BY b.USUA_CEDULA, b.USUA_NOMB, b.USUA_APELLIDO, e.esta_desc, e.esta_codi";
                    //echo $queryE;
					//order by ".$ordenar_por.$tipo_ordenar;
                 }
                else
                 {
                	//listado otros
                    $queryE = "SELECT b.USUA_NOMB||' '||b.USUA_APELLIDO as USUARIO, e.ESTA_DESC as ESTA_DESC, count(*) as RADICADOS, b.USUA_CEDULA,
                    e.esta_codi as ESTA_CODI, MIN(USUA_CODI) as HID_COD_USUARIO, MIN(b.depe_codi) as HID_DEPE_USUA,
                    (select depe_nomb from dependencia where depe_codi=MIN(b.depe_codi)) AS NOMB_HID_DEPE_USUA
                    FROM radicado r
                      left outer join usuarios b on r.radi_usua_actu=b.usua_codi
                      left outer join estado e on r.esta_codi=e.esta_codi
                    WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                            AND r.radi_inst_actu=".$_SESSION['inst_codi']."
                            AND radi_fech_agend is null
                            $condicionE $whereTipoRadi1
                            $condicionL
                    GROUP BY  b.USUA_CEDULA, b.USUA_NOMB, b.USUA_APELLIDO, e.esta_desc, e.esta_codi ORDER BY ".$ordenar_por.$tipo_ordenar;
                       //echo $queryE;
                       
				}
            }
			else{

            

            //listado general
            
                $queryE = "SELECT b.USUA_NOMB||' '||b.USUA_APELLIDO as USUARIO, e.ESTA_DESC, count(*) as RADICADOS, b.USUA_CEDULA, e.esta_codi,
                MIN(USUA_CODI) as HID_COD_USUARIO, MIN(b.depe_codi) as HID_DEPE_USUA,
                (select depe_nomb from dependencia where depe_codi=MIN(b.depe_codi)) AS NOMB_HID_DEPE_USUA
                    FROM radicado r
                      left outer join usuarios b on r.radi_usua_actu=b.usua_codi
                      left outer join estado e on r.esta_codi=e.esta_codi
                WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                    AND r.radi_inst_actu=".$_SESSION['inst_codi']."
                    AND radi_fech_agend is null
                    AND r.esta_codi not in (2,8,3,5)
                    $condicionE $whereTipoRadi1
                    $condicionL
                GROUP BY  b.USUA_CEDULA, b.USUA_NOMB, b.USUA_APELLIDO, e.esta_desc, e.esta_codi

                union

                SELECT
                    b.USUA_NOMB||' '||b.USUA_APELLIDO as USUARIO, e.ESTA_DESC as ESTA_DESC,count(*) as RADICADOS,
                    b.USUA_CEDULA,e.esta_codi as ESTA_CODI,min(b.USUA_CODI) as HID_COD_USUARIO,
                    min(b.depe_codi) as HID_DEPE_USUA,
                    (select d.depe_nomb from dependencia d where d.depe_codi=min(b.depe_codi)) AS NOMB_HID_DEPE_USUA
                    FROM radicado r
                      left outer join usuarios b on r.radi_usua_actu=b.usua_codi
                      left outer join estado e on r.esta_codi=e.esta_codi
                    WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                    AND r.radi_fech_agend is null AND
                    e.ESTA_CODI= 2  and
                    r.radi_inst_actu=".$_SESSION['inst_codi']."
                    $condicionE
                    $whereTipoRadi1 AND
                    r.radi_nume_radi not in(
                        select
                        (p.radi_nume_radi) as RADICADOS
                        from ( select distinct r.radi_nume_radi, bb.USUA_NOMBre as USUARIO
                            from
                            (select radi_usua_actu,radi_nume_radi from radicado where
                             esta_codi=2 and
                             radi_inst_actu=".$_SESSION['inst_codi']."  and
                             ".$db->conn->SQLDate('Y/m/d', 'radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' )r

                        left outer join datos_usuarios bb on r.radi_usua_actu=bb.usua_codi
                        left outer join hist_eventos h on h.radi_nume_radi=r.radi_nume_radi
                        where h.sgd_ttr_codigo=9
                        $condicionE2  $whereTipoRadi3 )P )
                        GROUP BY b.USUA_CEDULA, b.USUA_NOMB, b.USUA_APELLIDO, e.esta_desc, e.esta_codi


                    union

                    /*Asignado*/
                    select
                    p.USUARIO as USUARIO,
                    'Asignado' as ESTA_DESC,
                    count(p.radi_nume_radi) as RADICADOS,
                    '111111'as USUA_CEDULA
                    ,'1111' as esta_codi ,
                    MIN(p.USUA_CODI) as HID_COD_USUARIO, MIN(p.depe_codi) as HID_DEPE_USUA,
                    (select depe_nomb from dependencia where depe_codi=MIN(p.depe_codi)) AS NOMB_HID_DEPE_USUA
                    from (
                        select distinct
                        r.radi_nume_radi,
                        b.USUA_NOMBre as USUARIO,b.USUA_CODI ,b.depe_codi
                        from
                            (select radi_usua_actu,radi_nume_radi from radicado where  esta_codi=2  and radi_inst_actu=".$_SESSION['inst_codi']."
                            and  ".$db->conn->SQLDate('Y/m/d', 'radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                            )r
                        left outer join datos_usuarios b on r.radi_usua_actu=b.usua_codi
                        left outer join hist_eventos h on h.radi_nume_radi=r.radi_nume_radi
                    where
                        h.sgd_ttr_codigo=9
                        $condicionE
                        $whereTipoRadi1
                        )P

                    group by USUARIO,ESTA_DESC

            union

            /*reasignado*/
            select distinct
            u.usua_nombre as USUARIO ,
            (select sgd_ttr_descrip from sgd_ttr_transaccion where sgd_ttr_codigo=9 ) as ESTA_DESC,
            count (*) as RADICADOS,
            '11111' as usua_cedula,
            9 as esta_codi,
            u.usua_codi as hid_cod_usuario,
            u.depe_codi as HID_DEPE_USUA,
            (select depe_nomb from dependencia where depe_codi=MIN(u.depe_codi)) AS NOMB_HID_DEPE_USUA
            from (select radi_nume_radi, hist_codi
            from hist_eventos b  where b.sgd_ttr_codigo=9 and
            TO_CHAR(b.hist_fech,'YYYY/MM/DD') BETWEEN '$fecha_ini' AND '$fecha_fin'  $whereTipoRadi2) as h2
            left outer join hist_eventos h on h.hist_codi=h2.hist_codi
            left outer join
            (select usua_codi, usua_nomb||' '||usua_apellido as usua_nombre, depe_codi, inst_codi,usua_cedula from usuarios
            $condicionE1
            union all
            select ciu_codigo, ciu_nombre||' '||ciu_apellido,0,0,'0' from ciudadano) as u
            on h.usua_codi_ori=u.usua_codi ,
            (select * from radicado where radi_inst_actu=".$_SESSION['inst_codi']." ) as b where h.radi_nume_radi=b.radi_nume_radi
            and u.usua_nombre<>''
            group by u.usua_nombre,u.usua_cedula,usua_codi,u.depe_codi";

        }
            //echo $queryE;

			/*
                          //asignado
            select distinct b.USUA_NOMB||' '||b.USUA_APELLIDO as USUARIO,
            'Asignado'as esta_desc,
            count(distinct r.radi_nume_radi) as RADICADOS,
            '1111' as USUA_CEDULA,
            (select sgd_ttr_codigo from sgd_ttr_transaccion where sgd_ttr_codigo=9) as esta_codi,
            MIN(USUA_CODI) as HID_COD_USUARIO, MIN(b.depe_codi) as HID_DEPE_USUA,
            (select depe_nomb from dependencia where depe_codi=MIN(b.depe_codi)) AS NOMB_HID_DEPE_USUA
            from usuarios b, radicado r left outer join hist_eventos h on h.radi_nume_radi=r.radi_nume_radi
            where h.sgd_ttr_codigo=9 and (r.esta_codi=0 or r.esta_codi=2)
            AND r.radi_usua_actu=b.usua_codi
            AND r.radi_inst_actu=".$_SESSION['inst_codi']."
            AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
            $condicionE $whereTipoRadi1
            group by B.USUA_CEDULA, b.USUA_NOMB, b.USUA_APELLIDO

              union

		    select b.USUA_NOMB||' '||b.USUA_APELLIDO as USUARIO, 'Asignado' as ESTA_DESC, count(*) as RADICADOS, b.USUA_CEDULA,
            8888 as esta_codi, MIN(USUA_CODI) as HID_COD_USUARIO, MIN(b.depe_codi) as HID_DEPE_USUA,
            (select depe_nomb from dependencia where depe_codi=MIN(b.depe_codi)) AS NOMB_HID_DEPE_USUA
            from usuarios b, radicado r
			where ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
				AND r.radi_usua_actu=b.usua_codi
				AND r.radi_inst_actu=".$_SESSION['inst_codi']."
				and r.radi_fech_asig <= current_date
                AND R.ESTA_CODI<>6
			$condicionE $whereTipoRadi1
			group by B.USUA_CEDULA, b.USUA_NOMB, b.USUA_APELLIDO
            order by $ordenar_por $tipo_ordenar";
			}

        echo $queryE;*/



            //$_SESSION['queryE']=NULL;
			$_SESSION['queryE']=$queryE;


			//consulta para obtener los estados del trámite
			$queryR2="SELECT ESTA_CODI, ESTA_DESC FROM ESTADO";
			$rsE= $db->query($queryR2);

            while(!$rsE->EOF){
				$estados[]=$rsE->fields["ESTA_DESC"];
				$rsE->MoveNext();
				}

            $condicionE1 = " AND b.USUA_CEDULA= $usua_docu";
				if($dependencia_busq!='99999'){
					$whereDependencia1= "AND b.DEPE_CODI = $dependencia_busq";
				}


                    if($estado1=='1111'){
						$queryEDetalle = "select distinct RADI_NUME_TEXT as RADICADO
						,date(r.RADI_FECH_RADI) as FECHA_RADICADO
						,t.trad_descr as TIPO_DESC
						,extract(day from dias) as DIAS
						,USUA_NOMB||' '||USUA_APELLIDO as USUARIO
						,r.RADI_ASUNTO as ASUNTO
						,'Tramitado' AS ESTADO
						,r.radi_nume_radi as RAD_NUM
						from tiporad t, usuarios b, (select (current_date-radi_fech_radi) as dias,
 						radi_nume_radi from radicado) dia,
						radicado r left outer join hist_eventos h on h.radi_nume_radi=r.radi_nume_radi
						where h.sgd_ttr_codigo=9 and (r.esta_codi=0 or r.esta_codi=2)
						AND r.radi_usua_actu=b.usua_codi
						AND t.trad_codigo=r.radi_tipo
						AND r.radi_nume_radi=dia.radi_nume_radi
						AND r.radi_inst_actu=".$_SESSION['inst_codi']."
						AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
						$whereActivos $whereTipoRadicado
						$condicionE1
						order by $ordenar_por $tipo_ordenar";
					}elseif($estado1=='8888'){
						$queryEDetalle = "select RADI_NUME_TEXT as RADICADO
						,date(r.RADI_FECH_RADI) as FECHA_RADICADO
						,t.trad_descr as TIPO_DESC
						,extract(day from dias) as DIAS
						,USUA_NOMB||' '||USUA_APELLIDO AS USUARIO
						,r.RADI_ASUNTO as ASUNTO
						,'Vencido' AS ESTADO
						,r.radi_nume_radi as RAD_NUM
						from usuarios b, radicado r, tiporad t, (select (current_date-radi_fech_radi) as dias,
 						radi_nume_radi from radicado) dia
						where ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
						AND r.RADI_USUA_ACTU=b.USUA_CODI
						AND t.trad_codigo=r.radi_tipo
						AND r.radi_inst_actu=".$_SESSION['inst_codi']."
						AND r.radi_nume_radi=dia.radi_nume_radi
						AND r.radi_fech_asig <= current_date
						$whereActivos $whereTipoRadicado
						$condicionE1
						order by $ordenar_por $tipo_ordenar";
					}else{

                        
						$condicion1=" AND e.ESTA_CODI= $estado1";

						$queryEDetalle = "SELECT RADI_NUME_TEXT as RADICADO
						,date(r.RADI_FECH_RADI) as FECHA_RADICADO
						,t.trad_descr as TIPO_DESC
						,extract(day from dias) as DIAS
						,USUA_NOMB||' '||USUA_APELLIDO AS USUARIO
						,r.RADI_ASUNTO as ASUNTO
						,e.ESTA_DESC AS ESTADO
						,r.RADI_DESC_ANEX
						,b.usua_nomb||' '||b.USUA_APELLIDO as USUARIO
						,r.radi_nume_radi as RAD_NUM
						,r.RADI_PATH as HID_RADI_PATH
						FROM USUARIOS b, RADICADO r , estado e, tiporad t, (select (current_date-radi_fech_radi) as dias,radi_nume_radi from radicado) dia
						WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
							AND r.RADI_USUA_ACTU=b.USUA_CODI
							AND r.esta_codi=e.esta_codi
							AND radi_fech_agend is null
							AND r.radi_inst_actu=".$_SESSION['inst_codi']."
							AND t.trad_codigo=r.radi_tipo
							AND r.radi_nume_radi=dia.radi_nume_radi
							$whereActivos $whereTipoRadicado";

						$orderE = "	ORDER BY $ordenar_por $tipo_ordenar";

                        // CONSULTA PARA VER TODOS LOS DETALLES

						$queryETodosDetalle = $queryEDetalle . $orderE;
						$queryEDetalle .= $condicionE1 . $condicion1 . $orderE;
					}



	if(!empty($usua_docu)){
				$_SESSION['queryEDetalle']=$queryEDetalle;
			}


			//DATOS DEL REPORTE-------------
			//consulta para obtener el nombre del departamento.
			if(isset($_SESSION['uno'])){
				$dependencia_busq_rep=$_SESSION['uno'];
				if($dependencia_busq_rep!='99999'){
					$queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq_rep;
					$rsE= $db->query($queryR);
					$area=$rsE->fields["DEPE_NOMB"];
				}
			}

            //consulta para obtener el nombre del usuario.
			if(isset($_SESSION['cod_u'])){
				$cod_usuario_rep=$_SESSION['cod_u'];
				if($cod_usuario_rep!='99999'){
					$whereTipoRadi1 =" AND b.USUA_CODI= $cod_usuario_rep";
					$queryR1="SELECT b.USUA_NOMB, b.USUA_APELLIDO FROM USUARIOS b WHERE 1=1".$whereTipoRadi1." AND
					b.DEPE_CODI=".$dependencia_busq_rep;
					$rsE= $db->query($queryR1);
					$nombre=$rsE->fields["USUA_NOMB"].' '.$rsE->fields["USUA_APELLIDO"];

				}
			}
			//var_dump($queryR1);



            //enviar los datos REPORTE
            if ($condicionE!='')
            {
                        $_SESSION['d_reporte']=NULL;
                        $datos =array("1"=>$area, "2"=>$fecha_ini, "3"=>$fecha_fin, "4"=>$nombre);
                        $_SESSION['d_reporte']=$datos;
            }

            //var_dump($datos);

            //DATOS DEL REPORTE DETALLE-------------
			//consulta para obtener el nombre del departamento.
			if(!empty($usua_docu)){
				$queryR_det= "SELECT DEPE_NOMB FROM USUARIOS B, DEPENDENCIA D WHERE B.DEPE_CODI=D.DEPE_CODI".$condicionE1;
				$rsE= $db->query($queryR_det);
					$area1=$rsE->fields["DEPE_NOMB"];
			}

 //var_dump($queryR_det);


			//consulta para obtener el nombre del usuario.

            if(!empty($usua_docu)){
				$queryR1_det="SELECT b.USUA_NOMB, b.USUA_APELLIDO FROM USUARIOS b WHERE 1=1".$condicionE1;
				$rsE= $db->query($queryR1_det);
					$nombre1=$rsE->fields["USUA_NOMB"].' '.$rsE->fields["USUA_APELLIDO"];
			}
			//var_dump($queryR1_det);



            //enviar los datos REPORTE DET
			$_SESSION['d_reporte_det']=NULL;
			$datos1 =array("1"=>$area1, "2"=>$fecha_ini, "3"=>$fecha_fin, "4"=>$nombre1);
			$_SESSION['d_reporte_det']=$datos1;
			//var_dump($datos1);

 	break;

	}

 }
 
$_SESSION['fechaIni']=$datos1[2];
$_SESSION['fechaFin']=$datos1[3];
$_SESSION['nombre']=$datos1[4];



if(isset($_GET['genDetalle'])&& ($_GET['genDetalle']==1))
	{
        if ($condicionE!='')
		{
            $titulos=array("#","1#DOCUMENTO","2#FECHA INGRESO","3#TIPO DE DOCUMENTO","4#DIAS EN TRAMITE","5#USUARIO","6#ASUNTO","7#ESTADO");
        }
        elseif ($condicionE == '')
        {
            $titulos=array("#","1#DOCUMENTO","2#FECHA INGRESO","3#TIPO DE DOCUMENTO","4#DIAS EN TRAMITE","5#USUARIO","6#ASUNTO","7#ESTADO","8#AREA");
        }
	}
else
    {
        if ($condicionE!='')
		{
            $titulos=array("#","1#USUARIO","2#ESTADO DEL TRAMITE", "3#NUMERO DE DOCUMENTOS");
        }
        elseif ($condicionE == '')
        {
            $titulos=array("#","1#AREA","2#USUARIO", "3#ESTADO DEL TRAMITE","4#NUMERO DE DOCUMENTOS");
            //$titulos=array("#","1#USUARIO","2#ESTADO DEL TRAMITE", "3#NUMERO DE DOCUMENTOS","4#AREA");
        }
	}




function pintarEstadistica($fila,$indice,$numColumna){
global $ruta_raiz,$_POST,$_GET;
global $condicionE;
$salida="";
$m=0;
if ($condicionE == '')
{
    		switch ($numColumna){
				case  0:
                    $salida=$indice;
					break;
				case 1:
                    $salida=$fila['NOMB_HID_DEPE_USUA'];
				break;
				case 2:
                   $salida=$fila['USUARIO'];
				break;
				case 3:
                    $salida=$fila['ESTA_DESC'];
                    break;
                case 4:
                   $datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".$fila['USUA_CEDULA']."&amp;estado1=".$fila['ESTA_CODI']."&amp;estado=".$_POST['estado']."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;codUs=".$fila['HID_COD_USUARIO']."&amp;user=".$fila['USUARIO']."&amp;area=".$fila['NOMB_HID_DEPE_USUA']."&amp;descEstado=".$fila['ESTA_DESC']."&amp;numDocs=".$fila['RADICADOS'];
        		   $datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
                   $salida="<a class=\"grid\" href=\"vistaFormConsulta.php?Z=333&amp;{$datosEnvioDetalle}\">".$fila['RADICADOS']."</a>";
                   break;
			default: $salida=false;
			}
			return $salida;
//

}
elseif ($condicionE != '')
{
    switch ($numColumna){
				case  0:
                    $salida=$indice;
					break;
            case 1:
                    $salida=$fila['USUARIO'];
				break;
				case 2:
                   $salida=$fila['ESTA_DESC'];
				break;
				case 3:

                    $datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".$fila['USUA_CEDULA']."&amp;estado1=".$fila['ESTA_CODI']."&amp;estado=".$_POST['estado']."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;codUs=".$fila['HID_COD_USUARIO']."&amp;user=".$fila['USUARIO']."&amp;area=".$fila['NOMB_HID_DEPE_USUA']."&amp;descEstado=".$fila['ESTA_DESC']."&amp;numDocs=".$fila['RADICADOS'];
        		    $datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
                    $salida="<a  class=\"grid\" href=\"vistaFormConsulta.php?Z=333&amp;{$datosEnvioDetalle}\">".$fila['RADICADOS']." </a>";
                    break;

			default: $salida=false;
			}
			return $salida;
}
}


function pintarEstadisticaDetalle($fila,$indice,$numColumna){
			///////CODIGO REPORTES
			if($_SESSION['ban']==0){
				$boton_det='<input type=submit class="botones_largo" id="imp_det" value="imprimir" onClick="imprimir_det()"/><br><br>';
				echo $boton_det;
				$_SESSION['ban']=1;
			}
			//////////////

			global $ruta_raiz,$encabezado,$krd;
			$verImg=($fila['SGD_SPUB_CODIGO']==1)?($fila['USUARIO']!=$_SESSION['usua_nomb']?false:true):($fila['USUA_NIVEL']>$_SESSION['nivelus']?false:true);
        	$numRadicado=$fila['RADICADO'];
            
            
			switch ($numColumna){
					case 0:
						$salida=$indice;
						break;
                case 1:

							$salida="<center class=\"leidos\">{$numRadicado}</center>";	
						break;
					case 2:
						if($verImg)
							//echo "aqui";
		   					$salida="<center><a class=\"vinculos\" href=\"{$ruta_raiz}verradicado.php?verrad=".$fila['RAD_NUM']."&estadisticas=1"."&amp;".session_name()."=".session_id()."&amp;krd=".$_GET['krd']."&amp;carpeta=8&amp;nomcarpeta=Busquedas&amp;tipo_carp=0 \" >".$fila['FECHA_RADICADO']."</a></center>";
		   				else 
							//echo "aqui1";
		   					//$salida="<a class=\"vinculos\" href=\"#\" onclick=\"alert(\"ud no tiene permisos para ver el Documento\");\">".$fila['FECHA_RADICADO']."</a>";
							$salida="<center class=\"leidos\">".$fila['FECHA_RADICADO']."</center>";
						break;
					case 3:
						$salida="<center class=\"leidos\">".$fila['TIPO_DESC']."</center>";
						break;
					case 4:
						$salida="<center class=\"leidos\">".$fila['DIAS']."</center>";
						break;
					case 5:
						$salida="<center class=\"leidos\">".$fila['USUARIO']."</center>";
						break;
					case 6:
						$salida="<center class=\"leidos\">".$fila['ASUNTO']."</center>";
						break;
					case 7:
						$salida="<center class=\"leidos\">".$fila['ESTADO']."</center>";
						break;
			}
			return $salida;
	}


//echo "condicion".$condicionE;

?>


<!--///////CODIGO REPORTES-->
<script language="Javascript">
function imprimir_det()
{
 window.open ("<?=$ruta_raiz?>/reporte_det_consulta1.php/");
}
</script>

