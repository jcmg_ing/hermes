<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

//ECHO "ESTOY AQUI";

?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--link rel="stylesheet" href="../estilos/orfeo.css"-->
<link href="<?= $ruta_raiz ?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?


include_once "$ruta_raiz/rec_session.php";
include_once "../include/db/ConnectionHandler.php";
if(!$db)  { $db = new ConnectionHandler('.');}


if(empty($ruta_raiz)) $ruta_raiz = "..";


//////CODIGO REPORTES
session_start();
unset($_SESSION['queryE']);

$ruta_raiz = "..";
$_SESSION['ban']=0;
$cod_usuario=$_POST['codus'];

///AREA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$dependencia_busq;
}

///USUARIO DE
if(!empty($_POST["codus"]))
{
	$cod_u=$_POST["codus"];
	unset($_SESSION['cod_u']);
	$_SESSION['cod_u']=$cod_u;
}

///USUARIO PARA
if(!empty($_POST["para"]))
{
	$cod_para=$_POST["para"];
	unset($_SESSION['cod_para']);
	$_SESSION['cod_para']=$cod_para;
}

///TIPO DOCUMENTO
if(!empty($_POST["tipo"]))
{
	$cod_tipo=$_POST["tipo"];
	unset($_SESSION['cod_tipo']);
	$_SESSION['cod_tipo']=$cod_tipo;
}

//consulta para obtener el nombre del departamento.
if(isset($_SESSION['uno'])){
    $dependencia_busq_rep=$_SESSION['uno'];
    if($dependencia_busq_rep!='99999'){
        $queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq_rep;
        $rsE= $db->query($queryR);
        $area=$rsE->fields["DEPE_NOMB"];
    }
}

$_SESSION['$area']=$area;



switch($db->driver)
{
	case 'postgres':
	{
			//CONDICION AREA
            if($dependencia_busq!='99999'){
				if(isset($_SESSION['uno'])){
					$dependencia_busq=$_SESSION['uno'];
					if($dependencia_busq!='99999'){
						$condicionE = "	AND ur.depe_codi = $dependencia_busq";
					}
				}
			}

            $queryE="select distinct
            ur.depe_codi,
            (select p.depe_nomb from dependencia p where p.depe_codi=ur.depe_codi) AS NOMAREA,
            b.RADI_NUME_RADI as CHK_CHKANULAR ,ur.usua_nombre as DE ,ud.usua_nombre as PARA ,
            b.RADI_ASUNTO as ASUNTO ,TO_CHAR(b.radi_fech_radi,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO,
            b.RADI_NUME_RADI as HID_RADI_NUME_RADI ,b.RADI_NUME_TEXT as NUMERO_DOCUMENTO ,b.radi_cuentai as NO_REFERENCIA,
            'No Enviado' as ESTADO,b.RADI_LEIDO as HID_RADI_LEIDO
            from (select * from radicado where radi_inst_actu=".$_SESSION["inst_codi"]." and esta_codi=5 ) as b
            left outer join
            (select radi_nume_radi, radi_path from radicado where radi_inst_actu=".$_SESSION["inst_codi"]." and
            (radi_permiso=0 or radi_usua_actu=".$_SESSION["usua_codi"].")) as bs on bs.radi_nume_radi=b.radi_nume_radi
            left outer join (select usua_codi, usua_nomb||' '||usua_apellido as usua_nombre,
            depe_codi, inst_codi from usuarios
            union all select ciu_codigo, ciu_nombre||' '||ciu_apellido,0,0 from ciudadano) as ud
            on replace(b.radi_usua_dest,'-','')=ud.usua_codi
            left outer join (select usua_codi, usua_nomb||' '||usua_apellido as usua_nombre, depe_codi, inst_codi
            from usuarios union all select ciu_codigo, ciu_nombre||' '||ciu_apellido,0,0 from ciudadano) as ur
            on replace(b.radi_usua_rem,'-','')=ur.usua_codi
            where ".$db->conn->SQLDate('Y/m/d', 'b.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
            $condicionE
            and b.radi_leido=0  order by 2,4,7 DESC ";

           //echo $queryE;
           if(!$db)  { $db = new ConnectionHandler('.');}
           $query1= $db->query($queryE);
}

//Consulta sql
$_SESSION['queryE']=$queryE;
//ECHO $_SESSION['queryE'];

//Datos de Cabecera
$_SESSION['d_reporte']=NULL;
//echo 'sesion'.$dependencia_busq;

$datos =array("1"=>$dependencia_busq, "2"=>$cod_u, "3"=>$cod_para, "4"=>$cod_tipo, "5"=>$fecha_ini, "6"=>$fecha_fin, "7"=>$estado);
$_SESSION['d_reporte']=$datos;

break;


}//fin switch


$rsE= $db->query($queryE);


if ($dependencia_busq_rep!="99999"){
    $titulos=array("#","1#DE","2#PARA","3#ASUNTO","4#FECHA DOCUMENTO","5#No.DOCUMENTO","6#No.REFERENCIA","7#ESTADO");
}else{
    $titulos=array("#","1#AREA","2#DE","3#PARA","4#ASUNTO","5#FECHA DOCUMENTO","6#No.DOCUMENTO","7#No.REFERENCIA","8#ESTADO");
}

$_SESSION['$titulos']=$titulos;

if ($dependencia_busq_rep!="99999"){
    function pintarEstadistica($fila,$indice,$numColumna){
    global $ruta_raiz,$_POST,$_GET;
    global $condicionE;
    $salida="";
    $m=0;

    switch ($numColumna){
        case  0:
            $salida=$indice;
            break;
        case 1:
            $salida=$fila['DE'];
        break;
        case 2:
            $salida=$fila['PARA'];
        break;
        case 3:
            $salida=$fila['ASUNTO'];
            break;
        case 4:
            $salida=$fila['DAT_FECHA_DOCUMENTO'];
            break;
        case 5:
            $salida=$fila['NUMERO_DOCUMENTO'];
            break;
        case 6:
            $salida=$fila['NO_REFERENCIA'];
            break;
        case 7:
           $salida=$fila['ESTADO'];
            break;
    default: $salida=false;
    }
    return $salida;
    }
}else{
     function pintarEstadistica($fila,$indice,$numColumna){
    global $ruta_raiz,$_POST,$_GET;
    global $condicionE;
    $salida="";
    $m=0;

    switch ($numColumna){
        case  0:
            $salida=$indice;
            break;
        case 1:
            $salida=$fila['NOMAREA'];
        break;
        case 2:
            $salida=$fila['DE'];
        break;
        case 3:
            $salida=$fila['PARA'];
        break;
        case 4:
            $salida=$fila['ASUNTO'];
            break;
        case 5:
            $salida=$fila['DAT_FECHA_DOCUMENTO'];
            break;
        case 6:
            $salida=$fila['NUMERO_DOCUMENTO'];
            break;
        case 7:
            $salida=$fila['NO_REFERENCIA'];
            break;
        case 8:
           $salida=$fila['ESTADO'];
            break;
    default: $salida=false;
    }
    return $salida;
    }

}

