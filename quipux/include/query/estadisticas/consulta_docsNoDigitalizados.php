<?

/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--link rel="stylesheet" href="../estilos/orfeo.css"-->
<link href="<?= $ruta_raiz ?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?


include_once "$ruta_raiz/rec_session.php";
include_once "../include/db/ConnectionHandler.php";
if(!$db)  { $db = new ConnectionHandler('.');}



if(empty($ruta_raiz)) $ruta_raiz = "..";



//////CODIGO REPORTES
session_start();
$cod_usuario=$_POST['codus'];

///DEPENDENCIA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$_SESSION['uno'];
}
if ($dependencia_busq!=99999)
    $whereArea="s.depe_codi= $dependencia_busq and";
else
    $whereArea="";


switch($db->driver)
{
	case 'postgres':
	{
        $queryE="select
        distinct ver_usuarios(r.radi_usua_rem,',') as De,ver_usuarios(r.radi_usua_dest,',') as Para,
        r.radi_asunto as Asunto,TO_CHAR(r.radi_fech_radi,'YYYY-MM-DD / HH24:MI:SS') as Fecha_Documento
        ,r.RADI_NUME_TEXT as Numero_Documento,
        (select p.esta_desc from estado p where p.esta_codi=r.esta_codi )as Estado
        from radicado r
        left outer join datos_usuarios s on r.radi_usua_actu=s.usua_codi 
        where
        $whereArea
        (radi_path='' or  radi_path is null) and radi_tipo=2 
        and TO_CHAR(radi_fech_radi,'YYYY/MM/DD') BETWEEN '$fecha_ini' AND '$fecha_fin'
        and r.radi_inst_actu=".$_SESSION["inst_codi"]."
        and r.radi_usua_actu=".$_SESSION["usua_codi"]."
        and (r.esta_codi=6 or r.esta_codi=2) ";
            
        //echo $queryE;


    }
if(!$db)  { $db = new ConnectionHandler('.');}

//Consulta sql
$_SESSION['queryE']=$queryE;

//consulta para obtener el nombre del departamento.
if(isset($_SESSION['uno'])){
    $dependencia_busq_rep=$_SESSION['uno'];
    if($dependencia_busq_rep!='99999'){
        $queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq_rep;
        $rsE= $db->query($queryR);
        $area=$rsE->fields["DEPE_NOMB"];
    }
}

$_SESSION['$area']=$area;
//Datos de Cabecera
$_SESSION['d_reporte']=NULL;
$datos =array("1"=>$dependencia_busq, "2"=>$cod_u, "3"=>$cod_para, "4"=>$cod_tipo, "5"=>$fecha_ini, "6"=>$fecha_fin, "7"=>$estado);
$_SESSION['d_reporte']=$datos;
break;
}//fin switch



$titulos=array("#","1#De","2#Para","3#Asunto","4#Fecha documento","5#No.documento","6#Estado");
$_SESSION['$titulos']=$titulos;




    function pintarEstadistica($fila,$indice,$numColumna){
    $salida="";


    switch ($numColumna){
        case  0:
            $salida=$indice;
            break;
        case 1:
            $salida=$fila['DE'];
        break;
        case 2:
            $salida=$fila['PARA'];
        break;
        case 3:
            $salida=$fila['ASUNTO'];
        break;
        case 4:
            $salida=$fila['FECHA_DOCUMENTO'];
        break;
        case 5:
            $salida=$fila['NUMERO_DOCUMENTO'];
        break;
        case 6:
            $salida=$fila['ESTADO'];
        break;

    default: $salida=false;
    }
    return $salida;

}

