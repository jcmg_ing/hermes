<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */ 
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                      */
/*   Hollman Ladino       hladino@gmail.com                Desarrollador             */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion      */
/*  Martha Yaneth Mera    mymera@gmail.com     2006-05-10*/
/*************************************************************************************/
?>
<?
/** RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA
	* 
	* @autor JAIRO H LOSADA - SSPD
	* @version ORFEO 3.1
	* 
	*/
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';	
if(!$orno) $orno=1;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */
switch($db->driver)
	{
		case 'postgres':
		{	if ( $dependencia_busq != 99999)
				{	$condicionE = "	AND b.DEPE_CODI=$dependencia_busq AND r.RADI_DEPE_ACTU=$dependencia_busq ";
				}
				if($tipoDocumento=='9999')
				{	$queryE = "SELECT 
						count($radi_nume_radi) as RADICADO
						,b.usua_nomb as USUARIO
						,b.USUA_doc
						FROM RADICADO r, USUARIO b 
					WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' 
						AND r.RADI_USUA_ACTU=b.USUA_CODI 
						AND r.RADI_DEPE_ACTU=b.DEPE_CODI
						$condicionE
						$whereTipoRadicado 
						GROUP by b.USUA_doc, b.usua_nomb
					ORDER BY $orno $ascdesc"; 
				}
				else
				{	$queryE = "SELECT  
						, b.USUA_NOMB as USUARIO
						, t.SGD_TPR_DESCRIP as TIPO_DOCUMENTO
						, count($radi_nume_radi) as RADICADO
						, MIN(b.USUA_CODI) as HID_COD_USUARIO
						, MIN(SGD_TPR_CODIGO) as HID_TPR_CODIGO	
						, b.usua_doc		
					FROM RADICADO r 
						INNER JOIN USUARIO b ON r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=b.DEPE_CODI  
						LEFT OUTER JOIN SGD_TPR_TPDCUMENTO t ON r.tdoc_codi=t.SGD_TPR_CODIGO
					WHERE ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' AND 
						$condicionE $whereTipoRadicado  
					GROUP BY b.usua_doc, b.USUA_NOMB, t.SGD_TPR_DESCRIP
					ORDER BY $orno $ascdesc"; 
				}

		/** CONSULTA PARA VER DETALLES */
		
		$condicionE = " AND b.USUA_DOC= $usua_docu";
		
		if ($tipoDocumento != '9999')
		{
		$condicionE .= " AND t.SGD_TPR_CODIGO = $tipoDocumento ";
		}

		$queryEDetalle = "SELECT 
				$radi_nume_radi as RADICADO
				,t.SGD_TPR_DESCRIP as TIPO_DE_DOCUMENTO
				, b.USUA_NOMB as USUARIO
				, r.RA_ASUN as ASUNTO
				, ".$db->conn->SQLDate('Y/m/d H:i:s','r.radi_fech_radi')." as FECHA_RADICACION
				, bod.NOMBRE_DE_LA_EMPRESA as ESP
				,r.RADI_PATH as HID_RADI_PATH
				FROM USUARIO b, RADICADO r
					LEFT OUTER JOIN SGD_TPR_TPDCUMENTO t ON r.tdoc_codi = t.SGD_TPR_CODIGO 
					LEFT OUTER JOIN bodega_empresas bod ON r.eesp_codi = bod.identificador_empresa 
			WHERE   ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' 
				AND r.RADI_USUA_ACTU=b.USUA_CODI 
				AND r.RADI_DEPE_ACTU=b.DEPE_CODI";
			$orderE = "	ORDER BY $orno $ascdesc";			
					
		/** CONSULTA PARA VER TODOS LOS DETALLES 
		*/ 
		$queryETodosDetalle = $queryEDetalle . $orderE;
		$queryEDetalle .= $condicionE . $orderE;
		break;
		}
	}
	/*	case 'oracle':
	case 'oci8':
	case 'oci805':
	case 'ocipo':
	
	if ( $dependencia_busq != 99999)  {
		$condicionE = "				AND b.DEPE_CODI=$dependencia_busq 
			AND r.RADI_DEPE_ACTU=$dependencia_busq ";
	}

	if($tipoDocumento=='9999')
	{
	$queryE = "SELECT  b.USUA_NOMB USUARIO,
			count(r.RADI_NUME_RADI) RADICADOS,
			MIN(b.USUA_CODI) HID_COD_USUARIO
			FROM RADICADO r,
			USUARIO b,
			SGD_DIR_DRECCIONES dir
		WHERE	r.RADI_USUA_ACTU=b.USUA_CODI AND 
			r.RADI_DEPE_ACTU = b.DEPE_CODI AND
			r.radi_nume_radi = dir.radi_nume_radi (+) AND 
			dir.sgd_dir_tipo = 2 AND
			".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
			$condicionE
			$whereTipoRadicado 
			GROUP BY b.USUA_NOMB
		ORDER BY $orno $ascdesc"; 
	} else {
	$queryE = "
	    SELECT  b.USUA_NOMB USUARIO
			, t.SGD_TPR_DESCRIP TIPO_DOCUMENTO
			, count(r.RADI_NUME_RADI) RADICADOS
			, MIN(b.USUA_CODI) HID_COD_USUARIO
			, MIN(SGD_TPR_CODIGO) HID_TPR_CODIGO			
			FROM RADICADO r, USUARIO b, SGD_TPR_TPDCUMENTO t, sgd_dir_drecciones dir
		WHERE 
			r.RADI_USUA_ACTU=b.USUA_CODI 
			AND r.tdoc_codi=t.SGD_TPR_CODIGO (+)
			AND r.RADI_DEPE_ACTU=b.DEPE_CODI
			AND r.radi_nume_radi = dir.radi_nume_radi (+)
                        AND dir.sgd_dir_tipo = 2
			$condicionE
			$whereTipoRadicado 
			GROUP BY b.USUA_NOMB, t.SGD_TPR_DESCRIP
		ORDER BY $orno $ascdesc"; 
	}
	/** CONSULTA PARA VER DETALLES 
	 */
	/*$condicionE = " AND b.USUA_CODI= $codUs ";
	$whereCarpUsua = (!empty($codUs)) ? "car.usua_codi = $codUs AND" : "";

	if($tipoDocumento=='9998'){
		$condicionE .= " AND t.SGD_TPR_CODIGO = $tipoDOCumento ";
	}
	$queryEDetalle = "SELECT DISTINCT r.radi_nume_radi RADICADO,
				TO_CHAR(r.RADI_FECH_RADI, 'DD/MM/YYYY HH24:MI:SS') FECHA_RADICACION,
				t.SGD_TPR_DESCRIP TIPO_DE_DOCUMENTO,
				r.radi_nume_deri RAD_PADRE,
				r.RADI_CUENTAI CTA_INTERNA,
				exp.sgd_exp_numero NUMERO_EXPEDIENTE,
				car.nomb_carp CARPETA_PERSONAL,
				b.USUA_NOMB USUARIO_ACTUAL,
				r.RA_ASUN ASUNTO,
				bod.NOMBRE_DE_LA_EMPRESA ESP,
				bod.DIRECCION DIR_ESP,
				r.RADI_PATH HID_RADI_PATH,
				dir1.sgd_dir_nomremdes NOMBRE_PREDIO,
				dir1.sgd_dir_direccion DIRECCION_PREDIO,
				dep1.dpto_nomb DPTO_PREDIO,
				muni1.muni_nomb MPIO_PREDIO
                          FROM  radicado r,
				usuario b,
				sgd_tpr_tpdcumento t,
				bodega_empresas bod,
				par_serv_servicios n,
				sgd_caux_causales o,
				carpeta_per car,
				sgd_exp_expediente exp,
				sgd_dir_drecciones dir1,
				sgd_dir_drecciones dir2,
				departamento dep1,
				departamento dep2,
				municipio muni1,
				municipio muni2
                          WHERE r.eesp_codi = bod.identificador_empresa (+) AND
				r.RADI_USUA_ACTU = b.USUA_CODI AND
				r.tdoc_codi = t.SGD_TPR_CODIGO (+) AND
				r.RADI_DEPE_ACTU = $dependencia_busq AND
				b.DEPE_CODI = $dependencia_busq AND
				car.depe_codi = $dependencia_busq AND
				$whereCarpUsua
				r.par_serv_secue = n.par_serv_codigo(+) AND
				r.radi_nume_radi = o.radi_nume_radi(+) AND
				car.codi_carp(+) = r.carp_codi AND
				r.radi_nume_radi = exp.radi_nume_radi(+) AND
				r.eesp_codi = bod.identificador_empresa (+) AND
				r.radi_nume_radi = dir1.radi_nume_radi (+) AND
				r.radi_nume_radi = dir2.radi_nume_radi (+) AND
				dir1.sgd_dir_tipo = 2 AND
				dir1.dpto_codi = dep1.dpto_codi AND
				dir2.dpto_codi = dep2.dpto_codi AND
                                dir1.muni_codi = muni1.muni_codi AND
				dir2.muni_codi = muni2.muni_codi AND
	                        dir1.dpto_codi = muni1.dpto_codi AND
				dir2.dpto_codi = muni2.dpto_codi
				$whereTipoRadicado";
	$orderE = "ORDER BY $orno $ascdesc";
	// Consulta para ver todos los detalles  
	$queryETodosDetalle = $queryEDetalle . $orderE;
	$queryEDetalle .= $condicionE . $orderE;
	break;
	}*/

	if(isset($_GET['genDetalle'])&& $_GET['denDetalle']=1){ 
			$titulos=array("#","1#RADICADO","2#FECHA RADICACION","3#TIPO DOCUMENTO","4#USUARIO ","5#ASUNTO");
		}else{ 
			$titulos=array("#","1#USUARIO","2#Documentos");
		} 

	function pintarEstadistica($fila,$indice,$numColumna){
			global $ruta_raiz,$_POST,$_GET;
			$salida="";
			switch ($numColumna){
				case  0:
					$salida=$indice;
					break;
				case 1:
					$salida=$fila['USUARIO'];
				break;
				case 2:
				$datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".urlencode($fila['USUA_DOC'])."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;codUs=".$fila['HID_COD_USUARIO']."&amp;depeUs=".$fila['HID_DEPE_USUA'];
	        		$datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
				$salida="<a href=\"genEstadistica.php?{$datosEnvioDetalle}\" target=\"detallesSec\" >".$fila['RADICADO']."</a>";
				break;
			default: $salida=false;
			}
			return $salida;
		}
	function pintarEstadisticaDetalle($fila,$indice,$numColumna){
                        global $ruta_raiz,$encabezado,$krd;
                        $verImg=($fila['SGD_SPUB_CODIGO']==1)?($fila['USUARIO']!=$_SESSION['usua_nomb']?false:true):($fila['USUA_NIVEL']>$_SESSION['nivelus']?false:true);
                        $verImg=$verImg&&($fila['SGD_EXP_PRIVADO']!=1);
                        $numRadicado=$fila['RADI_NUME_RADI'];
                        switch ($numColumna){
                                case 0:
				$salida=$indice;
				break; 
				case 1:
					if($verImg)
						$salida="<a class=\"vinculos\" href=\"{$ruta_raiz}verradicado.php?verrad=".$fila['RADICADO']."&amp;".session_name()."=".session_id()."&amp;krd=".$_GET['krd']."&amp;carpeta=8&amp;nomcarpeta=Busquedas&amp;tipo_carp=0 \" >".$fila['RADICADO']."</a>";
					else 
					$salida="<a class=\"vinculos\" href=\"#\" onclick=\"alert(\"ud no tiene permisos para ver el radicado\");\">".$fila['RADICADO']."</a>";
				break;
				case 2:
					$salida="<center class=\"leidos\">".$fila['FECHA_RADICACION']."</center>";
				break;
				case 3:
					$salida="<center class=\"leidos\">".$fila['TIPO_DE_DOCUMENTO']."</center>";
				break;
				case 4:
					$salida="<center class=\"leidos\">".$fila['USUARIO']."</center>";
				break;
				case 5:
					$salida="<center class=\"leidos\">".$fila['ASUNTO']."</center>";
				break;
                        }
			return $salida;
                }	
?>
