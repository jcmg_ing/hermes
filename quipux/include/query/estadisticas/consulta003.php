<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

/** CONSUTLA 001 
  *Estadisticas por medio de envio -Salida *******
  *se tienen en cuenta los registros enviados por la dep xx contando la masiva ----
	*ej. COnsulta Base SELECT a.sgd_fenv_codigo,b.sgd_fenv_descrip,a.tot_reg 
  * FROM (SELECT SUM(SGD_RENV_CANTIDAD)AS tot_reg,sgd_fenv_codigo FROM fldoc.SGD_RENV_REGENVIO 
 	*  WHERE TO_CHAR(SGD_RENV_FECH,'yyyy/mm/%') LIKE '2005/05/%' 
	*   AND depe_codi LIKE 529
	*   AND RADI_NUME_SAL LIKE '2005%'
	*  GROUP BY sgd_fenv_codigo) a, fldoc.SGD_FENV_FRMENVIO b
  *  WHERE a.sgd_fenv_codigo=b.sgd_fenv_codigo;
	*
	* @autor JAIRO H LOSADA - SSPD
	* @version ORFEO 3.1
	* 
	*/
//////CODIGO REPORTES
session_start();
$_SESSION['ban']=0;

//var_dump($usua_docu);
//var_dump($whereDependencia);
//var_dump($whereActivos);
//var_dump($whereTipoRadicado);
//var_dump($whereUsSelect);
////////
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';	
if(!$orno) $orno=2;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */
$seguridad=",B.CODI_NIVEL USUA_NIVEL,R.SGD_SPUB_CODIGO";
switch($db->driver)
	{
	case 'mssql':
	$isql = '';	
	break;
	case 'oracle':
	case 'oci8':
	case 'oci805':
	case 'postgres':
	if ($whereDependencia && $dependencia_busq != 99999)  {
		$wdepend = " AND b.depe_codi = $dependencia_busq ";
	}
	$queryE = "
			SELECT 
			b.USUA_DOC as HID_COD_USUARIO
			, HID_DEPE_USUA
			,b.USUARIO
			,b.sgd_fenv_codigo as CODIGO_ENVIO
			,c.sgd_fenv_descrip as MEDIO_ENVIO
			,b.tot_reg as TOTAL_ENVIADOS
			,b.USUA_DOC
			,b.sgd_fenv_codigo as HID_CODIGO_ENVIO
		   FROM 
			(SELECT COUNT(c.SGD_RENV_CANTIDAD) as tot_reg,c.sgd_fenv_codigo , b.USUA_NOMB as USUARIO, MIN(b.depe_codi) as HID_DEPE_USUA, MIN(b.usua_doc) as USUA_DOC
				FROM SGD_RENV_REGENVIO c, USUARIO b, radicado r
				WHERE 
					TO_CHAR(c.SGD_RENV_FECH,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
					AND r.radi_nume_radi = c.radi_nume_sal
					$wdepend
					AND substr(c.usua_doc,1,15) = b.usua_doc
					AND (c.sgd_renv_planilla != '00' or c.sgd_renv_planilla is null)
					and (c.sgd_renv_observa not like 'Masiva%' or  c.sgd_renv_observa is null)
					$whereTipoRadicado
					GROUP BY b.USUA_NOMB, c.sgd_fenv_codigo) b
				, SGD_FENV_FRMENVIO c
		WHERE b.sgd_fenv_codigo=c.sgd_fenv_codigo
			ORDER BY $orno $ascdesc";

			///////CODIGO REPORTES
			//encerar las variables de sesion.
			$_SESSION['usua']=NULL;
			$_SESSION['cod']=NULL;
			$_SESSION['medio']=NULL;
			$_SESSION['total1']=NULL;
			$rs1 = $db->query($queryE);
			//var_dump($rs1);
			while(!$rs1->EOF)  {
				$usua[] = $rs1->fields["USUARIO"]; 
				$cod[] = $rs1->fields["CODIGO_ENVIO"]; 
				$medio[] = $rs1->fields["MEDIO_ENVIO"];
				$total1[] = $rs1->fields["TOTAL_ENVIADOS"];
				$_SESSION['usua']=$usua;
				$_SESSION['cod']=$cod;
				$_SESSION['medio']=$medio;
				$_SESSION['total1']=$total1;
				$rs1->MoveNext();
			}
			////////////
 /** CONSULTA PARA VER DETALLES 
	 */  
 	$condicionDep = " AND b.depe_codi = $depeUs ";
	$condicionE = " AND c.sgd_fenv_codigo = $fenvCodi AND b.USUA_doc = $codUs ";

$queryEDetalle = "SELECT  c.RADI_NUME_SAL as RADICADO
				,d.sgd_fenv_descrip as ENVIO_POR
				,b.USUA_NOMB as USUARIO_QUE_ENVIO
				,c.sgd_renv_fech as FECHA_ENVIO
				,c.sgd_renv_planilla as PLANILLA
				,c.sgd_fenv_codigo as HID_CODIGO_ENVIO				
				FROM SGD_RENV_REGENVIO c, SGD_FENV_FRMENVIO d, USUARIO b, radicado r
				WHERE 
				    c.sgd_fenv_codigo=d.sgd_fenv_codigo
					AND TO_CHAR(c.SGD_RENV_FECH,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
					AND r.radi_nume_radi = c.radi_nume_sal
					and substr(c.usua_doc,1,15) =  b.USUA_doc
					$wdepend
					AND (c.sgd_renv_planilla != '00' or c.sgd_renv_planilla is null)
					and (c.sgd_renv_observa not like 'Masiva%' or  c.sgd_renv_observa is null)
					
					$whereTipoRadicado ";

	$orderE = "	ORDER BY $orno $ascdesc ";
 /** CONSULTA PARA VER TODOS LOS DETALLES 
	 */ 
 	$queryETodosDetalle = $queryEDetalle . $orderE;
	$queryEDetalle .= $condicionE . $condicionDep . $orderE;

		///////CODIGO REPORTES
			//encerar las variables de sesion.
			$_SESSION['doc1']=NULL;
			$_SESSION['enviado']=NULL;
			$_SESSION['usuario']=NULL;
			$_SESSION['fecha']=NULL;
			$_SESSION['plan']=NULL;			

			if(!empty($usua_docu)){
				$rsE = $db->query($queryEDetalle);
				while(!$rsE->EOF){
					$doc1[] = $rsE->fields["RADICADO"];
					$enviado[] = $rsE->fields["ENVIO_POR"];
					$usuario[] = $rsE->fields["USUARIO_QUE_ENVIO"];
					$fecha[] = $rsE->fields["FECHA_ENVIO"];
					$plan[] = $rsE->fields["PLANILLA"];
					$_SESSION['doc1']=$doc1;
					$_SESSION['enviado']=$enviado;
					$_SESSION['usuario']=$usuario;
					$_SESSION['fecha']=$fecha;
					$_SESSION['plan']=$plan;
					$rsE->MoveNext();
				}	
			}
			////////
			var_dump($doc1);

	break;
	}

if(isset($_GET['genDetalle'])&& $_GET['denDetalle']=1) 
                $titulos=array("#","1#RADICADO","2#ENVIO POR","3#USUARIO QUE ENVIO","4#FECHA ENVIO","5#PLANILLA");
        else             
                $titulos=array("#","1#USUARIO","2#CODIGO ENVIO","3#MEDIO DE ENVIO","4#TOTAL ENVIADOS");
                 
function pintarEstadistica($fila,$indice,$numColumna){ 
                global $ruta_raiz,$_POST,$_GET; 
                $salida=""; 
                switch ($numColumna){ 
                        case  0: 
                                $salida=$indice; 
                                break;
                        case 1:
                                $salida=$fila['USUARIO'];
                        break;
                        case 2:
				$salida=$fila['CODIGO_ENVIO'];
			break;
			 case 3:
                                $salida=$fila['MEDIO_ENVIO'];
                        break;
			 case 4:
                   $datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".$fila['USUA_DOC']."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;codUs=".$fila['HID_COD_USUARIO']."&amp;depeUs=".$fila['HID_DEPE_USUA']."&amp;fenvCodi=".$fila['HID_CODIGO_ENVIO']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento'];
                  $datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
                  $salida="<a href=\"genEstadistica.php?{$datosEnvioDetalle}\"  target=\"detallesSec\" >".$fila['TOTAL_ENVIADOS']."</a>";
                        break;
                default: $salida=false;
                }
                return $salida;
        }
function pintarEstadisticaDetalle($fila,$indice,$numColumna){
			///////CODIGO REPORTES
			if($_SESSION['ban']==0){
				$boton_det='<input type=submit class="botones_largo" id="imp_det" value="imprimir" onClick="imprimir_det()"/><br><br>';
				echo $boton_det;
				$_SESSION['ban']=1;
			}
			//////////////
                        global $ruta_raiz,$encabezado,$krd;
                        //$verImg=($fila['SGD_SPUB_CODIGO']==1)?($fila['USUARIO']!=$_SESSION['usua_nomb']?false:true):($fila['USUA_NIVEL']>$_SESSION['nivelus']?false:true);
                $numRadicado=$fila['RADICADO'];
                        switch ($numColumna){
                                        case 0:
                                                $salida=$indice;
                                                break;
                                        case 1:
                                                if($fila['HID_RADI_PATH'] && $verImg)
                                                        $salida="<center><a href=\"{$ruta_raiz}bodega".$fila['RADI_PATH']."\">".$fila['RADICADO']."</a></center>";
                                                else
                                                        $salida="<center class=\"leidos\">{$numRadicado}</center>";
                                                break;
                                        case 2:
						$salida=$fila['ENVIO_POR'];
                                                break;
                                        case 3:  
                                              $salida="<center class=\"leidos\">".$fila['USUARIO_QUE_ENVIO']."</center>";                                                 break;
                                        case 4:
                                                $salida="<center class=\"leidos\">".$fila['FECHA_ENVIO']."</center>";
                                                break;
                                        case 5:
                                                $salida="<center class=\"leidos\">".$fila['PLANILLA']."</center>";
                                                break;
                        }
                        return $salida;               
		
 }
                                                              
?>
<!--///////CODIGO REPORTES-->
<script language="Javascript">
function imprimir_det(){
window.open ("/orfeo/reporte_det_consulta3.php/");
}
</script>
