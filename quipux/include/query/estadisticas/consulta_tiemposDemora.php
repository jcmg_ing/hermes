<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--link rel="stylesheet" href="../estilos/orfeo.css"-->
<link href="<?= $ruta_raiz ?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?

include_once "$ruta_raiz/rec_session.php";
include_once "../include/db/ConnectionHandler.php";
if(!$db)  { $db = new ConnectionHandler('.');}


if(empty($ruta_raiz)) $ruta_raiz = "..";


/*
$sqlTbl="delete from Temp_TiemposDemora".$_SESSION["usua_codi"];
$queryTbl= $db->query($sqlTbl);

$sqlTbl="drop table Temp_TiemposDemora".$_SESSION["usua_codi"];
$queryTbl= $db->query($sqlTbl);

$_SESSION['tabla']="Temp_TiemposDemora".$_SESSION["usua_codi"];


//CREO TABLA TEMPORAL PARA GUARDAR TIEMPOS DEMORA  ---> LO CORRECTO ES MANEJAR UN STORE PROCEDURE
$sqlTbl="create table Temp_TiemposDemora".$_SESSION["usua_codi"]."
(CODAREA integer ,
NOMAREA character varying(100),
NUMDOC numeric(20) ,
USUA_ORI character varying(150),
TOTAL INTEGER,
DE character varying(150),
PARA character varying(150),
ASUNTO character varying(350) ,
DOCREFE character varying(20),
FECHA_REGISTRO timestamp with time zone ,
FECHA_DOC timestamp with time zone ,
ESTADO character varying(100) ,
TIPODOC character varying(100) ,
HIST_OBSE character varying(350),
DOCUMENTO character varying(50))
";
$queryTbl= $db->query($sqlTbl);
*/

//////CODIGO REPORTES
session_start();
unset($_SESSION['queryE']);

$ruta_raiz = "..";
$_SESSION['ban']=0;
$cod_usuario=$_POST['codus'];

///AREA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$dependencia_busq;
}
///USUARIO DE
if(!empty($_POST["codus"]))
{
	$cod_u=$_POST["codus"];
	unset($_SESSION['cod_u']);
	$_SESSION['cod_u']=$cod_u;
}

///USUARIO PARA
if(!empty($_POST["para"]))
{
	$cod_para=$_POST["para"];
	unset($_SESSION['cod_para']);
	$_SESSION['cod_para']=$cod_para;
}

///TIPO DOCUMENTO
if(!empty($_POST["tipo"]))
{
	$cod_tipo=$_POST["tipo"];
	unset($_SESSION['cod_tipo']);
	$_SESSION['cod_tipo']=$cod_tipo;
}



//$coltp3Esp = '"'.$tip3Nombre[3][2].'"';
//if(!$orno) $orno=2;


/*echo "AREA".$_SESSION['uno'];
echo "DE".$_SESSION['cod_u'];
echo "PARA".$_SESSION['cod_para'];
echo "TIPO".$_SESSION['cod_tipo'];
echo "ESTADO".$estado;
*/
//echo "area".$area_demora;

switch($db->driver)
{
	case 'postgres':
	{
			//CONDICION AREA
            if($area_demora!='99999'){
				if(isset($_SESSION['uno'])){
					//$area_demora=$_SESSION['uno'];
					if($area_demora!='99999'){
						//$condicionE = "	AND uo.depe_codi = $dependencia_busq";
                        //$condicionE = "AND (select depe_codi from usuario where  usua_codi=b.usua_codi_ori)=$dependencia_busq";
                        if ($_SESSION["usua_codi"]==0){
                            $condicionE = "and  uo.depe_codi=$area_demora";
                        }
                        elseif($_SESSION["usua_codi"]!=0 and $dependencia_busq<>'99999'){
                            $condicionE = "and  uo.depe_codi=$dependencia_busq";
                        }

					}
				}
			}


            //CONDICION "DE"
        	if(isset($_SESSION['cod_u'])){
				$cod_usuarioDE=$_SESSION['cod_u'];
				if($cod_usuarioDE!='99999'){
					//$whereTipoRadi1 =" AND UO.USUA_CODI= $cod_usuarioDE";
                    //$whereTipoRadi1 =" and b.usua_codi_ori=$cod_usuarioDE";
                    $whereTipoRadi1 =" and a.usua_codi_ori=$cod_usuarioDE";

				}
			}

            //CONDICION "PARA"
        	if(isset($_SESSION['cod_para'])){
				$cod_usuarioPARA=$_SESSION['cod_para'];
				if($cod_usuarioPARA!='99999'){
					//$whereTipoRadi2 =" AND ud.USUA_CODI= $cod_usuarioPARA";
                    //$whereTipoRadi2 =" and b.usua_codi_dest=$cod_usuarioPARA";
                    $whereTipoRadi2=" and a.usua_codi_dest=$cod_usuarioPARA";

                    
				}
			}

            //CONDICION TIPO DOCUMENTO
        	if(isset($_SESSION['cod_tipo'])){
				$cod_tipo=$_SESSION['cod_tipo'];
				if($cod_tipo!='99999'){
					//$whereTipoRadi3 =" AND r.radi_tipo= $cod_tipo";
                    //$whereTipoRadi3 =" AND radi_tipo= $cod_tipo";
                    $whereTipoRadi3 =" and r.radi_tipo= $cod_tipo";
                   
				}
			}

            //ESTADO DEL DOCUMENTO
            if($estado!='9999'){//estado 9999, selecciono todos los estados
                //$condicionL=" AND t.sgd_ttr_codigo= $estado";
                $condicionL="and esta_codi= $estado";
            }



            


                $queryE="select  a.radi_nume_radi as NUMDOC,uo.depe_codi as CODAREA, uo.depe_nomb as NOMAREA,
                a.tot as TOTAL,
                uo.usua_nombre as USUA_ORI ,
                uo.usua_nombre as DE , ud.usua_nombre as PARA ,r.radi_asunto AS ASUNTO, TO_CHAR(a.HIST_FECH,'DD-MM-YYYY HH24:MI AM') AS FECHA_REGISTRO,
                (select trad_descr from tiporad where trad_codigo=r.radi_tipo)as TIPODOC,
                t.sgd_ttr_descrip as ESTADO,
                R.RADI_NUME_TEXT AS DOCUMENTO
                from
                (select max(to_date(CURRENT_DATE,'yyyy-mm-dd') - to_date(hist_referencia,'yyyy-mm-dd')) as tot,radi_nume_radi,HIST_FECH,
                usua_codi_ori,usua_codi_dest,sgd_ttr_codigo
                from hist_eventos
                where to_date(hist_referencia,'YYYY/MM/DD')<>to_date(hist_fech,'yyyy-mm-dd') and sgd_ttr_codigo=9
                group by radi_nume_radi,HIST_FECH,usua_codi_ori,usua_codi_dest,sgd_ttr_codigo) a
                left outer join radicado r on r.radi_nume_radi=a.radi_nume_radi
                left outer join datos_usuarios uo on uo.usua_codi=a.usua_codi_ori
                left outer join datos_usuarios ud on ud.usua_codi=a.usua_codi_dest
                left outer join sgd_ttr_transaccion t on t.sgd_ttr_codigo=a.sgd_ttr_codigo
                where a.tot >0 and
                ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                and r.radi_inst_actu=".$_SESSION['inst_codi']."
                $whereTipoRadi3
                $whereTipoRadi1
                $whereTipoRadi2
                $condicionE";

                $query2= $db->query($queryE);
                

    }
//echo  $queryE;
//Consulta sql
$_SESSION['queryE']=$queryE;
//ECHO $_SESSION['queryE'];

//Datos de Cabecera
$_SESSION['d_reporte']=NULL;
//echo 'sesion'.$dependencia_busq;

$datos =array("1"=>$dependencia_busq, "2"=>$cod_u, "3"=>$cod_para, "4"=>$cod_tipo, "5"=>$fecha_ini, "6"=>$fecha_fin, "7"=>$estado);
$_SESSION['d_reporte']=$datos;

break;


}//fin switch



//Parametros de filtro
$_SESSION['area']=$datos[1];
$_SESSION['de']=$datos[2];
$_SESSION['para']=$datos[3];
$_SESSION['tipodoc']=$datos[4];
$_SESSION['fecha_ini']=$datos[5];
$_SESSION['fecha_fin']=$datos[6];
$_SESSION['estado']=$datos[7];





//consulta para obtener el nombre del departamento.
if(isset($_SESSION['uno'])){
    $dependencia_busq_rep=$_SESSION['uno'];
    if($dependencia_busq_rep!='99999' and $_SESSION["usua_codi"]==0){

        $queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$area_demora;
    }elseif ($_SESSION["usua_codi"]!=0){
        $queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq;
    }

    $rsE= $db->query($queryR);
        $area=$rsE->fields["DEPE_NOMB"];
    
}






$_SESSION['$area']=$area;
//echo $area;
$rsE= $db->query($queryE);

 if($dependencia_busq!='99999'){

    $titulos=array("#","1#Documento","2#Usuario Act","3#Dias","4#Asunto","5#Fecha","6#Estado","7#Doc");
 }
 else{
        $titulos=array("#","1#Area","2#Documento","3#Usuario Act","4#Dias","5#Asunto","6#Fecha","7#Estado","8#Doc");
 }
$_SESSION['$titulos']=$titulos;




if($dependencia_busq!='99999'){
    function pintarEstadistica($fila,$indice,$numColumna){
    global $ruta_raiz,$_POST,$_GET;
    global $condicionE;
    $salida="";
    $m=0;


    switch ($numColumna){
        case  0:
            $salida=$indice;
            break;
        case 1:
            $salida=$fila['TIPODOC'];
        break;
        case 2:
            $salida=$fila['USUA_ORI'];
            break;
        case 3:
            $salida=$fila['TOTAL'];
            break;
        case 4:
            $salida=$fila['ASUNTO'];
            break;
        case 5:
           $salida=$fila['FECHA_REGISTRO'];
            break;
        case 6:
            $salida=$fila['ESTADO'];
            break;
        case 7:
           $datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;numdoc=".$fila['NUMDOC']."&amp;usua_ori=".$fila['USUA_ORI']."&amp;total=".$fila['TOTAL']."&amp;de=".$fila['DE']."&amp;para=".$fila['PARA']."&amp;asunto=".$fila['ASUNTO']."&amp;docrefe=".$fila['DOCREFE']."&amp;fechaReg=".$_fila['FECHA_REGISTRO']."&amp;fechaDoc=".$_fila['FECHA_DOC']."&amp;estado=".$fila['ESTADO']."&amp;tipodoc=".$fila['TIPODOC']."&amp;documento=".$fila['DOCUMENTO'];
           $datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
           $salida="<a  class=\"grid\" href=\"vistaFormConsulta.php?Z=444&amp;{$datosEnvioDetalle}\">".$fila['DOCUMENTO']." </a>";
           break;
    default: $salida=false;
    }
    return $salida;
    }
}else{
    
    function pintarEstadistica($fila,$indice,$numColumna){
    global $ruta_raiz,$_POST,$_GET;
    global $condicionE;
    $salida="";
    $m=0;

    switch ($numColumna){
        case  0:
            $salida=$indice;
            break;
        case 1:
            $salida=$fila['NOMAREA'];
            break;
        case 2:
            $salida=$fila['TIPODOC'];
            break;
        case 3:
            $salida=$fila['USUA_ORI'];
            break;
        case 4:
            $salida=$fila['TOTAL'];
            break;
        case 5:
            $salida=$fila['ASUNTO'];
            break;
        case 6:
           $salida=$fila['FECHA_REGISTRO'];
            break;
        case 7:
            $salida=$fila['ESTADO'];
            break;
        case 8:
           $datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;numdoc=".$fila['NUMDOC']."&amp;usua_ori=".$fila['USUA_ORI']."&amp;total=".$fila['TOTAL']."&amp;de=".$fila['DE']."&amp;para=".$fila['PARA']."&amp;asunto=".$fila['ASUNTO']."&amp;docrefe=".$fila['DOCREFE']."&amp;fechaReg=".$_fila['FECHA_REGISTRO']."&amp;fechaDoc=".$_fila['FECHA_DOC']."&amp;estado=".$fila['ESTADO']."&amp;tipodoc=".$fila['TIPODOC']."&amp;documento=".$fila['DOCUMENTO'];
           $datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
           $salida="<a  class=\"grid\" href=\"vistaFormConsulta.php?Z=444&amp;{$datosEnvioDetalle}\">".$fila['DOCUMENTO']." </a>";
           break;
    default: $salida=false;
    }
    return $salida;
    }
}


