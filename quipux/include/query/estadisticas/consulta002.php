<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

//////CODIGO REPORTES
session_start();
$_SESSION['ban']=0;

//var_dump($usua_docu);
//var_dump($whereDependencia);
//var_dump($whereActivos);
//var_dump($whereTipoRadicado);
//var_dump($whereUsSelect);
////////
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';	
if(!$orno) $orno=2;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */
switch($db->driver)
{	case 'mssql':
	case 'postgres':
		{if ( $dependencia_busq != 99999)
			{	$condicionE = "	AND substr($radi_nume_radi,5,3)=$dependencia_busq AND b.depe_codi = $dependencia_busq";	}
			$queryE = "SELECT c.mrec_desc as MEDIO_RECEPCION, COUNT(*) as Radicados, max(c.MREC_CODI) as HID_MREC_CODI, b.usua_nomb as USUARIO,  b.USUA_DOC
					FROM RADICADO r, MEDIO_RECEPCION c, USUARIO b
					WHERE 
						r.RADI_USUA_ACTU=b.USUA_CODI 
						AND r.RADI_DEPE_ACTU=b.DEPE_CODI 
						AND r.mrec_codi=c.mrec_codi
						$condicionE
						AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin'
						$whereTipoRadicado
						$whereUsuario
					GROUP BY b.USUA_DOC, c.mrec_desc, b.usua_nomb
					ORDER BY $orno $ascdesc";

			echo $queryE;
		
			///////CODIGO REPORTES
			//encerar las variables de sesion.
			$_SESSION['medio']=NULL;
			$_SESSION['docu']=NULL;
			$_SESSION['usua2']=NULL;
			$rs1 = $db->query($queryE);
			//var_dump($rs1);
			while(!$rs1->EOF)  {
				$medio[] = $rs1->fields["MEDIO_RECEPCION"]; 
				$usua2[] = $rs1->fields["USUARIO"];
				$docu[] = $rs1->fields["RADICADOS"]; 
				$_SESSION['medio']=$medio;
				$_SESSION['usua2']=$usua2;
				$_SESSION['docu']=$docu;
				$rs1->MoveNext();
				}
			////////////
			
			
			//consulta para obtener el nombre del departamento.
			$queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq;
			$rsE= $db->query($queryR);
				$area=$rsE->fields["DEPE_NOMB"];

			//consulta para obtener el nombre del usuario.
			/*$queryR1="SELECT USUA_NOMB FROM USUARIO b WHERE 1=1".$whereTipoRadicado;
			$rsE= $db->query($queryR1);
				$nombre=$rsE->fields["USUA_NOMB"];*/


			//enviar los datos
			$datos =array("1"=>$area, "2"=>$fecha_ini, "3"=>$fecha_fin);
			$_SESSION['d_reporte']=$datos;
			//var_dump($datos);

 			/** CONSULTA PARA VER DETALLES 
	 		*/
  			$condicionE = " AND b.USUA_DOC= $usua_docu";;

			$queryEDetalle = "SELECT $radi_nume_radi as RADICADO, 
						".$db->conn->SQLDate('Y/m/d h:i:s','r.radi_fech_radi')." as FECHA_RADICADO
						,c.MREC_DESC as MEDIO_RECEPCION
						,r.RA_ASUN as ASUNTO
						,b.usua_nomb as USUARIO
						,r.RADI_PATH as HID_RADI_PATH{$seguridad}
					FROM RADICADO r, USUARIO b, MEDIO_RECEPCION c
					WHERE 
						r.RADI_USUA_ACTU=b.USUA_CODI 
						AND r.RADI_DEPE_ACTU=b.DEPE_CODI
						AND r.mrec_codi=c.mrec_codi
						AND c.mrec_codi=$mrecCodi
						$condicionE
						AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini'  AND '$fecha_fin'
						$whereTipoRadicado";			

					$orderE = "	ORDER BY $orno $ascdesc";			

		 	/** CONSULTA PARA VER TODOS LOS DETALLES 
	 		*/ 

			$queryETodosDetalle = $queryEDetalle . $condicionDep . $orderE;
			$queryEDetalle .=  $orderE;

			///////CODIGO REPORTES
			//encerar las variables de sesion.
			$_SESSION['doc2']=NULL;
			$_SESSION['fecha1']=NULL;
			$_SESSION['medio1']=NULL;
			$_SESSION['asunto1']=NULL;
			$_SESSION['usua1']=NULL;			

			if(!empty($usua_docu)){
				$rsE = $db->query($queryEDetalle);
				while(!$rsE->EOF){
					$doc2[] = $rsE->fields["RADICADO"];
					$fecha1[] = $rsE->fields["FECHA_RADICADO"];
					$medio1[] = $rsE->fields["MEDIO_RECEPCION"];
					$asunto1[] = $rsE->fields["ASUNTO"];
					$usua1[] = $rsE->fields["USUARIO"];
					$_SESSION['doc2']=$doc2;
					$_SESSION['fecha1']=$fecha1;
					$_SESSION['medio1']=$medio1;
					$_SESSION['asunto1']=$asunto1;
					$_SESSION['usua1']=$usua1;
					$rsE->MoveNext();
				}	
			}
			////////

		}break;
	//case 'oracle':
	//case 'ocipo':
	case 'oci8':
	case 'oci805':
		{	if ( $dependencia_busq != 99999)
			{	$condicionE = "	AND substr(r.radi_nume_radi,5,3)=$dependencia_busq AND b.depe_codi = $dependencia_busq";	}
			$queryE = "SELECT c.mrec_desc MEDIO_RECEPCION, COUNT(*) Radicados, max(c.MREC_CODI) HID_MREC_CODI,  B.USUA_DOC
					FROM RADICADO r, MEDIO_RECEPCION c, USUARIO b
					WHERE 
						r.radi_usua_radi=b.usua_CODI 
						AND r.mrec_codi=c.mrec_codi
						AND c.mrec_codi=$mrecCodi
						$condicionE
						AND TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
						$whereTipoRadicado
						$whereUsuario
					GROUP BY c.mrec_desc
					ORDER BY $orno $ascdesc";	
 			/** CONSULTA PARA VER DETALLES 
	 		*/
  			$condicionDep = " AND b.depe_codi = $depeUs ";

			$queryEDetalle = "SELECT r.RADI_NUME_RADI RADICADO
						,TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd hh:mi:ss') FECHA_RADICADO
						,c.MREC_DESC MEDIO_RECEPCION
						,r.RA_ASUN ASUNTO
						,b.usua_nomb USUARIO
						,r.RADI_PATH HID_RADI_PATH{$seguridad}
					FROM RADICADO r, USUARIO b, MEDIO_RECEPCION c
					WHERE 
						r.radi_usua_radi=b.usua_CODI 
						AND r.mrec_codi=c.mrec_codi
						AND c.mrec_codi=$mrecCodi
						$condicionE
						AND TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin'
						$whereTipoRadicado";			

					$orderE = "	ORDER BY $orno $ascdesc";			

		 	/** CONSULTA PARA VER TODOS LOS DETALLES 
	 		*/ 

			$queryETodosDetalle = $queryEDetalle . $condicionDep . $orderE;
			$queryEDetalle .=  $orderE;
		}break;
}
if(isset($_GET['genDetalle'])&& $_GET['denDetalle']=1)
		$titulos=array("#","1#MEDIO DE RECEPCION","2#FECHA RADICADO","3#TIPO DOCUMENTO","4#ASUNTO","5#ANEXOS","6#ANEXOS","7#NO HOJAS","8#MEDIO DE RECEPCION","9#USUARIO","10#ESP","11#SECTOR","12#CAUSAL","13#DETALLE CAUSAL");
	else 		
		$titulos=array("#","1#MEDIO","2#USUARIO", "3#DOCUMENTOS");
		
function pintarEstadistica($fila,$indice,$numColumna){
        	global $ruta_raiz,$_POST,$_GET;
        	$salida="";
        	switch ($numColumna){
        		case  0:
        			$salida=$indice;
        			break;
        		case 1:	
        			$salida=$fila['MEDIO_RECEPCION'];
        		break;
			case 2:
				$salida=$fila['USUARIO'];
			break;
        		case 3:
        			$datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".$fila['USUA_DOC']."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;mrecCodi=".$fila['HID_MREC_CODI'];
	        		$datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
	        		
        			$salida="<a href=\"genEstadistica.php?{$datosEnvioDetalle}\"  target=\"detallesSec\" >".$fila['RADICADOS']."</a>";
        	break;
        	default: $salida=false;
        	}
        	return $salida;
        }
function pintarEstadisticaDetalle($fila,$indice,$numColumna){
			///////CODIGO REPORTES
			if($_SESSION['ban']==0){
				$boton_det='<input type=submit class="botones_largo" id="imp_det" value="imprimir" onClick="imprimir_det()"/><br><br>';
				echo $boton_det;
				$_SESSION['ban']=1;
			}
			//////////////
			global $ruta_raiz,$encabezado,$krd;
			$verImg=($fila['SGD_SPUB_CODIGO']==1)?($fila['USUARIO']!=$_SESSION['usua_nomb']?false:true):($fila['USUA_NIVEL']>$_SESSION['nivelus']?false:true);
        	$numRadicado=$fila['RADICADO'];	
			switch ($numColumna){
					case 0:
						$salida=$indice;
						break;
					case 1:
						if($fila['HID_RADI_PATH'] && $verImg)
							$salida="<center><a href=\"{$ruta_raiz}bodega".$fila['RADI_PATH']."\">".$fila['RADICADO']."</a></center>";
						else 
							$salida="<center class=\"leidos\">{$numRadicado}</center>";	
						break;
					case 2:
						if($verImg)
		   					$salida="<a class=\"vinculos\" href=\"{$ruta_raiz}verradicado.php?verrad=".$fila['RADICADO']."&amp;".session_name()."=".session_id()."&amp;krd=".$_GET['krd']."&amp;carpeta=8&amp;nomcarpeta=Busquedas&amp;tipo_carp=0 \" >".$fila['FECHA_RADICADO']."</a>";
		   				else 
		   				$salida="<a class=\"vinculos\" href=\"#\" onclick=\"alert(\"ud no tiene permisos para ver el radicado\");\">".$fila['RADI_FECH_RADI']."</a>";
						break;
					case 3:
						$salida="<center class=\"leidos\">".$fila['TIPO_DE_DOCUMENTO']."</center>";		
						break;
					case 4:
						$salida="<center class=\"leidos\">".$fila['ASUNTO']."</center>";
						break;
					case 5:
						$salida="<center class=\"leidos\">".$fila['ANEXOS']."</center>";
						break;
					case 6:
						$salida="<center class=\"leidos\">".$fila['N_HOJAS']."</center>";			
						break;	
					case 7:
						$salida="<center class=\"leidos\">".$fila['MEDIO_RECEPCION']."</center>";			
						break;	
					case 8:
						$salida="<center class=\"leidos\">".$fila['USUARIO']."</center>";			
						break;	
					case 9:
						$salida="<center class=\"leidos\">".$fila['ESP']."</center>";			
						break;	
					case 10:
						$salida="<center class=\"leidos\">".$fila['SECTOR']."</center>";			
						break;
					case 11:
						$salida="<center class=\"leidos\">".$fila['CAUSAL']."</center>";			
						break;
					case 12:
						$salida="<center class=\"leidos\">".$fila['DETALLE_CAUSAL']."</center>";			
						break;
			}
			return $salida;
		}
echo '<pre>';
var_dump($_SESSION['medio']);
var_dump($_SESSION['docu']);
var_dump($_SESSION['usua2']);
echo '</pre>';
?>
<!--///////CODIGO REPORTES-->
<script language="Javascript">
function imprimir_det(){
window.open ("/orfeo/reporte_det_consulta2.php/");
}
</script>
