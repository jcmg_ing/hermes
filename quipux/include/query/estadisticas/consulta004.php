<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

/** CONSUTLA 004
	* Estadiscas de Numero de Radicados digitalizados y Hojas Digitalizadas.
	* @autor JAIRO H LOSADA - SSPD
	* @version ORFEO 3.1
	* 
	*/
//////CODIGO REPORTES
session_start();
unset($_SESSION['queryE']);

$ruta_raiz = "..";
//unset($_SESSION['queryEDetalle']);
//$_SESSION['queryE']=null;
$_SESSION['ban']=0;
/////////////
///DEPENDENCIA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$_SESSION['uno'];
}
///USUARIO
if(!empty($_POST["codus"]))
{
	$cod_u=$_POST["codus"];
	unset($_SESSION['cod_u']);
	$_SESSION['cod_u']=$cod_u;
}
//var_dump($usua_docu);
//var_dump($whereDependencia);
//var_dump($whereActivos);
//var_dump($estado);
//var_dump($_SESSION['ban']);
//
/*echo '<pre>';
var_dump('condicion'.$condicion_usuario);
var_dump('sesion'.$_SESSION['uno']);
var_dump('user'.$_SESSION['cod_u']);
var_dump('CODIGO_USER='.$cod_u);
echo '</pre>';*/
////////
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';	
if(!$orno) $orno=2;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */
switch($db->driver)
{	case 'postgres':
		{
			if($dependencia_busq!='99999'){
				if(!isset($_SESSION['uno'])){
					$condicionE = "	AND b.depe_codi = $dependencia_busq";
				}else{
					$dependencia_busq=$_SESSION['uno'];
					if($dependencia_busq!='99999'){
						$condicionE = "	AND b.depe_codi = $dependencia_busq";
					}
				}
			}
			if(isset($_SESSION['cod_u'])){
				$cod_usuario=$_SESSION['cod_u'];
				if($cod_usuario!='99999'){
					$whereTipoRadi1 =" AND b.USUA_CODI= $cod_usuario";
				}
			}

			/* anterior $queryE = "
	    		SELECT b.USUA_NOMB as USUARIO
				, count(*) as RADICADOS
				, SUM(r.RADI_NUME_HOJA) as HOJAS_DIGITALIZADAS			
				, MIN(b.USUA_CODI) as HID_COD_USUARIO, B.USUA_DOC
			FROM RADICADO r, USUARIO b, HIST_EVENTOS h
			WHERE 
				h.USUA_CODI=b.usua_CODI 
				AND b.depe_codi = h.depe_codi
				$condicionE
				AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
				AND h.SGD_TTR_CODIGO IN(22,42)
				AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini'  AND '$fecha_fin' 
				$whereTipoRadi1
			GROUP BY B.USUA_DOC, b.USUA_NOMB
			ORDER BY $orno $ascdesc";*/

			$queryE = "select rad_dig.usua_nomb||' '||rad_dig.usua_apellido as USUARIO
				, uno as RADI_REGI
				, coalesce(dos,0) as RADI_DIGI
				, rad_dig.USUA_CEDULA
			from (select usua_nomb, usua_apellido, count(r.radi_nume_radi) as uno, b.usua_cedula
				from radicado r, usuarios b
				where substr(radi_nume_RADI,20)=2
				$condicionE
				AND r.RADI_USUA_RADI=b.USUA_CODI 
				AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini'  AND '$fecha_fin'
				$whereTipoRadi1
				group by usua_nomb, usua_apellido, b.usua_cedula
				order by 1) as rad 
			RIGHT OUTER JOIN
				(select usua_nomb, usua_apellido, count(distinct r.radi_nume_radi) as dos, b.usua_cedula
				from radicado r, usuarios b, hist_eventos h
				where substr(r.radi_nume_RADI,20)=2
				$condicionE
				AND r.RADI_USUA_RADI=b.USUA_CODI 
				and h.radi_nume_radi=r.radi_nume_radi
				and r.radi_path is not null
				AND r.radi_inst_actu=".$_SESSION['inst_codi']."
				AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini'  AND '$fecha_fin'
				$whereTipoRadi1
				group by usua_nomb, usua_apellido, b.usua_cedula
				order by 1) as rad_dig
			ON rad.usua_nomb=rad_dig.usua_nomb";

			//$_SESSION['queryE']=NULL;
			$_SESSION['queryE']=$queryE;


 			/** CONSULTA PARA VER DETALLES 
	 		*/

			$condicionE_det = " AND b.USUA_CEDULA= $usua_docu";

			$queryEDetalle = "SELECT 
				r.radi_nume_text as RADICADO
				,r.radi_nume_radi as RAD_NUM
				, b.USUA_NOMB||' '||b.USUA_APELLIDO as USUARIO_DIGITALIZADOR
				, replace(h.HIST_OBSE, 'href=\"./','href=\"../') as OBSERVACIONES, ".
				$db->conn->SQLDate('Y/m/d H:i:s','r.radi_fech_radi')." as FECHA_RADICACION, ".
				$db->conn->SQLDate('Y/m/d H:i:s','h.HIST_FECH')." as FECHA_DIGITALIZACION
			FROM RADICADO r, USUARIOS b, HIST_EVENTOS h
			WHERE substr(r.radi_nume_RADI,20)=2
				$condicionE_det
				AND r.RADI_USUA_RADI=b.USUA_CODI 
				and h.radi_nume_radi=r.radi_nume_radi
				and r.radi_path is not null
				AND r.radi_inst_actu=".$_SESSION['inst_codi']."
				AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini'  AND '$fecha_fin' 
			ORDER BY $orno $ascdesc";


			if(!empty($usua_docu)){
				$_SESSION['queryEDetalle']=$queryEDetalle;
			}
			//

			//DATOS DEL REPORTE-------------
			//consulta para obtener el nombre del departamento.
			if(isset($_SESSION['uno'])){
				$dependencia_busq_rep=$_SESSION['uno'];
				if($dependencia_busq_rep!='99999'){
					$queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq_rep;
					$rsE= $db->query($queryR);
					$area=$rsE->fields["DEPE_NOMB"];
				}
			}
			//consulta para obtener el nombre del usuario.
			if(isset($_SESSION['cod_u'])){
				$cod_usuario_rep=$_SESSION['cod_u'];
				if($cod_usuario_rep!='99999'){
					$whereTipoRadi1 =" AND b.USUA_CODI= $cod_usuario_rep";
					$queryR1="SELECT b.USUA_NOMB, b.USUA_APELLIDO FROM USUARIOS b WHERE 1=1".$whereTipoRadi1." AND
					b.DEPE_CODI=".$dependencia_busq_rep;
					$rsE= $db->query($queryR1);
					$nombre=$rsE->fields["USUA_NOMB"].' '.$rsE->fields["USUA_APELLIDO"];
				}
			}

			//enviar los datos REPORTE
			$_SESSION['d_reporte']=NULL;
			$datos =array("1"=>$area, "2"=>$fecha_ini, "3"=>$fecha_fin, "4"=>$nombre);
			$_SESSION['d_reporte']=$datos;
			//var_dump($datos);
			
			///DATOS DEL REPORTE DETALLE-------------
			//consulta para obtener el nombre del departamento.
			if(!empty($usua_docu)){
				$queryR_det= "SELECT DEPE_NOMB FROM USUARIOS B, DEPENDENCIA D WHERE B.DEPE_CODI=D.DEPE_CODI".$condicionE_det;
				$rsE= $db->query($queryR_det);
					$area1=$rsE->fields["DEPE_NOMB"];
			}
			//var_dump($queryR_det);

			//consulta para obtener el nombre del usuario.
			if(!empty($usua_docu)){
				$queryR1_det="SELECT b.USUA_NOMB, b.USUA_APELLIDO FROM USUARIOS b WHERE 1=1".$condicionE_det;
				$rsE= $db->query($queryR1_det);
					$nombre1=$rsE->fields["USUA_NOMB"].' '.$rsE->fields["USUA_APELLIDO"];
			}
			//var_dump($queryR1_det);

			//enviar los datos REPORTE DET
			$_SESSION['d_reporte_det']=NULL;
			$datos1 =array("1"=>$area1, "2"=>$fecha_ini, "3"=>$fecha_fin, "4"=>$nombre1);
			$_SESSION['d_reporte_det']=$datos1;
			//var_dump($datos1);

		}break;

	/*case 'oracle':
	case 'oci8':
	case 'oci805':
	case 'ocipo':
		{	if ( $dependencia_busq != 99999)
			{	$condicionE = "	AND h.DEPE_CODI=$dependencia_busq AND b.depe_codi = $dependencia_busq";	}
			$queryE = "
	    	SELECT b.USUA_NOMB USUARIO
				, count(*) RADICADOS
				, SUM(a.RADI_NUME_HOJA) HOJAS_DIGITALIZADAS						
				, MIN(b.USUA_CODI) HID_COD_USUARIO
			FROM RADICADO a, USUARIO b, HIST_EVENTOS h
			WHERE 
				h.USUA_CODI=b.usua_CODI 
				AND b.depe_codi = h.depe_codi
				$condicionE
				AND h.RADI_NUME_RADI=a.RADI_NUME_RADI
				AND h.SGD_TTR_CODIGO IN(22,42)
				AND TO_CHAR(a.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin' 
				$whereTipoRadicado 
			GROUP BY b.USUA_NOMB
			ORDER BY $orno $ascdesc";
 			/** CONSULTA PARA VER DETALLES 
	 		

			$queryEDetalle = "SELECT 
				R.RADI_NUME_RADI RADICADO
				, b.USUA_NOMB USUARIO_DIGITALIZADOR
				, h.HIST_OBSE OBSERVACIONES
				, TO_CHAR(a.RADI_FECH_RADI, 'DD/MM/YYYY HH24:MI:SS') FECHA_RADICACION
				, TO_CHAR(h.HIST_FECH, 'DD/MM/YYYY HH24:MI:SS') FECHA_DIGITALIZACION
				,R.RADI_PATH HID_RADI_PATH{$seguridad}
				FROM RADICADO R, USUARIO b, HIST_EVENTOS h
			WHERE 
				h.USUA_CODI=b.usua_CODI 
				AND b.depe_codi = h.depe_codi
				$condicionE
				AND h.RADI_NUME_RADI=a.RADI_NUME_RADI
				AND b.USUA_CODI=$codUs
				AND h.SGD_TTR_CODIGO IN(22,42)
				AND TO_CHAR(a.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin' 
				$whereTipoRadicado 
			ORDER BY $orno $ascdesc";
		}break;
	*/
}

if(isset($_GET['genDetalle'])&& $_GET['denDetalle']=1)
		$titulos=array("#","1#DOCUMENTO","2#USUARIO DIGITALIZADOR","3#OBSERVACIONES","4#FECHA INGRESO","5#FECHA DIGITALIZACION");
	else 		
		$titulos=array("#","1#USUARIO","2#DOC. REGISTRADO","3#DOC. DIGITALIZADO");

function pintarEstadistica($fila,$indice,$numColumna){
        	global $ruta_raiz,$_POST,$_GET;
        	$salida="";
        	switch ($numColumna){
        		case  0:
        			$salida=$indice;
        			break;
        		case 1:	
        			$salida=$fila['USUARIO'];
        		break;
        		case 2:
        			$salida=$fila['RADI_REGI'];
        		break;
        		case 3:
			$datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".$fila['USUA_CEDULA']."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;cod_u=".$_POST['cod_u'];
	        	$datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
        	 	//$salida=$fila['HOJAS_DIGITALIZADAS'];
			$salida="<a href=\"genEstadistica.php?{$datosEnvioDetalle}\"  target=\"detallesSec\" >".$fila['RADI_DIGI']."</a>";
        	 	break;
        	default: $salida=false;
        	}
        	return $salida;
        }
function pintarEstadisticaDetalle($fila,$indice,$numColumna){
			///////CODIGO REPORTES
			if($_SESSION['ban']==0){
				$boton_det='<input type=submit class="botones_largo" id="imp_det" value="imprimir" onClick="imprimir_det()"/><br><br>';
				echo $boton_det;
				$_SESSION['ban']=1;
			}
			//////////////
			global $ruta_raiz,$encabezado,$krd;
			$verImg=($fila['SGD_SPUB_CODIGO']==1)?($fila['USUARIO']!=$_SESSION['usua_nomb']?false:true):($fila['USUA_NIVEL']>$_SESSION['nivelus']?false:true);
        	$numRadicado=$fila['RADICADO'];	
			switch ($numColumna){
					case 0:
						$salida=$indice;
						break;
					case 1:
						/*if($fila['HID_RADI_PATH'] && $verImg)
							$salida="<center><a href=\"{$ruta_raiz}bodega".$fila['RADI_PATH']."\">".$fila['RADICADO']."</a></center>";
						else */
							$salida="<center class=\"leidos\">{$numRadicado}</center>";	
						break;
						case 2:
							$salida="<center class=\"leidos\">".$fila['USUARIO_DIGITALIZADOR']."</center>";
							break;
						case 3:
							$salida="<center class=\"leidos\">".$fila['OBSERVACIONES']."</center>";
							break;
						case 4:
						if($verImg)
		   					$salida="<a class=\"vinculos\" href=\"{$ruta_raiz}verradicado.php?verrad=".$fila['RAD_NUM']."&estadisticas=1"."&amp;".session_name()."=".session_id()."&amp;krd=".$_GET['krd']."&amp;carpeta=8&amp;nomcarpeta=Busquedas&amp;tipo_carp=0 \" >".$fila['FECHA_RADICACION']."</a>";
		   				else 
		   				$salida="<a class=\"vinculos\" href=\"#\" onclick=\"alert(\"ud no tiene permisos para ver el radicado\");\">".$fila['FECHA_RADICACION']."</a>";
						break;
					case 5:
						$salida="<center class=\"leidos\">".$fila['FECHA_DIGITALIZACION']."</center>";		
						break;
			}
			return $salida;
		}
?>
<!--///////CODIGO REPORTES-->
<script language="Javascript">
function imprimir_det(){
window.open ("<?=$ruta_raiz?>/reporte_det_consulta4.php/");
}
</script>