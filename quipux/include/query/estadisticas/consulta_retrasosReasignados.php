<?

/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--link rel="stylesheet" href="../estilos/orfeo.css"-->
<link href="<?= $ruta_raiz ?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?


include_once "$ruta_raiz/rec_session.php";
include_once "../include/db/ConnectionHandler.php";
if(!$db)  { $db = new ConnectionHandler('.');}



if(empty($ruta_raiz)) $ruta_raiz = "..";



//////CODIGO REPORTES
session_start();
$cod_usuario=$_POST['codus'];


switch($db->driver)
{
	case 'postgres':
	{
       $queryE="select ver_usuarios(l.radi_usua_rem,',') as De,ver_usuarios(l.radi_usua_dest,',') as Para,
        ver_usuarios_r2(l.usua_codi_dest,',') as ASIGNADO,
        l.usua_codi_dest,l.radi_asunto as Asunto ,TO_CHAR(l.fecha,'YYYY-MM-DD / HH24:MI:SS') as Fecha_Documento,
        l.RADI_NUME_TEXT as Numero_Documento,
        l.hist_fech as Fecha_Asignacion, l.HIST_REFERENCIA as Fecha_Maxima,
        (to_date(CURRENT_DATE,'yyyy-mm-dd') - to_date(l.HIST_REFERENCIA,'yyyy-mm-dd'))as Dias_Demora,
        l.usua_codi_dest
        from (
        select h1.radi_nume_radi,
        b.radi_usua_rem, b.radi_usua_dest, b.radi_asunto ,
        CASE WHEN b.radi_fech_firma is not null
        THEN radi_fech_firma ELSE radi_fech_ofic
        END as fecha ,b.radi_usua_actu,b.RADI_NUME_TEXT,b.radi_cuentai,
        h1.hist_fech, h1.HIST_REFERENCIA,h1.usua_codi_dest
        from
        (
        select distinct radi_nume_radi,hist_referencia,hist_fech,usua_codi_dest from hist_eventos where usua_codi_ori=".$_SESSION["usua_codi"]."
        and sgd_ttr_codigo=9 and hist_referencia<>''
        and to_char(hist_fech,'YYYY-MM-DD') BETWEEN '$fecha_ini' AND '$fecha_fin'
        EXCEPT
        select distinct a.radi_nume_radi,a.hist_referencia,a.hist_fech,a.usua_codi_dest from
        (select radi_nume_radi,hist_referencia,hist_fech,usua_codi_dest  from hist_eventos where
        usua_codi_ori=".$_SESSION["usua_codi"]."
        and sgd_ttr_codigo=9 and hist_referencia<>''
        and hist_referencia<TO_CHAR(now(),'YYYY-MM-DD')
        and to_char(hist_fech,'YYYY-MM-DD') BETWEEN '$fecha_ini' AND '$fecha_fin'
        ) as a ,hist_eventos b
        where  a.radi_nume_radi= b.radi_nume_radi
        and b.usua_codi_ori=".$_SESSION["usua_codi"]."
        and b.sgd_ttr_codigo  not in (2,9)
        ) as h1,radicado as b
        where h1.radi_nume_radi=b.radi_nume_radi
        and  b.radi_inst_actu=".$_SESSION["inst_codi"]."
        and (to_date(CURRENT_DATE,'yyyy-mm-dd') - to_date(h1.HIST_REFERENCIA,'yyyy-mm-dd'))>0
        ) as l";

//echo $queryE;
              

    }
if(!$db)  { $db = new ConnectionHandler('.');}

//Consulta sql
$_SESSION['queryE']=$queryE;


//Datos de Cabecera
$_SESSION['d_reporte']=NULL;


$datos =array("1"=>$dependencia_busq, "2"=>$cod_u, "3"=>$cod_para, "4"=>$cod_tipo, "5"=>$fecha_ini, "6"=>$fecha_fin, "7"=>$estado);
$_SESSION['d_reporte']=$datos;

break;


}//fin switch



$titulos=array("#","1#De","2#Para","4#Asignado","5#Asunto","6#Fecha documento","7#No.documento","8#Fecha Asignación","9#Trámite máximo","10#Demora");
$_SESSION['$titulos']=$titulos;




    function pintarEstadistica($fila,$indice,$numColumna){
    $salida="";


    switch ($numColumna){
        case  0:
            $salida=$indice;
            break;
        case 1:
            $salida=$fila['DE'];
        break;
        case 2:
            $salida=$fila['PARA'];
        break;
        case 3:
            $salida=$fila['ASIGNADO'];
        break;
        case 4:
            $salida=$fila['ASUNTO'];
        break;
        case 5:
            $salida=$fila['FECHA_DOCUMENTO'];
        break;
        case 6:
            $salida=$fila['NUMERO_DOCUMENTO'];
            break;
        case 7:
            $salida=$fila['FECHA_ASIGNACION'];
            break;
        case 8:
            $salida=$fila['FECHA_MAXIMA'];
            break;
        case 9:
            $salida=$fila['DIAS_DEMORA'];
            break;
    default: $salida=false;
    }
    return $salida;

}

