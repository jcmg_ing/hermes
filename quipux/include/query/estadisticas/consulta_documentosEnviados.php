<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--link rel="stylesheet" href="../estilos/orfeo.css"-->
<link href="<?= $ruta_raiz ?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?



include_once "$ruta_raiz/rec_session.php";
include_once "../include/db/ConnectionHandler.php";
if(!$db)  { $db = new ConnectionHandler('.');}


if(empty($ruta_raiz)) $ruta_raiz = "..";


$condicionTodos=substr($_SESSION['$condiArea'],0,strlen($_SESSION['$condiArea'])-1);



//////CODIGO REPORTES
session_start();
unset($_SESSION['queryE']);

$ruta_raiz = "..";
$_SESSION['ban']=0;
$cod_usuario=$_POST['codus'];

///AREA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$dependencia_busq;
}



///USUARIO DE
if(!empty($_POST["codus"]))
{
	$cod_u=$_POST["codus"];
	unset($_SESSION['cod_u']);
	$_SESSION['cod_u']=$cod_u;
}



///USUARIO PARA
if(!empty($_POST["para"]))
{
	$cod_para=$_POST["para"];
	unset($_SESSION['cod_para']);
	$_SESSION['cod_para']=$cod_para;
}

//echo $_POST["codus"];
//echo $_POST["cmbCiudadano"];

//INSTITUCION CIUDADANO
if(!empty($_POST["cmbCiudadano"]))
{
	$Instciudadano=$_POST["cmbCiudadano"];
	unset($_SESSION['ciudadano']);
	$_SESSION['ciudadano']=$Instciudadano;
}



///TIPO DOCUMENTO
if(!empty($_POST["tipo"]))
{
	$cod_tipo=$_POST["tipo"];
	unset($_SESSION['cod_tipo']);
	$_SESSION['cod_tipo']=$cod_tipo;
}

//AREA DE LA INSTITUCION FUNCIONARIO
if(!empty($_POST[dependencia_busq_docs]))
{
    $area_instFunc=$_POST[dependencia_busq_docs];
    unset($_SESSION[dependencia_busq_docs]);
    $_SESSION[dependencia_busq_docs]=$area_instFunc;
}

//consulta para obtener el nombre del departamento.
if(isset($_SESSION['uno'])){
    $dependencia_busq_rep=$_SESSION['uno'];
    if($dependencia_busq_rep!='99999'){
        $queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq_rep;
        $rsE= $db->query($queryR);
        $area=$rsE->fields["DEPE_NOMB"];
    }
}

$_SESSION['$area']=$area;


//Identifica nombre de reporte en PDF
if ($_POST["genero"]=="F"){
     $_SESSION['$NombreRpt']="Funcionario";

 }elseif ($_POST["genero"]=="C"){
     $_SESSION['$NombreRpt']="Ciudadano";
 }


switch($db->driver)
{
	case 'postgres':
	{

     //CONDICION SI AREA(todos) E INSTITUTO(todos),AreaIns_F(todos)
            if ( $dependencia_busq=='99999' && $_SESSION['inst_codi_enviados']==0 && $area_instFunc=='99999'){
                $condicionI="";
                $condicionE=" where  (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )in ($condicionTodos)";
                //$condicionE="";
                $selectDE="( SELECT USUA_NOMBRE FROM DATOS_USUARIOS where usua_codi=d.radi_usua_actu ) as DE,
                d.radi_usua_actu as CODDE,";
                $selectPARA="(select inst_codi from usuarios where usua_codi=d.usua_codi_dest) as CODINST,
                (select inst_nombre from DATOS_USUARIOS where usua_codi=d.usua_codi_dest) as INSTITUTO,";
                $condicionAreaInstF="";

            }
            //CONDICION SI AREA(una) E INSTITUTO(todos),AreaIns_F(*)
            elseif ( $dependencia_busq!='99999' && $_SESSION['inst_codi_enviados']==0 && $area_instFunc=='99999'){
                $condicionE=" where  (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )=$dependencia_busq";
                $condicionI="";
                $selectDE="( SELECT USUA_NOMBRE FROM DATOS_USUARIOS where usua_codi=d.radi_usua_actu ) as DE,
                d.radi_usua_actu as CODDE,";
                $selectPARA="(select inst_codi from usuarios where usua_codi=d.usua_codi_dest) as CODINST,
                (select inst_nombre from DATOS_USUARIOS where usua_codi=d.usua_codi_dest) as INSTITUTO,";
                $condicionAreaInstF="";
            }
            //CONDICION SI AREA(todos) E INSTITUTO(uno),AreaIns_F(*)
            elseif ( $dependencia_busq=='99999' && $_SESSION['inst_codi_enviados']!=0 && $area_instFunc=='99999'){
                $condicionE=" and (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )in ($condicionTodos)";
                $condicionI= " where (select inst_codi from usuarios where usua_codi=c.usua_codi_dest )=".$_SESSION['inst_codi_enviados'] ;
                $selectDE="( SELECT USUA_NOMBRE FROM DATOS_USUARIOS where usua_codi=d.radi_usua_actu ) as DE,
                d.radi_usua_actu as CODDE,";
                $select=$_SESSION['inst_codi_enviados']." as CODINST,(select inst_nombre from institucion where inst_codi=".$_SESSION['inst_codi_enviados'].") as INSTITUTO,";
                $condicionAreaInstF="";
            }
            //CONDICION SI AREA(uno) E INSTITUTO(uno) AreaIns_F(uno)
            elseif ( $dependencia_busq!='99999' && $_SESSION['inst_codi_enviados']!=0 && $area_instFunc!='99999'){
                $condicionI= " where (select inst_codi from usuarios where usua_codi=c.usua_codi_dest )=".$_SESSION['inst_codi_enviados'] ;
                $condicionE = " and  (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )=$dependencia_busq";
                $selectDE="( SELECT USUA_NOMBRE FROM DATOS_USUARIOS where usua_codi=d.radi_usua_actu ) as DE,
                d.radi_usua_actu as CODDE,";
                $select=$_SESSION['inst_codi_enviados']." as CODINST,(select inst_nombre from institucion where inst_codi=".$_SESSION['inst_codi_enviados'].") as INSTITUTO,";
                $condicionAreaInstF="and (select depe_codi from usuarios where usua_codi=c.usua_codi_dest )=$area_instFunc";
            }
            //CONDICION SI AREA(todos) E INSTITUTO(todos), AreaIns_F(uno)
             elseif ( $dependencia_busq!='99999' && $_SESSION['inst_codi_enviados']!=0 && $area_instFunc!='99999'){
                $condicionI= " where (select inst_codi from usuarios where usua_codi=c.usua_codi_dest )=".$_SESSION['inst_codi_enviados'] ;
                $condicionE = " and  (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )=$dependencia_busq";
                $selectDE="( SELECT USUA_NOMBRE FROM DATOS_USUARIOS where usua_codi=d.radi_usua_actu ) as DE,
                d.radi_usua_actu as CODDE,";
                $select=$_SESSION['inst_codi_enviados']." as CODINST,(select inst_nombre from institucion where inst_codi=".$_SESSION['inst_codi_enviados'].") as INSTITUTO,";
                $condicionAreaInstF="";
            }
            //CONDICION SI AREA(uno) E INSTITUTO(uno), AreaIns_F(Todos)
             elseif ( $dependencia_busq!='99999' && $_SESSION['inst_codi_enviados']!=0 && $area_instFunc=='99999'){
                $condicionI= " where (select inst_codi from usuarios where usua_codi=c.usua_codi_dest )=".$_SESSION['inst_codi_enviados'] ;
                $condicionE = " and  (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )=$dependencia_busq";
                $selectDE="( SELECT USUA_NOMBRE FROM DATOS_USUARIOS where usua_codi=d.radi_usua_actu ) as DE,
                d.radi_usua_actu as CODDE,";
                $select=$_SESSION['inst_codi_enviados']." as CODINST,(select inst_nombre from institucion where inst_codi=".$_SESSION['inst_codi_enviados'].") as INSTITUTO,";
                $condicionAreaInstF="";
            }
            //CONDICION SI AREA(uno) E INSTITUTO(uno), AreaIns_F(Todos)
             elseif ( $dependencia_busq!='99999' && $_SESSION['inst_codi_enviados']!=0 && $area_instFunc=='99999'){
                $condicionI= " where (select inst_codi from usuarios where usua_codi=c.usua_codi_dest )=".$_SESSION['inst_codi_enviados'] ;
                $condicionE = " and  (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )=$dependencia_busq";
                $selectDE="( SELECT USUA_NOMBRE FROM DATOS_USUARIOS where usua_codi=d.radi_usua_actu ) as DE,
                d.radi_usua_actu as CODDE,";
                $select=$_SESSION['inst_codi_enviados']." as CODINST,(select inst_nombre from institucion where inst_codi=".$_SESSION['inst_codi_enviados'].") as INSTITUTO,";
                $condicionAreaInstF="";
            }
            //CONDICION SI AREA(todos) E INSTITUTO(uno),AreaIns_F()
            elseif ( $dependencia_busq=='99999' && $_SESSION['inst_codi_enviados']!=0 && $area_instFunc!='99999'){
                $condicionE=" and (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )in ($condicionTodos)";
                $condicionI= " where (select inst_codi from usuarios where usua_codi=c.usua_codi_dest )=".$_SESSION['inst_codi_enviados'] ;
                $selectDE="( SELECT USUA_NOMBRE FROM DATOS_USUARIOS where usua_codi=d.radi_usua_actu ) as DE,
                d.radi_usua_actu as CODDE,";
                $select=$_SESSION['inst_codi_enviados']." as CODINST,(select inst_nombre from institucion where inst_codi=".$_SESSION['inst_codi_enviados'].") as INSTITUTO,";
                $condicionAreaInstF="";
            }


            //Selecciona un DE en la bùsqueda
            if($dependencia_busq!='99999' &&  $cod_u!='99999'){
                $condicionPer = " and b.radi_usua_actu = $cod_u";
                $selectPer="$cod_u as COD_DE,(select usua_nombre from  datos_usuarios where usua_codi=$cod_u) as NOMB_DE," ;
            }
            //No Selecciona un DE en la bùsqueda, la busqueda se realiza por todos los uduarios "DE"
            elseif ($cod_u=='99999'){
                $condicionPer="";

            }

             if ($_POST["genero"]=="F"){

                  $queryE="select DISTINCT
                    $selectDE
                    $selectPARA
                    $select
                    $selectPer
                    d.RADI_NUME_RADI as CHK_CHKANULAR,
                    ver_usuarios(d.radi_usua_dest,',') as PARA,
                    d.RADI_ASUNTO as ASUNTO,
                    CASE WHEN d.radi_fech_firma is not null THEN TO_CHAR(d.radi_fech_firma,'YYYY-MM-DD / HH24:MI:SS')
                    ELSE TO_CHAR(d.radi_fech_ofic,'YYYY-MM-DD / HH24:MI:SS') END as DAT_FECHA_DOCUMENTO,
                    d.RADI_NUME_RADI as HID_RADI_NUME_RADI , d.RADI_NUME_TEXT as NUMERO_DOCUMENTO ,
                    d.radi_cuentai as NO_REFERENCIA ,
                    CASE WHEN d.radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as FIRMA_DIGITAL,
                    d.RADI_LEIDO as HID_RADI_LEIDO
                    from (
                    select  distinct c.radi_nume_radi,c.radi_usua_dest,c.RADI_ASUNTO,c.radi_fech_firma,c.radi_fech_ofic,c.RADI_NUME_TEXT,c.radi_cuentai,
                        c.RADI_LEIDO,c.radi_usua_actu,c.usua_codi_dest
                     from (
                        select a.usua_codi_dest,a.radi_nume_radi,b.radi_usua_dest,b.RADI_ASUNTO,b.radi_fech_firma ,b.radi_fech_ofic,
                        b.RADI_NUME_TEXT,b.radi_cuentai,b.RADI_LEIDO,b.radi_usua_actu
                        from hist_eventos a, radicado b where ".$db->conn->SQLDate('Y/m/d', 'radi_fech_ofic')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                        and a.radi_nume_radi=b.radi_nume_radi and sgd_ttr_codigo=2 and
                        hist_obse like '%Se g%' and b.esta_codi=6 and b.radi_nume_radi=b.radi_nume_temp
                        and (select inst_codi from usuarios where usua_codi=a.usua_codi_dest)<>0
                        $condicionPer ) as c

                    $condicionI
                    $condicionAreaInstF
                    $condicionE
                    $condicionArInst
                    ) as d
                    order by 4 DESC";//8

                   //ECHO $queryE;
                   
                    //Consulta sql
                    $_SESSION['queryE']=$queryE;
                    //ECHO $_SESSION['queryE'];

                    //Datos de Cabecera
                    $_SESSION['d_reporte']=NULL;
                    //echo 'sesion'.$dependencia_busq;

                    $datos =array("1"=>$dependencia_busq, "2"=>$cod_u, "3"=>$cod_para, "4"=>$cod_tipo, "5"=>$fecha_ini, "6"=>$fecha_fin, "7"=>$estado);
                    $_SESSION['d_reporte']=$datos;


                    $rsE= $db->query($queryE);
                    //echo mysql_num_rows($queryE);



                    $titulos=array("#","1#De","2#Institución(Para)","3#Para","4#Asunto","5#Fecha Documento","6#No.Documento","7#No.Referencia","8#Firma Digital");
                    $_SESSION['$titulos']=$titulos;
                     break;
              } //FIN FUNCIONARIO
    ELSE{//CIUDADANO

        //AREA(*)-INSTITUTO(*)
        if($dependencia_busq=='99999' AND $_SESSION['ciudadano']=='99999'){
            $condicionArea="";
            $condicionInst="";

        }
        //AREA(1)-INSTITUTO(*)
        if($dependencia_busq!='99999' AND $_SESSION['ciudadano']=='99999'){
            $condicionArea="where (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )=$dependencia_busq";
            $condicionInst="";
        }

        //AREA(*)-INSTITUTO(1)
        if($dependencia_busq=='99999' AND $_SESSION['ciudadano']!='99999'){
            $condicionArea="";
            $condicionInst="and (select inst_nombre from DATOS_USUARIOS where usua_codi=a.usua_codi_dest)='".$_SESSION['ciudadano']."'";
        }

        //AREA(1)-INSTITUTO(1)
        if($dependencia_busq!='99999' AND $_SESSION['ciudadano']!='99999'){
            $condicionArea="where (select depe_codi from usuarios where usua_codi=c.radi_usua_actu )=$dependencia_busq";
            $condicionInst="and (select inst_nombre from DATOS_USUARIOS where usua_codi=a.usua_codi_dest)='".$_SESSION['ciudadano']."'";
        }

        $queryE="select DISTINCT
        $selectDE
        $selectPARA
                    d.RADI_NUME_RADI as CHK_CHKANULAR,
                    ver_usuarios(d.radi_usua_dest,',') as PARA,
                    d.RADI_ASUNTO as ASUNTO,
                    CASE WHEN d.radi_fech_firma is not null THEN TO_CHAR(d.radi_fech_firma,'YYYY-MM-DD / HH24:MI:SS')
                    ELSE TO_CHAR(d.radi_fech_ofic,'YYYY-MM-DD / HH24:MI:SS') END as DAT_FECHA_DOCUMENTO,
                    d.RADI_NUME_RADI as HID_RADI_NUME_RADI , d.RADI_NUME_TEXT as NUMERO_DOCUMENTO ,
                    d.radi_cuentai as NO_REFERENCIA ,
                    CASE WHEN d.radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as FIRMA_DIGITAL,
                    d.RADI_LEIDO as HID_RADI_LEIDO
                    from (
                    select  distinct c.radi_nume_radi,c.radi_usua_dest,c.RADI_ASUNTO,c.radi_fech_firma,c.radi_fech_ofic,c.RADI_NUME_TEXT,c.radi_cuentai,
                        c.RADI_LEIDO,c.radi_usua_actu,c.usua_codi_dest
                     from (
                        select a.usua_codi_dest,a.radi_nume_radi,b.radi_usua_dest,b.RADI_ASUNTO,b.radi_fech_firma ,b.radi_fech_ofic,
                        b.RADI_NUME_TEXT,b.radi_cuentai,b.RADI_LEIDO,b.radi_usua_actu
                        from hist_eventos a, radicado b where ".$db->conn->SQLDate('Y/m/d', 'radi_fech_ofic')." BETWEEN '$fecha_ini' AND '$fecha_fin'
                        and a.radi_nume_radi=b.radi_nume_radi and sgd_ttr_codigo=2 and
                        hist_obse like '%Se g%' and b.esta_codi=6 and b.radi_nume_radi=b.radi_nume_temp
                        $condicionInst
                        $condicionPer ) as c $condicionArea
                    ) as d
                    order by 4 DESC";//8


       //echo $queryE;

        
        //Consulta sql
        $_SESSION['queryE']=$queryE;
        //ECHO $_SESSION['queryE'];

        //Datos de Cabecera
        $_SESSION['d_reporte']=NULL;
        //echo 'sesion'.$dependencia_busq;

        $datos =array("1"=>$dependencia_busq, "2"=>$cod_u, "3"=>$cod_para, "4"=>$cod_tipo, "5"=>$fecha_ini, "6"=>$fecha_fin, "7"=>$estado);
        $_SESSION['d_reporte']=$datos;


        $rsE= $db->query($queryE);
        



        $titulos=array("#","1#De","2#Institución(Para)","3#Para","4#Asunto","5#Fecha Documento","6#No.Documento","7#No.Referencia","8#Firma Digital");
        $_SESSION['$titulos']=$titulos;
        break;

    }

    }//fin case

}//fin switch







function pintarEstadistica($fila,$indice,$numColumna){
global $ruta_raiz,$_POST,$_GET;
global $condicionE;
$salida="";
$m=0;

switch ($numColumna){
    case  0:
        $salida=$indice;
        break;
    case 1:
        $salida=$fila['DE'];
    break;
    case 2:
        $salida=$fila['INSTITUTO'];
    break;
    case 3:
        $salida=$fila['PARA'];
        break;
    case 4:
        $salida=$fila['ASUNTO'];
        break;
    case 5:
        $salida=$fila['DAT_FECHA_DOCUMENTO'];
        break;
    case 6:
        $salida=$fila['NUMERO_DOCUMENTO'];
        break;
    case 7:
        $salida=$fila['NO_REFERENCIA'];
        break;
    case 8:
       $salida=$fila['FIRMA_DIGITAL'];
        break;
default: $salida=false;
}
return $salida;
}


