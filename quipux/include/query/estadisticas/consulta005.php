<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

/** RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA
	* 
	* @autor JAIRO H LOSADA - SSPD
	* @version ORFEO 3.1
	* 
	*/
//////CODIGO REPORTES
session_start();
$_SESSION['ban']=0;

//var_dump($usua_docu);
//var_dump($whereDependencia);
//var_dump($whereActivos);
//var_dump($whereTipoRadicado);
//var_dump($whereUsSelect);
////////
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';	
if(!$orno) $orno=2;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */
switch($db->driver)
{	case 'postgres':
		{	if ( $dependencia_busq != 99999)
			{	$condicionE = "	AND h.DEPE_CODI_DEST=$dependencia_busq AND b.DEPE_CODI=$dependencia_busq ";	}
			$queryE = "
	    		SELECT b.USUA_NOMB as USUARIO
					, count($radi_nume_radi) as RADICADOS
					, MIN(b.USUA_CODI) as HID_COD_USUARIO
					, MIN(b.depe_codi) as HID_DEPE_USUA
					, b.USUA_doc
				FROM RADICADO r, USUARIO b, HIST_EVENTOS h
				WHERE 
					r.RADI_USUA_ACTU=b.USUA_CODI 
					AND r.RADI_DEPE_ACTU=b.DEPE_CODI
					AND h.HIST_DOC_DEST=b.usua_doc
					$condicionE
					AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
					AND h.SGD_TTR_CODIGO=2
					AND ".$db->conn->SQLDate('Y/m/d', 'r.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' 
					AND r.RADI_NUME_RADI like '%2'
				$whereTipoRadicado 
				GROUP BY b.USUA_doc, b.USUA_NOMB
				ORDER BY $orno $ascdesc";

			///////CODIGO REPORTES
			//encerar las variables de sesion.
			$_SESSION['usuadoc']=NULL;
			$_SESSION['doc']=NULL;
			$rs1 = $db->query($queryE);
			//var_dump($rs1);
			while(!$rs1->EOF)  {
				$usuadoc[] = $rs1->fields["USUARIO"]; 
				$doc[] = $rs1->fields["RADICADOS"]; 
				$_SESSION['usuadoc']=$usuadoc;
				$_SESSION['doc']=$doc;
				$rs1->MoveNext();
			}
			////////////
 			/** CONSULTA PARA VER DETALLES 
	 		*/
			$condicionE_det = " AND b.USUA_DOC= $usua_docu";

			$queryEDetalle = "SELECT 
					$radi_nume_radi as RADICADO
					, b.USUA_NOMB as USUARIO_ACTUAL
					, r.RA_ASUN as ASUNTO 
					, ".$db->conn->SQLDate('Y/m/d H:i:s','r.radi_fech_radi')." as FECHA_RADICACION, 
					".$db->conn->SQLDate('Y/m/d H:i:s','h.HIST_FECH')." as FECHA_DIGITALIZACION
					,r.RADI_PATH as HID_RADI_PATH{$seguridad}
				FROM RADICADO r, USUARIO b, HIST_EVENTOS h
				WHERE 
					r.RADI_USUA_ACTU=b.USUA_CODI 
					AND r.RADI_DEPE_ACTU=b.DEPE_CODI
					AND h.HIST_DOC_DEST=b.usua_doc
					$condicionE_det
					AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
					AND b.USUA_CODI=$codUs
					AND h.SGD_TTR_CODIGO=2
					AND r.RADI_NUME_RADI like '%2'
					AND ".$db->conn->SQLDate('Y/m/d','r.radi_fech_radi')." BETWEEN '$fecha_ini'  AND '$fecha_fin' 
				$whereTipoRadicado 
				ORDER BY $orno $ascdesc";

			///////CODIGO REPORTES
			//encerar las variables de sesion.
			$_SESSION['doc1']=NULL;
			$_SESSION['usua']=NULL;
			$_SESSION['asunto']=NULL;
			$_SESSION['fecha_r']=NULL;
			$_SESSION['fecha_d']=NULL;			

			if(!empty($usua_docu)){
				$rsE = $db->query($queryEDetalle);
				while(!$rsE->EOF){
					$doc1[] = $rsE->fields["RADICADO"];
					$usua[] = $rsE->fields["USUARIO_ACTUAL"];
					$asunto[] = $rsE->fields["ASUNTO"];
					$fecha_r[] = $rsE->fields["FECHA_RADICACION"];
					$fecha_d[] = $rsE->fields["FECHA_DIGITALIZACION"];
					$_SESSION['doc1']=$doc1;
					$_SESSION['usua']=$usua;
					$_SESSION['asunto']=$asunto;
					$_SESSION['fecha_r']=$fecha_r;
					$_SESSION['fecha_d']=$fecha_d;
					$rsE->MoveNext();
				}	
			}
			////////
		}break;
	case 'oracle':
	case 'oci8':
	case 'oci805':
	case 'ocipo':
		{	if ( $dependencia_busq != 99999)
			{	$condicionE = "	AND h.DEPE_CODI_DEST=$dependencia_busq AND b.DEPE_CODI=$dependencia_busq ";	}
			$queryE = "
	    		SELECT b.USUA_NOMB USUARIO
					, count(r.RADI_NUME_RADI) RADICADOS
					, MIN(b.USUA_CODI) HID_COD_USUARIO
					, MIN(b.depe_codi) HID_DEPE_USUA
				FROM RADICADO a, USUARIO b, HIST_EVENTOS h
				WHERE 
					h.HIST_DOC_DEST=b.usua_doc
					$condicionE
					AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
					AND h.SGD_TTR_CODIGO=2
					AND TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin' 
					AND r.RADI_NUME_RADI like '%2'
				$whereTipoRadicado 
				GROUP BY b.USUA_NOMB
				ORDER BY $orno $ascdesc";
 			/** CONSULTA PARA VER DETALLES 
	 		*/
			$queryEDetalle = "SELECT 
					r.RADI_NUME_RADI RADICADO
					, b.USUA_NOMB USUARIO_ACTUAL
					, r.RA_ASUN ASUNTO 
					, TO_CHAR(r.RADI_FECH_RADI, 'DD/MM/YYYY HH24:MM:SS') FECHA_RADICACION
					, TO_CHAR(h.HIST_FECH, 'DD/MM/YYYY HH24:MM:SS') FECHA_DIGITALIZACION
					,r.RADI_PATH HID_RADI_PATH{$seguridad}
				FROM RADICADO r, USUARIO b, HIST_EVENTOS h
				WHERE Radicados
					h.HIST_DOC_DEST=b.usua_doc
					$condicionE
					AND h.RADI_NUME_RADI=r.RADI_NUME_RADI
					AND h.SGD_TTR_CODIGO=2
					AND b.USUA_CODI=$codUs
					AND b.depe_codi = $depeUs
					AND r.RADI_NUME_RADI like '%2'
					AND TO_CHAR(r.radi_fech_radi,'yyyy/mm/dd') BETWEEN '$fecha_ini'  AND '$fecha_fin' 
				$whereTipoRadicado 
				ORDER BY $orno $ascdesc";
	}break;
}
if(isset($_GET['genDetalle'])&& $_GET['denDetalle']=1)
		$titulos=array("#","1#RADICADO","2#USUARIO DIGITALIZADOR","3#ASUNTO","4#FECHA INGRESO","5#FECHA DIGITALIZACION");
	else 		
		$titulos=array("#","1#Usuario","2#Documentos","3#HOJAS DIGITALIZADAS");

function pintarEstadistica($fila,$indice,$numColumna){
        	global $ruta_raiz,$_POST,$_GET;
        	$salida="";
        	switch ($numColumna){
        		case  0:
        			$salida=$indice;
        			break;
        		case 1:	
        			$salida=$fila['USUARIO'];
        		break;
        		case 2:
        			$datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".$fila['USUA_DOC']."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;codUs=".$fila['HID_COD_USUARIO']."&amp;depeUs=".$fila['HID_DEPE_USUA'];
	        		$datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
	        		$salida="<a href=\"genEstadistica.php?{$datosEnvioDetalle}\"  target=\"detallesSec\" >".$fila['RADICADOS']."</a>";
        	break;
        	 case 3:
        	 	$salida=$fila['HOJAS_DIGITALIZADAS'];
        	 	break;
        	default: $salida=false;
        	}
        	return $salida;
        }
function pintarEstadisticaDetalle($fila,$indice,$numColumna){
			///////CODIGO REPORTES
			if($_SESSION['ban']==0){
				$boton_det='<input type=submit class="botones_largo" id="imp_det" value="imprimir" onClick="imprimir_det()"/><br><br>';
				echo $boton_det;
				$_SESSION['ban']=1;
			}
			//////////////
			global $ruta_raiz,$encabezado,$krd;
			$verImg=($fila['SGD_SPUB_CODIGO']==1)?($fila['USUARIO']!=$_SESSION['usua_nomb']?false:true):($fila['USUA_NIVEL']>$_SESSION['nivelus']?false:true);
        	$numRadicado=$fila['RADICADO'];	
			switch ($numColumna){
					case 0:
						$salida=$indice;
						break;
					case 1:
						/*if($fila['HID_RADI_PATH'] && $verImg)
							$salida="<center><a href=\"{$ruta_raiz}bodega".$fila['RADI_PATH']."\">".$fila['RADICADO']."</a></center>";
						else */
							$salida="<center class=\"leidos\">{$numRadicado}</center>";	
						break;
						case 2:
							$salida="<center class=\"leidos\">".$fila['USUARIO_ACTUAL']."</center>";
							break;
						case 3:
							$salida="<center class=\"leidos\">".$fila['ASUNTO']."</center>";
							break;
						case 4:
						if($verImg)
		   					$salida="<a class=\"vinculos\" href=\"{$ruta_raiz}verradicado.php?verrad=".$fila['RADICADO']."&amp;".session_name()."=".session_id()."&amp;krd=".$_GET['krd']."&amp;carpeta=8&amp;nomcarpeta=Busquedas&amp;tipo_carp=0 \" >".$fila['FECHA_RADICACION']."</a>";
		   				else 
		   				$salida="<a class=\"vinculos\" href=\"#\" onclick=\"alert(\"ud no tiene permisos para ver el radicado\");\">".$fila['FECHA_RADICACION']."</a>";
						break;
					case 5:
						$salida="<center class=\"leidos\">".$fila['FECHA_DIGITALIZACION']."</center>";		
						break;
			}
			return $salida;
		}
?>
<!--///////CODIGO REPORTES-->
<script language="Javascript">
function imprimir_det(){
window.open ("/orfeo/reporte_det_consulta5.php/");
}
</script>