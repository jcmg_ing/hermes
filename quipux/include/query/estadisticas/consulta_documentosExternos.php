<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/


?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--link rel="stylesheet" href="../estilos/orfeo.css"-->
<link href="<?= $ruta_raiz ?>/estilos/light_slate.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/splitmenu.css" rel="stylesheet" type="text/css">
<link href="<?= $ruta_raiz ?>/estilos/template_css.css" rel="stylesheet" type="text/css">
</head>
<body>
<br /><br />
<?


include_once "$ruta_raiz/rec_session.php";
include_once "../include/db/ConnectionHandler.php";
if(!$db)  { $db = new ConnectionHandler('.');}



if(empty($ruta_raiz)) $ruta_raiz = "..";


//////CODIGO REPORTES
session_start();
unset($_SESSION['queryE']);

$ruta_raiz = "..";
$_SESSION['ban']=0;
$cod_usuario=$_POST['codus'];

///AREA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$dependencia_busq;
}

///USUARIO DE
if(!empty($_POST["codus"]))
{
	$cod_u=$_POST["codus"];
	unset($_SESSION['cod_u']);
	$_SESSION['cod_u']=$cod_u;
}

///USUARIO PARA
if(!empty($_POST["para"]))
{
	$cod_para=$_POST["para"];
	unset($_SESSION['cod_para']);
	$_SESSION['cod_para']=$cod_para;
}

///TIPO DOCUMENTO
if(!empty($_POST["tipo"]))
{
	$cod_tipo=$_POST["tipo"];
	unset($_SESSION['cod_tipo']);
	$_SESSION['cod_tipo']=$cod_tipo;
}

//INSTITUCION CIUDADANO
if(!empty($_POST["cmbCiudadano"]))
{
	$Instciudadano=$_POST["cmbCiudadano"];
	unset($_SESSION['ciudadano']);
	$_SESSION['ciudadano']=$Instciudadano;
}


//consulta para obtener el nombre del departamento.
if(isset($_SESSION['uno'])){
    $dependencia_busq_rep=$_SESSION['uno'];
    if($dependencia_busq_rep!='99999'){
        $queryR= "SELECT DEPE_NOMB FROM DEPENDENCIA WHERE DEPE_CODI=".$dependencia_busq_rep;
        $rsE= $db->query($queryR);
        $area=$rsE->fields["DEPE_NOMB"];
    }
}

$_SESSION['$area']=$area;


//consulta para obtener el nombre de la institución
if(isset($_SESSION['insti'])){
    $institucion_busq_rep=$_SESSION['insti'];
    //if($institucion_busq_rep!='99999'){
        $queryR= "SELECT INST_NOMBRE from institucion WHERE INST_CODI=".$institucion_busq_rep;
        $rsE= $db->query($queryR);
        $institucion=$rsE->fields["INST_NOMBRE"];
    //}
}


switch($db->driver)
{
	case 'postgres':
	{
      if ($_POST["genero"]=="F"){

           if($_SESSION['inst_codi_Externos']==0 && $dependencia_busq_ext=='99999' && $dependencia_busq=='99999'){
                $selectAreaInst="'Todos' AS NOMAREA_INST,";
                $selectAreaPara="'Todos' as NOMBREAREA_PARA,";
                
                $condicionInst="";
                $condicionAreaInst="";
                $condicionE="";
                


            }
            elseif($_SESSION['inst_codi_Externos']==0 && $dependencia_busq_ext=='99999' && $dependencia_busq!='99999'){
                
                $selectAreaInst="'Todos' AS NOMAREA_INST,";
                $selectAreaPara="$dependencia_busq as CODAREAPARA,
                            (select depe_nomb from dependencia where depe_codi=$dependencia_busq) as NOMBREAREA_PARA,";

                $condicionInst="";
                $condicionAreaInst="";
                $condicionE=" and (select depe_codi from usuarios where usua_codi=replace(radi_usua_dest,'-',''))=$dependencia_busq";
            }
            elseif($_SESSION['inst_codi_Externos']!=0 &&  $dependencia_busq_ext=='99999' && $dependencia_busq=='99999'){
              $selectAreaInst="'Todos' AS NOMAREA_INST,";
               $selectAreaPara="'Todos' as NOMBREAREA_PARA,";

                $condicionInst="and inst_codi=".$_SESSION['inst_codi_Externos'];
                $condicionAreaInst="";
                $condicionE="";
            }
            elseif($_SESSION['inst_codi_Externos']!=0 && $dependencia_busq_ext!='99999' && $dependencia_busq!='99999'){
                $selectAreaInst="$dependencia_busq_ext as CODAREA_INST,
                                (select  p.depe_nomb from dependencia p  where p.depe_codi=$dependencia_busq_ext) AS NOMAREA_INST,";

                $selectAreaPara="$dependencia_busq as CODAREAPARA,
                            (select depe_nomb from dependencia where depe_codi=$dependencia_busq) as NOMBREAREA_PARA,";

                $condicionInst="and inst_codi=".$_SESSION['inst_codi_Externos'];
                $condicionAreaInst="AND depe_codi = $dependencia_busq_ext";
                $condicionE=" and (select depe_codi from usuarios where usua_codi=replace(radi_usua_dest,'-',''))=$dependencia_busq";
            
            }
            elseif($_SESSION['inst_codi_Externos']!=0 && $dependencia_busq_ext!='99999' && $dependencia_busq=='99999'){
                $selectAreaInst="$dependencia_busq_ext as CODAREA_INST,
                                (select  p.depe_nomb from dependencia p  where p.depe_codi=$dependencia_busq_ext) AS NOMAREA_INST,";

                $selectAreaPara="'Todos' as NOMBREAREA_PARA,";

                $condicionInst="and inst_codi=".$_SESSION['inst_codi_Externos'];
                $condicionAreaInst="AND depe_codi = $dependencia_busq_ext";
                $condicionE="";
            }
            elseif($_SESSION['inst_codi_Externos']!=0 && $dependencia_busq_ext=='99999' && $dependencia_busq!='99999'){
                $selectAreaInst="$dependencia_busq_ext as CODAREA_INST,
                                (select  p.depe_nomb from dependencia p  where p.depe_codi=$dependencia_busq_ext) AS NOMAREA_INST,";
                $selectAreaPara="$dependencia_busq as CODAREAPARA,
                            (select depe_nomb from dependencia where depe_codi=$dependencia_busq) as NOMBREAREA_PARA,";
                $condicionInst="and inst_codi=".$_SESSION['inst_codi_Externos'];
                $condicionAreaInst="";
                $condicionE=" and (select depe_codi from usuarios where usua_codi=replace(radi_usua_dest,'-',''))=$dependencia_busq";
            }

//$selectAreaInst
           $queryE="select
            distinct
            inst_codi,
            a.inst_nombre as INSTITUTO,
            
            $selectAreaPara
             depe_codi as AreaInst,
            a.depe_nomb AS NOMAREA,
            radi_nume_radi as CHK_CHKANULAR ,
            usua_nombre as DE ,
            ver_usuarios(radi_usua_dest,',') as PARA ,
            radi_asunto as ASUNTO,
            TO_CHAR(radi_fech_radi,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO,
            radi_nume_radi as HID_RADI_NUME_RADI ,
            radi_nume_text as NUMERO_DOCUMENTO,
            radi_cuentai as NO_REFERENCIA
            
            from ( select distinct tu.usua_nombre,tu.inst_codi,tu.depe_codi,b.radi_nume_radi,tu.inst_nombre,tu.depe_nomb,
            b.radi_usua_rem, b.radi_usua_dest, b.radi_asunto , b.radi_fech_radi, 1,
            b.radi_nume_text, b.radi_cuentai
            from (select  radi_nume_radi,radi_usua_rem,radi_usua_dest,radi_asunto,radi_fech_radi, radi_nume_text, radi_cuentai
                  from radicado where esta_codi=6 and radi_tipo=2
                  and radi_fech_agend is null ) as b
            left outer join datos_usuarios tu on replace(b.radi_usua_rem,'-','')=tu.usua_codi
            order by 5 DESC) as a
            where ".$db->conn->SQLDate('Y/m/d', 'a.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' and
            inst_codi<>0
            $condicionInst
            $condicionAreaInst
            $condicionE
            order by 5 DESC";
                //,radi_leido,radi_tipo/, b.radi_leido,b.radi_tipo/ trad_descr as TIPO_DOCUMENTO,            radi_leido as HID_RADI_LEIDO/left outer join tiporad td on b.radi_tipo=td.trad_codigo
//ver_usuarios(radi_usua_rem,',') as DE ,

         }
         elseif($_POST["genero"]=="C"){

            if ($cmbCiudadano=="99999" && $dependencia_busq=='99999'){
                $selectAreaPara="'Todos' as NOMBREAREA_PARA,";
                $condInstCiud="";
                $condicionECiud="";

            }
            elseif ($cmbCiudadano!="99999" && $dependencia_busq=='99999'){
                $selectAreaPara="'Todos' as NOMBREAREA_PARA,";
                $condInstCiud="and inst_nombre='".$_SESSION['ciudadano']."'";
                $condicionECiud="";

            }
            elseif ($cmbCiudadano=="99999" && $dependencia_busq!='99999'){
                $selectAreaPara="$dependencia_busq as CODAREAPARA,
                            (select depe_nomb from dependencia where depe_codi=$dependencia_busq) as NOMBREAREA_PARA,";
                $condInstCiud="";
                $condicionECiud=" and (select depe_codi from usuarios where usua_codi=replace(radi_usua_dest,'-',''))=$dependencia_busq";
            }
            elseif ($cmbCiudadano!="99999" && $dependencia_busq!='99999'){
                $selectAreaPara="$dependencia_busq as CODAREAPARA,
                            (select depe_nomb from dependencia where depe_codi=$dependencia_busq) as NOMBREAREA_PARA,";
                if ($_SESSION['ciudadano']=="Sin Institución" or $_SESSION['ciudadano']==" " ){
                    $condInstCiud="";
                }else{
                    $condInstCiud="and inst_nombre='".$_SESSION['ciudadano']."'";
                }

                $condicionECiud=" and (select depe_codi from usuarios where usua_codi=replace(radi_usua_dest,'-',''))=$dependencia_busq";
            }
             $queryE="select
                        distinct
                        inst_nombre as INSTITUTO,
                        $selectAreaPara
                         depe_codi as AreaInst,
                        (select  p.depe_nomb from dependencia p  where p.depe_codi=a.depe_codi) AS NOMAREA,
                        radi_nume_radi as CHK_CHKANULAR ,
                        usua_nombre as DE ,
                        ver_usuarios(radi_usua_dest,',') as PARA ,
                        radi_asunto as ASUNTO,
                        TO_CHAR(radi_fech_radi,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO,
                        radi_nume_radi as HID_RADI_NUME_RADI ,
                        radi_nume_text as NUMERO_DOCUMENTO,
                        radi_cuentai as NO_REFERENCIA
                        from ( select distinct tu.usua_nombre,tu.inst_nombre,tu.inst_codi,tu.depe_codi,b.radi_nume_radi,
                        b.radi_usua_rem, b.radi_usua_dest, b.radi_asunto , b.radi_fech_radi, 1,
                        b.radi_nume_text, b.radi_cuentai
                        from (select  radi_nume_radi,radi_usua_rem,radi_usua_dest,radi_asunto,radi_fech_radi, radi_nume_text, radi_cuentai
                              from radicado where esta_codi=6 and radi_tipo=2
                              and radi_fech_agend is null ) as b
                        left outer join DATOS_USUARIOS tu on replace(b.radi_usua_rem,'-','')=tu.usua_codi
                        order by 5 DESC) as a
                        where ".$db->conn->SQLDate('Y/m/d', 'a.radi_fech_radi')." BETWEEN '$fecha_ini' AND '$fecha_fin' and
                        inst_codi=0
                        $condInstCiud
                        $condicionECiud
                        order by 5 DESC";

                 //trad_descr as TIPO_DOCUMENTO,--left outer join tiporad td on b.radi_tipo=td.trad_codigo,,radi_leido,radi_tipo,ver_usuarios(radi_usua_rem,',') as DE ,

    }
if(!$db)  { $db = new ConnectionHandler('.');}
  
//Consulta sql
$_SESSION['queryE']=$queryE;


//Datos de Cabecera
$_SESSION['d_reporte']=NULL;


$datos =array("1"=>$dependencia_busq, "2"=>$cod_u, "3"=>$cod_para, "4"=>$cod_tipo, "5"=>$fecha_ini, "6"=>$fecha_fin, "7"=>$estado);
$_SESSION['d_reporte']=$datos;

break;
    }

}//fin switch


if($_POST["genero"]=="F"){
        $titulos=array("#","1#Instituto","2#De","3#Area","4#Para","5#Asunto","6#Fecha Documento","7#No.Documento","8#No.Referencia");
}
elseif($_POST["genero"]=="C"){
        $titulos=array("#","1#Instituto","2#De","3#Area","4#Para","5#Asunto","6#Fecha Documento","7#No.Documento","8#No.Referencia");
}
$_SESSION['$titulos']=$titulos;



if($_POST["genero"]=="F"){
//if($dependencia_busq!='99999'){
    function pintarEstadistica($fila,$indice,$numColumna){
    $salida="";
    

    switch ($numColumna){
        case  0:
            $salida=$indice;
            break;
        case 1:
            $salida=$fila['INSTITUTO'];
        break;
        case 2:
            $salida=$fila['DE'];
        break;
        case 3:
            $salida=$fila['NOMBREAREA_PARA'];
        break;
        case 4:
            $salida=$fila['PARA'];
            break;
        case 5:
            $salida=$fila['ASUNTO'];
            break;
        case 6:
            $salida=$fila['DAT_FECHA_DOCUMENTO'];
            break;
        case 7:
            $salida=$fila['NUMERO_DOCUMENTO'];
            break;
        case 8:
           $salida=$fila['NO_REFERENCIA'];
            break;
       

    default: $salida=false;
    }
    return $salida;
    }
}else{
    function pintarEstadistica($fila,$indice,$numColumna){
    $salida="";
    
    switch ($numColumna){
        case  0:
            $salida=$indice;
            break;
        case 1:
            $salida=$fila['INSTITUTO'];
        break;
        case 2:
            $salida=$fila['DE'];
        break;
        case 3:
            $salida=$fila['NOMBREAREA_PARA'];
        break;
        case 4:
            $salida=$fila['PARA'];
            break;
        case 5:
            $salida=$fila['ASUNTO'];
            break;
        case 6:
            $salida=$fila['DAT_FECHA_DOCUMENTO'];
            break;
        case 7:
            $salida=$fila['NUMERO_DOCUMENTO'];
            break;
        case 8:
           $salida=$fila['NO_REFERENCIA'];
            break;
       

    default: $salida=false;
    }
    return $salida;
    }
}

