<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

/** REPORTE DE VENCIMIENTOS
* @autor LUCIA OJEDA ACOSTA - CRA
* @version ORFEO 3.5
* 22-Feb-2006
* 
* Optimizado por HLP. En este archivo trat�de generar las sentencias a est�dar de ADODB para que puediesen ejecutar
* en cualquier BD. En caso de no llegar a funcionar mover el contenido en tre las l�eas 18 y 60 a la secci� MSSQL y 
* descomentariar el switch.
*/
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';	
if(!$orno) $orno=1;
$orderE = "	ORDER BY $orno $ascdesc ";

$desde = $fecha_ini . " ". "00:00:00";
$hasta = $fecha_fin . " ". "23:59:59";

$sWhereFec =  " and ".$db->conn->SQLDate('r.fech_vcmto')." >= '$desde'
				and ".$db->conn->SQLDate('Y/m/d H:i:s', 'r.fech_vcmto')." <= '$hasta'";

if ( $dependencia_busq != 99999)	$condicionE = "	AND r.RADI_DEPE_ACTU=$dependencia_busq ";
//echo $radi_nume_radi;

$sWhere = " where r.radi_depe_actu=d.depe_codi
			and r.radi_fech_asig >= '$desde'
			and r.radi_fech_asig <= '$hasta'
			and r.radi_fech_asig is not null 
			$whereDependencia
			and (r.radi_nume_radi like '%2' or r.radi_nume_radi like '%3' or r.radi_nume_radi like '%4')";

$queryEDetalle = "SELECT r.radi_nume_radi AS radicado,  
			r.ra_asun AS asunto, 
			d.depe_nomb AS depe_actu,  
			r.radi_usu_ante AS usant,
			r.RADI_FECH_ASIG AS FECH_VCMTO,
			r.RADI_PATH AS HID_RADI_PATH
		FROM radicado as r, dependencia as d
		where r.radi_depe_actu=d.depe_codi
			and r.radi_fech_asig >= '$desde'
			and r.radi_fech_asig <= '$hasta'
			and r.radi_fech_asig is not null 
			and (r.radi_nume_radi like '%2' or r.radi_nume_radi like '%3' or r.radi_nume_radi like '%4')
		 ORDER BY d.depe_nomb";

$queryE = "SELECT  d.depe_nomb as DEPE_NOM, count(r.radi_nume_radi) as RADICADOS
		from radicado as r, dependencia as d
			$sWhere GROUP BY depe_nomb ORDER BY d.depe_nomb";
	
if (!is_null($fecSel)) $sWhereFecE = " AND r.fech_vcmto = '".$db->conn->SQLDate('Y/m/d H:i:s', $fecSel)."'";

// CONSULTA PARA VER DETALLES 
//$queryEDetalle = $sSQL . $sWhere . $sWhereFecE . $orderE;	

// CONSULTA PARA VER TODOS LOS DETALLES 
//$queryETodosDetalle = $sSQL . $sWhere . $sWhereFec . $orderE;	


	
if(isset($_GET['genDetalle'])&& $_GET['denDetalle']=1){
		$titulos=array("#","1#RADICADO","2#ASUNTO","3#DEPENDENCIA ACTUAL","4#USUARIO ACTUAL","5#FECHA DE VENCIMIENTO");
	}else{ 		
		$titulos=array("#","1#DEPE_NOM","2#Documentos");
	}
		
function pintarEstadistica($fila,$indice,$numColumna){
        	global $ruta_raiz,$_POST,$_GET;
        	$salida="";
        	switch ($numColumna){
        		case  0:
        			$salida=$indice;
        			break;
        		case 1:	
        			$salida=$fila['depe_nomb'];
        		break;
        		case 2:
        			$datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".urlencode($fila['HID_USUA_DOC'])."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;codUs=".$fila['HID_COD_USUARIO']."&amp;fecSel=".$fila['HID_FECH_SELEC'];
	        		$datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
	        		$salida="<a href=\"genEstadistica.php?{$datosEnvioDetalle}\"  target=\"detallesSec\" >".$fila['RADICADOS']."</a>";
        	break;
        	}
        	return $salida;
        }
function pintarEstadisticaDetalle($fila,$indice,$numColumna){
			global $ruta_raiz,$encabezado,$krd;
			$verImg=($fila['SGD_SPUB_CODIGO']==1)?($fila['USUARIO']!=$_SESSION['usua_nomb']?false:true):($fila['USUA_NIVEL']>$_SESSION['nivelus']?false:true);
        	$numRadicado=$fila['RADICADO'];	
			switch ($numColumna){
					case 0:
						$salida=$indice;
						break;
					case 1:
						if($verImg)
		   					$salida="<a class=\"vinculos\" href=\"{$ruta_raiz}verradicado.php?verrad=".$fila['RADICADO']."&amp;".session_name()."=".session_id()."&amp;krd=".$_GET['krd']."&amp;carpeta=8&amp;nomcarpeta=Busquedas&amp;tipo_carp=0 \" >".$fila['RADICADO']."</a>";
		   				else 
		   				$salida="<a class=\"vinculos\" href=\"#\" onclick=\"alert(\"ud no tiene permisos para ver el radicado\");\">".$fila['RADICADO']."</a>";
						break;
					case 2:
						$salida="<center class=\"leidos\">".$fila['ASUNTO']."</center>";
						break;
					case 3:
						$salida="<center class=\"leidos\">".$fila['DEPE_ACTU']."</center>";			
						break;
					case 4:
						$salida="<center class=\"leidos\">".$fila['USANT']."</center>";			
						break;
					case 5:
						$salida="<center class=\"leidos\">".$fila['FECH_VCMTO']."</center>";			
						break;	
			}
			return $salida;
		}





/*
switch($db->driver)
{	case 'mssql':
		{	
		}break;
	case 'oracle':
	case 'oci8':
	case 'oci805':
	case 'ocipo':
		{	$sWhereFec =  " AND r.fech_vcmto >= to_date('" . $desde . "','yyyy/mm/dd HH24:MI:ss')
							AND r.fech_vcmto <= to_date('" . $hasta . "','yyyy/mm/dd HH24:MI:ss')";
			$sWhere = " where r.radi_nume_radi not in (select anex_radi_nume from anexos where anex_estado > 2) 
							AND r.radi_nume_radi not in 
								(select radi_nume_radi from hist_eventos 
								where upper(substr(hist_obse,1,3)) = 'NRR'
								OR upper(substr(hist_obse,1,3)) = 'RSA'
								OR upper(substr(hist_obse,1,2)) = 'CE'
								OR upper(substr(hist_obse,1,3)) = 'TRA')
							AND r.radi_nume_radi like '%2' 
							AND r.tdoc_codi = td.sgd_tpr_codigo
							AND r.radi_usua_actu=u.usua_codi 
							AND r.radi_depe_actu=u.depe_codi
							AND u.depe_codi= d.depe_codi $condicionE";
			$sSQL = "SELECT r.radi_nume_radi							AS radicado, 
					to_char(r.radi_fech_radi,'yyyy/mm/dd hh24:mi:ss') 	AS fech_radi, 
					td.sgd_tpr_descrip 									AS tipo, 
					td.sgd_termino_real									AS termino,
					r.ra_asun 											AS asunto, 
					d.depe_nomb 										AS depe_actu, 
					u.usua_nomb 										AS nomb_actu, 
					r.radi_usu_ante 									AS usant,
					r.fech_vcmto										AS fech_vcmto,
					r.RADI_PATH 										AS HID_RADI_PATH
				FROM radicado r, sgd_tpr_tpdcumento td, usuario u, dependencia d ";
			$queryE = "SELECT r.fech_vcmto, 
					count(r.RADI_NUME_RADI) RADICADOS,
					r.fech_vcmto HID_FECH_SELEC
					from radicado r, sgd_tpr_tpdcumento td, usuario u, dependencia d
					$sWhere $sWhereFec GROUP BY fech_vcmto ORDER BY $orno $ascdesc";
	
	    	$sWhereFecE = " AND r.fech_vcmto = to_date('" . $fecSel . "','yyyy/mm/dd HH24:MI:ss')";
			// CONSULTA PARA VER DETALLES 
			$queryEDetalle = $sSQL . $sWhere . $sWhereFecE . $orderE;	
			// CONSULTA PARA VER TODOS LOS DETALLES 
			$queryETodosDetalle = $sSQL . $sWhere . $sWhereFec . $orderE;	
		}break;
}
*/
//$db->conn->debug = true;
?>
