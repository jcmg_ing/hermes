<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

//echo $_POST['estado'];
//echo "estado".$_REQUEST['estado'];
//echo "estado1".$_REQUEST['estado1'];
//$_REQUEST['estado']
//ECHO $_SESSION['queryE'];

//////CODIGO REPORTES
session_start();
unset($_SESSION['queryE']);

$ruta_raiz = "..";
//unset($_SESSION['queryEDetalle']);
//$_SESSION['queryE']=null;
$_SESSION['ban']=0;
$cod_usuario=$_POST['codus'];


//DEPENDENCIA
if(!empty($dependencia_busq)){
	unset($_SESSION['uno']);
	$_SESSION['uno']=$dependencia_busq;
	//echo 'sesion'.$_SESSION['uno'];
}
//USUARIO
if(!empty($_POST["codus"]))
{
	$cod_u=$_POST["codus"];
	unset($_SESSION['cod_u']);
	$_SESSION['cod_u']=$cod_u;
}

if (isset($_SESSION['CondAsignado'])){
    $condAsigDepe=$_SESSION['CondAsignado'];
}

if (isset($_SESSION['CondAsignadoUs'])) {
    $condAsigUser=$_SESSION['CondAsignadoUs'];
}


switch($db->driver)
{
	case 'postgres':
	{

        if (isset($_REQUEST['estado1']) )
        {
            switch ($_REQUEST['estado1'])
            {

                case 0: //Archivados
                    $titulos=array("#","1#De","2#Para","3#Asunto", "4#Fecha Documento", "5#Número Documento", "6#No.Referencia");

                    $isql = "select RADI_NUME_RADI as CHK_CHKANULAR,ver_usuarios(radi_usua_rem,',') as DE,
                    ver_usuarios(radi_usua_dest,',') as PARA ,RADI_ASUNTO as ASUNTO ,
                    CASE WHEN radi_fech_firma is not null THEN TO_CHAR(radi_fech_firma,'YYYY-MM-DD / HH24:MI:SS')
                    ELSE TO_CHAR(radi_fech_ofic,'YYYY-MM-DD / HH24:MI:SS') END as DAT_FECHA_DOCUMENTO ,
                    RADI_NUME_RADI as HID_RADI_NUME_RADI ,
                    RADI_NUME_TEXT as NUMERO_DOCUMENTO ,
                    radi_cuentai as NO_REFERENCIA ,
                    RADI_LEIDO as HID_RADI_LEIDO
                    from ( select distinct b.radi_nume_radi,radi_usua_rem, b.radi_usua_dest, b.radi_asunto, b.radi_fech_ofic, 1 ,
                           b.radi_nume_text, b.radi_cuentai, b.radi_leido, b.radi_fech_firma from radicado b
                           where radi_usua_actu= ". $_REQUEST['codUs']." and esta_codi=0
                    and TO_CHAR(b.radi_fech_radi,'YYYY/MM/DD') BETWEEN '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                    order by 5 DESC  ) as a";

                    $_SESSION['$ORDEN']="order by 2 DESC";
                    //echo $isql;
                    break;

                case 1://Edición

                    $titulos=array("#","1#De","2#Para","3#Asunto", "4#Fecha Documento", "5#Número Documento", "6#No.Referencia", "7#Tipo Documento");

                    $isql = "select distinct radi_nume_radi as CHK_CHKANULAR ,ver_usuarios(radi_usua_rem,',') as DE ,
                    ver_usuarios(radi_usua_dest,',') as PARA ,radi_asunto as ASUNTO ,
                    TO_CHAR(radi_fech_radi,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO ,
                    radi_nume_radi as HID_RADI_NUME_RADI ,radi_nume_text as NUMERO_DOCUMENTO ,
                    radi_cuentai as NO_REFERENCIA ,trad_descr as TIPO_DOCUMENTO ,radi_leido as HID_RADI_LEIDO
                    from
                     ( select distinct b.radi_nume_radi, b.radi_usua_rem, b.radi_usua_dest, b.radi_asunto , b.radi_fech_radi,
                       1, b.radi_nume_text, b.radi_cuentai, td.trad_descr, b.radi_leido
                       from (select * from radicado
                       where radi_usua_actu= ". $_REQUEST['codUs']." and esta_codi= ". $_REQUEST['estado1'] ."
                       and radi_inst_actu=".$_SESSION['inst_codi']."
                       and TO_CHAR(radi_fech_radi,'YYYY/MM/DD') BETWEEN  '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                       and radi_fech_agend is null) as b
                       left outer join tiporad td on b.radi_tipo=td.trad_codigo order by 5 DESC) as a" ;//order by 5,2 DESC";
                       $_SESSION['$ORDEN']="order by 2 DESC";
                       //echo $isql ;
                        break;

                case 2: //Recibidos ò en Trámite
                    $titulos=array("#","1#De","2#Asunto", "3#Fecha Documento", "4#Número Documento", "5#No.Referencia", "6#Estado", "7#Firma Digital");

                    $isql="SELECT ver_usuarios(r.radi_usua_rem,',') as DE,
                    r.RADI_ASUNTO as ASUNTO ,CASE WHEN r.radi_fech_firma is not null THEN TO_CHAR(r.radi_fech_firma,'YYYY-MM-DD / HH24:MI:SS')
                    ELSE TO_CHAR(r.radi_fech_ofic,'YYYY-MM-DD / HH24:MI:SS') END as DAT_FECHA_DOCUMENTO ,
                    r.RADI_NUME_TEXT as NUMERO_DOCUMENTO ,r.radi_cuentai as NO_REFERENCIA ,
                    e.esta_desc as ESTADO ,CASE WHEN r.radi_fech_firma is not null THEN 'SI'
                    ELSE 'NO' END as FIRMA_DIGITAL
                    FROM radicado r
                      left outer join usuarios b on r.radi_usua_actu=b.usua_codi
                      left outer join estado e on r.esta_codi=e.esta_codi
                    WHERE TO_CHAR(r.radi_fech_radi,'YYYY/MM/DD') BETWEEN  '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                    AND r.radi_fech_agend is null AND e.ESTA_CODI= 2 and r.radi_inst_actu=".$_SESSION['inst_codi']."
                    AND b.depe_codi ='". $_SESSION['uno']."' AND 
                    b.USUA_CODI='".$_REQUEST['codUs']."' AND r.radi_nume_radi
                    not in
                        ( select (p.radi_nume_radi) as RADICADOS 
                        from ( select distinct r.radi_nume_radi, bb.USUA_NOMBre as USUARIO 
                        from (select radi_usua_actu,radi_nume_radi from radicado where esta_codi=2 and radi_inst_actu=".$_SESSION['inst_codi']."
                        and TO_CHAR(radi_fech_radi,'YYYY/MM/DD') BETWEEN  '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."')r 
                        left outer join datos_usuarios bb on r.radi_usua_actu=bb.usua_codi 
                        left outer join hist_eventos h on h.radi_nume_radi=r.radi_nume_radi where h.sgd_ttr_codigo=9 and bb.depe_codi ='".$_SESSION['uno']."' 
                         AND bb.USUA_CODI='". $_REQUEST['codUs']."' )P ) ";
                    
                    $_SESSION['$ORDEN']="order by 2 DESC";

                    //echo $isql;
                     break;

                case ($_REQUEST['estado1']==3 || $_REQUEST['estado1']==4 || $_REQUEST['estado1']==5): //No enviado(originalmente),(electrónicamente).(manualmente)
                    $titulos=array("#","1#Para","2#Asunto","3#Fecha Documento", "4#Número Documento", "5#No.Referencia");

                    $isql="SELECT
                    r.RADI_NUME_RADI as CHK_CHKANULAR ,
                    ver_usuarios(radi_usua_dest,',') as PARA ,
                    r.RADI_ASUNTO as ASUNTO ,
                    TO_CHAR(r.radi_fech_ofic,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO ,
                    r.RADI_NUME_RADI as HID_RADI_NUME_RADI,
                    r.RADI_NUME_TEXT as NUMERO_DOCUMENTO ,
                    r.radi_cuentai as NO_REFERENCIA ,
                    r.RADI_LEIDO as HID_RADI_LEIDO
                    FROM radicado r
                      left outer join usuarios b on r.radi_usua_actu=b.usua_codi
                      left outer join estado e on r.esta_codi=e.esta_codi
                    WHERE TO_CHAR(r.radi_fech_radi,'YYYY/MM/DD') BETWEEN  '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                    AND radi_fech_agend is null AND
                    r.esta_codi=".$_REQUEST['estado1']." AND
                    r.radi_inst_actu=".$_SESSION['inst_codi']." AND
                    r.radi_usua_actu=".$_REQUEST['codUs'];
                   
                   $_SESSION['$ORDEN']="order by 2 DESC";
                   
                    break;

                case 6: //Enviados
                    $titulos=array("#","1#Para","2#Asunto","3#Fecha Documento", "4#Número Documento", "5#No.Referencia","6#Firma Digital");

                    $isql="select RADI_NUME_RADI as CHK_CHKANULAR ,ver_usuarios(radi_usua_dest,',') as PARA ,RADI_ASUNTO as ASUNTO ,
                    CASE WHEN radi_fech_firma is not null THEN TO_CHAR(radi_fech_firma,'YYYY-MM-DD / HH24:MI:SS')
                    ELSE TO_CHAR(radi_fech_ofic,'YYYY-MM-DD / HH24:MI:SS') END as DAT_FECHA_DOCUMENTO ,
                    RADI_NUME_RADI as HID_RADI_NUME_RADI ,RADI_NUME_TEXT as NUMERO_DOCUMENTO ,
                    radi_cuentai as NO_REFERENCIA ,CASE WHEN radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as FIRMA_DIGITAL ,
                    RADI_LEIDO as HID_RADI_LEIDO
                    from ( select distinct b.radi_nume_radi, b.radi_usua_dest, b.radi_asunto, b.radi_fech_ofic, 1 , b.radi_nume_text, b.radi_cuentai,
                    b.radi_fech_firma, b.radi_leido
                    from radicado b
                    where b.esta_codi=".$_REQUEST['estado1']." and b.radi_nume_radi=b.radi_nume_temp and b.radi_usua_actu=".$_REQUEST['codUs']."
                    and b.radi_inst_actu=".$_SESSION['inst_codi']."
                    and  TO_CHAR(b.radi_fech_radi,'YYYY/MM/DD')  BETWEEN  '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                    order by 4 DESC) as a";// order by 4 DESC";

                    $_SESSION['$ORDEN']="order by 2 DESC";
          
                    break;


                case ($_REQUEST['estado1']==9): //Reasignados  && $_REQUEST['usua_doc']=='11111'
                    $titulos=array("#","1#De","2#Para","3#Asunto","4#Fecha Documento","5#Número Documento","6#No.Referencia");//"3#Observación",

                    $isql="select
                    b.usua_codi_ori , c.usua_nomb ||' '||c.USUA_APELLIDO as DE,
                    (SELECT D.usua_nomb ||' '||D.USUA_APELLIDO FROM usuario D WHERE D.USUA_CODI=b.usua_codi_dest) AS PARA,
                    a.radi_asunto as ASUNTO,b.hist_obse as OBSERVACION,
                    TO_CHAR(b.hist_fech ,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO,
                    a.radi_nume_text as NUMERO_DOCUMENTO, a.radi_cuentai as NO_REFERNCIA
                    from radicado a, hist_eventos b,usuario c
                    where
                    a.radi_nume_radi=b.radi_nume_radi and
                    b.usua_codi_ori=c.usua_codi and
                    TO_CHAR(b.hist_fech,'YYYY/MM/DD') BETWEEN '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                    and b.sgd_ttr_codigo=".$_REQUEST['estado1']."
                    and a.radi_inst_actu=".$_SESSION['inst_codi']."
                    and b.usua_codi_ori=".$_REQUEST['codUs'];

                    $_SESSION['$ORDEN']="order by 2 DESC";
                    //echo $isql;

                    break;

                case  ($_REQUEST['estado1']=='1111'  || $_REQUEST['estado']=='11111'): //Asignados-->$_REQUEST['estado1']==9 && $_REQUEST['usua_doc']=='1111'
                    $titulos=array("#","1#UserOrig","2#Asunto","3#Fecha Documento", "4#Número Documento");

                    $isql="select distinct
                    r.radi_nume_text as NUMERO_DOCUMENTO,
                    c.USUA_NOMBRE as USUARIO_ORIGEN ,
                    b.depe_nomb AS NOMB_HID_DEPE_USUA,
                    r.radi_asunto as ASUNTO,
                    TO_CHAR(r.radi_fech_radi,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO
                    from
                        (select radi_asunto, radi_fech_radi,radi_nume_text,radi_usua_ante,radi_usua_actu,radi_nume_radi from radicado where  esta_codi=2  and radi_inst_actu=".$_SESSION['inst_codi']."
                         and TO_CHAR(radi_fech_radi,'YYYY/MM/DD') BETWEEN  '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                        )r
                    left outer join datos_usuarios b on r.radi_usua_actu=b.usua_codi
                    left outer join datos_usuarios c on r.radi_usua_ante=c.usua_codi
                    left outer join hist_eventos h on h.radi_nume_radi=r.radi_nume_radi
                    where
                    h.sgd_ttr_codigo=9
                    $condicionAsig
                    $condAsigUser";
                    $_SESSION['$ORDEN']="order by 2 DESC";

                    /*--'Asignado' as ESTA_DESC,
                    --b.USUA_CEDULA, 1111 as esta_codi,
                    --b.USUA_CODI as HID_COD_USUARIO,
                    --b.depe_codi as HID_DEPE_USUA,*/

                    //ECHO $isql;

                    break;
                case 8888: //Vencidos

                   $titulos=array("#","1#Para","2#Asunto","3#Fecha Documento","4#Número Documento");

                    $isql="select
                    r.radi_usua_actu,
                    b.USUA_NOMB||' '||b.USUA_APELLIDO as PARA,
                    r.radi_asunto as ASUNTO,
                    TO_CHAR(r.radi_fech_radi,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO,
                    r.radi_nume_text as NUMERO_DOCUMENTO
                    from usuarios b, radicado r
                    where TO_CHAR(r.radi_fech_radi,'YYYY/MM/DD')  BETWEEN  '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                    AND r.radi_usua_actu=b.usua_codi AND
                    r.radi_fech_asig <= current_date AND
                    R.ESTA_CODI<>6 AND
                    r.radi_inst_actu=".$_SESSION['inst_codi']."
                    and r.radi_usua_actu=".$_REQUEST['codUs'];

                    $_SESSION['$ORDEN']="order by 2 DESC";
                    break;

                case 7: //Eliminados

                   $titulos=array("#","1#Para","2#Asunto","3#Fecha Documento","4#Número Documento");

                    $isql="select RADI_NUME_RADI as CHK_CHKANULAR ,ver_usuarios(radi_usua_dest,',') as PARA ,
                    RADI_ASUNTO as ASUNTO ,TO_CHAR(radi_fech_radi,'YYYY-MM-DD / HH24:MI:SS') as DAT_FECHA_DOCUMENTO ,
                    RADI_NUME_RADI as HID_RADI_NUME_RADI ,RADI_NUME_TEXT as NUMERO_DOCUMENTO ,RADI_LEIDO as HID_RADI_LEIDO
                    from ( select distinct b.radi_nume_radi, b.radi_usua_dest, b.radi_asunto, b.radi_fech_radi, 1, b.radi_nume_text, b.radi_leido
                    from radicado b where b.radi_usua_actu=".$_REQUEST['codUs']." and b.esta_codi=7
                    and TO_CHAR(b.radi_fech_radi,'YYYY/MM/DD')  BETWEEN  '" . $_REQUEST['fecha_ini'] ."' AND '".  $_REQUEST['fecha_fin']."'
                    order by 4 DESC  )
                    as a";
                    $_SESSION['$ORDEN']="order by 2 DESC";
                    //ECHO $isql;
                    break;

            }
        }
    }
}




//Asigno Variables  para manejo en PDF
$_SESSION['$isql']=$isql;
$_SESSION['$estado']=$_REQUEST['estado1'];
$_SESSION['$titulos']=$titulos;
$_SESSION['$usua_doc']=$_REQUEST['usua_doc'];
$_SESSION['$descEstado']=str_replace (" ", "_", $_REQUEST['descEstado']);  //trim($_REQUEST['descEstado']);
$TituloFrame="Documentos Con Estado    ".$_REQUEST['descEstado'];//$_SESSION['$descEstado'];


//Encabezados para reporte en PDF
$_SESSION['d_reporte']=NULL;
$datos =array("1"=>$_REQUEST['area'], "2"=>$fecha_ini, "3"=>$fecha_fin, "4"=>$nombre,"5"=>$_REQUEST['user'],"6"=>$_REQUEST['descEstado'],"7"=>$_REQUEST['numDocs']);
$_SESSION['d_reporte']=$datos;





//Pone encabezados en los reportes
$encabezadopdf='
<table width="100%"  border="0" cellpadding="5" cellspacing="2" class="borde_tab">
<tr><th colspan=4 class="titulos4">'.$TituloFrame.'</tr>
<tr><th class="titulos4">Usuario<th class="titulos4">Area<th class="titulos4">Estado<th class="titulos4"># Documentos
<tr BGCOLOR="#DFDFDF"><td align="center" class="listado2"><font SIZE=2>'.$_REQUEST['user'].'</font></td><td align="center" class="listado2"><font SIZE=2>'.$_REQUEST['area'].'</font></td><td align="center" class="listado2"><font SIZE=2>'.$_REQUEST['descEstado'].'</font></td><td Align="center" class="listado2"><font SIZE=2>'.$_REQUEST['numDocs'].'</font>
</td><tr></tr>
</table>
<table align="center">
<tr><td></td><td>
</td>
</tr>
</table></ br>';

echo $encabezadopdf;
echo (
"<table class='borde_tab' width=100% >
<b>
<center>
<tr class=listado1>
  <td >
  </td>
   <td align=center>
     <A  class=botones class=vinculos HREF='javascript:history.back();'>Regresar</A>
   </td>
</tr>
</center>
</b>
</table>");

//CELSPACING=5

function pintarEstadistica($fila,$indice,$numColumna)
{
    global $ruta_raiz,$_POST,$_GET,$Estado;
    $salida="";
    $m=0;
    
    if ($_REQUEST['estado1']==0 ) //Archivados
    {
            switch ($numColumna){
            case  0:
                $salida=$indice;
                break;
            case 1:
                $salida=$fila['DE'];
            break;
            case 2:
               $salida=$fila['PARA'];
            break;
            case 3:
                $salida=$fila['ASUNTO'];
                break;
            case  4:
                $salida=$fila['DAT_FECHA_DOCUMENTO'];
                break;
            case 5:
                $salida=$fila['NUMERO_DOCUMENTO'];
            break;
            case 6:
               $salida=$fila['NO_REFERENCIA'];
            break;
            default: $salida=false;
        }
        return $salida;
    }
    elseif ($_REQUEST['estado1']==1 ) //En edcición , Trámite
    {
            switch ($numColumna){
            case  0:
                $salida=$indice;
                break;
            case 1:
                $salida=$fila['DE'];
            break;
            case 2:
               $salida=$fila['PARA'];
            break;
            case 3:
                $salida=$fila['ASUNTO'];
                break;
            case  4:
                $salida=$fila['DAT_FECHA_DOCUMENTO'];
                break;
            case 5:
                $salida=$fila['NUMERO_DOCUMENTO'];
            break;
            case 6:
               $salida=$fila['NO_REFERENCIA'];
            break;
            case 7:
                $salida=$fila['TIPO_DOCUMENTO'];
                break;
            default: $salida=false;
        }
        return $salida;
    }

  elseif ($_REQUEST['estado1']==2) //Recibidos
    {
            //$titulos=array("#","1#De","2#Asunto", "3#Fecha Documento", "4#Numero Documento", "5#No.Referencia", "6#Estado", "7#Firma Digital");
            switch ($numColumna){
            case  0:
                $salida=$indice;
                break;
            case 1:
               $salida=$fila['DE'];
            break;
            case 2:
               $salida=$fila['ASUNTO'];
            break;
            case 3:
                $salida=$fila['DAT_FECHA_DOCUMENTO'];
                break;
            case  4:
                $salida=$fila['NUMERO_DOCUMENTO'];
                break;
            case 5:
               $salida=$fila['NO_REFERENCIA'];
                break;
            case 6:
               $salida=$fila['ESTADO'];
                break;
            case 7:
               $salida=$fila['FIRMA_DIGITAL'];
                break;
        default: $salida=false;
        }
        return $salida;
    }

    elseif ($_REQUEST['estado1']==3 || $_REQUEST['estado1']==4 || $_REQUEST['estado1']==5) //No enviado
    {
            $titulos=array("#","1#Para","2#Asunto","3#Fecha Documento", "4#Número Documento", "5#No.Referencia");
            switch ($numColumna){
            case  0:
                $salida=$indice;
                break;
            case 1:
               $salida=$fila['PARA'];
            break;
            case 2:
               $salida=$fila['ASUNTO'];
            break;
            case 3:
                $salida=$fila['DAT_FECHA_DOCUMENTO'];
                break;
            case  4:
                $salida=$fila['NUMERO_DOCUMENTO'];
                break;
            case 5:
               $salida=$fila['NO_REFERENCIA'];
                break;
        default: $salida=false;
        }
        return $salida;
    }

    elseif ($_REQUEST['estado1']==6 ) //Enviados
    {
            switch ($numColumna){
            case  0:
                $salida=$indice;
                break;
            case 1:
               $salida=$fila['PARA'];
            break;
            case 2:
               $salida=$fila['ASUNTO'];
            break;
            case 3:
                $salida=$fila['DAT_FECHA_DOCUMENTO'];
                break;
            case  4:
                $salida=$fila['NUMERO_DOCUMENTO'];
                break;
            case 5:
               $salida=$fila['NO_REFERENCIA'];
                break;
            case 6:
               $salida=$fila['FIRMA_DIGITAL'];
                break;
        default: $salida=false;
        }
        return $salida;
    }
    elseif($_REQUEST['estado1']==9 ) //Reasignados  && $_REQUEST['usua_doc']=='11111'
    {
         // $titulos=array("#","1#Para","2#Asunto","3#Observación", "4#Fecha Documento", "5#Número Documento", "6#No.Referencia");

            switch ($numColumna){
          case  0:
                $salida=$indice;
                break;
            case 1:
               $salida=$fila['DE'];
            break;
            case 2:
               $salida=$fila['PARA'];
            break;
            case 3:
               $salida=$fila['ASUNTO'];
            break;
            case 4:
                $salida=$fila['DAT_FECHA_DOCUMENTO'];
                break;
            case 5:
                $salida=$fila['NUMERO_DOCUMENTO'];
                break;
            case 6:
               $salida=$fila['NO_REFERNCIA'];
                break;
            
        default: $salida=false;
        }
        return $salida;
  }

   elseif($_REQUEST['estado1']=='1111' || $_REQUEST['estado1']=='11111') //Asignados $_REQUEST['estado1']==9 && $_REQUEST['usua_doc']=='1111'
    {

        switch ($numColumna){
            case  0:
                $salida=$indice;
            break;
            case 1:
               $salida=$fila['USUARIO_ORIGEN'];
            break;
            case 2:
               $salida=$fila['ASUNTO'];
            break;
            case 3:
                $salida=$fila['DAT_FECHA_DOCUMENTO'];
                break;
            case  4:
                $salida=$fila['NUMERO_DOCUMENTO'];
                break;
        default: $salida=false;
        }
        return $salida;
  }



  elseif ($_REQUEST['estado1']==8888 || $_REQUEST['estado1']==7) //Vencidos
    {
            switch ($numColumna){
            case  0:
                $salida=$indice;
                break;
            case 1:
               $salida=$fila['PARA'];
            break;
            case 2:
               $salida=$fila['ASUNTO'];
            break;
            case 3:
                $salida=$fila['DAT_FECHA_DOCUMENTO'];
                break;
            case  4:
                $salida=$fila['NUMERO_DOCUMENTO'];
                break;
        default: $salida=false;
        }
        return $salida;
  }
}
?>

