<?
/*************************************************************************************/
/* ORFEO GPL:Sistema de Gestion Documental		http://www.orfeogpl.org	     */
/*	Idea Original de la SUPERINTENDENCIA DE SERVICIOS PUBLICOS DOMICILIARIOS     */
/*				COLOMBIA TEL. (57) (1) 6913005  orfeogpl@gmail.com   */
/* ===========================                                                       */
/*                                                                                   */
/* Este programa es software libre. usted puede redistribuirlo y/o modificarlo       */
/* bajo los terminos de la licencia GNU General Public publicada por                 */
/* la "Free Software Foundation"; Licencia version 2. 			             */
/*                                                                                   */
/* Copyright (c) 2005 por :	  	  	                                     */
/* SSPS "Superintendencia de Servicios Publicos Domiciliarios"                       */
/*   Jairo Hernan Losada  jlosada@gmail.com                Desarrollador             */
/*   Sixto Angel Pinzón López --- angel.pinzon@gmail.com   Desarrollador             */
/* C.R.A.  "COMISION DE REGULACION DE AGUAS Y SANEAMIENTO AMBIENTAL"                 */ 
/*   Liliana Gomez        lgomezv@gmail.com                Desarrolladora            */
/*   Lucia Ojeda          lojedaster@gmail.com             Desarrolladora            */
/* D.N.P. "Departamento Nacional de Planeación"                                      */
/*   Hollman Ladino       hladino@gmail.com                Desarrollador             */
/*                                                                                   */
/* Colocar desde esta lInea las Modificaciones Realizadas Luego de la Version 3.5    */
/*  Nombre Desarrollador   Correo     Fecha   Modificacion      */
/*  Martha Yaneth Mera    mymera@gmail.com     2006-05-10*/
/*************************************************************************************/
?>
<?
//var_dump('tipo'.$tipoDocumento);
/*echo '<pre>';
//echo $whereDependencia;
echo $whereTipoRadicado;
//echo $whereUsuario;
//echo $dependencia_busq;
echo $tipoDocumento.'//';
echo $codUs.'//';
echo $usua_docu;
//echo 'codigo medio '.$mrecCodi;
echo '</pre>';*/
/** RADICADOS DE ENTRADA RECIBIDOS DEL AREA DE CORRESPONDENCIA
	* 
	* @autor JAIRO H LOSADA - SSPD
	* @version ORFEO 3.1
	* 
	*/
$coltp3Esp = '"'.$tip3Nombre[3][2].'"';	
if(!$orno) $orno=1;
 /**
   * $db-driver Variable que trae el driver seleccionado en la conexion
   * @var string
   * @access public
   */
 /**
   * $fecha_ini Variable que trae la fecha de Inicio Seleccionada  viene en formato Y-m-d
   * @var string
   * @access public
   */
/**
   * $fecha_fin Variable que trae la fecha de Fin Seleccionada
   * @var string
   * @access public
   */
/**
   * $mrecCodi Variable que trae el medio de recepcion por el cual va a sacar el detalle de la Consulta.
   * @var string
   * @access public
   */

switch($db->driver)
	{
	case 'mssql':
	case 'postgres':	
	{	if ( $dependencia_busq != 99999)
			{	$condicionE = "	AND b.DEPE_CODI=$dependencia_busq AND r.RADI_DEPE_ACTU=$dependencia_busq ";
			}
			if($tipoDocumento=='9999')
			{	$queryE = "SELECT  b.USUA_NOMB as USUARIO
					, count($radi_nume_radi) as RADICADOS
					, MIN(b.USUA_CODI) as HID_COD_USUARIO
					, b.USUA_doc
					FROM RADICADO r, USUARIO b 
				WHERE
					r.RADI_USUA_ACTU=b.USUA_CODI 
					AND r.RADI_DEPE_ACTU=b.DEPE_CODI
					$condicionE
					$whereTipoRadicado 
					GROUP BY b.USUA_doc, b.USUA_NOMB
				ORDER BY $orno $ascdesc"; 
			}
			else
			{	$queryE = "SELECT b.USUA_NOMB as USUARIO
					, t.SGD_TPR_DESCRIP as TIPO_DOCUMENTO
					, count($radi_nume_radi) as RADICADOS
					, MIN(b.USUA_CODI)  as HID_COD_USUARIO
					, MIN(SGD_TPR_CODIGO) as HID_TPR_CODIGO
					, b.USUA_doc			
				FROM RADICADO r 
					INNER JOIN USUARIO b ON r.RADI_USUA_ACTU=b.USUA_CODI AND r.RADI_DEPE_ACTU=b.DEPE_CODI  
					LEFT OUTER JOIN SGD_TPR_TPDCUMENTO t ON r.tdoc_codi=t.SGD_TPR_CODIGO
				WHERE 1=1 $condicionE $whereTipoRadicado  
				GROUP BY b.USUA_doc, b.USUA_NOMB, t.SGD_TPR_DESCRIP
				ORDER BY $orno $ascdesc"; 
			}
			/** CONSULTA PARA VER DETALLES 
	 */

	$condicionE = " AND b.USUA_DOC= $usua_docu";
	if (!is_null($tipoDOCumento))
	{	$condicionE .= " AND t.SGD_TPR_CODIGO = $tipoDOCumento ";
	}
	$queryEDetalle = "SELECT DISTINCT
			$radi_nume_radi as RADICADO
			,t.SGD_TPR_DESCRIP as TIPO_DE_DOCUMENTO
			, b.USUA_NOMB as USUARIO
			, r.RA_ASUN as ASUNTO
			, ".$db->conn->SQLDate('Y/m/d H:i:s','r.radi_fech_radi')." as FECHA_RADICACION
			, bod.NOMBRE_DE_LA_EMPRESA as ESP
			,r.RADI_PATH as HID_RADI_PATH{$seguridad}
			FROM USUARIO b, RADICADO r
				LEFT OUTER JOIN SGD_TPR_TPDCUMENTO t ON r.tdoc_codi = t.SGD_TPR_CODIGO 
				LEFT OUTER JOIN bodega_empresas bod ON r.eesp_codi = bod.identificador_empresa 
		WHERE 
			r.RADI_USUA_ACTU=b.USUA_CODI 
			AND r.RADI_DEPE_ACTU=b.DEPE_CODI
			$condicionE ";
		$orderE = "	ORDER BY $orno $ascdesc";			
				
	 /** CONSULTA PARA VER TODOS LOS DETALLES 
	 */ 
	$queryETodosDetalle = $queryEDetalle . $orderE;
	$queryEDetalle .= $whereTipoRadicado . $orderE;
		}break;
	case 'oracle':
	case 'oci8':
	case 'oci805':
	case 'ocipo':
	if ( $dependencia_busq != 99999)  {
		$condicionE = "				AND b.DEPE_CODI=$dependencia_busq 
			AND r.RADI_DEPE_ACTU=$dependencia_busq ";
	}

	if($tipoDocumento=='9999')
	{
	$queryE = "
	    SELECT  b.USUA_NOMB USUARIO,
			 count(r.RADI_NUME_RADI) 	RADICADOS
			, MIN(b.USUA_CODI)			HID_COD_USUARIO
			FROM RADICADO r, USUARIO b 
		WHERE 
			r.RADI_USUA_ACTU=b.USUA_CODI 
			AND r.RADI_DEPE_ACTU=b.DEPE_CODI
			$condicionE
			$whereTipoRadicado 
			GROUP BY b.USUA_NOMB
		ORDER BY $orno $ascdesc"; 
	} else {
	$queryE = "
	    SELECT  b.USUA_NOMB USUARIO
			, t.SGD_TPR_DESCRIP TIPO_DOCUMENTO
			, count(r.RADI_NUME_RADI) 	RADICADOS
			, MIN(b.USUA_CODI)			HID_COD_USUARIO
			, MIN(SGD_TPR_CODIGO) 		HID_TPR_CODIGO			
			FROM RADICADO r, USUARIO b, SGD_TPR_TPDCUMENTO t
		WHERE 
			r.RADI_USUA_ACTU=b.USUA_CODI 
			AND r.tdoc_codi=t.SGD_TPR_CODIGO (+)
			AND r.RADI_DEPE_ACTU=b.DEPE_CODI
			$condicionE
			$whereTipoRadicado 
			GROUP BY b.USUA_NOMB, t.SGD_TPR_DESCRIP
		ORDER BY $orno $ascdesc"; 
	}
	/** CONSULTA PARA VER DETALLES 
	 */

		$condicionE = " AND b.USUA_CODI= $codUs ";

	if($tipoDocumento=='9998'){
	$condicionE .= " AND t.SGD_TPR_CODIGO = $tipoDOCumento ";
	}
	$queryEDetalle = "SELECT DISTINCT
			r.RADI_NUME_RADI RADICADO
			,TO_CHAR(r.RADI_FECH_RADI, 'DD/MM/YYYY HH24:MI:SS') FECHA_RADICACION
			,t.SGD_TPR_DESCRIP TIPO_DE_DOCUMENTO
			, b.USUA_NOMB USUARIO_ACTUAL
			, r.RA_ASUN ASUNTO
			, bod.NOMBRE_DE_LA_EMPRESA ESP
			,n.par_serv_nombre SECTOR			
			,(select CAU.sgd_cau_DESCRIP
				from  sgd_dcau_causal dc, sgd_cau_causal cau
      				where dc.sgd_dcau_codigo=o.sgd_dcau_codigo
      	    			and dc.SGD_cau_codigo=cau.sgd_cau_codigo) CAUSAL			
			,(select dc.sgd_dcau_descrip
				from sgd_dcau_causal dc
     				 where dc.sgd_dcau_codigo=o.sgd_dcau_codigo) DETALLE_CAUSAL 
			, r.radi_usu_ante USUARIO_ANTERIOR
			,r.RADI_PATH HID_RADI_PATH{$seguridad}
			FROM RADICADO r, USUARIO b
			   , SGD_TPR_TPDCUMENTO t
			   , bodega_empresas bod
			   ,par_serv_servicios n
			   ,sgd_caux_causales o
		WHERE 
		 	r.eesp_codi = bod.identificador_empresa (+)
		    AND r.RADI_USUA_ACTU=b.USUA_CODI
			AND r.tdoc_codi=t.SGD_TPR_CODIGO (+)
			AND r.RADI_DEPE_ACTU=$dependencia_busq 			
			AND b.DEPE_CODI=$dependencia_busq 
			AND r.RADI_DEPE_ACTU=$dependencia_busq 
			and r.par_serv_secue=n.par_serv_codigo(+)
			and r.radi_nume_radi=o.radi_nume_radi(+)

		$whereTipoRadicado ";
		$orderE = "	ORDER BY $orno $ascdesc";			
				
	 /** CONSULTA PARA VER TODOS LOS DETALLES 
	 */ 
	$queryETodosDetalle = $queryEDetalle . $orderE;
	$queryEDetalle .= $condicionE . $orderE;
	break;
	}
	
if(isset($_GET['genDetalle'])&& $_GET['denDetalle']=1){
		$titulos=array("#","1#RADICADO","2#FECHA RADICACION","3#TIPO DE DOCUMENTO","4#USUARIO ACTUAL","5#ASUNTO","6#ESP","7#SECTOR","8#CAUSAL","9#DETALLE CAUSAL","10#USUARIO ANTERIOR");
	}else{ 		
		$titulos=($tipoDocumento=='9999')?array("#","1#Usuario","2#Documentos"):array("#","1#Usuario","3#TIPO DE DOCUMENTO","2#Documentos");
	}
		
function pintarEstadistica($fila,$indice,$numColumna){
        	global $ruta_raiz,$_POST,$_GET;
        	$salida="";
        	switch ($numColumna){
        		case  0:
        			$salida=$indice;
        			break;
        		case 1:	
        			$salida=$fila['USUARIO'];
        		break;
        		case 2:
        			if(isset($fila['TIPO_DOCUMENTO'])){
        				$salida=$fila['TIPO_DOCUMENTO']; 
        			}else{
        			$datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".$fila['USUA_DOC']."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;codUs=".$fila['HID_COD_USUARIO']."&amp;";
	        		$datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
	        		$salida="<a href=\"genEstadistica.php?{$datosEnvioDetalle}\"  target=\"detallesSec\" >".$fila['RADICADOS']."</a>";}        	break;
        		case 3:
        			$datosEnvioDetalle="tipoEstadistica=".$_POST['tipoEstadistica']."&amp;genDetalle=1&amp;usua_doc=".urlencode($fila['HID_USUA_DOC'])."&amp;dependencia_busq=".$_POST['dependencia_busq']."&amp;fecha_ini=".$_POST['fecha_ini']."&amp;fecha_fin=".$_POST['fecha_fin']."&amp;tipoRadicado=".$_POST['tipoRadicado']."&amp;tipoDocumento=".$_POST['tipoDocumento']."&amp;codUs=".$fila['HID_COD_USUARIO']."&amp;tipoDOCumento=".$fila['HID_TPR_CODIGO'];
	        		$datosEnvioDetalle=(isset($_POST['usActivos']))?$datosEnvioDetalle."&amp;usActivos=".$_POST['usActivos']:$datosEnvioDetalle;
	        		$salida="<a href=\"genEstadistica.php?{$datosEnvioDetalle}\"  target=\"detallesSec\" >".$fila['RADICADOS']."</a>";
        	break;
        	}
        	return $salida;
        }
function pintarEstadisticaDetalle($fila,$indice,$numColumna){
			global $ruta_raiz,$encabezado,$krd;
			$verImg=($fila['SGD_SPUB_CODIGO']==1)?($fila['USUARIO']!=$_SESSION['usua_nomb']?false:true):($fila['USUA_NIVEL']>$_SESSION['nivelus']?false:true);
        	$numRadicado=$fila['RADICADO'];	
			switch ($numColumna){
					case 0:
						$salida=$indice;
						break;
					case 1:
						if($fila['HID_RADI_PATH'] && $verImg)
							$salida="<center><a href=\"{$ruta_raiz}bodega".$fila['HID_RADI_PATH']."\">".$fila['RADICADO']."</a></center>";
						else 
							$salida="<center class=\"leidos\">{$numRadicado}</center>";	
						break;
					case 2:
						if($verImg)
		   					$salida="<a class=\"vinculos\" href=\"{$ruta_raiz}verradicado.php?verrad=".$fila['RADICADO']."&amp;".session_name()."=".session_id()."&amp;krd=".$_GET['krd']."&amp;carpeta=8&amp;nomcarpeta=Busquedas&amp;tipo_carp=0 \" >".$fila['FECHA_RADICACION']."</a>";
		   				else 
		   				$salida="<a class=\"vinculos\" href=\"#\" onclick=\"alert(\"ud no tiene permisos para ver el radicado\");\">".$fila['FECHA_RADICADO']."</a>";
						break;
					case 3:
						$salida="<center class=\"leidos\">".$fila['TIPO_DE_DOCUMENTO']."</center>";		
						break;
					case 4:
						$salida="<center class=\"leidos\">".$fila['USUARIO_ACTUAL']."</center>";
						break;
					case 5:
						$salida="<center class=\"leidos\">".$fila['ASUNTO']."</center>";
						break;
					case 6:
						$salida="<center class=\"leidos\">".$fila['ESP']."</center>";			
						break;	
					case 7:
						$salida="<center class=\"leidos\">".$fila['SECTOR']."</center>";			
						break;	
					case 8:
						$salida="<center class=\"leidos\">".$fila['CAUSAL']."</center>";			
						break;	
					case 9:
						$salida="<center class=\"leidos\">".$fila['DETALLE_CAUSAL']."</center>";			
						break;	
					case 10:
						$salida="<center class=\"leidos\">".$fila['USUARIO_ANTERIOR']."</center>";			
						break;
			}
			return $salida;
		}	
?>
