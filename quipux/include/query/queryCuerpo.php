<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

// Codigo modificado por M. Haro - email: mauricioharo21@gmail.com
// se incluyo un select adicional y *LIMIT**OFFSET* en varios queries para mejorar el rendimiento de la BDD
// La función ver_usuarios terda mucho tiempo en ejecutarse y cuando son muchos registros la ejecución de
// esta consulta se hace muy pesada.
// Para mejorar esto se cambiaron algunas librerias de ADODB para que al momento de realizar el count elimine la función
// y el limit y el offset se los pone en el query interior para que la función se ejecute solo para los registros que se van a mostrar.
// Adicionalmente se elimino la ejecución de la función en el count del paginador
// Archivos ADODB: (revisión svn 456)
// - adodb/adodb-lib.inc.php    - function _adodb_getcount()
// - adodb/drivers/adodb-postgres7.inc.php  - function SelectLimit()

 
switch($db->driver) {
    case 'postgres':
    $datos_usuarios = "(select usua_codi, usua_nomb||' '||usua_apellido as \"usua_nombre\", depe_codi, inst_codi from usuarios
                        union all select ciu_codigo, ciu_nombre||' '||ciu_apellido,0,0 from ciudadano) as ";

    $datos_usuarios_imprimir = "(select usua_codi, coalesce(usua_nomb,'')||' '||coalesce(usua_apellido,'') as \"usua_nombre\", depe_codi, inst_codi from usuarios where depe_codi=".$_SESSION["depe_codi"].") as ";
    if ($carpeta==0)//condicion solo al momento de ingresar al sistema mantis: 3744
        $carpeta=2;
    switch ($carpeta) {

        case 1:  // En elaboración
            if ($orderNo=='') $orderNo=6;
            $isql = "select -- En elaboracion
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(radi_fech_radi::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,trad_descr as \"Tipo Documento\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select b.radi_nume_radi,1,2, b.radi_usua_rem, b.radi_usua_dest, b.radi_asunto
                        , b.radi_fech_radi, 3, b.radi_nume_text, b.radi_cuentai, td.trad_descr, cat.cat_descr, b.radi_leido
                        , b.radi_nume_temp, b.radi_path, b.radi_tipo
                        from (select * from radicado b where radi_usua_actu=".$_SESSION["usua_codi"]
                    . " and esta_codi=1 and radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro ) as b
                        left outer join tiporad td on b.radi_tipo = td.trad_codigo
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            //ECHO $isql ;
            break;
            
        case 2:  // Recibidos
            if ($orderNo=='') $orderNo=6;
            $recibido='<div align="center"><a href="#" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/folder_page.png" width="15" height="15" alt="Recibido" border="0"><span>Recibido</span></div>';
            $vencido='<div align="center"><a href="#" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/vencidos.png" width="15" height="15" alt="Vencido" border="0"><span>Vencido Reasignado</span></div>';
            $reasignado='<div align="center"><a href="#" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/email_go.png" width="15" height="15" alt="Reasignado" border="0"><span>Reasignado</span></div>';
            $isql = "select -- Recibidos
                    case when radi_fech_asig is null then '$recibido' else
                    (case when radi_fech_asig::date < now()::date then '$vencido'
                     else '$reasignado' end) end as \" \"
                    ,radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,radi_nume_text as \"DAT_Número Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,radi_asunto as \"Asunto\"
                    ,CASE WHEN radi_fech_firma is not null THEN substr(radi_fech_firma::text,1,19)
                     ELSE substr(radi_fech_ofic::text,1,19) END as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI2\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,esta_desc as \"Estado\"
                    ,CASE WHEN radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as \"Firma Digital\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"                    
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select distinct 1, b.radi_nume_radi, 2, 3, b.radi_usua_rem, b.radi_asunto, b.radi_fech_ofic, 1,
                        b.radi_nume_text, b.radi_cuentai, es.esta_desc, b.radi_fech_firma, cat.cat_descr, b.radi_leido,
                        b.radi_nume_temp, b.radi_path, b.radi_tipo, b.radi_fech_asig
                        from (select * from radicado b where radi_usua_actu=".$_SESSION["usua_codi"]
                    . " and esta_codi = 2 and radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro
                        and radi_nume_radi not in (select radi_nume_radi from tarea where estado=1 and usua_codi_ori=".$_SESSION["usua_codi"].")) as b
                        left outer join estado es on b.esta_codi = es.esta_codi
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            
          //echo $isql;
            break;

        case 6:  // Eliminados
        case 84:  // Ciudadanos Firma - Eliminados
            if ($orderNo=='') $orderNo=5;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- Eliminados
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                     ,radi_asunto as \"Asunto\"
                    ,substr(radi_fech_radi::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select radi_nume_radi, 1, 2, radi_usua_dest, radi_asunto, radi_fech_radi, 1, radi_nume_text, radi_leido
                        from radicado b where b.radi_usua_actu = " . $_SESSION["usua_codi"] .
                      " and b.esta_codi=7 and b.radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            break;

        case 7:  // No enviados, sin firma digital
            if ($orderNo=='') $orderNo=5;
//            $db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- No enviados
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(radi_fech_ofic::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select b.radi_nume_radi, 1, 2, b.radi_usua_dest, b.radi_asunto, b.radi_fech_ofic, 1,
                        b.radi_nume_text, b.radi_cuentai, cat.cat_descr, b.radi_leido
                        from (select * from radicado b where radi_usua_actu = " . $_SESSION["usua_codi"] .
                      " and esta_codi=3 and radi_nume_radi = radi_nume_temp and
                        radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro) as b
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET* 
                    ) as a order by ".($orderNo+1)." $orderTipo";
                
            break;
            
        case 8:  // Enviados
            if ($orderNo=='') $orderNo=6;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- Bandeja Enviados
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(fecha::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,CASE WHEN radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as \"Firma Digital\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select radi_nume_radi, 1, 2, radi_usua_rem, radi_usua_dest, radi_asunto
                        ,CASE WHEN radi_fech_firma is not null THEN radi_fech_firma ELSE radi_fech_ofic END as \"fecha\"
                        ,radi_nume_temp, radi_nume_text, radi_cuentai, radi_fech_firma, cat.cat_descr, radi_leido
                        from
                        ( select * from radicado b where esta_codi=6 and radi_nume_radi=radi_nume_temp
                            and radi_usua_actu = " . $_SESSION["usua_codi"]. " and radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro) as b
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";

                //echo $isql;
            break;

        case 10: //Archivados
            if ($orderNo=='') $orderNo=6;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- Bandeja Archivados
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(fecha::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select radi_nume_radi, 1, 2, radi_usua_rem, radi_usua_dest, radi_asunto
                        ,CASE WHEN radi_fech_firma is not null THEN radi_fech_firma ELSE radi_fech_ofic END as \"fecha\"
                        ,radi_nume_temp, radi_nume_text, radi_cuentai, cat.cat_descr ,radi_leido
                        from
                        ( select * from radicado b where radi_usua_actu = " . $_SESSION["usua_codi"]
                        . " and esta_codi=0 and radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro) as b
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            break;

        case 12:  // Bandeja Reasignados
            if ($orderNo=='') $orderNo=6;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");
            
            $isql = "select -- Bandeja Reasignados
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,substr(fecha::text,1,19) as \"Fecha Documento\"
                    ,usua_dest as \"Reasignado a\"
                    ,hist_obse as \"Comentario\"
                    ,substr(hist_fech::text,1,19) as \"DAT_Fecha Reasignación\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,hist_referencia as \"Fecha Max. de Respuesta\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,radi_nume_text as \"Número Documento\"
                    ,esta_desc as \"Estado\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select b.radi_nume_radi, 1, 2
                        ,CASE WHEN b.radi_fech_firma is not null THEN radi_fech_firma ELSE radi_fech_ofic END as \"fecha\"
                        , usuDest.usua_nomb || ' ' || usuDest.usua_apellido as usua_dest
                        , h2.hist_obse
                        , h2.hist_fech
                        , 3
                        , h2.hist_referencia
                        , b.radi_usua_rem, b.radi_usua_dest, b.radi_asunto
                        , b.radi_nume_text, es.esta_desc,b.radi_leido
                        from
                        (select radi_nume_radi, hist_fech, usua_codi_dest, hist_obse, hist_referencia from hist_eventos where usua_codi_ori=".$_SESSION["usua_codi"]." and sgd_ttr_codigo=9) as h2
                        left outer join usuarios usuDest on h2.usua_codi_dest = usuDest.usua_codi
                        left outer join radicado as b on h2.radi_nume_radi=b.radi_nume_radi
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        left outer join estado es on b.esta_codi=es.esta_codi
                        where radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro ";
            if($tipoLectura!=2)
                $isql.=" and b.radi_leido = ".$tipoLectura;            
            if ($estado !=-1 )
                $isql.= " and b.esta_codi=".$estado;
            if ( $txt_fecha_desde!='' and  $txt_fecha_hasta!=''){//si selecciono fechas
               $isql.=" and h2.hist_fech::date between '".$txt_fecha_desde."' and '".$txt_fecha_hasta."'";
            }
            $isql.=" order by " . ($orderNo+1) . " $orderTipo *LIMIT**OFFSET*
                    ) as a order by " . ($orderNo+1). " $orderTipo";
            //echo $isql;
            break;

        case 13: // Bandeja Informados
            if ($orderNo=='') $orderNo=5;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- Bandeja Informados
                    radi_nume_radi AS \"CHK_checkValue\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios('-' || usua_info || '-',',')  AS \"Informador\"
                    ,radi_asunto AS \"Asunto\"
                    ,substr(info_fech::text,1,19) as \"DAT_Fecha Información\"
                    ,radi_nume_radi AS \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text AS \"Número Documento\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"
                    ,info_leido as \"HID_RADI_LEIDO\"
                    from (
                        select b.radi_nume_radi, 1, 2, i.usua_info, b.radi_asunto, i.info_fech
                        , b.radi_nume_temp, b.radi_nume_text, cat.cat_descr, i.info_leido
                        from 
                        ( select * from informados where usua_codi = " . $_SESSION["usua_codi"] . ") as i
                        left outer join radicado b on b.radi_nume_radi=i.radi_nume_radi
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        where b.radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro ";
            if($tipoLectura!='2')
                 $isql.=" and info_leido = ".$tipoLectura;
            $isql.=" order by " . ($orderNo+1) . " $orderTipo *LIMIT**OFFSET*
                    ) as a order by " . ($orderNo+1) . " $orderTipo";
            //echo $isql;
            break;

        case 14: //Bandeja Compartida solo Documentos Recibidos
            if ($orderNo=='') $orderNo=5;

            $isql = "select -- Bandeja Compartida
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,radi_asunto as \"Asunto\"
                    ,CASE WHEN radi_fech_firma is not null THEN substr(radi_fech_firma::text,1,19)
                     ELSE substr(radi_fech_ofic::text,1,19) END as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,esta_desc as \"Estado\"
                    ,CASE WHEN radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as \"Firma Digital\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select distinct b.radi_nume_radi, 1, 2, b.radi_usua_rem,b.radi_asunto, b.radi_fech_ofic, 1,
                        b.radi_nume_text, b.radi_cuentai, es.esta_desc, b.radi_fech_firma, cat.cat_descr, b.radi_leido,
                        b.radi_nume_temp, b.radi_path, b.radi_tipo
                        from (select * from radicado b where radi_usua_actu=".$_SESSION["usua_codi_jefe"]
                    . " and esta_codi = 2 and radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro) as b
                        left outer join estado es on b.esta_codi = es.esta_codi
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            break;

        case 15: //Tareas Recibidas o asignadas al usuario actual
            if ($orderNo=='') $orderNo=9;
            $where_tarea = "(usua_codi_dest=".$_SESSION["usua_codi"].")";
            if ($tarea_tipo == 1)  {
                $where_tarea = "usua_codi_dest=".$_SESSION["usua_codi"];
            } else if ($tarea_tipo == 2)  {
                $where_tarea = "usua_codi_ori=".$_SESSION["usua_codi"];
            }
            if ($slc_tarea_estado != 0)  {
                $where_tarea .= " and estado=$tarea_estado";
            }

            $isql = "select -- Tareas Recibidas
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,substr(fecha_inicio::text,1,10) as \"Fecha Asignación\"
                    ,usua_ori as \"Asignado por\"
                    --,usua_dest as \"Asignado para\"
                    ,comentario as \"Comentario\"
                    ,substr(fecha_maxima::text,1,10) as \"Fecha Máxima\"
                    ,avance||'%' as \"Avance\"
                    ,estado as \"Estado\"
                    ,case when dias_retraso>0 then '<font color=\"red\">'||dias_retraso||' d&iacute;as</font>' else '' end as \"Dias Retraso\"
                    ,radi_nume_text as \"Número Documento\"
                    ,substr(radi_fech_ofic::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    from (
                        select b.radi_nume_radi, t.fecha_inicio, uo.usua_nombre as usua_ori, ud.usua_nombre as usua_dest, th.comentario, t.fecha_maxima, t.avance
                        , case when t.estado=1 then 'Pendiente' else (case when t.estado=2 then 'Finalizado' else 'Cancelado' end) end as \"estado\"
                        , coalesce(t.fecha_fin,now())::date-t.fecha_maxima::date as \"dias_retraso\"
                        , b.radi_nume_text, b.radi_fech_ofic, 1, b.radi_usua_rem, b.radi_usua_dest, b.radi_asunto
                        from
                            (select * from tarea where $where_tarea) as t
                            left outer join tarea_hist_eventos th on th.tarea_hist_codi=t.comentario_inicio
                            left outer join radicado b on t.radi_nume_radi=b.radi_nume_radi
                            left outer join nombres_usuarios uo on uo.usua_codi = t.usua_codi_ori
                            left outer join nombres_usuarios ud on ud.usua_codi = t.usua_codi_dest
                        where 1=1 $whereFiltro
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            break;

        case 16: //Tareas Enviadas o Asignadas a otros funcionarios
            if ($orderNo=='') $orderNo=9;
            $where_tarea = "(usua_codi_ori=".$_SESSION["usua_codi"].")";
            if ($tarea_tipo == 1)  {
                $where_tarea = "usua_codi_dest=".$_SESSION["usua_codi"];
            } else if ($tarea_tipo == 2)  {
                $where_tarea = "usua_codi_ori=".$_SESSION["usua_codi"];
            }
            if ($slc_tarea_estado != 0)  {
                $where_tarea .= " and estado=$tarea_estado";
            }

            $isql = "select -- Tareas Enviadas
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,substr(fecha_inicio::text,1,10) as \"Fecha Asignación\"
                    --,usua_ori as \"Asignado por\"
                    ,usua_dest as \"Asignado para\"
                    ,comentario as \"Comentario\"
                    ,substr(fecha_maxima::text,1,10) as \"Fecha Máxima\"
                    ,avance||'%' as \"Avance\"
                    ,estado as \"Estado\"
                    ,case when dias_retraso>0 then '<font color=\"red\">'||dias_retraso||' d&iacute;as</font>' else '' end as \"Dias Retraso\"
                    ,radi_nume_text as \"Número Documento\"
                    ,substr(radi_fech_ofic::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    from (
                        select b.radi_nume_radi, t.fecha_inicio, uo.usua_nombre as usua_ori, ud.usua_nombre as usua_dest, th.comentario, t.fecha_maxima, t.avance
                        , case when t.estado=1 then 'Pendiente' else (case when t.estado=2 then 'Finalizado' else 'Cancelado' end) end as \"estado\"
                        , coalesce(t.fecha_fin,now())::date-t.fecha_maxima::date as \"dias_retraso\"
                        , b.radi_nume_text, b.radi_fech_ofic, 1, b.radi_usua_rem, b.radi_usua_dest, b.radi_asunto
                        from
                            (select * from tarea where $where_tarea) as t
                            left outer join tarea_hist_eventos th on th.tarea_hist_codi=t.comentario_inicio
                            left outer join radicado b on t.radi_nume_radi=b.radi_nume_radi
                            left outer join nombres_usuarios uo on uo.usua_codi = t.usua_codi_ori
                            left outer join nombres_usuarios ud on ud.usua_codi = t.usua_codi_dest
                        where 1=1 $whereFiltro
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            break;
            
        case 80:  // Enviados Ciudadano
            if ($orderNo=='') $orderNo=6;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- Enviados Ciudadanos
                    distinct radi_nume_radi as \"CHK_CHKANULAR\",'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    , u.usua_nombre as \"Para\"
                    , u.inst_nombre as \"Institución\"
                    , b.radi_asunto as \"Asunto\"
                    , substr(radi_fech_radi::text,1,19) as \"DAT_Fecha Documento\"
                    , b.radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    , b.radi_nume_text as \"Número Documento\"
                    , b.radi_cuentai as \"No. Referencia\"
                    , es.esta_desc as \"Estado\"
                    from (select * from radicado b where radi_usua_rem like '%-".$_SESSION["usua_codi"]."-%' and radi_nume_radi::text like '%1' and esta_codi in (0,2) $whereFiltro) as b
                        left outer join estado es on b.esta_codi=es.esta_codi
                        left outer join usuario u on replace(b.radi_usua_dest,'-','')::integer = u.usua_codi
                    order by " . ($orderNo+1) . " $orderTipo";
            break;

        case 81:  // Recibidos Ciudadano
            if ($orderNo=='') $orderNo=6;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select  -- Recibidos Ciudadanos
                    distinct radi_nume_radi as \"CHK_CHKANULAR\",'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,u.usua_nombre as \"De\"
                    , u.inst_nombre as \"Institución\"
                    , b.radi_asunto as \"Asunto\"
                    , substr(radi_fech_ofic::text,1,19) as \"DAT_Fecha Documento\"
                    , b.radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    , b.radi_nume_text as \"Número Documento\"
                    , b.radi_cuentai as \"No. Referencia\"
                    , es.esta_desc as \"Estado\"
                    from (select * from radicado b where radi_usua_dest like '%-".$_SESSION["usua_codi"]."-%' and radi_nume_radi::text like '%1' and esta_codi=6 $whereFiltro) as b
                        left outer join estado es on b.esta_codi=es.esta_codi
                        left outer join usuario u on replace(b.radi_usua_rem,'-','')::integer = u.usua_codi
                    order by " . ($orderNo+1) . " $orderTipo";
            break;

        case 90: //Pendientes recibidos de ciudadanos
            if ($orderNo=='') $orderNo=6;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- Bandeja Pendientes recibidos de ciudadanos
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(fecha::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,ver_usuarios(usua_redirigido::text,',') as \"Redirigir a\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select radi_nume_radi, 1, 2, radi_usua_rem, radi_usua_dest, radi_asunto
                        ,CASE WHEN radi_fech_firma is not null THEN radi_fech_firma ELSE radi_fech_ofic END as \"fecha\"
                        ,radi_nume_temp, radi_nume_text, radi_cuentai
                        , case when radi_usua_redirigido=0 then radi_usua_actu else radi_usua_redirigido end as usua_redirigido ,radi_leido
                        from
                        ( select * from radicado b where esta_codi=9 and radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro) as b
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            break;

        case 99:  // Documentos por imprimir, envío manual
            if ($orderNo=='') $orderNo=6;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- Documentos por Imprimir
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,usua_remitente as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(radi_fech_ofic::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,'No Enviado' as \"Estado\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"
                    ,usua_radi_nombre as \"Redactado por\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select distinct b.radi_nume_radi, 1, 2, coalesce(ur.usua_nomb,'')||' '||coalesce(ur.usua_apellido,'') as usua_remitente
                        , b.radi_usua_dest, b.radi_asunto,b.radi_fech_ofic, b.radi_nume_temp, b.radi_nume_text, b.radi_cuentai, 3, cat.cat_descr
                        , coalesce(ugr.usua_nomb,'')||' '||coalesce(ugr.usua_apellido,'') as usua_radi_nombre, b.radi_leido
                        from 
                        ( select * from radicado b where radi_inst_actu=".$_SESSION["inst_codi"]." and esta_codi=5 $whereFiltro) as b
                        left outer join usuarios ur on split_part(b.radi_usua_rem,'-',2)::integer=ur.usua_codi and ur.depe_codi=".$_SESSION["depe_codi"]."
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        left outer join radicado rp on b.radi_nume_temp=rp.radi_nume_radi
                        left outer join usuarios ugr on rp.radi_usua_radi=ugr.usua_codi
                        where ur.depe_codi is not null
                        order by " . ($orderNo+1) . " $orderTipo *LIMIT**OFFSET*
                    ) as a order by " . ($orderNo+1) . " $orderTipo";
                    //echo $isql;
            break;

        case 82:  // Ciudadanos Firma - En elaboración
            if ($orderNo=='') $orderNo=5;
            $isql = "select -- En elaboracion
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(radi_fech_radi::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,radi_leido as \"HID_RADI_LEIDO\"
                    from (
                        select b.radi_nume_radi, 1, 2, b.radi_usua_dest, b.radi_asunto
                        , b.radi_fech_radi, 3, b.radi_nume_text, b.radi_cuentai, b.radi_leido
                        , b.radi_nume_temp, b.radi_path, b.radi_tipo
                        from (select * from radicado b where radi_usua_actu=".$_SESSION["usua_codi"]
                    . " and esta_codi=1 and radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro ) as b
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";
            //ECHO $isql ;
            break;

        case 83:  // Recibidos
            if ($orderNo=='') $orderNo=6;
            $recibido='<div align="center"><a href="#" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/folder_page.png" width="15" height="15" alt="Recibido" border="0"><span>Recibido</span></div>';
            $vencido='<div align="center"><a href="#" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/vencidos.png" width="15" height="15" alt="Vencido" border="0"><span>Vencido Reasignado</span></div>';
            $reasignado='<div align="center"><a href="#" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/email_go.png" width="15" height="15" alt="Reasignado" border="0"><span>Reasignado</span></div>';
            $isql = "select -- Recibidos
                    case when radi_fech_asig is null then '$recibido' else
                    (case when radi_fech_asig::date < now()::date then '$vencido'
                     else '$reasignado' end) end as \" \"
                    ,radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,radi_asunto as \"Asunto\"
                    ,CASE WHEN radi_fech_firma is not null THEN substr(radi_fech_firma::text,1,19)
                     ELSE substr(radi_fech_ofic::text,1,19) END as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,CASE WHEN radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as \"Firma Digital\"
                    ,1 as \"HID_RADI_LEIDO\"
                    from (
                        select distinct 1, b.radi_nume_radi, 2, 3, b.radi_usua_rem, b.radi_asunto, b.radi_fech_ofic, 1,
                        b.radi_nume_text, b.radi_cuentai, b.radi_fech_firma,
                        b.radi_nume_temp, b.radi_path, b.radi_tipo, b.radi_fech_asig
                        from (select * from radicado b where
                        radi_usua_dest = '-".$_SESSION["usua_codi"]."-' and radi_nume_radi::text like '%1' and esta_codi in (2,6) $whereFiltro) as b
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";

//          echo $isql;
            break;

        case 85:  // No enviados, sin firma digital
            if ($orderNo=='') $orderNo=5;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- No enviados
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(radi_fech_ofic::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,1 as \"HID_RADI_LEIDO\"
                    from (
                        select b.radi_nume_radi, 1, 2, b.radi_usua_dest, b.radi_asunto, b.radi_fech_ofic, 1,
                        b.radi_nume_text, b.radi_cuentai, b.radi_leido
                        from (select * from radicado b where radi_usua_actu = " . $_SESSION["usua_codi"] .
                      " and esta_codi=3 and radi_nume_radi = radi_nume_temp and
                        radi_inst_actu = " . $_SESSION["inst_codi"] . " $whereFiltro) as b
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";

            break;

        case 86:  // Enviados
            if ($orderNo=='') $orderNo=6;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");

            $isql = "select -- Bandeja Enviados
                    radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,ver_usuarios(radi_usua_rem,',') as \"De\"
                    ,ver_usuarios(radi_usua_dest,',') as \"Para\"
                    ,radi_asunto as \"Asunto\"
                    ,substr(fecha::text,1,19) as \"DAT_Fecha Documento\"
                    ,radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    ,radi_nume_text as \"Número Documento\"
                    ,radi_cuentai as \"No. Referencia\"
                    ,CASE WHEN radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as \"Firma Digital\"
                    ,1 as \"HID_RADI_LEIDO\"
                    from (
                        select radi_nume_radi, 1, 2, radi_usua_rem, radi_usua_dest, radi_asunto
                        ,CASE WHEN radi_fech_firma is not null THEN radi_fech_firma ELSE radi_fech_ofic END as \"fecha\"
                        ,radi_nume_temp, radi_nume_text, radi_cuentai, radi_fech_firma, radi_leido
                        from
                        ( select * from radicado b where 
                            (radi_usua_rem like '%-" . $_SESSION["usua_codi"]. "-%' and radi_nume_radi::text like '%1' and esta_codi in (0,2))
                             or (radi_usua_rem = '-" . $_SESSION["usua_codi"]. "-' and radi_nume_radi::text like '%0' and esta_codi in (6)) $whereFiltro
                        ) as b
                        left outer join categoria cat on b.cat_codi = cat.cat_codi
                        order by ".($orderNo+1)." $orderTipo *LIMIT**OFFSET*
                    ) as a order by ".($orderNo+1)." $orderTipo";

                //echo $isql;
            break;

        default:
            if ($orderNo=='') $orderNo=5;
            //$db = new ConnectionHandler("$ruta_raiz","busqueda");
            
            $isql = "select  -- Bandeja Nuevos
                    b.radi_nume_radi as \"CHK_CHKANULAR\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    --, coalesce(ua.usua_nomb,'')||' '||coalesce(ua.usua_apellido,'') as \"Enviado Por\"
                    , ver_usuarios('-' || ua.usua_codi || '-',',') as \"Enviado Por\"
                    , b.radi_asunto as \"Asunto\"
                    ,substr(b.radi_fech_radi::text,1,19) as \"DAT_Fecha Documento\"
                    , b.radi_nume_radi as \"HID_RADI_NUME_RADI\"
                    , b.radi_nume_text as \"Número Documento\"
                    , b.radi_cuentai as \"No. Referencia\"
                    , es.esta_desc as \"Estado\"
                    ,CASE WHEN b.radi_fech_firma is not null THEN 'SI' ELSE 'NO' END as \"Firma Digital\"
                    ,case when cat_descr is null then 'Normal' else case when upper(cat_descr)='URGENTE' then '<font color=\"red\">'||cat_descr||'</font>' else cat_descr end end as \"Categoría\"
                    ,b.radi_leido as \"HID_RADI_LEIDO\"
                    from (select * from radicado b where radi_usua_actu=".$_SESSION["usua_codi"]
                    . " and (esta_codi=1 or esta_codi=2) and radi_leido=0 $whereFiltro) as b
                    left outer join estado es on b.esta_codi=es.esta_codi
                    left outer join usuarios ua on b.radi_usua_ante=ua.usua_codi
                    left outer join categoria cat on b.cat_codi = cat.cat_codi
                    order by " . ($orderNo+1) . " $orderTipo";

            
            break;
    }
	break;	
}

//echo "filtro--".$whereFiltro."<br><br>";
//echo $isql;
?>