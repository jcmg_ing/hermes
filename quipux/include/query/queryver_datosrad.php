<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

switch ($db->driver) 
	{ 
	case "oracle" :
	case 'oci8':
	case 'postgres':
		$numero = "a.RADI_NUME_RADI as RADI_NUME_DERI1";
		$radi_nume_radi = " a.radi_nume_radi ";
		$radi_nume_deri = " a.radi_nume_deri ";
		$radi_nume_text = " a.radi_nume_text ";
	break;	
	case "mssql":
		$numero = "convert(varchar(14), a.RADI_NUME_RADI) as RADI_NUME_DERI1";
		$radi_nume_radi = " convert(varchar(15), a.radi_nume_radi) ";
		$radi_nume_deri = " convert(varchar(15), a.radi_nume_deri) ";
	break;				   			   
	}
?>
