<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

	/**
	  * CONSULTA VERIFICACION PREVIA A LA RADICACION
	  */
	switch($db->driver)
	{  
	 case 'mssql':
	 
	 $query = "select  
		d.sgd_fenv_descrip,
		c.depe_nomb,
		a.radi_nume_sal,
		a.sgd_renv_nombre,
		a.sgd_renv_dir,
		a.sgd_renv_mpio,
		a.sgd_renv_depto,
		a.sgd_renv_fech,
		a.sgd_deve_fech,
		b.sgd_deve_desc,
		'-' firma,
		a.sgd_renv_planilla,
		a.sgd_renv_cantidad,
		a.sgd_renv_valor
		from SGD_RENV_REGENVIO a, 
			 sgd_deve_dev_envio  b,
			 dependencia c,
			 SGD_FENV_FRMENVIO d ";
		$fecha_mes = substr($fecha_ini,0,7);
	 	$where_isql = ' WHERE a.sgd_deve_fech BETWEEN
   	                  '.$db->conn->DBTimeStamp($fecha_ini).' and '.$db->conn->DBTimeStamp($fecha_fin).'
	                  and a.sgd_deve_codigo=b.sgd_deve_codigo
	                  and a.sgd_fenv_codigo=d.sgd_fenv_codigo
	                  and '.$db->conn->substr.'(convert(char(15),a.radi_nume_sal, 5, 3)=c.depe_codi
	                  and a.sgd_deve_codigo is not null
	                  ';
	break;		
	case 'oracle':
	case 'oci8':
	case 'oci805':			
	
	$query = "select  
		d.sgd_fenv_descrip,
		c.depe_nomb,
		a.radi_nume_sal,
		a.sgd_renv_nombre,
		a.sgd_renv_dir,
		a.sgd_renv_mpio,
		a.sgd_renv_depto,
		a.sgd_renv_fech,
		a.sgd_deve_fech,
		b.sgd_deve_desc,
		'-' firma,
		a.sgd_renv_planilla,
		a.sgd_renv_cantidad,
		a.sgd_renv_valor
		from SGD_RENV_REGENVIO a, 
			 sgd_deve_dev_envio  b,
			 dependencia c,
			 SGD_FENV_FRMENVIO d ";
		$fecha_mes = substr($fecha_ini,0,7);
				$where_isql = ' WHERE a.sgd_deve_fech BETWEEN
				'.$db->conn->DBTimeStamp($fecha_ini).' and '.$db->conn->DBTimeStamp($fecha_fin).'
				and a.sgd_deve_codigo=b.sgd_deve_codigo
				and a.sgd_fenv_codigo=d.sgd_fenv_codigo
				and '.$db->conn->substr.'(a.radi_nume_sal, 5, 3)=c.depe_codi
				and a.sgd_deve_codigo is not null
			';
	break;	
	case 'postgres':
	$query = "select  
		d.sgd_fenv_descrip,
		c.depe_nomb,
		a.radi_nume_sal,
		a.sgd_renv_nombre,
		a.sgd_renv_dir,
		a.sgd_renv_mpio,
		a.sgd_renv_depto,
		a.sgd_renv_fech,
		a.sgd_deve_fech,
		b.sgd_deve_desc,
		'-' firma,
		a.sgd_renv_planilla,
		a.sgd_renv_cantidad,
		a.sgd_renv_valor
		from SGD_RENV_REGENVIO a, 
			 sgd_deve_dev_envio  b,
			 dependencia c,
			 SGD_FENV_FRMENVIO d ";
		$fecha_mes = substr($fecha_ini,0,7);
				$where_isql = ' WHERE a.sgd_deve_fech BETWEEN
				'.$db->conn->DBTimeStamp($fecha_ini).' and '.$db->conn->DBTimeStamp($fecha_fin).'
				and a.sgd_deve_codigo=b.sgd_deve_codigo
				and a.sgd_fenv_codigo=d.sgd_fenv_codigo
				and '.$db->conn->substr.'(a.radi_nume_sal, 5, 3)=c.depe_codi
				and a.sgd_deve_codigo is not null
			';
	break;		
	}
?>