<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

/**
*	Autor			Iniciales		Fecha (dd/mm/aaaa)
*
*
*	Modificado por		Iniciales		Fecha (dd/mm/aaaa)
*
*
*	Comentado por		Iniciales		Fecha (dd/mm/aaaa)
*	Sylvia Velasco		SV			02-12-2008
**/

/**
* Consultar datos de ciudadano dependiendo del nombre para busqueda, realiza conversion del nombre a mayúsculas,
* reemplaza letras con tilde por letras sin tilde, las conversiones se realizan para obtener todos los datos
* esperados.
**/


include_once "$ruta_raiz/funciones.php";
    switch($db->driver)	{
	case 'postgres':
        $buscar_nom = trim(strtoupper($buscar_nom));
        
        $sql= "select ciu_nombre AS \"SCR_Nombre\"
            ,'seleccionar_ciudadano(\"'|| ciu_codigo ||'\");' as \"HID_FUNCION\",
               ciu_cedula AS \"Cédula\" ,ciu_email AS \"Email\",
               case (select sol_estado from solicitud_firma_ciudadano f where f.ciu_codigo = s.ciu_codigo) when 0 then 'Rechazado'
               when 1 then 'En Edición'
               when 2 then 'Enviado'
               when 3 then 'Autorizado'
               else 'No existe estado'
               end AS \"Estado\"
               from datos_solicitud as s
               where 
               (s.sol_estado =2 or s.sol_estado =0 or s.sol_estado =3)";
      
            if($opc == a)
            $sql .=  ' and sol_estado = 0';
            else if($opc == c)
            $sql .=  ' and sol_estado = 2';
            else if($opc == d)
            $sql .=  ' and sol_estado = 3';
            else if($opc == e)
            $sql .=  ' and (sol_estado = 0 or sol_estado = 2 or sol_estado = 3)';

	    if ($buscar_nom!="") {
            $sql .=  ' and ' . buscar_nombre_cedula_solicitud($buscar_nom);
		}
	    $sql .= " order by ".($orderNo+1)." $orderTipo";
        //echo $sql;
        //die();
break;
}
?>
