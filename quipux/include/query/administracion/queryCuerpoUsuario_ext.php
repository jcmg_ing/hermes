<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

/**
*	Autor			Iniciales		Fecha (dd/mm/aaaa)
*
*
*	Modificado por		Iniciales		Fecha (dd/mm/aaaa)
*
*
*	Comentado por		Iniciales		Fecha (dd/mm/aaaa)
*	Sylvia Velasco		SV			02-12-2008
**/

/**
* Consultar datos de ciudadano dependiendo del nombre para busqueda, realiza conversion del nombre a mayúsculas,
* reemplaza letras con tilde por letras sin tilde, las conversiones se realizan para obtener todos los datos
* esperados.
**/


include_once "$ruta_raiz/funciones.php";
    switch($db->driver)	{
	case 'postgres':
        $buscar_nom = trim(strtoupper($buscar_nom));
        if (($accion==2 and ($_POST["ciu_ver"]=="a"  or $opc=="a")) and ($_SESSION["usua_codi"]==0 or substr($_SESSION["krd"],0,6)=="UADMIN")){
        //echo "1";
        $sql= "select case when trim(usua_nombre)='' then 'S/N' else usua_nombre end AS \"SCR_Nombre\"
                               ,'seleccionar_usuario(\"'|| usua_codi ||'\");' as \"HID_FUNCION\"
                               , usua_cedula AS \"Cédula\"
                                        ,usua_email AS \"Email\"
                                        , usua_cargo AS \"".$descCargo."\"
                                        , inst_nombre AS \"".$descEmpresa."\"
                                        ,case usua_esta
                                         when 1 then 'Activo'
                                         else 'Inactivo'
                                         end as \"Estado\"
                               from datos_usuarios
                               where inst_codi=0 and usua_esta=1 and usua_codi>0";
        }
        elseif (($accion==2 and ($_POST["ciu_ver"]=="b" or $opc=="b")) and ($_SESSION["usua_codi"]==0 or substr($_SESSION["krd"],0,6)=="UADMIN")){
        //echo "2";
        $sql= "select case when trim(usua_nombre)='' then 'S/N' else usua_nombre end AS \"SCR_Nombre\"
                               ,'seleccionar_usuario(\"'|| usua_codi ||'\");' as \"HID_FUNCION\"
                               , usua_cedula AS \"Cédula\"
                                        ,usua_email AS \"Email\"
                                        , usua_cargo AS \"".$descCargo."\"
                                        , inst_nombre AS \"".$descEmpresa."\"
                                        ,case usua_esta
                                         when 1 then 'Activo'
                                         else 'Inactivo'
                                         end as \"Estado\"
                               from datos_usuarios
                               where inst_codi=0 and usua_esta=0 and usua_codi>0";
        }
        elseif (($accion==2 and ($_POST["ciu_ver"]=="c"  or $opc=="c" )) and ($_SESSION["usua_codi"]==0 or substr($_SESSION["krd"],0,6)=="UADMIN")){
        //echo "3";
        $sql= "select case when trim(usua_nombre)='' then 'S/N' else usua_nombre end AS \"SCR_Nombre\"
                               ,'seleccionar_usuario(\"'|| usua_codi ||'\");' as \"HID_FUNCION\"
                               , usua_cedula AS \"Cédula\"
                                        ,usua_email AS \"Email\"
                                        , usua_cargo AS \"".$descCargo."\"
                                        , inst_nombre AS \"".$descEmpresa."\"
                                        ,case usua_esta
                                         when 1 then 'Activo'
                                         else 'Inactivo'
                                         end as \"Estado\"
                               from datos_usuarios
                               where inst_codi=0 and usua_codi>0";
        }
        elseif (($accion==2 and ($_POST["ciu_ver"]=="" or   $opc=="")) and ($_SESSION["usua_codi"]==0 or substr($_SESSION["krd"],0,6)=="UADMIN")){
        //echo "4";
        $sql= "select case when trim(usua_nombre)='' then 'S/N' else usua_nombre end AS \"SCR_Nombre\"
                               ,'seleccionar_usuario(\"'|| usua_codi ||'\");' as \"HID_FUNCION\"
                               , usua_cedula AS \"Cédula\"
                                        ,usua_email AS \"Email\"
                                        , usua_cargo AS \"".$descCargo."\"
                                        , inst_nombre AS \"".$descEmpresa."\"
                                        ,case usua_esta
                                         when 1 then 'Activo'
                                         else 'Inactivo'
                                         end as \"Estado\"
                               from datos_usuarios
                               where inst_codi=0 and usua_esta=1 and usua_codi>0";
        }
        elseif ($accion==2 and $_SESSION["usua_codi"]!=0 ){
        //echo "5";
        $sql= "select case when trim(usua_nombre)='' then 'S/N' else usua_nombre end AS \"SCR_Nombre\"
                               ,'seleccionar_usuario(\"'|| usua_codi ||'\");' as \"HID_FUNCION\"
                               , usua_cedula AS \"Cédula\"
                                        ,usua_email AS \"Email\"
                                        , usua_cargo AS \"".$descCargo."\"
                                        , inst_nombre AS \"".$descEmpresa."\"
                                        ,case usua_esta
                                         when 1 then 'Activo'
                                         else 'Inactivo'
                                         end as \"Estado\"
                               from datos_usuarios
                               where inst_codi=0 and usua_esta=1 and usua_codi>0";
        }


        if ($accion!=2){
         //echo "6";
         $sql.= "select case when trim(usua_nombre)='' then 'S/N' else usua_nombre end AS \"SCR_Nombre\"
                               ,'seleccionar_usuario(\"'|| usua_codi ||'\");' as \"HID_FUNCION\"
                               , usua_cedula AS \"Cédula\"
                                        ,usua_email AS \"Email\"
                                        , usua_cargo AS \"".$descCargo."\"
                                        , inst_nombre AS \"".$descEmpresa."\"
                                        ,case usua_esta
                                         when 1 then 'Activo'
                                         else 'Inactivo'
                                         end as \"Estado\"
                               from datos_usuarios
                               where inst_codi=0 and usua_esta=1 and usua_codi>0";
        }

	    if ($buscar_nom!="") {
            $sql .=  ' and ' . buscar_nombre_cedula($buscar_nom);
		}
	    $sql .= " order by ".($orderNo+1)." $orderTipo";
        //echo $sql;
        //die();
break;
}
?>
