<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/


if (!$db->driver){	$db = $this->db; }	//Esto sirve para cuando se llama este archivo dentro de clases donde no se conoce $db.

switch($db->driver)
{
    case 'postgres':
    	$sqlFecha = $db->conn->SQLDate("Y-m-d H:i A","R.RADI_FECH_RADI");
        $area_recorrido = "and u1.depe_codi=$dependenciaSel and u2.depe_codi=$dependenciaSel";
	$usr_recorrido = "";
	if ($dependenciaSel=="0") {
	    $area_recorrido = "and (".str_replace("depe_codi","u1.depe_codi",$visibilidad_areas[1]).") ";
	    $area_recorrido .= "and (".str_replace("depe_codi","u2.depe_codi",$visibilidad_areas[1]).")";
	}
	if ($slc_docs_usr=="1")
	    $usr_recorrido = "and h.usua_codi_ori=".$_SESSION["usua_codi"]." and h.usua_codi_dest=".$_SESSION["usua_codi"];
	$from_usr_recorrido = " (select distinct radi_nume_radi from hist_eventos h, usuario u1, usuario u2
		where h.usua_codi_ori=u1.usua_codi and h.usua_codi_dest=u2.usua_codi $area_recorrido $usr_recorrido) as RDU, ";
	$where_usr_recorrido = " R.RADI_NUME_RADI = RDU.RADI_NUME_RADI ";
    	$isql = "select distinct
		R.RADI_NUME_TEXT as \"IDT_Numero ".$_SESSION["descRadicado"]."\"
		,R.RADI_PATH as \"HID_RADI_PATH\"
		,$sqlFecha as \"DAT_Fecha ".$_SESSION["descRadicado"]."\"
		, R.RADI_NUME_RADI as \"HID_RADI_NUME_RADI\"
		, R.RADI_CUENTAI as \"Numero de Referencia\"
		,UPPER(R.RADI_ASUNTO)  as \"Asunto\"
		,UPPER(R.RADI_RESUMEN)  as \"Resumen\"
		,U.USUA_NOMBRE AS \"Usuario Actual\" 
		,T.TRAD_DESCR as \"Tipo de Documento\"
		,U.depe_nomb as \"Area Actual del Documento\"
		from $from_usr_recorrido radicado R left outer join datos_usuarios U on R.radi_usua_actu=U.usua_codi
		left outer join tiporad T on R.radi_tipo=T.trad_codigo
		left outer join radi_texto TR on R.radi_texto=tr.text_codi 
		WHERE ". $where_usr_recorrido;

    	$isql .= $sWhere." order by ".($orderNo+1)." $orderTipo";
        //  $rs = $db->conn->Execute($isql);
//echo $isql;
      

///busqueda anterior
/*$isql = 'select distinct
		R.RADI_NUME_TEXT as "IDT_Numero '.$_SESSION["descRadicado"].'"
		,bs.RADI_PATH as "HID_RADI_PATH"
		,'.$sqlFecha.' as "DAT_Fecha '.$_SESSION["descRadicado"].'"
		, R.RADI_NUME_RADI as "HID_RADI_NUME_RADI"
		,UPPER(R.RA_ASUN)  as "Asunto"
		,UPPER(R.RADI_RESUMEN)  as "Resumen"
		,c.SGD_TPR_DESCRIP as "Tipo Documento" 
		,U.USUA_NOMB AS "Usuario Actual"
		,D.DEPE_NOMB as "'.$_SESSION["descDependencia"].'"
		,rem.usua_nomb as "Remitente"
		,dest.usua_nomb as "Destinatario"
		,concat(cca.usua_nomb) as "Con Copia"
	 from radicado R
	left outer join SGD_TPR_TPDCUMENTO c on R.tdoc_codi=c.sgd_tpr_codigo
	left outer join (select radi_nume_radi, radi_path from radicado where sgd_spub_codigo=0 or (radi_depe_actu='.$dependencia.' and radi_usua_actu='.$codusuario.')) as bs on bs.radi_nume_radi=R.radi_nume_radi 
	left outer join (select usua_doc, usua_nomb from usuario union all select TO_CHAR(sgd_ciu_codigo,'."'99999999999999'".'), sgd_ciu_nombre from sgd_ciu_ciudadano) as rem on R.usua_rem=rem.usua_doc
	left outer join (select usua_doc, usua_nomb from usuario union all select TO_CHAR(sgd_ciu_codigo,'."'99999999999999'".'), sgd_ciu_nombre from sgd_ciu_ciudadano) as dest on R.usua_dest=dest.usua_doc
	left outer join (select usua_doc, usua_nomb from usuario union all select TO_CHAR(sgd_ciu_codigo,'."'99999999999999'".'), sgd_ciu_nombre from sgd_ciu_ciudadano) as cca on '."R.radi_cca like '%' || cca.usua_doc || '%'".'

	INNER JOIN USUARIO U ON R.RADI_USUA_ACTU=U.USUA_CODI AND R.RADI_DEPE_ACTU=U.DEPE_CODI 
	INNER JOIN DEPENDENCIA D ON D.DEPE_CODI=R.RADI_DEPE_ACTU 

WHERE r.RADI_FECH_RADI >=
	'.$db->conn->DBTimeStamp($fecha_ini).' and r.RADI_FECH_RADI <= '.$db->conn->DBTimeStamp($fecha_fin) . $sWhere
	. ' group by R.RADI_NUME_TEXT,bs.RADI_PATH,R.RADI_FECH_RADI,R.RADI_NUME_RADI,R.RA_ASUN,R.RADI_RESUMEN, 
	c.SGD_TPR_DESCRIP,U.USUA_NOMB,D.DEPE_NOMB,rem.usua_nomb,dest.usua_nomb ';*/
	break;
}
?>
