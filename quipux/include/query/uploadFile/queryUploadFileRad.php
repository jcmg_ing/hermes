<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

    /**
      * CONSULTA DE UPLOAD FILE
      * @author JAIRO LOSADA DNP - SSPD 2006/03/01
      * @version 3.5.1
      *
      * @param $query String Almacena Consulta que se enviara
      * @param $sqlFecha String  Almacena fecha en  formato Y-m-d que devuelve ADODB para la base de datos escogida
      */

//    if (trim($busq_radicados_tmp)=="")
//        $busq_radicados_tmp2 = $busq_radicados_tmp;
//    else
//        $busq_radicados_tmp2 = " and ($busq_radicados_tmp)";

    switch($db->driver)
    {
	case 'postgres':
//	$sqlFecha = "r.RADI_FECH_RADI";
	$sqlFecha = "substr(r.radi_fech_radi::text, 1,19)";
        if ($orderNo=='') $orderNo = 4;
        //Asociar imagen
        $query = "SELECT --Documentos sin Asociar imagen
                radi_nume_radi as \"CHR_DATO\"
                ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                ,radi_nume_text as \"Numero $descRadicado\",
                $sqlFecha as \"DAT_Fecha $descRadicado\",
                radi_nume_radi as \"HID_RADI_NUME_RADI\",
                radi_asunto as \"Asunto\",
                ver_usuarios(radi_usua_rem,',') as \"De\",
                ver_usuarios(radi_usua_dest,',') as \"Para\",
                case when radi_usua_redirigido<>0 then
                    ver_usuarios('-'||radi_usua_redirigido||'-',',') 
                end as \"Diridigo a\"
                from (
                select a.radi_nume_radi, 1, 2, a.radi_nume_text, a.radi_fech_radi, 3, 
                a.radi_asunto, a.radi_usua_rem, a.radi_usua_dest,a.radi_usua_redirigido
                from (select r.* from radicado r where (esta_codi=6 or esta_codi=0) and radi_inst_actu=".$_SESSION["inst_codi"]
                ." $busq_radicados_tmp and r.radi_nume_radi in (select radi_nume_temp
                                                                from radicado where radi_nume_radi::text like '%1' and radi_tipo_archivo=0 )) as a
                --where a.radi_nume_radi=sp.radi_nume_temp
                order by " . ($orderNo+1) . " $orderTipo *LIMIT**OFFSET*
                ) as r order by ".($orderNo+1)." $orderTipo";
        //echo $query;
        //Imprimir Comprobantes
	$sqlFecha = "substr(radi_fech_radi::text, 1,19)";
        $query1 = "select --Imprimir Comprobantes
                    radi_nume_radi as \"CHR_Dato\"
                    ,'<img src=\"$ruta_raiz/iconos/popup.png\" border=0>' as \"SCR_ \"
                    ,'mostrar_documento(\"'||radi_nume_radi||'\",\"'||radi_nume_text||'\",\"'||$carpeta||'\")' as \"HID_POPUP\"
                    ,radi_nume_text as \"No. $descRadicado\",
                    $sqlFecha as \"DAT_Fecha $descRadicado\",
                    radi_nume_radi as \"HID_RADI_NUME_RADI\",
                    radi_asunto as \"Asunto\",
                    ver_usuarios(radi_usua_rem,',') as \"De\"
                    from (select * from radicado where radi_nume_radi::text like '%2'
                    and radi_inst_actu=".$_SESSION["inst_codi"].
                    $busq_radicados_tmp .
                  " order by " . ($orderNo+1) . " $orderTipo *LIMIT**OFFSET*) as r
                    order by ".($orderNo+1)." $orderTipo";

        //uploadTx.php
        $query2 = "SELECT
                    RADI_NUME_TEXT as \"Numero $descRadicado\",
                    $sqlFecha as \"DAT_Fecha $descRadicado\",
                    RADI_NUME_RADI as \"HID_RADI_NUME_RADI\",
                    RADI_ASUNTO as \"Asunto\"
                    FROM RADICADO
                    WHERE radi_inst_actu=".$_SESSION["inst_codi"]."
                    $busq_radicados_tmp order by 2 ";
        break;
    }
?>
