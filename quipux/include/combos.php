<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
//los comentarios son superfluos!!1  ;-)
require_once("$ruta_raiz/include/db/ConnectionHandler.php"); 
class combo
{	var $row;
	var $respuesta;
	var $cursor;
	

	function combo($cur)
	{	$this->cursor=$cur;	}
	
	
	function conectar($dbsql,$valu,$tex,$verific,$muestreo,$simple)
	{	error_reporting(7);
		//print("PREVIO A LA CONEXION ****************");
		$this->cursor->conn->SetFetchMode(ADODB_FETCH_ASSOC);	 
		//$this->cursor->conn->debug=true;
		$rs=$this->cursor->query($dbsql);
 		//esta opcion permite cargar en un select de html una consulta... tambien
		//se selecciona el campo ke va a actuar como valor y cual desplegado haci como el de verificacion

		if($simple==0)
		{	while(!$rs->EOF)
			{	
				if(strcmp(trim($verific),trim($rs->fields[$valu]))==0)
				{
					$sel="selected";
				}
				else $sel ="";
				
				echo "<option value='".$rs->fields[strtoupper($valu)]."' $sel>".$rs->fields[strtoupper($tex)]."</option>";
				$rs->MoveNext();
			}
		}   
  	}
}
?>