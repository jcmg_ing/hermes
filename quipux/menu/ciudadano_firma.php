<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

$bandeja[82] = array ("nombre" => "En Elaboraci&oacute;n", "descripcion"=>"Documentos que se est&aacute;n elaborando.");
$bandeja[83] = array ("nombre" => "Recibidos", "descripcion"=>"Documentos recibidos por el ciudadano desde cualquier Instituci&oacute;n P&uacute;blica.");
$bandeja[84] = array ("nombre" => "Eliminados", "descripcion"=>"Documentos eliminados.");
$bandeja[85] = array ("nombre" => "No Enviados", "descripcion"=>"Documentos que no se pudieron firmar electr&oacute;nicamente.");
$bandeja[86] = array ("nombre" => "Enviados", "descripcion"=>"Documentos enviados por el ciudadano a cualquier Instituci&oacute;n P&uacute;blica.");
//$bandeja[87] = array ("nombre" => "Archivados", "descripcion"=>"Documentos archivados.");

$usr = $_SESSION["usua_codi"];
$sql = "select 82 as carpeta, count(radi_nume_radi) as contador from radicado where radi_usua_actu=$usr and esta_codi=1
        UNION ALL -- Recibidos
        select 83, count(radi_nume_radi) from radicado
        where radi_usua_dest = '-$usr-' and radi_nume_radi::text like '%1' and esta_codi in (2,6)
        UNION ALL -- Eliminados
        select 84 as carpeta, count(radi_nume_radi) as contador from radicado where radi_usua_actu=$usr and esta_codi=7
        UNION ALL -- No enviados
        select 85 as carpeta, count(radi_nume_radi) as contador from radicado where radi_usua_actu=$usr and esta_codi=3
        UNION ALL -- Enviados
        select 86, count(radi_nume_radi) from radicado
            where (radi_usua_rem like '%-$usr-%' and radi_nume_radi::text like '%1' and esta_codi in (0,2))
                or (radi_usua_rem = '-$usr-' and radi_nume_radi::text like '%0' and esta_codi in (6))";
/*        UNION ALL -- Archivados
        select 87 as carpeta, count(radi_nume_radi) as contador from radicado where radi_usua_actu=$usr and esta_codi=0";/* */

//echo $sql;
        $rs = $db->query($sql);


?>

<tr>
    <td class="menu_titulo">Bandejas</td>
</tr>
<?
    while ($rs && !$rs->EOF) {
        $carp_codi = $rs->fields["CARPETA"];
        $num_reg = $rs->fields["CONTADOR"];
        $nombre = $bandeja[$carp_codi]["nombre"];
        $descripcion = $bandeja[$carp_codi]["descripcion"];
        echo "<tr " . atributos_tr(++$num) .">
                <td>&nbsp;&nbsp;
                    <a onclick='cambioMenu($num);' class='menu_princ' target='mainFrame' title='$descripcion' href='cuerpo.php?nomcarpeta=$nombre&carpeta=$carp_codi'>
                        $nombre <spam id='spam_carpeta_$carp_codi'>($num_reg)</spam>
                    </a>
                </td>
              </tr>";
        $rs->MoveNext();
    }
    ?>

<tr>
    <td class="menu_titulo">Administraci&oacute;n</td>
</tr>
<tr <?=atributos_tr(++$num)?>>
    <td>&nbsp;&nbsp;
        <a target='mainFrame' class="menu_princ" onclick="cambioMenu(<?=$num?>);"
           href="Administracion/usuarios/cambiar_password.php">Cambiar Contrase&ntilde;a</a>
    </td>
</tr>

<? if($_SESSION["inst_codi"] != 1) { ?>
    <tr <?=atributos_tr(++$num)?>>
        <td>&nbsp;&nbsp;
            <a target='mainFrame' class="menu_princ" onclick="cambioMenu(<?=$num?>);"
             href="Administracion/usuarios/adm_ciudadano.php">Editar Datos Personales</a>
        </td>
    </tr>
<? } ?>