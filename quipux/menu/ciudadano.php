<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/
?>

<tr>
    <td class="menu_titulo">Bandejas</td>
</tr>
<?php
$carp_codi = 80;
$nombre = "Documentos Enviados";
$descripcion = "Documentos enviados por el ciudadano a cualquier Instituci&oacute;n P&uacute;blica";
$isql = "select count(*) as CONTADOR from radicado
        where radi_usua_rem like '%-".$_SESSION["usua_codi"]."-%' and radi_nume_radi::text like '%1' and esta_codi in (0,2)";
$rs = $db->query($isql);
$num_reg = $rs->fields["CONTADOR"];
?>
<tr <?=atributos_tr(++$num)?>>
    <td>&nbsp;&nbsp;
        <a onclick="cambioMenu(<?=$num?>);" class="menu_princ" target="mainFrame" title="<?=$descripcion?>"
            href='cuerpo.php?<?="nomcarpeta=$nombre&carpeta=$carp_codi&adodb_next_page=1"?>' >
            <?="$nombre <spam id='spam_carpeta_$carp_codi'>($num_reg)</spam>"?>
        </a>
    </td>
</tr>

<?php
$carp_codi = 81;
$nombre = "Documentos Recibidos";
$descripcion = "Documentos recibidos por el ciudadano desde cualquier Instituci&oacute;n P&uacute;blica";
$isql = "select count(*) as CONTADOR from radicado
        where radi_usua_dest||coalesce(radi_cca,'') like '%-".$_SESSION["usua_codi"]."-%' and radi_nume_radi::text like '%1' and esta_codi=6";
$rs = $db->query($isql);
$num_reg = $rs->fields["CONTADOR"];
?>
<tr <?=atributos_tr(++$num)?>>
    <td>&nbsp;&nbsp;
        <a onclick="cambioMenu(<?=$num?>);" class="menu_princ" target="mainFrame" title="<?=$descripcion?>"
            href='cuerpo.php?<?="nomcarpeta=$nombre&carpeta=$carp_codi&adodb_next_page=1"?>' >
            <?="$nombre <spam id='spam_carpeta_$carp_codi'>($num_reg)</spam>"?>
        </a>
    </td>
</tr>

<tr>
    <td class="menu_titulo">Administraci&oacute;n</td>
</tr>
<tr <?=atributos_tr(++$num)?>>
    <td>&nbsp;&nbsp;
        <a target='mainFrame' class="menu_princ" onclick="cambioMenu(<?=$num?>);"
            href="Administracion/usuarios/cambiar_password.php">Cambiar Contrase&ntilde;a</a>
    </td>
</tr>
<tr <?=atributos_tr(++$num)?>>
    <td>&nbsp;&nbsp;
        <a target='mainFrame' class="menu_princ" onclick="cambioMenu(<?=$num?>);"
            href="Administracion/usuarios/adm_ciudadano.php">Editar Datos Personales</a>
    </td>
</tr>

<!--tr <?=atributos_tr(++$num)?>>
    <td>&nbsp;&nbsp;
        <a target='mainFrame' class="menu_princ" onclick="cambioMenu(<?=$num?>);"
            href="Administracion/usuarios/adm_solicitud.php">Generar/Firmar Documentos</a>
    </td>
</tr-->