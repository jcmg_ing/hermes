<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/
?>

<tr>
    <td class="menu_titulo">Administraci&oacute;n</td>
</tr>
<tr <?=atributos_tr(++$num)?>>
    <td>&nbsp;&nbsp;
        <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('Administracion/formAdministracion.php'); cambioMenu(<?=$num?>);"
         href="javascript:void(0);">Administraci&oacute;n</a>
    </td>
</tr>

<?php
if ($_SESSION["depe_codi"]!=0) { //Si no tiene definida el area no puede realizar acciones
    if ($_SESSION["usua_perm_trd"]==1) {
?>
        <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('tipo_documental/menu_trd.php'); cambioMenu(<?=$num?>);"
                href="javascript:void(0);"><?=$descTRDpl?></a>
            </td>
        </tr>
<?php
    }
    if($_SESSION["usua_admin_archivo"]==1 or $_SESSION["usua_perm_archivo"]==1) {
?>
        <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('archivo/menu_archivo.php'); cambioMenu(<?=$num?>);"
                href="javascript:void(0);">Archivo F&iacute;sico</a>
            </td>
        </tr>
<?
    }
?>

    <tr>
        <td class="menu_titulo">Otros</td>
    </tr>
    <tr <?=atributos_tr(++$num)?>>
        <td>&nbsp;&nbsp;
            <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('busqueda/busqueda.php'); cambioMenu(<?=$num?>);"
            href="javascript:void(0);">B&uacute;squeda Avanzada</a>
        </td>
    </tr>
    <!--tr <?=atributos_tr(++$num)?>>
        <td>&nbsp;&nbsp;
            <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('busqueda/busqueda_tramites.php'); cambioMenu(<?=$num?>);"
               href="javascript:void(0);">Seguimiento de documentos</a>
        </td>
    </tr-->
    <tr <?=atributos_tr(++$num)?>>
        <td>&nbsp;&nbsp;
            <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('tipo_documental/consultar_trd.php'); cambioMenu(<?=$num?>);"
            href="javascript:void(0);"><?=$descTRDpl?></a>
        </td>
    </tr>
<?php
    if($_SESSION["usua_perm_impresion"] == 1) {
    $isql="select count(radi_nume_radi) as CONTADOR from radicado r left outer join usuarios u
           on r.radi_usua_actu=u.usua_codi
           where r.esta_codi=5 and u.depe_codi=".$_SESSION["depe_codi"];
        $rs=$db->conn->Execute($isql);
        $num_reg = $rs->fields["CONTADOR"];
    $nombre = "Por Imprimir";

    $parametrosFuncion = "cuerpo.php?&adodb_next_page=1&nomcarpeta=$nombre&carpeta=99&adodb_next_page=1";
?>
    <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
            <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('<?=$parametrosFuncion?>'); cambioMenu(<?=$num?>);"
               title="Documentos para Imprimir" href="javascript:void(0);">
               <?="$nombre <spam id='spam_carpeta_99'>($num_reg)</spam>"?>
            </a>
            </td>
        </tr>
<?
    }

    if($_SESSION["usua_perm_estadistica"]==1) {
?>
        <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('reportes_new/reportes.php'); cambioMenu(<?=$num?>);"
                   href="javascript:void(0);">Reportes</a>
            </td>
        </tr> 
    <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('reportes_nuevos/index.php'); cambioMenu(<?=$num?>);"
                   href="javascript:void(0);">Reportes Nuevos</a>
            </td>
        </tr>   
	<tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('graficas_quipux/prueba.php'); cambioMenu(<?=$num?>);"
                   href="javascript:void(0);">Graficas</a>
            </td>
        </tr>       
<? 
        }
    }
?>
