
<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/
//$usua_codi = $_SESSION["usua_codi"];
?>

<tr>
    <td class="menu_titulo">Bandejas</td>
</tr>
<?php
    // Traemos los contadores de cada carpeta
    $usuario = $_SESSION["usua_codi"];
    $isql = "select count(*) as \"contador\", esta_codi from radicado where esta_codi in (0,1,3,7) and radi_usua_actu=$usuario group by esta_codi
             UNION ALL --Recibidos
             select count(*), 2 from radicado where esta_codi=2 and radi_usua_actu=$usuario and radi_nume_radi not in (select radi_nume_radi from tarea where estado=1 and usua_codi_ori=$usuario) group by 2
             UNION ALL --Enviados
             select count(*), 6 from radicado where esta_codi=6 and radi_nume_radi=radi_nume_temp and radi_usua_actu=$usuario
             UNION ALL -- Reasignados
             select count(radi_nume_radi), 12 from hist_eventos where usua_codi_ori=$usuario and sgd_ttr_codigo=9
             UNION ALL -- Informados
             select count(*), 13 from informados where usua_codi=$usuario
             UNION ALL -- Tareas Recibidas
             select count(1), 15 from tarea where estado=1 and $usuario in (usua_codi_dest)
             UNION ALL -- Tareas Enviadas
             select count(1), 16 from tarea where $usuario in (usua_codi_ori)";//--(usua_codi_ori=$usuario or usua_codi_dest=$usuario)";

    //Bandeja compartida
    if($_SESSION["usua_codi_jefe"] != 0)
        $isql .= " UNION ALL select count(1), 14 from radicado where esta_codi=2 and radi_usua_actu=".$_SESSION["usua_codi_jefe"];
    //echo $isql;
    $rs = $db->query($isql);
    $contador = array();    
    unset ($contador);
    while(!$rs->EOF) {
        if ($rs->fields["ESTA_CODI"]==12)
            $est_reasignados=$rs->fields["CONTADOR"];
        if ($rs->fields["ESTA_CODI"]==13)
            $est_informados=$rs->fields["CONTADOR"];        
        $contador[$rs->fields["ESTA_CODI"]] = $rs->fields["CONTADOR"];
        $rs->MoveNext();
    }
    //NO LEIDOS
    unset($leidos);
    $leidos = array();
    $sqlL = "
    select count(*) as \"leidos\", esta_codi from radicado where esta_codi in (0,1,3,7) and radi_leido = 0 and radi_usua_actu=$usuario group by esta_codi    
    UNION ALL --Recibidos 
    select count(*) as \"leidos\", 2 as esta_codi from radicado where esta_codi=2 and radi_usua_actu=$usuario and radi_leido = 0 and radi_nume_radi not in (select radi_nume_radi from tarea where estado=1 and usua_codi_ori=$usuario) group by 2 
    UNION ALL --Enviados 
    select count(*) \"leidos\", 6 as esta_codi from radicado where esta_codi=6 and radi_nume_radi=radi_nume_temp and radi_leido = 0 and radi_usua_actu=$usuario
    UNION ALL -- Reasignados
    select count(*) as \"leidos\",12 as esta_codi from 
            (select radi_nume_radi from hist_eventos where usua_codi_ori=$usuario and sgd_ttr_codigo=9) as h2
            left outer join radicado as b on h2.radi_nume_radi=b.radi_nume_radi
            where b.radi_leido = 0
    UNION ALL -- Informados
    select count(*) as \"leidos\", 13 as esta_codi from informados where usua_codi=$usuario and info_leido = 0
    UNION ALL -- Tareas Recibidas
    select -- Tareas Recibidas 
    count(1) as leidos, 15 as esta_codi from ( select b.radi_nume_radi from (select * from tarea where (usua_codi_dest=$usuario) and estado=1) 
    as t left outer join tarea_hist_eventos th on th.tarea_hist_codi=t.comentario_inicio left outer join radicado b on t.radi_nume_radi=b.radi_nume_radi where 1=1 and b.radi_leido=0 ) as a 
    UNION ALL -- Tareas Enviadas
    select -- Tareas Enviadas 
    count(1) as leidos, 16 as esta_codi from ( select b.radi_nume_radi from (select * from tarea where (usua_codi_ori=$usuario) and estado=1) as t left outer join tarea_hist_eventos th on th.tarea_hist_codi=t.comentario_inicio left outer join radicado b on t.radi_nume_radi=b.radi_nume_radi where 1=1 and b.radi_leido=0 ) as a ";
    //echo $sqlL;

    if (!$version_light) {
        $rsL = $db->query($sqlL);
        while(!$rsL->EOF){
            //echo $rsL->fields['ESTADO'];
            $leidos[$rsL->fields['ESTA_CODI']] = $rsL->fields["LEIDOS"];
            $rsL->MoveNext();
        }
    }
      
    $isql ="select * from carpeta order by carp_orden asc";
    $rs = $db->query($isql);
    while(!$rs->EOF)
    {
        $carp_codi = trim($rs->fields["CARP_CODI"]);
        $nombre = trim($rs->fields["CARP_NOMBRE"]);
        $descripcion = trim($rs->fields["CARP_DESCRIPCION"]);
        $parametrosFuncion = "cuerpo.php?nomcarpeta=$nombre&carpeta=$carp_codi&adodb_next_page=1";
        switch ($carp_codi) {
            case 1: //Borradores
                $estado = 1;
                break;
            case 2: //Recibidos
                $estado = 2;
                break;
            case 6:	// Eliminados
                $estado = 7;
                break;
            case 7:	// No Enviados
                $estado = 3;
                break;
            case 8: //Enviados y Reasignados
                $estado = 6;
                break;
            case 10:	// Archivados
                $estado = 0;
                break;
            case 12:	// Reasignados
                $estado = 12;
                $parametrosFuncion .= "&est_reasignados=$est_reasignados";
                break;
            case 13:	// Informados
                $estado = 13;
                $parametrosFuncion .= "&est_reasignados=$est_informados";
                break;
            case 14:	// Bandeja Compartida
                $estado = 14;
                break;
            case 15:	// Bandeja Tareas Recibidas o Asignadas al Usuario Actual (Logueado)
                $estado = 15;
                break;
            case 16:	// Bandeja Tareas Enviadas o Asignadas a otros Usuarios
                $estado = 16;
                break;
            default:
                $estado = 1;
                break;
        }
        if (!isset($contador[$estado])) $contador[$estado] = 0;

        if($carp_codi != 14 or ($carp_codi == 14 and $_SESSION["usua_codi_jefe"] != 0)) {
            $numLeidos = (isset ($leidos[$estado]))? 0+$leidos[$estado] : 0;
            $parametrosFuncion=$parametrosFuncion."&noLeidos=$numLeidos";
?>
            <tr <?=atributos_tr(++$num)?>>
                <td>&nbsp;&nbsp;
                    <a onclick="llamaCuerpo('<?=$parametrosFuncion?>'); cambioMenu(<?=$num?>,<?=$numLeidos?>);" class="menu_princ"
                        target="mainFrame" title="<?=str_replace("*usuario*", $_SESSION["usua_nomb"], $descripcion)?>" href="javascript:void(0);">
                        <?php 
                        
                        if ($numLeidos!=0)
                            echo "$nombre <spam id='spam_carpeta_$carp_codi'>(".$numLeidos."/".$contador[$estado].") </spam>";
                        else
                            echo "$nombre <spam id='spam_carpeta_$carp_codi'>(".$contador[$estado].") </spam>";
                        ?>
                    </a>
                    <input type="hidden" name="<?=$nombre?>" id="<?=$nombre?>" value="<?=$num?>">
                </td>
            </tr>
        <?php
        }
	$rs->MoveNext();                
   }
?>