<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/

//TOTAL ALERTAS
$isqlTotal = "select count(*) as treasignados from (select radi_nume_radi from radicado
            where esta_codi=2 and radi_inst_actu = ".$_SESSION["inst_codi"]."
            and radi_usua_actu=".$_SESSION["usua_codi"]." and radi_nume_radi not in
            (select radi_nume_radi from tarea
            where estado=1 and usua_codi_ori=".$_SESSION["usua_codi"].")) as r left outer join
            (select radi_nume_radi, max(hist_codi) as hist_codi from hist_eventos
            where sgd_ttr_codigo = 9 and usua_codi_dest = ".$_SESSION["usua_codi"]." group by 1) as h on
            r.radi_nume_radi=h.radi_nume_radi left outer join hist_eventos hd on hd.hist_codi=h.hist_codi
            where h.radi_nume_radi is not null";
$rstotal = $db->query($isqlTotal);
$totalReasignados=$rstotal->fields['TREASIGNADOS'];
//Las Alertas se agregaron en este php, para que ejecute una sola vez
$isqlReasignados ="select r.radi_nume_text,r.radi_nume_radi,r.radi_leido , hd.hist_referencia,r.radi_cuentai, now()::date-hd.hist_referencia::date ";
$isqlReasignados.="from (select radi_nume_radi, radi_nume_text,radi_cuentai,radi_leido,radi_fech_radi from radicado where esta_codi=2 and radi_inst_actu = ".$_SESSION["inst_codi"];
$isqlReasignados.=" and radi_usua_actu=".$_SESSION["usua_codi"]." and radi_nume_radi not in (select radi_nume_radi from tarea where estado=1 and usua_codi_ori=".$_SESSION["usua_codi"].")) as r left outer join (select radi_nume_radi, ";
$isqlReasignados.="max(hist_codi) as hist_codi from hist_eventos ";
$isqlReasignados.="where sgd_ttr_codigo = 9 and usua_codi_dest = ".$_SESSION["usua_codi"]." group by 1) as h on ";
$isqlReasignados.=" r.radi_nume_radi=h.radi_nume_radi left outer join hist_eventos hd on hd.hist_codi=h.hist_codi";
$isqlReasignados.=" where h.radi_nume_radi is not null";
//contar numero de documentos
$isqlReasignados.=" order by r.radi_fech_radi limit 20 offset 0";

$rsAsignados = $db->query($isqlReasignados);
$contadorVencido=0;//cuenta los vencidos
$numeroDocumento = array();
$documentos=array();
$i=0;

while($rsAsignados && !$rsAsignados->EOF) {
    $fecha1 = $rsAsignados->fields['HIST_REFERENCIA'];
    if (date('Y-m-d') > $fecha1){//documentos vencidos
        $contadorVencido++;
        $registro[$i]=array($rsAsignados->fields['RADI_NUME_TEXT']);//cargo en el array que luego lo despliego en el modal
        //$documentos=array_push ($numeroDocumento,$registro);
    }
     //esta validacion se puede utilizar para, verificar la fecha de cada documento en que se caduca cuando no
     //se atienden
    if ($fecha1 <= $fechaProxima or $fechaProxima==''){//fecha calculada para vencerse
        $fechaProxima = $fecha1;
    } //fecha calculada por vencerse
    else{
        $fechaProxima=$fechaProxima;
    }
    $i++;
    $rsAsignados->MoveNext();
}

     //$compactada=serialize($registro);//serializo el array para que se envien con la misma estructura
     //$registro=urlencode($compactada);//codifico para que se envie al get
     //Armo el div
     //   if($_GET['nomcarpeta']=='Recibidos' and $_GET['adodb_next_page']!=''){

if ($contadorVencido>0){
?>
    <div id="div_bandeja_alerta" style="display: none;">

<?php
    $diasRestantes=CalculaEntreFechas(date('Y-m-d'),$fechaProxima);
    if ($contadorVencido!='') {
?>
        <br>
        <font color="black">Alerta!: De los Documento(s) reasignados,</font>
        <a href="javascript:verReferencia();">
            <font color="red"><?=$totalReasignados?> se encuentra(n) Vencido(s) </font>
        </a>
        <font color="black"><?php echo "desde la fecha: ".$fechaProxima; ?></font>

<?php
    }
    if ($registro) {//si existe documentos, en esta variable se guarda los documentos por vencer
?>
        <div id="miVentana" style="position: fixed; width: 350px; height: 320px; top: 0; left: 0; font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; border: #333333 3px solid; background-color: #FAFAFA; color: #000000; display: none;">
            <div style="font-weight: bold; text-align: left; color: #FFFFFF; padding: 5px; background-color:#6A819D"><DIV ALIGN=left><font size=1><a href="javascript:cerrarReferencia();"><img src="<?=$ruta_raiz?>/imagenes/close_button.gif" width="15" height="15" border="0" alt=""></a>&nbsp;<?php echo "Primeros (".$contadorVencido.") ";?>Documentos Vencidos</font></DIV>
                <div align="right"></div>
            </div>
<?php
        $contcolumnas =1;
        //empieza a cargar los datos del array (registro) que se cargo en la consulta
        foreach($registro as $key=>$value) {
            foreach ($value as $iKey => $iValue) {
                if ($contadorVencido>10) {
                    if ($contcolumnas % 2)
                        echo "<br><font color='black'>".$iValue."</font>";
                    else
                        echo "<br><font color='black'>".$iValue."</font>";
                    $contcolumnas++;
                } else {
                    echo "<br><font color='black'>".$iValue."</font>";
                }//else
            }//for value
        }//for registro
?>
        </div>
<?php
    }//if(registro)
?>
    </div>
<?php
}//if(contadorVencido)


function CalculaEntreFechas($fechaUno,$fechaDos) {
    $ano1 = substr($fechaUno,0,4);

    $mes1 = substr($fechaUno,5,2);
    $dia1 = substr($fechaUno,8,2);

    $ano2 = substr($fechaDos,0,4);
    $mes2 = substr($fechaDos,5,2);
    $dia2 = substr($fechaDos,8,2);

    //calculo timestam de las dos fechas
    $timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1);
    $timestamp2 = mktime(4,12,0,$mes2,$dia2,$ano2);

    //resto a una fecha la otra
    $segundos_diferencia = $timestamp1 - $timestamp2;
    //echo $segundos_diferencia;

    //convierto segundos en días
    $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

    //obtengo el valor absoulto de los días (quito el posible signo negativo)
    $dias_diferencia = abs($dias_diferencia);

    //quito los decimales a los días de diferencia
    $dias_diferencia = floor($dias_diferencia);
    return $dias_diferencia;
}

?>