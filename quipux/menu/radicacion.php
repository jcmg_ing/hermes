<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/

if($_SESSION["usua_prad_tp2"] == 1 or $_SESSION["usua_perm_digitalizar"] == 1 or $_SESSION["perm_tramitar_docs_ciudadano"] == 1) {
?>
    <tr>
        <td class="menu_titulo">Registro Docs. Externos</td>
    </tr>
<?
    if($_SESSION["usua_prad_tp2"] == 1) {
?>
        <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('radicacion/NEW.php?&ent=2&accion=Nuevo'); cambioMenu(<?=$num?>);"
                   title="Creaci&oacute;n de documentos de Entrada" href="javascript:void(0);">Registrar</a>
    	    </td>
        </tr>
        <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('uploadFiles/cargar_doc_digitalizado.php?imprimir=si');
                   cambioMenu(<?=$num?>);" title="Imprimir Comprobantes" href="javascript:void(0);">Comprobante</a>
            </td>
        </tr>
<?
    }
    if($_SESSION["usua_perm_digitalizar"] == 1) {
?>
        <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('uploadFiles/cargar_doc_digitalizado.php'); cambioMenu(<?=$num?>);"
                   title="Asociar imagen digitalizada del Documento" href="javascript:void(0);">Cargar Doc. Digitalizado</a>
            </td>
        </tr>
<?  }	
    /*if($_SESSION["usua_prad_tp2"] == 1) {
?>
        <tr <?=atributos_tr(++$num)?>>
            <td>&nbsp;&nbsp;
                <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('devolver_documentos.php'); cambioMenu(<?=$num?>);"
                   title="Registrar devoluciones de documentos" href="javascript:void(0);">Devoluci&oacute;n</a>
            </td>
        </tr>
<?  }/**/
//    if($_SESSION["perm_tramitar_docs_ciudadano"] == 1) {
//        $sql = "select count(radi_nume_radi) as contador from radicado where esta_codi=9 and radi_inst_actu=".$_SESSION["inst_codi"];
//        $rs = $db->query($sql);
//        $num_reg = $rs->fields["CONTADOR"];
//        $carp_codi = 90;
//        $nombre = "Documentos Ciudadanos";
//        $descripcion = "Documentos enviados firmados electr&oacute;nicamente por ciudadanos";
//        echo "<tr " . atributos_tr(++$num) .">
//                <td>&nbsp;&nbsp
//                    <a onclick='cambioMenu($num);' class='menu_princ' target='mainFrame' title='$descripcion' href='cuerpo.php?nomcarpeta=$nombre&carpeta=$carp_codi'>
//                        $nombre <spam id='spam_carpeta_$carp_codi'>($num_reg)</spam>
//                    </a>
//                </td>
//              </tr>";
//
//    }
}
?>