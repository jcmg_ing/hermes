<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
*****************************************************************************************/
if($_SESSION["usua_prad_tp1"] == 1) {
?>
<tr <?=atributos_tr(++$num)?>>
    <td>&nbsp;&nbsp;
        <a target='mainFrame' class="menu_princ" onclick="llamaCuerpo('radicacion/NEW.php?&ent=1&accion=Nuevo'); cambioMenu(<?=$num?>);" 
           title="Crear Memos - Oficios" href="javascript:void(0);">NUEVO</a>
    </td>
</tr>
<?  }	?>