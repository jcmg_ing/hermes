<?php

class bd {

    var $servidor;
    var $usuario;
    var $clave;
    var $bd;
    var $puerto;
    var $conexion;    

    public function __construct() {
		include_once 'Session.php';
		$session= new Session();
		$session->init();
        $this->servidor = "127.0.0.1";
        $this->usuario = "postgres";
        $this->clave = "pr0curaduri@";
        $this->bd = "quipux";
        $this->puerto = "5432";
		
    }
	
	
	 
	 
	public function Conectar() {        
        $this->conexion = pg_connect("host=$this->servidor port=$this->puerto
            dbname=$this->bd user=$this->usuario password=$this->clave")
                or die("Error al conectar: " . pg_last_error());        
    }

    private function Desconectar() {
        pg_close($this->conexion);        
    }    
    
    public function Consulta($consulta){
        $this->Conectar();
        pg_query($consulta) or die('Error en consulta: '.  pg_last_error());      
        $this->Desconectar();
    }

    public function cargarObjeto($consulta) {
        $this->Conectar();
        $respuesta = pg_query($consulta) or die('Error en consulta: '.  pg_last_error());
        if (pg_num_rows($respuesta) == 0){
            return null;
        }else if(pg_num_rows($respuesta) == 1) {
            $s = pg_fetch_array($respuesta);
            return $s;
        }
        $this->Desconectar();
    }

    public function cargarObjetos($consulta){
        $this->Conectar();
        $respuesta = pg_query($consulta) or die('Error en consulta: '.  pg_last_error());
        if (pg_num_rows($respuesta) == 0){
            return null;
        }else if(pg_num_rows($respuesta) > 0) {
            while($fila = pg_fetch_array($respuesta)){
                $s[]=$fila;
            }
            return $s;
        }
        $this->Desconectar();
    }
	
	function contar($cons) 
	{
		 $this->Conectar();
		$this->consulta= pg_query($cons);
		return pg_num_rows($this->consulta);
	}
	public function contador($sql){  
		$num=1;
		$mostrar=$this->cargarObjetos($sql);
		if($mostrar!=NULL)
		{
			foreach ($mostrar as $reg) 
			{
				$num=$num+1;
			}
		}	
		return $num;	
	}
	
	public function mayor($sql){  
		$may=0;
		$mostrar=$this->cargarObjetos($sql);
		if($mostrar!=NULL)
		{
			foreach ($mostrar as $reg) 
			{
				$num1=$reg['0'];
				if($num1>$may){
					$may=$num1;
				}
			}
		}	
		return $may;	
	}
	
	//Funcion para la fecha al mostrar
	public function Fsalida($cad2)
	{
		$tres=substr($cad2, 0, 4);		
		$dos=substr($cad2, 5, 2);		
		$uno=substr($cad2, 8, 2);		
		$cad = ($uno."/".$dos."/".$tres);		
		return $cad;	
	}
	
	//Funcion para generar .pdf
	public function doc_pdf($pdfContent)
	{
		$fp = fopen('fichero.pdf', 'w');
		fwrite($fp, $pdfContent);
		fclose($fp);
		$extensiones = array("pdf", "jpeg", "png", "gif", "xlsx");//AQUI COLOCAS LAS EXTENCIONES DE LOS TIPOS DE ARCHIVO QUE  DESEAS QUE SE DESCARGUE
		$f = "fichero.pdf";
	
		$ftmp = explode(".",$f);
		$fExt = strtolower($ftmp[count($ftmp)-1]);
	
		if(!in_array($fExt,$extensiones)){
			die("<b>ERROR!</b> no es posible descargar archivos con la extension $fExt");
		}
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"$f\"\n");
		$fp=fopen("$f", "r");
		fpassthru($fp); 
	}
	
	
	//Funcion para generar .pdf
	public function descargar($nombre)
	{
	
		$extensiones = array("wma","mp3","txt","pdf", "doc", "docx", "png",  "gif","jpeg","jpg");//AQUI COLOCAS LAS EXTENCIONES DE LOS TIPOS DE ARCHIVO QUE  DESEAS QUE SE DESCARGUE
		$f = "$nombre";
	
		$ftmp = explode(".",$f);
		$fExt = strtolower($ftmp[count($ftmp)-1]);
	
		if(!in_array($fExt,$extensiones)){
			die("<b>ERROR!</b> no es posible descargar archivos con la extension $fExt");
		}
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"$f\"\n");
		
		$fp=fopen("$f", "r");
		fpassthru($fp); 
	}
	
	
	//Funcion para  cargar una foto
	public function seleccionar_foto($carpeta,$archivo,$arc_pend)
	{
		//si ya tiene foto y no la cambia se queda
		if($arc_pend!='')
		{
			$foto=$arc_pend;
		}
		
		//si no tiene ni foto ni se carga alguna se guarda una imagen de nuevo
		if(($arc_pend=='') && ($archivo==''))
		{
			$foto="images/nuevo.gif";
			
		}
		
		//si carga alguna foto se copia en la carpeta y se  guarda
		if($archivo!='')
		{
			$foto=$carpeta.$archivo;
			copy($_FILES['foto']['tmp_name'],"$foto");
		}
		
		//si tiene guardado y se carga otra se borra la q estaba y queda la nueva
		if(($archivo!='') && ($arc_pend!='') && ($arc_pend!='images/nuevo.gif'))
			unlink($arc_pend);
		
		return $foto;
	}
	
	
	//Funcion para  cargar un archivo
	public function seleccionar_archivo($carpeta,$archivo,$arc_pend,$boton)
	{
		//si carga algun archivo se copia en la carpeta y se  guarda
		if($archivo!='')
		{
			$arc_cargado=$carpeta.$archivo;
			copy($_FILES['foto']['tmp_name'],"$arc_cargado");
		}
		
		return $arc_cargado;
	}
	
	public function icono_archivo($archivo)
	{
		$arc_ext1 = explode(".",$archivo);
		$arc_ext2 = strtolower($arc_ext1[count($arc_ext1)-1]);
		if($arc_ext2=='pdf')
		{	
			$icono= "../../images/pdf2.jpg";
		}
		if(($arc_ext2=='doc') || ($arc_ext2=='docx'))
		{	
			$icono= "../../images/page_word.png";
		}
		if($arc_ext2=='txt')
		{	
			$icono= "../../images/block.png";
		}
		if(($arc_ext2=='png') || ($arc_ext2=='jpg') || ($arc_ext2=='jpeg') || ($arc_ext2=='gif'))
		{
			$icono= "../../images/image.png";
		}
		if(($arc_ext2=='wma') || ($arc_ext2=='flv') || ($arc_ext2=='mpg') )
		{	
			$icono= "../../images/film.png";
		}
		return $icono;
	}
	
	
	public function extraer($q)
	{
		$sql="select * from e_cvv.usuario where nombre like '%$q%'";
		$result =pg_query($sql) or die("error query." .pg_last_error());
		$a=pg_num_rows($result);
		echo "<ul id='buscador'>";
		if($a==0)
		{
			echo "<li>no se encontro resultados </li>";
		}
		else
		{
			
			while($row=pg_fetch_assoc($result))
			{
				$ced=$row['cedula'];
				$nom=$row['nombre'];
				echo "<li><a href='perfil_mundo.php?ced=$ced'>$nom</a></li>";
			}
			
		}
		echo "</ul>";
	}
	
	
	
	 public function  select($id_select,$tabla,$campo1,$campo2,$id_atributo){
	 /* 1: NOMBRE DEL SELECT
	 	2: NOMBRE DE LA TABLA
		3: NOMBRE DEL PRIMER CAMPO
		4: NOMBRE DEL SEGUNDO CAMPO
		5: VALOR DEL SELECT OBTENIDO DESDE LA ABSE DE DATOS SI ES QUE EXISTE REGISTRO
	 */
	 	$bd=new bd();
	 	echo "<div id='demo' style='width:200px;'>"; 
			echo "<div id='demoIzq'>"; 
				echo "<select name='$id_select' id='$id_select' class='required'>";
						echo "<option selected='selected' value='$id_atributo'>";  
							$sql = "select * from informatica.$tabla  where $campo1='$id_atributo'";		
							$result=$row=$bd->cargarObjeto($sql); 
							if($result)
							{
								echo $row[$campo2];
							}
						echo"</option>"; 
						$sql = "select * from informatica.$tabla  order by $campo1";		
						$result =pg_query($sql) or die("error query." .pg_last_error()); 
						if($result)
						{
							while($raw = pg_fetch_array($result, null, PGSQL_ASSOC))
							{ 
								$valor= $raw[$campo1];
								$valor2= $raw[$campo2];
								echo "<option value=".$valor.">".$valor.", ".$valor2."</option>\n";
							}
						} 
				echo "</select>";
			echo "</div>";
		echo "</div>";
	 }
	 
	 
	public function dibujar_tablas($color, $imagen, $titulo, $texto) {
    $color_fondo = 'left';
    if ($color == "amarillo") $color_fondo = 'center';
    elseif ($color == "azul") $color_fondo = 'right';
    echo '<table class="borde-left-externo" border="0" cellpadding="0">
            <tr>
                <td align="center">
                    <img src="../imagenes/'.$imagen.'" width="200" height="150" alt="Imagen">
                </td>
            </tr>
            <tr>
                <td>
                    <div class="borde-'.$color_fondo.'">
                    <table border="0">
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <font color="white" size="4">
                                <div style="text-align:left">
                                <span style="text-decoration:underline;">'.$titulo.'</span>
                                </div>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" class="tabla_'.$color_fondo.'" cellpadding="0">
                                    <tr><td>
                                        <div style="text-align:justify">
                                        <font color="white" size="2">'.$texto.'</font></div>
                                    </td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </div>
                </td>
            </tr>
        </table>';
}
	function calculaFecha($modo,$valor,$fecha_inicio=false){
	 
	   if($fecha_inicio!=false) {
			  $fecha_base = strtotime($fecha_inicio);
	   }else {
			  $time=time();
			  $fecha_actual=date("Y-m-d",$time);
			  $fecha_base=strtotime($fecha_actual);
	   }
	 
	   $calculo = strtotime("$valor $modo","$fecha_base");
	 
	   return date("Y-m-d", $calculo);
	 
	}
		
}
?>
