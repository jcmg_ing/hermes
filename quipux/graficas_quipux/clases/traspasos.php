<?php
include_once 'bd.php';

class Traspasos extends bd
{
	
	public function get_id_traspaso(){
		return $this->id_traspaso;
	}
	
	public function get_carpeta_traspaso(){
		return $this->carpeta_traspaso;
	}
	
	public function get_area_traspaso(){
		return $this->area_traspaso;
	}
	
	public function get_emisor_traspaso(){
		return $this->emisor_traspaso;
	}
	
	public function get_receptor_traspaso(){
		return $this->receptor_traspaso;
	}
	
	public function get_fecha_traspaso(){
		return $this->fecha_traspaso;
	}
	
	public function get_motivo_traspaso(){
		return $this->motivo_traspaso;
	}
	
	public function get_observacion_traspaso(){
		return $this->observacion_traspaso;
	}
	
	public function get_status(){
		return $this->status;
	}
	
	
	public function buscar_traspaso($id_traspaso){
		$sql="select * from informatica.traspasos where id_traspaso='$id_traspaso'"; 
		$row=$this->cargarObjeto($sql);
		$this->id_traspaso=$row['id_traspaso'];
		$this->carpeta_traspaso=$row['carpeta_traspaso'];
		$this->area_traspaso=$row['area_traspaso'];
		$this->emisor_traspaso=$row['emisor_traspaso'];
		$this->receptor_traspaso=$row['receptor_traspaso']; 
		$this->fecha_traspaso=$row['fecha_traspaso'];
		$this->motivo_traspaso=$row['motivo_traspaso']; 
		$this->observacion_traspaso=$row['observacion_traspaso'];
		$this->status=$row['status']; 
		
	}
	  
	public function Guardar_Traspasos($id_arcvar,$ced_emisor,$ced_receptor,$fecha_tras,$motivo_tras,$obser_tras){
		$sql="select * from informatica.personal where cedula='$ced_emisor'"; 
		$row=$this->cargarObjeto($sql);
		$area_tras=$row['area'];
		
		$num=$this->contador("select * from informatica.traspasos");
		$registrar="INSERT INTO informatica.traspasos(id_traspaso, carpeta_traspaso, area_traspaso, emisor_traspaso, receptor_traspaso, fecha_traspaso, motivo_traspaso, observacion_traspaso, status) VALUES ('$num', '$id_arcvar', '$area_tras', '$ced_emisor', '$ced_receptor', '$fecha_tras', '$motivo_tras', '$obser_tras', '0')";
		$this->Consulta($registrar);  
		 
		 
	}
	
	
	public function Guardar_Traspasos2($id_traspaso,$id_arcvar,$ced_emisor,$ced_receptor,$fecha_tras,$motivo_tras,$obser_tras){
		$sql="select * from informatica.personal where cedula='$ced_emisor'"; 
		$row=$this->cargarObjeto($sql);
		$area_tras=$row['area'];
		
		$modificar="UPDATE informatica.traspasos SET  status='2' WHERE id_traspaso='$id_traspaso'";
		$this->Consulta($modificar);  
		 
		
		
		$num=$this->contador("select * from informatica.traspasos");
		$registrar="INSERT INTO informatica.traspasos(id_traspaso, carpeta_traspaso, area_traspaso, emisor_traspaso, receptor_traspaso, fecha_traspaso, motivo_traspaso, observacion_traspaso, status) VALUES ('$num', '$id_arcvar', '$area_tras', '$ced_emisor', '$ced_receptor', '$fecha_tras', '$motivo_tras', '$obser_tras', '0')";
		$this->Consulta($registrar);  
		 
		 
	}
	
	
}


?>