<?php
include_once 'bd.php';

class Archivo extends bd
{
	 public function get_cedula(){
		return $this->cedula;
	}
	 public function get_nombre(){
		return $this->nombres;
	}
	public function get_apellido(){
		return $this->apellidos;
	}
	public function get_dependencia(){
		return $this->dependencia;
	}
	
	public function get_num_carpeta(){
		return $this->num_carpeta;
	}
	
	public function buscar_beneficiaro($cedula){
		$sql="select * from archivo.beneficiarios where cedula='$cedula'"; 
		$row=$this->cargarObjeto($sql);
		$this->cedula=$row['cedula'];
		$this->nombres=$row['nombres'];
		$this->apellidos=$row['apellidos'];
		$this->dependencia=$row['dependencia'];
		$this->num_carpeta=$row['num_carpeta'];
	}
	
	public function Contar($buscar) {
		 $consulta=pg_query($buscar) or die('Error en consulta: '.  pg_last_error());       
		$num =pg_num_rows($consulta);  
		return $num;
				
    }
	
	public function Guardar_beneficiario($cedula,$apellido,$nombre,$persona){
		$sql="select * from archivo.beneficiarios  where dependencia='$persona'";  
		$num=1;
		$mostrar=$this->cargarObjetos($sql);
		if($mostrar!=NULL)
		{
			foreach ($mostrar as $reg) 
			{
				
				$num=$num+1;
			}
		} 
		$num=$persona[0].$persona[1].$persona[2].$num;
		$fecha=date("d/m/Y");
		 $modificar="update archivo.beneficiarios set nombres='$nombre', apellidos='$apellido', dependencia='$persona'  where cedula='$cedula'"; 
		 $this->Consulta($modificar);
		 $registrar="INSERT INTO archivo.beneficiarios( cedula, apellidos, nombres, dependencia, num_carpeta, status, fecha_registro) VALUES ('$cedula', '$apellido', '$nombre', '$persona', '$num', '1','$fecha')";
		 $this->Consulta($registrar); 
	 }  
	 
	 
	 public function get_tipobeneficiario(){
		return $this->tipo_beneficiario;
	}
	public function get_fecha_ndictamen(){
		return $this->fecha_ndictamen;
	}
	public function get_gaceta_decreto(){
		return $this->gaceta_decreto;
	}
	public function get_estante(){
		return $this->estante;
	}
	public function get_lado(){
		return $this->lado;
	}
	public function get_divicion(){
		return $this->divicion;
	}
	public function get_observaciones(){
		return $this->observaciones;
	}
	 
	 public function buscar_transferencia($cedula,$tipo){ 
		$sql="select * from archivo.detalle_beneficiario where cedula='$cedula' and tipo_beneficiario='$tipo'"; 
		$row=$this->cargarObjeto($sql);
		$this->tipo_beneficiario=$row['tipo_beneficiario'];
		$this->fecha_ndictamen=$row['fecha_ndictamen'];
		$this->gaceta_decreto=$row['gaceta_decreto'];
		$this->estante=$row['estante'];
		$this->lado=$row['lado'];
		$this->divicion=$row['divicion'];
		$this->observaciones=$row['observaciones'];
	}
	
	
	public function Guardar_transferencia($cedula,$tipo_beneficiario,$fecha_ndictamen,$gaceta_decreto,$estante,$lado,$divicion,$observaciones){ 
		$sql="select * from archivo.detalle_beneficiario where cedula='$cedula'  "; 
		$cant=$this->mayor($sql)+1;
		$fecha=date("d/m/Y");
		 $registrar="INSERT INTO archivo.detalle_beneficiario(id, cedula, tipo_beneficiario, fecha_ndictamen, gaceta_decreto, 
            estante, lado, divicion, observaciones, fecha_registro_detalleb) VALUES ('$cant', '$cedula', '$tipo_beneficiario', '$fecha_ndictamen', '$gaceta_decreto', '$estante', '$lado', '$divicion', '$observaciones','$fecha')";
		 $this->Consulta($registrar); 
		}
		
		public function Modificar_transferencia($cedula,$tipo_beneficiario,$fecha_ndictamen,$gaceta_decreto,$estante,$lado,$divicion,$observaciones){  
				$modificar="update archivo.detalle_beneficiario set fecha_ndictamen='$fecha_ndictamen', gaceta_decreto='$gaceta_decreto', estante='$estante', lado='$lado', divicion='$divicion', observaciones='$observaciones'  where cedula='$cedula' and tipo_beneficiario='$tipo_beneficiario'"; 
				 $this->Consulta($modificar);
	 
	 }  
	 
	  public function get_id_arcvar(){
		return $this->id_arcvar;
	}
	 public function get_titulo_arcvar(){
		return $this->titulo_arcvar;
	}
	public function get_descrip_arcvar(){
		return $this->descrip_arcvar;
	}
	public function get_tipo_arcvar(){
		return $this->tipo_arcvar;
	}
	public function get_estante_arcvar(){
		return $this->estante_arcvar;
	}
	public function get_lado_arcvar(){
		return $this->lado_arcvar;
	}
	public function get_division_arcvar(){
		return $this->division_arcvar;
	}
	public function get_obser_arcvar(){
		return $this->obser_arcvar;
	}
	
	 public function buscar_carpeta_varios($id_arcvar){ 
		$sql="select * from archivo.archivos_varios where id_arcvar='$id_arcvar'"; 
		$row=$this->cargarObjeto($sql);
		$this->id_arcvar=$row['id_arcvar'];
		$this->titulo_arcvar=$row['titulo_arcvar']; 
		$this->tipo_arcvar=$row['tipo_arcvar'];  
		$this->descrip_arcvar=$row['descrip_arcvar']; 
		$this->estante_arcvar=$row['estante_arcvar'];
		$this->lado_arcvar=$row['lado_arcvar']; 
		$this->division_arcvar=$row['division_arcvar']; 
		$this->obser_arcvar=$row['obser_arcvar'];  
		
	}
	
	public function Guardar_carpeta_varios($id_arcvar, $titulo_arcvar, $descrip_arcvar, $tipo_arcvar, $estante_arcvar, $lado_arcvar, $division_arcvar, $obser_arcvar){ 
		$sql="select * from archivo.archivos_varios where id_arcvar='$id_arcvar'";   
		$mostrar=$this->cargarObjeto($sql);
		if($mostrar!=NULL)
		{
			  
			 $modificar="update archivo.archivos_varios set titulo_arcvar='$titulo_arcvar', descrip_arcvar='$descrip_arcvar', tipo_arcvar='$tipo_arcvar', estante_arcvar='$estante_arcvar', lado_arcvar='$lado_arcvar', division_arcvar='$division_arcvar', obser_arcvar='$obser_arcvar' where id_arcvar='$id_arcvar'";
			 $this->Consulta($modificar);
		}
		else{
			$sql="select * from archivo.archivos_varios ";  
			$num=1;
			$mostrar=$this->cargarObjetos($sql);
			if($mostrar!=NULL)
			{
				foreach ($mostrar as $reg) 
				{
					$num=$num+1;
				}
			}		 
			$fecha=date("d/m/Y");	 
			$registrar="INSERT INTO archivo.archivos_varios(id_arcvar, titulo_arcvar, descrip_arcvar, tipo_arcvar, estante_arcvar, 
					lado_arcvar, division_arcvar, obser_arcvar,fecha_registro_arcvar) VALUES ('$num', '$titulo_arcvar', '$descrip_arcvar', '$tipo_arcvar', '$estante_arcvar', '$lado_arcvar', '$division_arcvar', '$obser_arcvar','$fecha')";
			$this->Consulta($registrar); 
		 }
		}
	 
	 public function get_cedula_otor(){
		return $this->cedula_otor;
	}
	 public function get_cedula_bene(){
		return $this->cedula_bene;
	}
	 public function get_nombres_bene(){
		return $this->nombres_bene;
	}
	 public function get_apellidos_bene(){
		return $this->apellidos_bene;
	}
	 
	 public function get_parentesco_bene(){
		return $this->parentesco_bene;
	} 
	
	public function buscar_BeneAdjun($cedula_otor,$cedula_bene){ 
		$sql="select * from archivo.adjuntar_beneficiario where cedula_otor='$cedula_otor' and cedula_bene='$cedula_bene'"; 
		$row=$this->cargarObjeto($sql);
		$this->cedula_otor=$row['cedula_otor'];
		$this->cedula_bene=$row['cedula_bene'];
		$this->nombres_bene=$row['nombres_bene'];
		$this->apellidos_bene=$row['apellidos_bene']; 
		$this->parentesco_bene=$row['parentesco_bene']; 
	}
	
	public function Guardar_BeneAdjun($cedula_otor,$cedula_bene,$nombre_bene,$apellido_bene,$parentesco_bene){
	$fecha=date("d/m/Y");
		$registrar="INSERT INTO archivo.adjuntar_beneficiario(cedula_otor, cedula_bene, nombres_bene, apellidos_bene,parentesco_bene,fecha_registro_bene)  VALUES ('$cedula_otor', '$cedula_bene', '$nombre_bene', '$apellido_bene', '$parentesco_bene','$fecha')";
		$this->Consulta($registrar);  
	}
	
	public function Modificar_BeneAdjun($cedula_otor,$cedula_bene,$nombre_bene,$apellido_bene,$parentesco_bene){
		$modificar="update archivo.adjuntar_beneficiario set  nombres_bene='$nombre_bene', apellidos_bene='$apellido_bene' ,parentesco_bene='$parentesco_bene'  where cedula_otor='$cedula_otor' and cedula_bene='$cedula_bene'";
		$this->Consulta($modificar);  
	}
	
	public function Deshabilitar_Beneficiario($ced,$estante,$lado,$divicion,$observaciones){
		$fecha=date("d/m/Y");
		$insertar="INSERT INTO archivo.beneficiario_deshabilitado(cedula, estante_desha, lado_desha, division_desha, observaciones_desha,fecha_registro_desha) VALUES ('$ced', '$estante', '$lado', '$divicion', '$observaciones','$fecha')";
		$this->Consulta($insertar);  
		
		$modificar="update archivo.beneficiarios set status='0'   where cedula='$ced'";
		$this->Consulta($modificar);
	}
	
	
	
	public function get_area_prestamo(){
		return $this->area_prestamo;
	}  
	public function get_persona_prestamo(){
		return $this->persona_prestamo;
	} 
	public function get_fecha_prestamo(){
		return $this->fecha_prestamo;
	}
	
	public function get_fecha_entrega_prestamo(){
		return $this->fecha_entrega_prestamo;
	}
	public function get_motivo_prestamo(){
		return $this->motivo_prestamo;
	}
	public function get_observacion_prestamo_prestamo(){
		return $this->observacion_prestamo;
	}
	
	public function get_id_prestamo(){
		return $this->id_prestamo;
	}
	 
	public function buscar_prestamo($id){ 
		$sql="select * from archivo.prestamos where id_prestamo='$id'"; 
		$row=$this->cargarObjeto($sql);
		$this->id_prestamo=$row['id_prestamo'];
		$this->carpeta_prestamo=$row['carpeta_prestamo'];
		$this->persona_prestamo=$row['persona'];
		$this->area_prestamo=$row['area_prestamo'];
		$this->fecha_prestamo=$row['fecha_prestamo'];
		$this->fecha_entrega_prestamo=$row['fechaentrega_prestamo'];
		$this->motivo_prestamo=$row['motivo_prestamo'];
		$this->observacion_prestamo=$row['observacion_prestamo'];
		$this->status=$row['status'];
	}
	
	
	public function contador($sql){  
		$num=1;
		$mostrar=$this->cargarObjetos($sql);
		if($mostrar!=NULL)
		{
			foreach ($mostrar as $reg) 
			{
				$num=$num+1;
			}
		}	
		return $num;	
	}
	public function Guardar_Prestamo($id_arcvar,$nombre_pres,$fecha_pres,$entrega_pres,$motivo_pres,$obser_pres){
		$sql="select * from informatica.personal where cedula='$nombre_pres'"; 
		$row=$this->cargarObjeto($sql);
		$area_pres=$row['area'];
		
		$num=$this->contador("select * from archivo.prestamos");
		$registrar="INSERT INTO archivo.prestamos(id_prestamo, carpeta_prestamo, area_prestamo, persona, fecha_prestamo, fechaentrega_prestamo, motivo_prestamo, observacion_prestamo, status) VALUES ('$num', '$id_arcvar', '$area_pres', '$nombre_pres', '$fecha_pres', '$entrega_pres', '$motivo_pres', '$obser_pres', '1')";
		$this->Consulta($registrar);  
		 
		 
	}
	
	public function Devolver_Prestamo($id){ 
		$devolver="update archivo.prestamos set status='0' where id_prestamo='$id'";
		$this->Consulta($devolver);  
		 
	}
	
	
	public function Eliminar_carpeta($cedula){ 
		$eliminar="delete from archivo.beneficiario_deshabilitado where cedula='$cedula'";
		$this->Consulta($eliminar);  
		
		$eliminar="delete from archivo.adjuntar_beneficiario where cedula_otor='$cedula'";
		$this->Consulta($eliminar);  
		
		$eliminar="delete from archivo.detalle_beneficiario where cedula='$cedula'";
		$this->Consulta($eliminar);  
		
		$eliminar="delete from archivo.beneficiarios where cedula='$cedula'";
		$this->Consulta($eliminar);  
		 
	}
	
	
}


?>