﻿/**
 * Autor: Lucas Forchino
 * Web: http://www.tutorialjquery.com
 *
 */
$(document).ready(function(){ //cuando el html fue cargado iniciar	
	
	 
	//añado opcion para abrir formulario de fotos de los usuarios
	 $('.vista_aceptar_traspaso').live('click',function(){ 
        var id=$(this).attr('data-id');
        //preparo los parametros
        params={};
        params.id=id;
        params.action="vista_aceptar_traspaso";
        $('#popupbox').load('perfil.php', params,function(){
            $('#block').show();
            $('#popupbox').show();
        })

    })
		// registrar usuario
  	$('#form2').live('submit',function(){
        var params={};
        params.action='registrar_cuenta';
        params.cedula=$('#cedula').val();
		params.nombre=$('#nombre').val();
		params.apellido=$('#apellido').val();
		params.usuario=$('#usuario').val();
		params.clave=$('#clave').val();
		params.codigo=$('#codigo').val();
		params.pregunta=$('#pregunta').val();
		params.resp1=$('#resp1').val();
		params.pregunta2=$('#pregunta2').val();
		params.resp2=$('#resp2').val(); 
		
        $.post('perfil.php',params,function(){
			$('#block').hide();
        	$('#popupbox').hide();
            $('#content').load('perfil.php',{action:"cuentas_usuario"});
        })
        return false;
    })
	
	
    // boton cancelar, uso live en lugar de bind para que tome cualquier boton
    // nuevo que pueda aparecer
    $('#cancel').live('click',function(){
        $('#block').hide();
        $('#popupbox').hide();
		
    })  
	
})
NS={};
