﻿/**
 * Autor: Lucas Forchino
 * Web: http://www.tutorialjquery.com
 *
 */
$(document).ready(function(){ //cuando el html fue cargado iniciar	
	
	 
	//añado la posibilidad de abrir formularios sobre pantallas
    $('.form_comentar').live('click',function(){ 
        //preparo los parametros
        params={}; 
        params.action="form_comentar";
        $('#popupbox').load('perfil.php', params,function(){
            $('#block').show();
            $('#popupbox').show();
        })

    })
	
		// registrar usuario
  	$('#form2').live('submit',function(){
        var params={};
        params.action='registrar_cuenta';
        params.cedula=$('#cedula').val();
		params.nombre=$('#nombre').val();
		params.apellido=$('#apellido').val();
		params.usuario=$('#usuario').val();
		params.clave=$('#clave').val();
		params.codigo=$('#codigo').val();
		params.pregunta=$('#pregunta').val();
		params.resp1=$('#resp1').val();
		params.pregunta2=$('#pregunta2').val();
		params.resp2=$('#resp2').val(); 
		
        $.post('perfil.php',params,function(){
			$('#block').hide();
        	$('#popupbox').hide();
            $('#content').load('perfil.php',{action:"cuentas_usuario"});
        })
        return false;
    })
	
	//añado opcion para abrir formulario de fotos de los usuarios
	 $('.edit_foto_usuario').live('click',function(){ 
        var ced=$(this).attr('data-ced');
        //preparo los parametros
        params={};
        params.ced=ced;
        params.action="edit_foto_usuario";
        $('#popupbox').load('perfil.php', params,function(){
            $('#block').show();
            $('#popupbox').show();
        })

    })
	 
	 
	  $('.view_traspasar_carpeta').live('click',function(){ 
         var id_arcvar=$(this).attr('data-id3');
		 var id_traspaso=$(this).attr('data-id4');
        params={}; 
		params.id_arcvar=id_arcvar;
		params.id_traspaso=id_traspaso;
        params.action="view_traspasar_carpeta";
        $('#popupbox').load('perfil.php', params,function(){
            $('#block').show();
            $('#popupbox').show();
        })

    })
	  
	  $('#form3').live('submit',function(){
        var params={};
        params.action='traspasar_carpeta'; 
		params.id_traspaso=$('#id_traspaso').val();  
		params.id_arcvar=$('#id_arcvar').val();  
		params.ced_receptor=$('#ced_receptor').val();  
		params.fecha=$('#fecha').val();  
		params.motivo_tras=$('#motivo_tras').val(); 
		params.obser_tras=$('#obser_tras').val(); 
		
		 $.post('perfil.php',params,function(){
			$('#block').hide();
        	$('#popupbox').hide();
            $('#content').load('principal.php',{action:"refreshGrid"});
        })
        return false;
    })
	  
	   $('#form33').live('submit',function(){
        var params={};  
		params.name=$('#name').val();  
		params.email=$('#email').val();  
		params.password=$('#password').val();  
		$.post('procesar.php',params,function(){ 
             
        })
        return false;
    })
	  
	  
    // boton cancelar, uso live en lugar de bind para que tome cualquier boton
    // nuevo que pueda aparecer
    $('#cancel').live('click',function(){
        $('#block').hide();
        $('#popupbox').hide();
		
    })  
	
})
NS={};
