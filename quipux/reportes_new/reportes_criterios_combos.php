<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

session_start();
$ruta_raiz = "..";
include_once "$ruta_raiz/rec_session.php";
require_once("$ruta_raiz/funciones.php");
include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head()."<body>";

$txt_tipo = limpiar_sql($_POST["txt_tipo"]);


switch ($txt_tipo) {
    case "A":
        $areas = buscar_areas_dependientes($_SESSION["depe_codi"]);
        $sql = "select depe_nomb, depe_codi from dependencia where depe_codi in ($areas) and inst_codi=".$_SESSION["inst_codi"]." order by 1 asc";
        $rs = $db->conn->Execute($sql);
        $menu  = $rs->GetMenu2("txt_depe_codi", 0, "0:&lt;&lt; Todas las &aacute;reas &gt;&gt;", false,""," id='txt_depe_codi' class='select' onChange=\"cargar_combos('U')\"" );
        break;

    case "U":
        $txt_depe_codi = limpiar_sql($_POST["txt_depe_codi"]);
        if ($txt_depe_codi == "0") $txt_depe_codi = $_SESSION["depe_codi"];
        $sql = "select usua_nombre, usua_codi from datos_usuarios where usua_esta=1 and depe_codi in ($txt_depe_codi) and inst_codi=".$_SESSION["inst_codi"]." order by 1 asc";
        $rs = $db->conn->Execute($sql);
        $menu  = $rs->GetMenu2("txt_usua_codi", 0, "0:&lt;&lt; Todos los usuarios &gt;&gt;", false,""," id='txt_usua_codi' class='select'" );
        //  $menu_usr  = $rs_usr->GetMenu2("usCodSelect[]", 0, false, true, 8," id='usCodSelect' class='select'" );
        break;

    default:
        die ("Error al cargar combo");
        break;
}

echo $menu;
//echo "<br>$sql";
?>

</body>
</html>