<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

// Defino la lista de reportes disponibles
$lista_reportes = array();
$lista_reportes["01"] = array ("nombre" => "Consulta de registros por estado del documento",
                            "descripcion"=>"Muestra el n&uacute;mero de documentos clasificados por su estado.",
                            "mostrar" => "SI");
$lista_reportes["01_1"] = array ("nombre" => "Lista de documentos por estado.",
                            "descripcion"=>"Muestra el n&uacute;mero de documentos clasificados por su estado.",
                            "mostrar" => "NO");
$lista_reportes["02"] = array ("nombre" => "Tiempo de demora al tramitar documentos.",
                            "descripcion"=>"Muestra las principales acciones que se realizaron con los documentos y el tiempo que cada usuario tard&oacute; en realizar cada acci&oacute;n mientras se lo tramitaba.",
                            "mostrar" => "SI");
$lista_reportes["03"] = array ("nombre" => "Tiempo de demora al registrar documentos externos.",
                            "descripcion"=>"Muestra las principales acciones que se realizaron con los documentos y el tiempo que cada usuario se demor&oacute; en el proceso de registro hasta que el documento fue enviado al destinatario.",
                            "mostrar" => "SI");
$lista_reportes["04"] = array ("nombre" => "Tiempo de demora al crear nuevos documentos.",
                            "descripcion"=>"Muestra las principales acciones que se realizaron con los documentos y el tiempo que cada usuario se demor&oacute; hasta que el documento fue firmado y enviado.",
                            "mostrar" => "SI");
$lista_reportes["05"] = array ("nombre" => "Tiempo de demora al responder documentos registrados manualmente.",
                            "descripcion"=>"Muestra el tiempo que se demoró un documento en ser respondido desde que se lo registró hasta que se envió la respuesta.",
                            "mostrar" => "SI");

// Creo el combo de reportes y la funcion que controla los mensajes de descripción de los mensajes
$combo_reportes = "<select name='slc_tipo_reporte' id='slc_tipo_reporte' class='select' onChange='reportes_cambiar_reporte();'>";
foreach ($lista_reportes as $id => $reporte) {
    if ($reporte["mostrar"] == "SI") {
        $tmp = "";
        if ($txt_tipo_reporte == $id) {
            $tmp = "selected";
        }
        $combo_reportes .= "<option value='$id' $tmp>" . $reporte["nombre"] . "</option>";
    }
}
$combo_reportes .= "</select>";


// Defino lista de criterios disponibles
$criterios_nombres["txt_fecha_desde"] = "Fecha desde";
$criterios_nombres["txt_fecha_hasta"] = "Fecha hasta";
$criterios_nombres["txt_depe_codi"] = "&Aacute;rea";
$criterios_nombres["txt_usua_codi"] = "Usuario";
$criterios_nombres["txt_inst_origen"] = "Instituci&oacute;n origen";
$criterios_nombres["txt_inst_destino"] = "Instituci&oacute;n destino";

$agrupar_reporte = false;
switch ($txt_tipo_reporte) {
    case "01":
        $columnas["fecha1"]  = "Fecha (aaaa)";
        $columnas["fecha2"]  = "Fecha (aaaa-mm)";
        $columnas["fecha3"]  = "Fecha (aaaa-mm-dd)";
        $columnas["area"]    = "&Aacute;rea";
        $columnas["usuario"] = "Usuario";
        $columnas["estado0"] = "Archivados";
        $columnas["estado1"] = "En Elaboraci&oacute;n";
        $columnas["estado2"] = "En Tr&aacute;mite";
        $columnas["estado5"] = "Por imprimir";
        $columnas["estado6"] = "Enviados";
        $columnas["estado6r"] = "Registrados";
        $columnas["firmados"] = "Firma Digital";
        $columnas["estadot"] = "Total";

        $columnas_desc["fecha1"]  = "A&ntilde;o en que se generaron los documentos que tiene el usuario en alguna de sus bandejas";
        $columnas_desc["fecha2"]  = "A&ntilde;o y mes en que se generaron los documentos que tiene el usuario en alguna de sus bandejas";
        $columnas_desc["fecha3"]  = "A&ntilde;o, mes y d&iacute;a en que se generaron los documentos que tiene el usuario en alguna de sus bandejas";
        $columnas_desc["area"]    = "Nombre del &aacute;rea a la que pertenece el usuario que tiene el documento actualmente en alguna de sus bandejas";
        $columnas_desc["usuario"] = "Nombre del funcionario p&uacute;blico o usuario que tiene el documento actualmente en alguna de sus bandejas";
        $columnas_desc["estado0"] = "N&uacute;mero de documentos que el usuario tiene en su bandeja de archivados";
        $columnas_desc["estado1"] = "N&uacute;mero de documentos que el usuario tiene en su bandeja en elaboraci&oacute;n";
        $columnas_desc["estado2"] = "N&uacute;mero de documentos que el usuario tiene en su bandeja Recibidos";
        $columnas_desc["estado5"] = "N&uacute;mero de documentos que el usuario tiene en su bandeja por imprimir";
        $columnas_desc["estado6"] = "N&uacute;mero de documentos que el usuario tiene en su bandeja de Enviados";
        $columnas_desc["estado6r"] = "N&uacute;mero de documentos que el usuario tiene en su bandeja de enviados y que son documentos externos registrados";
        $columnas_desc["firmados"] = "N&uacute;mero de documentos que el usuario tiene en su bandeja enviados y que se fueron firmados electr&oacute;nicamente";
        $columnas_desc["estadot"] = "N&uacute;mero total de documentos que el usuario tiene en sus bandejas";

        $columnas_grupo1 = 5; //Columnas que pertenecen al primer bloque
        $columnas_default = "fecha2,area,usuario,estado0,estado1,estado2,estado5,estado6,estado6r,firmados,estadot";
        $criterios_default = "txt_fecha_desde,txt_fecha_hasta,txt_depe_codi,txt_usua_codi";
        $num_max_registros = 300; // Es el tope de registros que se mostrarán en el reporte
        break;
    case "01_1":
        $columnas["remitente"]     = "De";
        $columnas["destinatario"]  = "Para";
        $columnas["asunto"]        = "Asunto";
        $columnas["fecha"]         = "Fecha";
        $columnas["num_doc"]       = "No. Documento";
        $columnas["referencia"]    = "No. Referencia";
        $columnas["tipo"]          = "Tipo Doc.";
        $columnas["area"]          = "&Aacute;rea Actual";
        $columnas["usuario"]       = "Usuario Actual";

        $columnas_desc["remitente"]     = "Remitente del documento";
        $columnas_desc["destinatario"]  = "Destinatario del documento";
        $columnas_desc["asunto"]        = "Asunto";
        $columnas_desc["fecha"]         = "Fecha del documento";
        $columnas_desc["num_doc"]       = "N&uacute;mero de documento";
        $columnas_desc["referencia"]    = "N&uacute;mero de referencia";
        $columnas_desc["tipo"]          = "Tipo de documento";
        $columnas_desc["area"]          = "&Aacute;rea donde se encuentra actualmente el documento";
        $columnas_desc["usuario"]       = "Usuario actual del documento";

        $columnas_grupo1 = 0; //Columnas que pertenecen al primer bloque
        $columnas_default = ",remitente,destinatario,asunto,fecha,num_doc,usuario,area,referencia,tipo";
        $criterios_default = "";
        $num_max_registros = 200; // Es el tope de registros que se mostrarán en el reporte
        break;

    case "02":
    case "03":
    case "04":
        $columnas["num_doc"]    = "No. Documento";
        $columnas["fecha_doc"]  = "Fecha de Registro";
        $columnas["asunto"]     = "Asunto";
        $columnas["usua_actu"]  = "Usuario Actual";
        $columnas["estado"]     = "Estado Actual";
        if ($txt_tipo_reporte == "04") $columnas["num_respondido"]     = "Respondido a";

        $columnas["hist_ori_usua"]      = "Usuario Origen";
        $columnas["hist_ori_desc"]      = "Acci&oacute;n Origen";
        $columnas["hist_ori_fecha"]     = "Fecha Origen";
        $columnas["hist_dest_usua"]     = "Usuario Destino";
        $columnas["hist_dest_desc"]     = "Acci&oacute;n Realizada";
        $columnas["hist_dest_fecha"]    = "Fecha Destino";
        $columnas["hist_tiempo"]        = "Tiempo Demora";

        $columnas_desc["num_doc"]    = "N&uacute;mero del documento";
        $columnas_desc["fecha_doc"]  = "Fecha de Registro del documento externo";
        $columnas_desc["asunto"]     = "Texto que se encuentra como Asunto del documento";
        $columnas_desc["usua_actu"]  = "Nombre del Usuario que actualmente tiene el documento en sus bandejas de Recibidos o Archivados";
        $columnas_desc["estado"]     = "Estado en que se encuentra actualmente el documento";
        $columnas_desc["num_respondido"]   = "N&uacute;mero del documento al que se dio respuesta con el documento actual";
        $columnas_desc["hist_ori_usua"]    = "Nombre del Usuario que registr&oacute; o reasign&oacute; el documento al usuario destino";
        $columnas_desc["hist_ori_desc"]    = "Acci&oacute;n realizada por el usuario origen, para entregar el documento a la bandeja del usuario destino";
        $columnas_desc["hist_ori_fecha"]   = "Fecha en la se realiz&oacute; la acci&oacute;n para entregar el documento a la bandeja del usuario destino";
        $columnas_desc["hist_dest_usua"]   = "Nombre del usuario que recibe el documento en la bandeja de recibidos o en elaboraci&oacute;n, usuario que recibe del usuario origen";
        $columnas_desc["hist_dest_desc"]   = "Acci&oacute;n que realiza el usuario destino despu&eacute;s de recibir el documento en su bandjea de recibidos o en elaboraci&oacute;n";
        $columnas_desc["hist_dest_fecha"]  = "Fecha en la que se entreg&oacute; en la bandeja del usuario destino despu&eacute;s de la acci&oacute;n realizada por el usuario origen";
        $columnas_desc["hist_tiempo"]      = "Tiempo que se demor&oacute; el usuario destino en realizar una acci&oacute;n desde que le lleg&oacute; el documento a su bandeja de recibidos o en elaboraci&oacute;n";

        $columnas_grupo1 = 6; //Columnas que pertenecen al primer bloque
        $columnas_default = "num_doc,fecha_doc,usua_actu,asunto,estado,hist_ori_fecha,hist_ori_usua,hist_ori_desc,hist_dest_fecha,hist_dest_usua,hist_dest_desc,hist_tiempo";
        if ($txt_tipo_reporte == "04") $columnas_default .= ",num_respondido";
        $criterios_default = "txt_fecha_desde,txt_fecha_hasta,txt_depe_codi,txt_usua_codi";
        $agrupar_reporte = true;
        $num_max_registros = 0; // Es el tope de registros que se mostrarán en el reporte
        break;

    case "05":
        $columnas["fecha_reg"]  = "Fecha Registro";
        $columnas["num_doc"]    = "No. Documento";
        $columnas["estado"]     = "Estado Actual";
        $columnas["usua_actu"]  = "Usuario Actual";
        $columnas["asunto"]     = "Asunto";
        $columnas["num_ref"]    = "No. Referencia";
        $columnas["fecha_ref"]  = "Fecha Referencia";
        $columnas["accion"]     = "Acci&oacute;n Realizada";
        $columnas["num_resp"]   = "No. Respuesta";
        $columnas["fecha_resp"] = "Fecha Respuesta";
        $columnas["observa"]    = "Observaci&oacute;n";
        $columnas["tiempo"]     = "Tiempo Demora";

        $columnas_desc["fecha_reg"]  = "Fecha de registro";
        $columnas_desc["num_doc"]    = "N&uacute;mero de documento";
        $columnas_desc["usua_actu"]  = "Nombre del Usuario que actualmente tiene el documento en sus bandejas";
        $columnas_desc["estado"]     = "Estado en que se encuentra actualmente el documento";
        $columnas_desc["asunto"]     = "Asunto";
        $columnas_desc["num_ref"]    = "N&uacute;mero de referencia";
        $columnas_desc["fecha_ref"]  = "Fecha de referencia";
        $columnas_desc["accion"]     = "Acci&oacute;n realizada con el documento";
        $columnas_desc["num_resp"]   = "N&uacute;mero de la respuesta enviada";
        $columnas_desc["fecha_resp"] = "Fecha en que se respondi&oacute; el documento";
        $columnas_desc["observa"]    = "Observaciones en el caso que se haya eliminado o archivado el documento";
        $columnas_desc["tiempo"]     = "Tiempo que tard&oacute; el tr&aacute;mite en ser respondido";

        $columnas_grupo1 = 0; //Columnas que pertenecen al primer bloque
        $columnas_default = "fecha_reg,num_doc,estado,usua_actu,accion,num_resp,fecha_resp,observa,tiempo";
        $criterios_default = "txt_fecha_desde,txt_fecha_hasta,txt_depe_codi";//,txt_usua_codi";
        $agrupar_reporte = false;
        $num_max_registros = 0; // Es el tope de registros que se mostrarán en el reporte
        break;

    default:
        $columnas = array();
        $columnas_grupo1 = 0;
        $columnas_default = "";
        $lista_criterios = "txt_fecha_desde,txt_fecha_hasta";
}

?>
