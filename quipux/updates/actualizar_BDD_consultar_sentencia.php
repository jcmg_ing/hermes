<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
*
*   Ejecuta las actualizaciones de la BDD
*
***/
$ruta_raiz = "..";
session_start();
if($_SESSION["admin_institucion"]!=1) die("Usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
require_once "$ruta_raiz/rec_session.php";

$sql = "select sentencia, sentencia_verificacion, num_registros_bloque from actualizar_sistema where actu_codi in (select min(actu_codi) from actualizar_sistema where estado=0)";
$rs = $db->conn->query($sql);

$sentencia = base64_decode($rs->fields["SENTENCIA"]);
$sentencia_verificacion = base64_decode($rs->fields["SENTENCIA_VERIFICACION"]) . " offset 0 limit ".$rs->fields["NUM_REGISTROS_BLOQUE"];
$sentencia = str_replace("**SENTENCIA_VERIFICACION**", $sentencia_verificacion, $sentencia);
$sentencia = str_replace(array("<","\n"," "), array("&lt;","<br>","&nbsp;"), $sentencia);

echo $sentencia;

?>
