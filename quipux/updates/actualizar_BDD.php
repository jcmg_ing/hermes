<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
*
*   Ejecuta las actualizaciones de la BDD
 *
 *  Falta hacer el desarrollo para que ejecute queries individuales, para que ejecute automaticamente todas las sentencias,
 *  habilitar un botón para que consulte la siguiente sentencia, que se sincronice automaticamente con el servidor de actualizaciones, etc
*
***/
$ruta_raiz = "..";
session_start();
if($_SESSION["admin_institucion"]!=1) die("Usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
require_once "$ruta_raiz/rec_session.php";
require_once "$ruta_raiz/funciones.php";
include_once "$ruta_raiz/funciones_interfaz.php";

echo "<html>".html_head();
include_once "$ruta_raiz/js/ajax.js";
?>
<script type="text/javascript">
    var estado = "Pausa";
    var flag_detener = false; // Si aprieta el botón de pausar ejecucion
    var timerID = 0;

    function consultar_siguiente_sentencia() {
        nuevoAjax('div_consultar_sentencia', 'POST', 'actualizar_BDD_consultar_sentencia.php', '');
    }

    function ejecutar_sentencia() {
        if (estado == "Pausa" && document.getElementById('div_ejecutar_sentencia').innerHTML != "") {
            document.getElementById('div_ejecutar_sentencia').innerHTML = "";
            nuevoAjax('div_ejecutar_sentencia', 'POST', 'actualizar_BDD_ejecutar_sentencia.php', '');
            estado = "Ejecutando";
            flag_detener = false;
            document.getElementById('lbl_estado').innerHTML = estado;
            //document.getElementById('lbl_hora_inicio').innerHTML = "";
            document.getElementById('lbl_hora_fin').innerHTML = "";
            document.getElementById('btn_ejecutar').style.display = 'none';
            document.getElementById('btn_pausar').style.display = '';
            esperar_respuesta_ejecucion();
        }
        return;
    }

    function pausar_sentencia() {
        if(estado == "Ejecutando") {
            estado = "Pausa";
            flag_detener = true;
            document.getElementById('btn_ejecutar').style.display = '';
            document.getElementById('btn_pausar').style.display = 'none';
            document.getElementById('lbl_estado').innerHTML = estado;
        }
        return;
    }


    function esperar_respuesta_ejecucion() {
        try {
            if (estado == "Error") return;
                hora_inicio = trim(document.getElementById('txt_actu_hora_inicio').innerHTML);
                hora_fin = trim(document.getElementById('txt_actu_hora_fin').innerHTML);
                mensaje = trim(document.getElementById('txt_actu_mensaje').innerHTML);
                mensaje_error = trim(document.getElementById('txt_actu_mensaje_error').innerHTML);
                mensaje_fin = trim(document.getElementById('txt_actu_mensaje_fin').innerHTML);

                if (trim(document.getElementById('lbl_hora_inicio').innerHTML) == '') {
                    document.getElementById('lbl_hora_inicio').innerHTML = hora_inicio;
                }
                document.getElementById('lbl_hora_fin').innerHTML += hora_fin;

                // Verifico si existieron errores al ejecutar los queries y bloqueo todo
                if (mensaje_error == '') {
                    document.getElementById('lbl_mensaje').innerHTML = mensaje;
                    estado = "Pausa";
                    document.getElementById('lbl_estado').innerHTML = estado;
                } else {
                    document.getElementById('lbl_mensaje').innerHTML = mensaje_error;
                    estado = "Error";
                    document.getElementById('lbl_estado').innerHTML = estado;
                    return;
                }

                if (mensaje_fin == "no" && !flag_detener) {
                    // Si es un query que se ejecuta por partes
                    ejecutar_sentencia();
                } else {
                    pausar_sentencia();
                    if (mensaje_fin == "si") {
                        estado = "Finalizado";
                        document.getElementById('lbl_estado').innerHTML = estado;
                    }
                    // Mostrar botón para ejecutar siguiente query
                }
//            }
        } catch (e) {
            timerID = setTimeout("esperar_respuesta_ejecucion()", 300);
            return;
        }
    }
</script>

<body onload="consultar_siguiente_sentencia()">
  <center>
    <table width="90%" border="0" cellpadding="0" cellspacing="5" class="borde_tab">
        <tr>
            <th colspan="4">Sentencia a Ejecutar</th>
        </tr>
        <tr>
            <td colspan="4" class="listado1" align="center"><div id="div_consultar_sentencia" class="borde_tab" style="height: 200px; width: 95%; overflow: auto; text-align: left;"></div></td>
        </tr>
        <tr>
            <td colspan="4" class="listado1" align="center" valign="middle">
                <input type="button" name="btn_ejecutar" id="btn_ejecutar" value="Ejecutar Sentencia" class="botones_largo" onclick="ejecutar_sentencia()" >
                <input type="button" name="btn_pausar" id="btn_pausar" value="Detener Ejecución" class="botones_largo" onclick="pausar_sentencia()" style="display: none;">
                <br>&nbsp;
            </td>
        </tr>
        <tr>
            <th colspan="4">Estado de la Ejecuci&oacute;n</th>
        </tr>
        <tr>
            <th width="50%">Avance</th>
            <th width="20%">Hora de Inicio</th>
            <th width="20%">Hora de Finalización</th>
            <th width="10%">Estado</th>
        </tr>
        <tr>
            <td class="listado1" width="50%" id="lbl_mensaje"></td>
            <td class="listado1" width="20%" id="lbl_hora_inicio" align="center"></td>
            <td class="listado1" width="20%" id="lbl_hora_fin" align="center"></td>
            <td class="listado1" width="10%" id="lbl_estado" align="center"></td>
        </tr>
    </table>
    <div id="div_ejecutar_sentencia" class="borde_tab" style="display: none;">Iniciar</div>
  </center>
</body>