<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
*
*   Ejecuta las actualizaciones de la BDD
*
***/
$ruta_raiz = "..";
session_start();
if($_SESSION["admin_institucion"]!=1) die("Usted no tiene permisos suficientes para acceder a esta p&aacute;gina.");
require_once "$ruta_raiz/rec_session.php";
//$db = new ConnectionHandler("$ruta_raiz","reportes");

$mensaje_error = "";
$mensaje = "";
$mensaje_fin = "si";
$hora_inicio = date("H:i:s");




$sql = "select * from actualizar_sistema where actu_codi in (select min(actu_codi) from actualizar_sistema where estado=0)";
$rs_actu = $db->conn->query($sql);
if (!$rs_actu or $rs_actu->EOF) {
    $mensaje_error = "No existen m&aacute;s sentencias por ejecutar.";
} else {

    // Si es un query que debe ejecutarse por partes
    if (trim($rs_actu->fields["SENTENCIA_VERIFICACION"])!="") {
        $sentencia_verificacion = base64_decode($rs_actu->fields["SENTENCIA_VERIFICACION"]);
        $sql = "select count(1) as num from ($sentencia_verificacion) as a";
        $rs_verif = $db->conn->query($sql);
        if (!$rs_verif or $rs_verif->EOF) {
            $mensaje_error = "Existieron errores al ejecutar la Sentencia de Verificación.";
        } else {
            $sql_actu = "update actualizar_sistema set num_registros_restantes=".$rs_verif->fields["NUM"];
            if ($rs_actu->fields["NUM_REGISTROS_TOTAL"]==0) {
                $sql_actu .= ", num_registros_total=".$rs_verif->fields["NUM"];
                $rs_actu->fields["NUM_REGISTROS_TOTAL"]=$rs_verif->fields["NUM"];
            }
            if ($rs_verif->fields["NUM"]==0) {
                $sql_actu .= ", estado=1";
                $mensaje = "Se actualizaron ".$rs_actu->fields["NUM_REGISTROS_TOTAL"]." registros.";
            }
            $sql_actu .= " where actu_codi=".$rs_actu->fields["ACTU_CODI"];
            $db->conn->query($sql_actu);
            if ($rs_verif->fields["NUM"]>0) {
                $sentencia = base64_decode($rs_actu->fields["SENTENCIA"]);
                $sentencia_verificacion = base64_decode($rs_actu->fields["SENTENCIA_VERIFICACION"]) . " offset 0 limit ".$rs_actu->fields["NUM_REGISTROS_BLOQUE"];
                $sentencia = str_replace("**SENTENCIA_VERIFICACION**", $sentencia_verificacion, $sentencia);
                $ok = $db->conn->query($sentencia);
                if (!$ok) {
                    $mensaje_error = "Existieron errores al ejecutar la Sentencia (bloques).$sentencia";
                } else {
                    $mensaje = "Actualizados ".($rs_actu->fields["NUM_REGISTROS_TOTAL"]-$rs_verif->fields["NUM"])." de ".$rs_actu->fields["NUM_REGISTROS_TOTAL"]." registros.";
                    $mensaje_fin = "no";
                }
            }
        }
    } else {
        //Queries unitarios
        $sentencia = base64_decode($rs_actu->fields["SENTENCIA"]);
        $ok = $db->conn->query($sentencia);
        if (!$ok) {
            $mensaje_error = "Existieron errores al ejecutar la Sentencia.";
        } else {
            $mensaje = "Se ejecut&oacute; la sentencia correctamente.";
            $sql_actu = "update actualizar_sistema set estado=1 where actu_codi=".$rs_actu->fields["ACTU_CODI"];
            $db->conn->query($sql_actu);
        }

    }
}

$hora_fin = date("H:i:s");

echo "<span id='txt_actu_hora_inicio'>$hora_inicio</span>";
echo "<span id='txt_actu_hora_fin'>$hora_fin</span>";
echo "<span id='txt_actu_mensaje'>$mensaje</span>";
echo "<span id='txt_actu_mensaje_error'>$mensaje_error</span>";
echo "<span id='txt_actu_mensaje_fin'>$mensaje_fin</span>";
?>