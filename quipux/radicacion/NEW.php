
<?php
/*<script src='ckeditor/ckeditor.js'></script> */


/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*************************************************************************************************
**  En esta pagina se maneja la interfaz para crear, modificar y responder radicados		**
**												**
**  PARAMETROS:											**
**    	$accion		Accion a realizar: Nuevo - Responder - Editar				**
**    	$ent		(opcional, nuevo) si es un documento de entrada=2 o de salida=1 	**
**    	$nurad		(opcional, edicion o modificacion) es el numero de radicado		**
**    	$textrad	(opcional, edicion o modificacion) es el texto del radicado		**
**												**
**  INCLUDES:											**
**	../include/db/ConnectionHandler.php	Maneja las conexiones con la BDD		**
**	secciones_tipos_doc.php			Funcion Javascript que define q campos se 	**
**						mostraran en la pagina				**
**												**
**												**
**												**
**												**
**                                                                                              **
**                                                                                              **
**************************************************************************************************/
$ruta_raiz = isset($ruta_raiz) ? $ruta_raiz : "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
//DESCRIPCIONES OPCIONES DE IMPRESION
require_once ("$ruta_raiz/tx/tx_actualiza_opcion_imp.php");
p_register_globals(array());

session_start();
if (!$ruta_raiz) $ruta_raiz = "..";
include_once "$ruta_raiz/rec_session.php";
include_once "$ruta_raiz/obtenerdatos.php";
include_once "$ruta_raiz/funciones_interfaz.php";

// se incluyo por register globals.
$ent=$_GET['ent'];
$accion = $_GET['accion'];
$krd = $_SESSION['krd'];
$mensaje = $_GET['mensaje'];
$nurad  = $_GET['nurad'];
$textrad = $_GET['textrad'];
//PARA INSERTAR EN HISTORICO AL MOMENTO DE RESPONDER DE BANDEJA COMPARTIDA.
$compResponder = $_GET['compResponder'];
if ($compResponder==1 and $nurad!=''){
    $jefe = array();
    $jefe = ObtenerDatosUsuario($_SESSION['usua_codi_jefe'],$db);
    $observacion = '';
    //INSERTAR EN TABLA HISTORICO
   include_once "$ruta_raiz/include/tx/Tx.php";
   $tx = new Tx($db);
   $numrad = array();
   //$numrad[0] = $nurad;
   $numrad[0] = $nurad;
   //print_r($numrad);
    $tx->reasignar( $numrad, $_SESSION['usua_codi_jefe'], $_SESSION['usua_codi'], $observacion, date("Y-m-d H:i:s"), false, $carpeta);            
}


//Se incluyo para prevenir  CSRF ( Cross-Site Request Forgeries)
$token = md5(uniqid(rand(), true));
$_SESSION['token'] = $token;


//En el caso de no existir $nurad (por cualquier error) se lo manejara como nuevo
if (trim($nurad)=="") $accion="Nuevo";
if ($accion=="Nuevo") {$nurad=""; $textrad="";}

/**
* Verifica si el documento es de entrada o salida, ingresa al if solo cuando es responder o editar radicado.
**/

if($nurad) {
    $textrad = trim($textrad);
    $nurad=trim($nurad);

    if (substr($nurad,-1)==2)
	$ent = 2;
    else
	$ent = 1;
    //$raditipo = $ent;
}

if($ent == 1)
    $raditipo = 1;
else
    $raditipo = 2;
if ($_SESSION["tipo_usuario"]==2) $raditipo = 7; // Ciudadanos


$optipoNotaBusq = ObtenerDatosOpcImpresion($nurad,$db);

$opc_nota_inicial=$optipoNotaBusq['OPC_IMP_TIPO_NOTA'];



echo "<html>".html_head();
?>

<link rel="stylesheet" type="text/css" href="<?=$ruta_raiz?>/js/spiffyCal/spiffyCal_v2_1.css"></script>

<script type="text/javascript" src="<?=$ruta_raiz?>/js/crea_combos_2.js"></script>
<!-- Llamada a la libreria fckeditor para creacion de editor de texto --> 

<? include_once "$ruta_raiz/acuerdoConfig.php"; ?>
<? include_once "$ruta_raiz/js/ajax.js"; ?>

<script type="text/javascript">
 var objetoImpresion=new Array();
 
    function vista_previa() {
        windowprops = "top=100,left=100,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=500,height=300";
        url = <?="'$ruta_raiz/VistaPrevia.php?verrad=$nurad&archivo=&textrad=$textrad'"?>;
        window.open(url , "Vista_Previa_<?=$noRad?>", windowprops);
        return;
    }


     contador_tiempo = 0;
     function timer_activar_session() {
        if (contador_tiempo < 3) {
            if (contador_tiempo > 0 && contador_tiempo < 3) {
                nuevoAjax('div_activar_sesion', 'POST', 'activar_session.php', '');
            }
            ++contador_tiempo;
            timerID = setTimeout("timer_activar_session()", 1200000); // 20*60*1000 = 1200000
        } else {
            clearTimeout(timerID);
            document.formulario.opc_grab.value = '2';
            if (document.formulario.documento_us1.value.length > 0 && document.formulario.documento_us2.value.length > 0 )
                document.formulario.submit();
            else
                alert('Su sesión ha expirado.\nPor favor seleccione y copie el texto introducido en su editor \ny peguelo en otro sitio para que este no se pierda.');
            return;
        }
    }
    timer_activar_session();

    function validarfecha()
    {
        // Controla que la informacion ingresada en la fecha del oficio sea correcta
        var fechaActual = new Date();
        fecha_doc = document.formulario.fecha_doc.value;
        dias_doc=fecha_doc.substring(0,2);
        mes_doc=fecha_doc.substring(3,5);
        ano_doc=fecha_doc.substring(6,10);
        var fecha = new Date(ano_doc,mes_doc-1, dias_doc);
        var tiempoRestante = fechaActual.getTime() - fecha.getTime();
        var dias = Math.floor(tiempoRestante / (1000 * 60 * 60 * 24));
        fecha_doc = 'no';
        if (dias >366 && dias < 1500)
        {
            alert("El documento tiene fecha anterior a un año!!");
        }
        else
        {
            if (dias > 1500) {
                    alert("Verifique la fecha del documento!!");
            } else {
                if (dias < 0 && <?=$ent?>==2)	{
                    alert("Verifique la fecha del documento, no puede ser superior a la fecha de hoy.");
                }
                else
                    fecha_doc = "ok";
            }
        }
        return fecha_doc;
    }

function refrescar_pagina(origen,cod_impresion)
{
    
    // Muestra la lista de usuarios Remitente, Destinatario y Con Copia
    ent = document.formulario.hidd_ent.value;    
    radi_lista_nombre = document.formulario.radi_lista_nombre.value;
    remitente = document.formulario.documento_us1.value;
    destinatario = document.formulario.documento_us2.value
    copia = document.formulario.concopiaa.value;
    //nuevoAjax('ifr_usr', 'GET', 'lista_concopiaa.php', 'documento_us1='+ remitente + '&documento_us2=' + destinatario + '&concopiaa=' + copia);
    document.getElementById('ifr_usr').src='lista_concopiaa.php?documento_us1='+ remitente + '&documento_us2=' + destinatario + '&concopiaa=' + copia+'&radi_lista_nombre='+radi_lista_nombre+'&ent='+ent;
    //CambiaTipoDocu();
    /**CAmbiar opcion tipo radicado**/
    raditipo = document.formulario.raditipo.value;
    flag_inst = document.formulario.flag_inst.value;
    //borrar opciones impresion
    if (document.formulario.hidden_actualiza_opciones.value==1){
        datos="codigo_opc="+document.formulario.codiOpcImp.value;
        nuevoAjax('div_borrar_opc_imp', 'GET', '<?=$ruta_raiz?>/tx/tx_borrar_opcion_imp.php', datos); 
        pestanas(1);
    }       
   
    if(origen=="")
    {
	if(raditipo==1 && flag_inst==0)
	{
	/**Cambio a Memo tipo documento**/
            for(i=0;i<document.formulario.raditipo.length;i++)
                if(document.formulario.raditipo.options[i].value=='3')
                {
                    if(confirm("Los Destinatarios pertenecen a su institución, \ndesea cambiar su tipo de documento a Memorando? ")){
                        document.formulario.raditipo.options[i].selected=true;
                        document.formulario.hidden_radi_actual.value = document.formulario.raditipo.options[i].value;
                    }
                    else return false;
                }
        }
	if (raditipo==3 && flag_inst==1)
	{
	/**Cambio a Oficio tipo documento**/
            for(i=0;i<document.formulario.raditipo.length;i++)
                if(document.formulario.raditipo.options[i].value=='1')
                {
                    if(confirm("Los Destinatarios no pertenecen a su institución, \ndesea cambiar su tipo de documento a Oficio? "))
                        document.formulario.raditipo.options[i].selected=true;
                    else return false;
                }
	}
	if(raditipo==2 && flag_inst==1)	{
	/**Si no selecciono un funcionario para registrar un documento externo**/
		alert("Ningún Destinatario pertenece a su institución.\nPor favor revise la lista de destinatarios.")
                
	}
    }
    //origen = OI (Opciones de Impresión) Ejecutar funcion para cargar estilos de impresion
    if(document.getElementById("raditipo") != null){        
        document.getElementById('rad_tipo_imp').value = cod_impresion;
        cargarEstiloImpresion();
    }
    
    return;

}

    function cambiar(){
        
        try {
            if(document.getElementById("NumDest").value==1)
            {
                document.getElementById("D").style.display='none';
            }
            else
            {
                
                if(document.getElementById("radi_tipo_impresion").value==1 )
                {
                    document.getElementById("D").style.display='';
                    document.getElementById("T").style.display='';
                    document.getElementById("N").style.display='';
                    document.getElementById("trCargo").style.display='';
                    document.getElementById("trInstitucion").style.display='';
                    
                }
                if(document.getElementById("radi_tipo_impresion").value==2)
                {
                    document.getElementById("D").style.display='';
                    document.getElementById("T").style.display='none';
                    document.getElementById("N").style.display='none';
                    document.getElementById("trCargo").style.display='';
                    document.getElementById("trInstitucion").style.display='';
                    
                }

                if(document.getElementById("radi_tipo_impresion").value==3 )
                {
                    document.getElementById("D").style.display='none';
                }

                if(document.getElementById("radi_tipo_impresion").value==4 )
                {
                    document.getElementById("D").style.display='';
                    document.getElementById("T").style.display='';
                    document.getElementById("N").style.display='';
                    document.getElementById("trCargo").style.display='';
                    document.getElementById("trInstitucion").style.display='none';
                }

                if(document.getElementById("radi_tipo_impresion").value==5 )
                {
                    document.getElementById("D").style.display='';
                    document.getElementById("T").style.display='';
                    document.getElementById("N").style.display='';
                    document.getElementById("trCargo").style.display='none';
                    document.getElementById("trInstitucion").style.display='';
                }
                 if(document.getElementById("radi_tipo_impresion").value==6 )
                {
                    document.getElementById("D").style.display='';
                    document.getElementById("T").style.display='';
                    document.getElementById("N").style.display='none';
                    document.getElementById("trCargo").style.display='';
                    document.getElementById("trInstitucion").style.display='';
                }
               
                  
            }
        } catch (e) {}
    }

    function mostrar_botones (flag) {
        deshabilitar = true;
        if (flag) deshabilitar = false;
        <? if ($ent != 2) { ?>
            document.formulario.Submit1.disabled = deshabilitar;
        <? } ?>
        document.formulario.Submit2.disabled = deshabilitar;
    }

    function cambio_cuerpo(codi_texto) {        
        mostrar_botones (false);
        try {
            tipo_docu = document.getElementById('raditipo').value;
            referencia = document.getElementById('referencia').value;
        } catch (e) {
            tipo_docu = '<?=$ent?>';
        }
        
        if (detectarPhone()==1)
            nuevoAjax('div_cuerpo', 'POST', 'cargar_editor.php', 'esphone=1&codi_texto='+ codi_texto + '&referencia=' + referencia + '&tipo_docu='+tipo_docu+'&accion=<?=$accion?>');
        else
            nuevoAjax('div_cuerpo', 'POST', 'cargar_editor.php', 'esphone=0&codi_texto='+ codi_texto + '&referencia=' + referencia + '&tipo_docu='+tipo_docu+'&accion=<?=$accion?>'); 
        // Desactivamos los botones para guardar por 5 segundos
        timerID = setTimeout("mostrar_botones (true)", 5000);
}

    function cancelar()
{
        <? if ($nurad) $var_envio = "window.location='$ruta_raiz/verradicado.php?verrad=$nurad&menu_ver=3&irVerRad=1&tipo_ventana=popup';";
           else $var_envio = "history.back();";
        echo $var_envio;
        ?>
    return;
}

function grabar_doc(dato)
{
         
        if (<?=$ent?>==2) {
            if(validarfecha()!="ok") return;
        }
        
        document.formulario.opc_grab.value=dato;
        
        if (trim(document.formulario.documento_us1.value) != '' && trim(document.formulario.documento_us2.value) != '' ) {
            if (trim(document.formulario.asunto.value) != '')
                document.formulario.submit();
            else
                alert("El Asunto del documento es obligatorio.");
        } else
            alert("El Remitente y Destinatario son obligatorios.");
        return;
}

function pestanas(valor)
{
    if (valor==1) {
        document.getElementById('etiqueta1').style.display = "none";
        document.getElementById('etiqueta1_R').style.display = "";
        document.getElementById('etiqueta2').style.display = "";
        document.getElementById('etiqueta2_R').style.display = "none";
         <?if($ent!= 2){?>
            document.getElementById('etiqueta3').style.display = "";
            document.getElementById('etiqueta3_R').style.display = "none";
        <?}?>

        document.getElementById('cuerpo_documento').style.display = "";
        document.getElementById('cuerpo_anexos').style.display = "none";
        document.getElementById('opciones_impresion').style.display = "none";
        document.getElementById('bandera_cambiarop').value=0;
    }
    if (valor==2) {
        document.getElementById('etiqueta1').style.display = "";
        document.getElementById('etiqueta1_R').style.display = "none";
        document.getElementById('etiqueta2').style.display = "none";
        document.getElementById('etiqueta2_R').style.display = "";
        <?if($ent!= 2){?>
            document.getElementById('etiqueta3').style.display = "";
            document.getElementById('etiqueta3_R').style.display = "none";
        <?}?>
        document.getElementById('cuerpo_documento').style.display = "none";
        document.getElementById('cuerpo_anexos').style.display = "";
        document.getElementById('opciones_impresion').style.display = "none";
    }
    if (valor==3) {        
        document.getElementById('etiqueta1').style.display = "";
        document.getElementById('etiqueta1_R').style.display = "none";
        document.getElementById('etiqueta2').style.display = "";
        document.getElementById('etiqueta2_R').style.display = "none";
        document.getElementById('etiqueta3').style.display = "none";
        document.getElementById('etiqueta3_R').style.display = "";
        document.getElementById('cuerpo_documento').style.display = "none";
        document.getElementById('cuerpo_anexos').style.display = "none";
        document.getElementById('opciones_impresion').style.display = ""; 
        document.getElementById('bandera_cambiarop').value=1;
        cargarEstiloImpresion();        
    }
    
}

    function Start(URL) {
        var x = (screen.width - 1100) / 2;
        var y = (screen.height - 740) / 2;
        windowprops = "top=0,left=0,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=1100,height=740";
        URL = URL + '?documento_us1=' + document.formulario.documento_us1.value + '&documento_us2=' + document.formulario.documento_us2.value + '&concopiaa=' + document.formulario.concopiaa.value + "&ent=<?=$ent?>"
              + "&radi_lista_dest=" + document.formulario.radi_lista_dest.value
              + "&radi_lista_nombre=" + document.formulario.radi_lista_nombre.value;
        preview = window.open(URL , "preview", windowprops);
        preview.moveTo(x, y);
        preview.focus();
    }

// Para mostrar algunas opciones de impresion
function mostrarOpcImp(){
    document.getElementById('hidden_radi_actual').value = document.formulario.raditipo.value;
    try {       
           
        if(document.formulario.raditipo.value == 1) // Para oficios
        {
            document.getElementById('opcCargo').style.display = "";
            document.getElementById('opcInstitucion').style.display = "";
            document.getElementById('opcListas').style.display = "";
            document.getElementById('div_fecha_documento').style.display = "none";
        }
        else
        {
            
            document.getElementById('opcCargo').style.display = "none";
            document.getElementById('opcInstitucion').style.display = "none";
            if (document.formulario.raditipo.value == 3)
               document.getElementById('opcListas').style.display = "none";
            else
                if (document.formulario.raditipo.value == 6)
                document.getElementById('opcListas').style.display = "none";
                else
                    document.getElementById('opcListas').style.display = "";
             document.getElementById('div_fecha_documento').style.display = "";
            //document.getElementById('radi_tipo_impresion').value = 1;
        }
    } catch (e) {}
}

function mostrar_div_sumillas(accion) {
    if (accion == 1)
        document.getElementById('div_sumillas').style.display = '';
    else
        document.getElementById('div_sumillas').style.display = 'none';
}


/*********************** INICIO FUNCIONES PARA ESTILO DE IMPRESION  *************************/

    function ltrim(s) {
       return s.replace(/^\s+/, "");
    }

    function cargarEstiloImpresion(){
        
        
        var parametros = "";
        var listDestinatarios = new Array();
        var numDestinatarios = 0;
        // Si el documento es tipo oficio se muestra las opciones de impresión
        if(document.getElementById("raditipo").value == 1 || document.getElementById("raditipo").value == 6)
        {
            document.getElementById("div_estilo_impresion").style.display="";
            parametros = "verRadicado=" + "<?=$nurad?>" + "&radiTipoDoc=" + document.getElementById("raditipo").value;
            parametros += "&codiDest=" + document.getElementById("documento_us1").value;
            parametros += "&cmb_tipo_imp=" + document.getElementById("rad_tipo_imp").value;
            
            listDestinatarios = document.getElementById("documento_us1").value.split("--");
            numDestinatarios = listDestinatarios.length;
            //para cambiar el tipo de impresion que quedo anterior
            if (numDestinatarios==1){
             if (document.getElementById("hidden_actualiza_opciones").value==1)
                     parametros+="&blanquear_cambiar_titulo=1";                 
            }
            // Si es un destinatario se muestra Información del Destinatario para modificar información
            opc_tipo_imp=document.getElementById('rad_tipo_imp').value;
            if (opc_tipo_imp==3)
                parametros += "&verDest=none";
            else{
                if(numDestinatarios==1)
                parametros += "&verDest=block";
            else // Si es mas de un destinatario no se muestra la Información del Destinario unicamente se muestra Saludo y Despedida del firmante
                parametros += "&verDest=none";
            }
            
            if (document.getElementById("documento_us1").value!='')
             parametros +="&editarCiudadano=1";
            
            document.getElementById("div_estilo_impresion").style.display="";
            nuevoAjax('div_estilo_impresion', 'POST', 'estilo_impresion.php', parametros);
            
        }
        else // Si el documento no es un oficio no se muestra las opciones de impresión
            document.getElementById("div_estilo_impresion").style.display="none";
    }

    function muestraBtnRemitente(opc){
        if (opc==1){//pie pagina se deshabilita la extension de la institucion
            document.getElementById('ImgInst').style.visibility='hidden';
            document.getElementById("txt_ext_institucion").style.visibility='hidden';
            document.getElementById('ImgFirm').style.visibility='hidden';
            document.getElementById("txt_opcFirmantes").style.visibility='hidden';
            //document.getElementById('txt_cab').value="PIE PÁGINA";
        }else{//cabecera
            document.getElementById('ImgInst').style.visibility='visible';
            document.getElementById("txt_ext_institucion").style.visibility='visible';
            document.getElementById('ImgFirm').style.visibility='visible';
            document.getElementById("txt_opcFirmantes").style.visibility='visible';

            //document.getElementById('txt_cab').value="CABECERA";
        }
        return true;
    }

    function habilitaObj(opcT){
       
        if (opcT=="tit"){//Título
            document.getElementById('txt_opcTitulo').readOnly = false;
            document.getElementById('txt_opcTitulo').className = 'caja_texto';
            if(ltrim(document.getElementById('txt_opcTitulo').value) == 'Sin título')
                document.getElementById('txt_opcTitulo').value = '';
            document.getElementById('txt_opcTitulo').focus();
        }else  if (opcT=="carg"){//cargo
            document.getElementById('txt_opcCargo').readOnly = false;
            document.getElementById('txt_opcCargo').className = 'caja_texto';
            if(ltrim(document.getElementById('txt_opcCargo').value) == 'Sin Cargo Cabecera')
                document.getElementById('txt_opcCargo').value = '';
            document.getElementById('txt_opcCargo').focus();
        }else if (opcT=="firm"){// y mas firmantes
            document.getElementById('txt_opcFirmantes').readOnly = false;
            document.getElementById('txt_opcFirmantes').className = 'caja_texto';
            if(ltrim(document.getElementById('txt_opcFirmantes').value) == '')
                document.getElementById('txt_opcFirmantes').value = '';
            document.getElementById('txt_opcFirmantes').focus();
        }else if (opcT=="ins"){
            document.getElementById('txt_ext_institucion').readOnly = false;
            document.getElementById('txt_ext_institucion').className = 'caja_texto';
            if(ltrim(document.getElementById('txt_ext_institucion').value) == '')
                document.getElementById('txt_ext_institucion').value = '';
            document.getElementById('txt_ext_institucion').focus();
        }else if (opcT=="sal"){
            document.getElementById('txt_opcSaludo').readOnly = false;
            document.getElementById('txt_opcSaludo').className = 'caja_texto';
            document.getElementById('txt_opcSaludo').focus();
        }else if (opcT=="des"){
            document.getElementById('txt_opcDespedida').readOnly = false;
            document.getElementById('txt_opcDespedida').className = 'caja_texto';
            document.getElementById('txt_opcDespedida').focus();
        }else if (opcT=="fra"){
            document.getElementById('txt_opcFrasedespedida').readOnly = false;
            document.getElementById('txt_opcFrasedespedida').className = 'caja_texto';
            document.getElementById('txt_opcFrasedespedida').focus();
        }else if (opcT=="dir"){
            if(document.getElementById('txt_direccion').value==""){
                if(document.getElementById('tipouser').value==2)
                    alert("El ciudadano no tiene dirección ingresada");
                else
                    alert("El funcionario no tiene ciudad ingresada");
            }else{
                document.getElementById('txt_opcSaludo').style.visibility = 'visible';
                document.getElementById('txt_opcSaludo').value=document.getElementById('txt_direccion').value;
            }
        }
        return true;
    }

    function deshabilitaObj(opcT){
        if (opcT=="tit"){
            document.getElementById('txt_opcTitulo').readOnly = true;
            document.getElementById('txt_opcTitulo').className = 'text_transparente';
            if(ltrim(document.getElementById('txt_opcTitulo').value) == '')
                document.getElementById('txt_opcTitulo').value = document.getElementById('txt_titulo').value;
            document.getElementById('txt_opcTitulo').focus();
        }if (opcT=="carg"){
            document.getElementById('txt_opcCargo').readOnly = true;
            document.getElementById('txt_opcCargo').className = 'text_transparente';
            if(ltrim(document.getElementById('txt_opcCargo').value) == '')
                document.getElementById('txt_opcCargo').value = document.getElementById('txt_Cargo').value;
            document.getElementById('txt_opcCargo').focus();
        }else if(opcT=="firm"){
            document.getElementById('txt_opcFirmantes').readOnly = true;
            document.getElementById('txt_opcFirmantes').className = 'text_transparente';
            document.getElementById('txt_opcFirmantes').focus();
        }else if(opcT=="ins"){
            document.getElementById('txt_ext_institucion').readOnly = true;
            document.getElementById('txt_ext_institucion').className = 'text_transparente';
            document.getElementById('txt_ext_institucion').focus();
        }else if(opcT=="sal"){
            document.getElementById('txt_opcSaludo').readOnly = true;
            document.getElementById('txt_opcSaludo').className = 'text_transparente';
            if(ltrim(document.getElementById('txt_opcSaludo').value) == '')
                document.getElementById('txt_opcSaludo').value = document.getElementById('txt_saludo').value;
            document.getElementById('txt_opcSaludo').focus();            
            txtSaludo=document.getElementById('txt_opcSaludo').value;
            numradop = document.getElementById("num_rad").value;
            if(numradop!=''){                 
            nuevoAjax('div_modificar_op', 'GET', 'opciones_impresion_mod.php', 'txtSaludo='+txtSaludo+'&num_radicado=' + numradop);
            }
             
        }else if (opcT=="des"){
            document.getElementById('txt_opcDespedida').readOnly = true;
            document.getElementById('txt_opcDespedida').className = 'text_transparente';
            if(ltrim(document.getElementById('txt_opcDespedida').value) == '')
                document.getElementById('txt_opcDespedida').value = document.getElementById('txt_despedida').value;
            document.getElementById('txt_opcDespedida').focus();
        }else if (opcT=="fra"){
            document.getElementById('txt_opcFrasedespedida').readOnly = true;
            document.getElementById('txt_opcFrasedespedida').className = 'text_transparente';
            if(ltrim(document.getElementById('txt_opcFrasedespedida').value) == '')
                document.getElementById('txt_opcFrasedespedida').value = document.getElementById('txt_frasedespedida').value;
            document.getElementById('txt_opcFrasedespedida').focus();
        }
    }

    function editarCiudadano(){        
        var url = "";
	var x = (screen.width - 1100) / 2;
	var y = (screen.height - 575) / 2;
        cod_impresion = document.getElementById("radi_tipo_impresion").value;
        url = "<?=$ruta_raiz?>" + "/Administracion/usuarios/adm_usuario_ext.php?cerrar=Si&accion=2&ciu_codigo=" + document.getElementById("usuaCodi").value+"&cod_impresion="+cod_impresion;
	ventana=window.open(url,"Editar_Ciudadano","toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes, width=1100, height=575");
	ventana.moveTo(x, y);
        ventana.focus();
    }

/**/
//Por David Gamboa
function ventanaNueva(url){
	window.open(url,'nuevaVentana','width=600, height=600,top=200');
}
function vista_previa2(ruta,numrad,textrad,path) {    
        windowprops = "top=100,left=100,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=500,height=300";

        url = ruta+"/VistaPrevia.php?verrad="+numrad+"&archivo="+path+"&textrad="+textrad;
        window.open(url , "Vista_Previa_<?=$noRad?>", windowprops);
        return;
    }
function buscarDatosDestinatario(tipo_busqueda)
{     
  if (document.getElementById("radi_lista_dest").value ==''){
       tipo_impresion=document.getElementById('radi_tipo_impresion').value;       
       
       if (tipo_impresion==6 || tipo_impresion==2 || tipo_impresion==1 || tipo_impresion==4 || tipo_impresion==5){
         destinatario = document.getElementById('documento_us1').value;
         nuevoAjax('div_ver_datos', 'GET', 'ver_datos_usuario.php', 'destinatario='+destinatario+'&tipo_busqueda='+tipo_busqueda);
       }
  }
}
function verOpcionNota(opcion){
    
    try {
    //segun opcion
        switch (opcion) {
            case 3: //verbal
                
                document.getElementById("div_despedida_firmante").style.display="none";
                document.getElementById("radio_cabecera").checked = true;
                document.getElementById("radio_cabecera").value = 1;
                document.getElementById("txt_pie_cabecera").value = 'Pie de Página';
                
                document.getElementById("div_fecha_documento").style.display='';
                
                
                if (document.getElementById("tituloDest").value=='Sin título')
                    document.getElementById("txt_opcTitulo").value = 'A la Honorable,';
                else if(document.getElementById("txt_opcTitulo").value!=document.getElementById("tituloDest").value)
                 document.getElementById("txt_opcTitulo").value = 'A la Honorable,';
                else if(document.getElementById("txt_titulo").value==document.getElementById("txt_opcTitulo").value)
                    document.getElementById("txt_opcTitulo").value = 'A la Honorable,';
                
                muestraBtnRemitente(0);
                break;

            case 1: //diplomatica
                document.getElementById("div_despedida_firmante").style.display='';
                document.getElementById("radio_cabecera").value = 0;
                document.getElementById("radio_cabecera").checked = true;
                document.getElementById("txt_pie_cabecera").value = 'Cabecera';
                document.getElementById("div_fecha_documento").style.display="none";
                //document.getElementById("ub_fecha").style.display="none";
                
                document.getElementById("txt_opcTitulo").value = document.getElementById("tituloDest").value;
                muestraBtnRemitente(0);
                break;

            case 2: //reverso
                document.getElementById("div_despedida_firmante").style.display='';
                document.getElementById("radio_cabecera").checked = true;
                document.getElementById("radio_cabecera").value = 1;
                document.getElementById("txt_pie_cabecera").value = 'Pie de Página';
                document.getElementById("div_fecha_documento").style.display="none";
                document.getElementById("txt_opcTitulo").value = document.getElementById("tituloDest").value;
                //document.getElementById("ub_fecha").style.display="none";
                muestraBtnRemitente(0);
                break;            
            default:
                
                break;
        }
    } catch (e) {}
}

function limpiardiv(){
    document. getElementById('div_ver_datos'). innerHTML='';
    
}

function copia(){
    document.getElementById("hidden_titulo_anterior").value=document.getElementById("txt_opcTitulo").value;
}
//tipo de transaccion: id donde hace clic
//obj: lo que ingresa el usuario en caja de texto, check o radio
//valor: el valor anterior
function histop(tipo_transaccion,obj,valor){     
    existe=0;       
    if (valor==obj.value)
    existe=1;

    if (existe==0){
        document.getElementById('hidden_acciones_datos').value+=tipo_transaccion+","+obj.value+","+valor+":";        
    }
}    
 function AsociarDocumento(nurad,refepadre,modificar) {
       if (trim(document.formulario.documento_us1.value) != '' && trim(document.formulario.documento_us2.value) != '' ) {
            if (trim(document.formulario.asunto.value) != ''){
             ventana = window.open("../asociar_documentos/asociar_documento.php?radi_nume="+nurad+"&radi_refe="+refepadre+"&cerrar=SI&modificar="+modificar, "asociar_documentos", "height=600,width=900,scrollbars=yes");
        ventana.focus();   
            }                
            else
                alert("El Asunto del documento es obligatorio.");
        } else
            alert("El Remitente y Destinatario son obligatorios.");
        
       
    }    
function detectarPhone(){
    var navegador = navigator.userAgent.toLowerCase();
    if ( navigator.userAgent.match(/iPad/i) != null)//detectar ipad
      return 2;
    else{//detectar phone        
        if( navegador.search(/iphone|ipod|blackberry|android/) > -1 )
           return 1;    
        else 
            return 0;
    }
}
</script>


<style type="text/css">

/*Tool Tip*/
a.Ntooltip {
position: relative; /* es la posición normal */
text-decoration: none !important; /* forzar sin subrayado */
color:#0080C0 !important; /* forzar color del texto */
font-weight:bold !important; /* forzar negritas */
}

a.Ntooltip:hover {
z-index:999; /* va a estar por encima de todo */
background-color:#000000; /* DEBE haber un color de fondo */
}

a.Ntooltip span {
display: none; /* el elemento va a estar oculto */
}

a.Ntooltip:hover span {
display: block; /* se fuerza a mostrar el bloque */
position: absolute; /* se fuerza a que se ubique en un lugar de la pantalla */
top:2em; left:2em; /* donde va a estar */
width:100px; /* el ancho por defecto que va a tener */
padding:5px; /* la separación entre el contenido y los bordes */
background-color: #FBFBEF; /* el color de fondo por defecto */
color: #000000; /* el color de los textos por defecto */
}
/*Tool Tip*/
/**/
</style>
</head>

<script type="text/javascript" src="<?=$ruta_raiz?>/js/spiffyCal/spiffyCal_v2_1.js"></script>

<?php
////////////////////    CARGO LOS DATOS SI NO ES NUEVO  //////////////////////////////
    $codi_texto = "0";
    $chk_plantilla = "1";
    $cmb_texto = "100";
    $txt_usua_redirigido = 0;
    if($ent==1)
    {        
        $usua=ObtenerDatosUsuario($_SESSION["usua_codi"],$db,"U");
        $documento_us2 = "-".$_SESSION["usua_codi"]."-";
//        if($nurad){
//            $usua=ObtenerDatosUsuario($krd,$db,"L");
//            $documento_us2 = "-".$usua["usua_codi"]."-";
//        }
//        else{
//            $usua=ObtenerDatosUsuario($_SESSION["usua_codi"],$db,"U");
//            $documento_us2 = "-".$usua["usua_codi"]."-";
//        }
        
    }
    if(trim($accion)=="Editar" || trim($accion)=="Responder")//editar responder
    {
        $radicado=ObtenerDatosRadicado($nurad,$db);
        $asunto         = $radicado["radi_asunto"];
        //Para copiar automaticamente las notas que han sido incluidas en el padre
        $notas          = $radicado["radi_resumen"];
        $codi_texto     = $radicado["radi_codi_texto"];
        $concopiaa      = str_replace("'","",$radicado["cca"]);
        $depe_actu      = $radicado["depe_actu"];
        $usua_actu      = $radicado["usua_actu"];
        $raditipo       = $radicado["radi_tipo"];
        $chk_plantilla  = $radicado["usar_plantilla"];
        $cmb_texto      = $radicado["ajust_texto"];
        $radi_tipo_impresion = $radicado["radi_tipo_impresion"];
        $cod_codi = 0+$radicado["cod_codi"];
        $cat_codi = 0+$radicado["cat_codi"];
        $refe_padre = $radicado["radi_padre"];
        
        $radi_asoc = $radicado["radi_nume_asoc"];
        //echo $_GET['radi_lista_nombre'];
        if ($_GET['radi_lista_nombre']=='')
        $radi_lista_dest     = $radicado["radi_lista_dest"];
        else
            $radi_lista_nombre     = '';
        //Verifico nùmero de destinatarios
        $destinatarios=$radicado["usua_dest"];
        
        $cadena1= $destinatarios;
        $cadena2= "--";
        if (strrpos($cadena1, $cadena2))
            $VariosDest="1";
        else
            $VariosDest="0";


        if ($accion=="Responder"){      //usua1=destinatario    usua2=remitente
            $documento_us1 = $radicado["usua_rem"];
            //$documento_us2  = $radicado["usua_dest"];
            if ($raditipo==2) $raditipo=1; else $radi_tipo=3;
            $referencia= $textrad;
        }else{
            include_once "$ruta_raiz/seguridad/obtener_nivel_seguridad.php";
            $nivel_seguridad_documento = obtener_nivel_seguridad_documento($db, $nurad);
            $referencia 	= $radicado["radi_referencia"];
            $radi_nume_padre = $radicado["radi_padre"];
            $fecha_doc	= $radicado["radi_fecha"];
            $fecha_doc	= substr($fecha_doc,8 ,2)."-".substr($fecha_doc,5 ,2)."-".substr($fecha_doc,0 ,4);
            $desc_anex	= $radicado["radi_desc_anexos"];
            $estado		= $radicado["estado"];
            $documento_us1 	= $radicado["usua_dest"];
            $documento_us2 	= $radicado["usua_rem"];
            $chk_ocultar_recorrido = $radicado["ocultar_recorrido"];
            $txt_usua_redirigido = $radicado["usua_redirigido"];
            if ($radicado["estado"] != 1 or $_SESSION["usua_codi"] != $radicado["usua_actu"]) {
                die (html_error("Usted no tiene los permisos suficientes para modificar este documento."));
            }
    	}
        
        if(trim($radi_lista_dest)!="")//si tiene listas
        {
            //fecha de actualizacion del documento
            
            
            $ultimaActualizacion = ObtenerUltimaFecha($nurad, '11', $db);
            if ($ultimaActualizacion=='')
            $ultimaActualizacion = ObtenerUltimaFecha($nurad, '2', $db);
            $codList = str_replace('--', ',', $radi_lista_dest);
            $codList = str_replace('-', '', $codList);
            $cuentaListas = count(explode(',',$codList));
            
            $msj_lista = "La(s) lista(s) ";
            if($cuentaListas>1)//si es mas de una lista
            {
            foreach (explode(',',$codList) as $lista) {//for each listas                                 
                    $datosLista = ObtenerDatosLista($lista,$db);                    
                    $radi_lista_nombre .= $datosLista['nombre'] . ', ';                                    
                    if($ultimaActualizacion<$datosLista['fecha'])
                        $msj_lista .= $datosLista['nombre'] . ', ';
                }//for ecach listas
                $msj_lista .= " ha(n) sido modificada(s),";
            }//si es mas de una lista
            else
            {//si es una lista
                $codList = str_replace('-', '', $codList);
                $datosLista = ObtenerDatosLista($codList,$db);                
                $radi_lista_nombre .= $datosLista['nombre'];
                //echo '<script>alert("'.$ultimaActualizacion."-".$datosLista['fecha'].'")</script>';                
                if (trim($ultimaActualizacion)!=''){
                    if($ultimaActualizacion<$datosLista['fecha'])
                        $msj_lista .= $datosLista['nombre'] . ', ';                    
                }
                $msj_lista .= " ha(n) sido modificada(s),";
            }//si es una lista
            
            if($msj_lista != 'La(s) lista(s)  ha(n) sido modificada(s),' and $radicado["estado"] == 1)
                echo '<script>alert("'.$msj_lista.' por favor, actualizar los destinatarios.");</script>';
        }//si tiene listas
    }//editar responder
    $estado=$ent;
    if(!$fecha_doc) $fecha_doc = date("d-m-Y");
    
    
    $compResponder = $_GET['compResponder'];
if ($compResponder==1){//VIENE DE LA BANDEJA COMPARTIDA Y DEBE REALIZAR LA REASIGNACION    
    $observaJefe ='Documento tomado por '.$_SESSION["usua_codi"].' de la Bandeja de Documentos Recibidos de '.$_SESSION['usua_codi_jefe'];    
}


////////////////////////////	CABECERA	////////////////////////
?>
<script type="text/javascript">
    var dateAvailable1 = new ctlSpiffyCalendarBox("dateAvailable1", "formulario", "fecha_doc","btnDate1","<?=$fecha_doc?>",scBTNMODE_CUSTOMBLUE);
</script>

<body bgcolor="#FFFFFF" onLoad="pestanas(1); cambiar(); mostrarOpcImp();">
    <input type="hidden" id="rad_tipo_imp" value="<?=$radi_tipo_impresion?>"/>    
   <div id="spiffycalendar" class="text"></div>
<?php
$var_envio="ent=$ent&nurad=$nurad&textrad=$textrad&accion=$accion";
?>
<form action="funciones_NEW.php?<?=$var_envio?>" ENCTYPE="multipart/form-data" method="post" name="formulario" id="formulario"> <!--onChange="document.formulario.fl_modificar1.value=1;"-->
<input type="hidden" name="hidden_tipo_anterior" id="hidden_tipo_anterior" value="<?=$raditipo?>"/>
<input type="hidden" name="hidden_radi_actual" id="hidden_radi_actual" value=""/>
<input type="hidden" name="hidden_actualiza_opciones" id="hidden_actualiza_opciones" value=""/>
<input type="hidden" name="hidden_titulo_anterior" id="hidden_titulo_anterior" value=""/>
<input type="hidden" name="bandera_cambiarop" id="bandera_cambiarop" value="1"/>
<input type="hidden" name="txt_refeResponder" id="txt_refeResponder" value="<?=$txt_refeResponder?>"/>


<!--table width="99%"  border="0" align="center" cellpadding="4" cellspacing="5" class="borde_tab">
<tr>
        <td width="94%" class="titulos2" align="center">
            <b>MODULO DE REGISTRO DE <?=trim(strtoupper($_SESSION["descRadicado"]))?>S</b>
    <?php
        if($nurad)
        {
                if ($accion=="Responder") {
			echo "<br>Respuesta al ".$_SESSION["descRadicado"]." No " .$textrad;
			$ent=1;
		} else {
			echo "<br>".$_SESSION["descRadicado"]." No " .$textrad;
		}
	}
        ?>
        </td>
</tr>
</table-->


<?php
//pestañas
	$imgTp1="infoGeneral";
	$imgTp2="documentos";
        $imgTp3="OpcImpresion";
?>
<table width="99%" align="center" border="0" cellspacing="4" cellpadding="0">
<tr>
      <td width="50%" align="left">
          <input type="button" name="btn_usuarios" id="btn_usuarios" value="Buscar De/Para" class="botones_largo" title="Buscar por nombre o cédula, al Remitente, Destinatarios del Documento, y/o Con copia a"
                            onClick="Start('buscar_usuario_nuevo.php');">&nbsp;&nbsp;
        <?php if ($ent != 2) { ?>
            <input type='button' onClick='grabar_doc(1)' name='Submit1' value='Vista Previa' class='botones' title="Graba el documento y genera una vista previa del mismo">&nbsp;&nbsp;
            
        <? } ?>
        <input type='button' onClick='grabar_doc(2)' name='Submit2' value='Aceptar' class="botones" title="Graba el documento y pasa a la página de consulta del documento">&nbsp;&nbsp;
        <input type='button' onClick='cancelar()' name='Submit3' value='Cancelar' class="botones" title="Sale de la creación de documentos sin grabar">

    </td>
</tr>

    <tr valign="bottom">
        <td  width="50%" height="10"  class="listado2">
        <a href="javascript:;"  onClick="pestanas(1);" class="etextomenu" title="Información general del Documento"><img src="../imagenes/<?=$imgTp1?>.gif" width="110" height="25" border="0" id="etiqueta1" alt="informacion"><img src="../imagenes/<?=$imgTp1?>_R.gif" width="110" height="25" border="0" id="etiqueta1_R" alt="informacion"></a>
        <a href="javascript:;"  onClick="pestanas(2);" class="etextomenu" title="Documentos que se colocan como Anexos al Documento"><img src="../imagenes/<?=$imgTp2?>.gif" width="110" border="0"  id="etiqueta2" alt="anexos"><img src="../imagenes/<?=$imgTp2?>_R.gif" width="110" border="0"  id="etiqueta2_R" alt="anexos"></a>
        <?php if($ent!= 2){//|| $raditipo==2?>      
            <a href="javascript:;"  onClick="cambiar(); pestanas(3); buscarDatosDestinatario(<?=$radi_tipo_impresion?>); verOpcionNota(<?=$opc_nota_inicial?>);" class="etextomenu" title="Opciones de Impresión Oficios"><img src="../imagenes/<?=$imgTp3?>.gif" width="110" border="0"  id="etiqueta3" alt="opc. impresion"><img src="../imagenes/<?=$imgTp3?>_R.gif" width="110" border="0"  id="etiqueta3_R" alt="opc. impresion"></a>
        <?php }?>
        </td>
        <td class="listado2_ver" align="right">
            <font size="2" color="black"><?php 
            if (substr($textrad,0,4)=='TEMP')
            echo "Número de Documento: ".$textrad;
            ?></font>
        </td>

    </tr>
    <tr valign="top">
        <td height="95" colspan="2">
             <input type="hidden" name="opc_grab" value=""/>
        <div id='cuerpo_documento'>           
            <input type="hidden" name="opc_ver" value=""/>
            <input type="hidden" name="fl_modificar1" value="0"/>
            <input type="hidden" name="fl_modificar2" value="1"/>             
            <textarea id="documento_us1" name="documento_us1" style="display: none" cols="1" rows="1"><?=$documento_us1?></textarea>
            <textarea id="documento_us2" name="documento_us2" style="display: none" cols="1" rows="1"><?=$documento_us2?></textarea>
            <textarea id="concopiaa" name="concopiaa" style="display: none" cols="1" rows="1"><?=$concopiaa?></textarea>
            <input type="hidden" name="radi_padre" value="<?=$radi_padre?>"> 
            
            <input type="hidden" name="radi_lista_nombre" id="radi_lista_nombre" value="<?=$radi_lista_nombre?>">
            <input type="hidden" name="hidd_ent" id="hidd_ent" value="<?=$ent?>">

            <!-- Cuando se desea un tipo de impresion diferente en caso de que los destinatarios pertenescan a una lista. -->
            <input type="hidden" id="radi_lista_dest" name="radi_lista_dest" value="<?=$radi_lista_dest?>">

            <input type="hidden" name="token" value="<?php echo $token; ?>" />

        <table width=100% border="0" class="borde_tab" align="center" id="cuerpo">
          <input type="hidden" name="flag_inst" id="flag_inst" value="<?=$flag_inst?>"/>
        <?php
        /////////////////////////////////        USUARIOS        //////////////////////////////
        $ifr_usr_env="lista_concopiaa.php?documento_us1=$documento_us1&documento_us2=$documento_us2&concopiaa=$concopiaa&radi_lista_nombre=$radi_lista_nombre&ent=$ent";
        
        ?>
        <tr>
            <td colspan="5">                
                <iframe border="0" class="borde_tab" align="center" height="110" width="100%" name="ifr_usr" id="ifr_usr" src="<?=$ifr_usr_env?>">
                    Su navegador no soporta iframes, por favor actualicelo.</iframe>                
                <br />
            </td>
        </tr>
<?php
        if ($_SESSION["tipo_usuario"]==2) { // Si es un ciudadano el que genera el documento
            echo "<input type='hidden' name='raditipo' id='raditipo' value='7'>";
            echo "<input type='hidden' name='fecha_doc' id='fecha_doc' value='$fecha_doc'>";
            echo "<input type='hidden' name='cat_codi' id='cat_codi' value='0'>";
            echo "<input type='hidden' name='cod_codi' id='cod_codi' value='0'>";
        } else {
?>
        <tr valign="middle">
            <?php  if ($ent==2) { ?>
                    <td width="15%" class="listado1_ver">Fecha Doc: (dd/mm/aaaa)</td>
                    <td width="60%" class="listado1" colspan="4">
                        <input type="hidden" name='raditipo' id='raditipo' value='2'>
                        <script type="text/javascript">
                                dateAvailable1.date = "<?=$fecha_doc?>";
                                dateAvailable1.writeControl();
                                dateAvailable1.dateFormat="dd-MM-yyyy";
                        </script>

            <?php  } else {?>
                    <td width="15%" class="listado1_ver">Tipo de Documento:</td>
                    <td width="55%" class="listado1" colspan="4">
                        <input type="hidden" name='fecha_doc' value='<?=$fecha_doc?>'>
            <?php
                    $query = "Select trad_descr, trad_codigo from tiporad where trad_tipo='S' and trad_estado=1 and trad_inst_codi in (0,".$_SESSION["inst_codi"].") order by 1";
                    $rs=$db->conn->query($query);
                    $tmp = "";
                   // if ($_SESSION["inst_codi"]==2 || $_SESSION["inst_codi"]==3) $tmp = "cambio_cuerpo(\"$codi_texto\")";
                    if(!$rs->EOF)
                        print $rs->GetMenu2("raditipo", $raditipo, "", false,"","class='select' id='raditipo' onChange='mostrarOpcImp(); $tmp'" );
                 }
            ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <font class="listado1_ver">Categor&iacute;a:</font>&nbsp;&nbsp;
                <?php
                    $queryCat = "Select cat_descr, cat_codi from categoria order by 1";
                    $rsCat=$db->conn->query($queryCat);
                    if(!$rsCat->EOF)
                        print $rsCat->GetMenu2("cat_codi", 0+$cat_codi,  "", false,"","class='select' " );
                ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php
                    $queryCod = "Select cod_descripcion, cod_codi from codificacion where inst_codi in (0,".(0+$_SESSION["inst_codi"]).") order by 1";
                    $rsCod=$db->conn->query($queryCod);
                    if(!$rsCod->EOF)
                    {
                        ?>
                        <font class="listado1_ver">Tipificaci&oacute;n:</font>&nbsp;&nbsp;
                        <?php
                        print $rsCod->GetMenu2("cod_codi", 0+$cod_codi,  "", false,"","class='select' style='width:220px' " );
                    }
                ?>
                </td>
                </tr>
<?php  } //IF CIUDADANO  ?>
                <tr>
                <td width="15%" class="listado1_ver">No. Referencia:</td>
                <td width="55%" class="listado1" colspan="4">
                    <?php
                    //CONTROL CAJA DE TEXTO REFERENCIA READONLY
                      if ($ent==2)
                          if ($referencia=='')
                            $readReferencia ='';
                          else
                              if($raditipo==2)
                                  $readReferencia="";
                              else
                              $readReferencia = "readonly";  
                      else{    
                          if($raditipo==2)
                                  $readReferencia="";
                          else
                            $readReferencia = "readonly";                        
                      }
//                     
                           
                           ?>
                        <input alt="Ingrese un Documento asociado" name="referencia" id="referencia" type="text" maxlength="80" size="60" class="tex_area"
                            value="<?=$referencia?>" <?=$readReferencia?> />
                       
                         <?php 
                         
                         if ($referencia!=''){//si existe referencia
                         $sql="select radi_nume_radi,radi_path from radicado where radi_nume_text = '".$referencia."'";                         
                         
                           $rs=$db->conn->query($sql);
                           while(!$rs->EOF)
                           {
                               $verrad1=$rs->fields["RADI_NUME_RADI"];
                               $path=$rs->fields["RADI_PATH"];
                               $rs->MoveNext();
                           }
                           $ventana = "../documento_online.php?verrad=$nurad&textrad=$textrad&nReferencia=$verrad1&menu_ver=3&irVerRad=1";
                           $ventana = "'".$ventana."'";
                           
                           if ($path){//si tiene path                           
                               $url = "$ruta_raiz/bodega".$path;
                           if (file_exists($url)){//si existe url
                             if ($verrad1!=''){//si existe verrad1    
                                  if (substr(trim($textrad),0,4)=='TEMP')
                                    echo '<a href="javascript:;" onClick="ventanaNueva('.$ventana.');" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/zoom_in.png" width="17" height="17" alt="Ver en línea Documento de Referencia" border="0"><span>Ver en línea Documento de Referencia</span></a>';
                                  else
                                      if (substr(trim($textrad),-1)=='E')
                                      echo '<a href="javascript:;" onClick="ventanaNueva('.$ventana.');" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/zoom_in.png" width="17" height="17" alt="Ver en línea Documento de Referencia" border="0"><span>Ver en línea Documento de Referencia</span></a>';
                                  
                                $verrad1="'".$verrad1."'";$textrad1="'".$referencia."'";$ruta_raiz1="'".$ruta_raiz."'";//para javascript
                               $path="'".str_replace(" ", "_", $path)."'";
                               $variables = $ruta_raiz1.",".$verrad1.",".$textrad1.",".$path;
                               echo '&nbsp<a href="javascript:;" onClick="vista_previa2('.$variables.');" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/document_down.jpg" width="17" height="17" alt="Descargar Documento de Referencia" border="0"><span>Descargar Documento de Referencia</span></a>';
                              }//si existe verrad1
                             }//si existe url
                           }//si tiene path
                         }//si existe referenfica
                        if ($_SESSION["tipo_usuario"]==1){//solo funcionarios
                             //REFERENCIA MOSTRAR ICONO
                             $nuradAso="'".$nurad."'";
                             $refe_padreAso="'".$refe_padre."'";
                             if ($ent==2){//externo
                                 if ($radi_tipo==2)
                                    echo '&nbsp<a href="javascript:;" onClick="grabar_doc(3); AsociarDocumento('.$nuradAso.','.$refe_padreAso.','.$ent.');" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/document_attach.jpg" width="17" height="17" alt="Asociar Documento" border="0"><span>Asociar Documento</span></a>';
                                 elseif($radi_tipo==''){
                                     if($textrad!='')
                                     echo '&nbsp<a href="javascript:;" onClick="grabar_doc(3); AsociarDocumento('.$nuradAso.','.$refe_padreAso.','.$ent.');" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/document_attach.jpg" width="17" height="17" alt="Asociar Documento" border="0"><span>Asociar Documento</span></a>';
                                 }
                            }else{                             
                                    if ($refe_padre==''){//NUEVO
                                        if (substr(trim($textrad),0,4)=='TEMP')//SI YA ESTA GUARDADO
                                        echo '&nbsp<a href="javascript:;" onClick="grabar_doc(3); AsociarDocumento('.$nuradAso.','.$refe_padreAso.',2);" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/document_attach.jpg" width="17" height="17" alt="Asociar Documento" border="0"><span>Asociar Documento</span></a>';
                                    }//SI ES DE REFERENCIA
                                    else                                    
                                        echo '&nbsp<a href="javascript:;" onClick="grabar_doc(3); AsociarDocumento('.$nuradAso.','.$refe_padreAso.',3);" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/document_attach.jpg" width="17" height="17" alt="Asociar Documento" border="0"><span>Asociar Documento</span></a>';
                                
                            }//FIN ELSE
                        }
                         ?>
                </td>
                </tr>

                <tr>
                    <td class="listado1_ver">Asunto:</td>
                    <td class="listado1" colspan="4">
                        <textarea  name="asunto" cols="150" class="tex_area" rows="1" onKeyUp='this.value=this.value.substring(0,350)' ><?php echo $asunto ?></textarea>
			
                        <!-- Copiar notas cuando en un doc nuevo -->
                        <input id="notas" name="notas" type="hidden" value="<?php echo $notas ?>">
                    </td>
                </tr>

                <tr>
            <?php  if ($ent==2) {
                    $sql = "select usua_apellido || ' ' || usua_nomb || 
                    case when usua_subrogado<>1 then '  (Subrogante)' else '' 
                    end as usua_nombre
                    , usua_codi from usuario where inst_codi = ".$_SESSION["inst_codi"].
                           " and usua_esta=1 and usua_codi 
                               in (select usua_codi from permiso_usuario where id_permiso=6) 
                               order by 1";                    
                    $rs=$db->conn->query($sql);

                    if($rs and !$rs->EOF)
                    {
                        echo '<tr>
                            <td class="listado1_ver">Dirigir documento a:</td>
                            <td class="listado1" colspan="4">';
                        print $rs->GetMenu2("txt_usua_redirigido", $txt_usua_redirigido,  "0:No redirigir", false,"","class='select' style='width:220px' " );
                        echo "</td></tr>";
                    }
              } ?>

                <!--<tr>
                    <td class="listado1_ver">Descripci&oacute;n de Anexos:</td>
                    <td class="listado1" colspan="4">
                        <textarea name="desc_anex" cols="100" class="tex_area" rows="1" onKeyUp='this.value=this.value.substring(0,100)'><?php echo $desc_anex ?></textarea>
                    </td>
                </tr>-->
                <?php
                if ($ent==1) {
                ?>
               </tr>
                <?php
                }
                ?>
                <tr>
                    <td class="listado1" colspan="5">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <p style="font-weight : bold; font-family: Arial, Helvetica, sans-serif; font-size: 10px;">&nbsp;&nbsp;
                        <?php if ($ent==2) echo "Resumen:"; else echo "Cuerpo del Documento:"?>
                        </p>
                    </td>
                    <td align="left" valign="middle">
                        <table border="0" width="100%"  >
                            <tr>
                                <td  >
        			
            <?php
            if ($nurad != "") {
                $query = "select TO_CHAR(text_fecha,'YYYY-MM-DD HH24:MI AM'), text_codi from radi_texto where radi_nume_radi=$nurad";
                $rs=$db->conn->query($query);
                if(!$rs->EOF) {
                    echo "<b>Versi&oacute;n: &nbsp;&nbsp;&nbsp;</b>";
                    print $rs->GetMenu2("codi_texto", $codi_texto, "", false,"","class='select' onChange='cambio_cuerpo(this.value)'" );
                }
            }
            ?>
                                </td>
                                <td width="35%" align="left" class="listados1" valign="middle">&nbsp;

            <?php
            if ($_SESSION["perm_borrar_recorrido"] == 1) {
                echo '<input type="checkbox" name="chk_ocultar_recorrido" id="chk_ocultar_recorrido" value="1"';
                if ($chk_ocultar_recorrido == "1") echo ' checked';
                echo '><b>Borrar recorrido luego de la firma del documento.</b>';
            }
            ?>
                                </td>
                                <td width="30%" align="right" class="listados1" valign="middle">&nbsp;
            <?php
            // Texto de la última sumilla
            $txt_sumilla = "";
            
            if ($nurad != "") {
                $query1 = "select h.hist_fech,h.hist_obse, us.usua_nomb, us.usua_apellido from hist_eventos h left outer join usuarios us on us.usua_codi = h.usua_codi_ori where radi_nume_radi=$nurad and sgd_ttr_codigo=9 order by hist_codi asc";
                if ($radi_nume_padre!='')
                $queryHijo = "select h.hist_fech, h.hist_obse,h.hist_referencia,us.usua_nomb, us.usua_apellido 
                from hist_eventos h left outer join usuarios us on us.usua_codi = h.usua_codi_ori where radi_nume_radi=$nurad and sgd_ttr_codigo=9 order by hist_codi asc";
               
            }
            if ($txt_sumilla=="" and $radi_nume_padre!="") {               
                $query1 = "select h.hist_fech,h.hist_obse, us.usua_nomb, us.usua_apellido from hist_eventos h left outer join usuarios us on us.usua_codi = h.usua_codi_ori where radi_nume_radi=$radi_nume_padre and sgd_ttr_codigo=9 order by hist_codi asc";
                //$query = "select hist_fech, hist_obse from hist_eventos where radi_nume_radi=$radi_nume_padre";
               //echo $query; 
            }     
            
            //COMENTARIOS DE PADRE
            $rs1=$db->conn->query($query1);            
           
            $txt_sumillaob = trim($rs1->fields["HIST_OBSE"]);//si no existe observaciones no dibuja            
            if ($txt_sumillaob!='')
                $sicrea=1;
            else 
                $sicrea=0;

            if ($nurad != "") {            
                $rs=$db->conn->query($query1);
                ?>
                <?php 
                if ($sicrea==1){
                    ?>
                <img src='../iconos/posit.jpg' title="Ver comentarios de la &uacute;ltima reasignaci&oacute;n" onclick="mostrar_div_sumillas('1')" alt="sumillas">
                <div id="div_sumillas" class="cal-TextBox" style="border: thin solid #006699; width: 100%; display: none; text-align: left;">
                 <table border="0">
                  <?php } ?>
                <?php
                 while(!$rs->EOF) {                    
                     if (trim($rs->fields["HIST_OBSE"])!=''){//si existe comentario
                ?>
                  <tr><?php //despliego los comentarios, fecha/observacion/usuario ?>
                  <td width="98%"><?= "- ".trim(substr($rs->fields["HIST_FECH"],0,10))." / ".trim($rs->fields["HIST_OBSE"])." / Por: ".trim($rs->fields["USUA_NOMB"])." ".trim($rs->fields["USUA_APELLIDO"])?></td>
                  </tr>                                        
                <? //
                }//si existe comentario
                $rs->MoveNext();
               }//rs
           
            ?>
            <?php
            //comentarios hijo            
            if ($queryHijo!=''){//query
                //COMENTARIOS DE PADRE
            $rs2=$db->conn->query($queryHijo);            
            
            $txt_sumillaobh = trim($rs2->fields["HIST_OBSE"]);
            if ($txt_sumillaobh!='')
                $sicreah=1;
            else 
                $sicreah=0;
            $rsq=$db->conn->query($queryHijo);
            ?>
             <?php 
                if ($sicrea==0 and $sicreah==1){
                    ?>
                <img src='../iconos/posit.jpg' title="Ver comentarios de la &uacute;ltima reasignaci&oacute;n" onclick="mostrar_div_sumillas('1')" alt="sumillas">
                <div id="div_sumillas" class="cal-TextBox" style="border: thin solid #006699; width: 100%; display: none; text-align: left;">
                 <table border="0">
                <?php }?>
                <?php 
                 while(!$rsq->EOF) {                     
                     if (trim($rsq->fields["HIST_REFERENCIA"])!=''){//si existe comentario                        
                ?>
                  <tr><?php //despliego los comentarios, fecha/observacion/usuario ?>
                  <td width="98%"><?= "- ".trim(substr($rsq->fields["HIST_FECH"],0,10))." / ".trim($rsq->fields["HIST_OBSE"])." / Por: ".trim($rsq->fields["USUA_NOMB"])." ".trim($rsq->fields["USUA_APELLIDO"])?></td>
                  </tr>                                        
                <?php //
                }//si existe comentario
                $rsq->MoveNext();
               }//rs
            }//query hijo
            //fin comentarios hijo
            ?> 
                  <?php if($sicrea==1 or $sicreah==1){ 
                      //graficar boton cerrar
                      ?>                  
                   <tr>
                   <td width="2%"><img src="../imagenes/close_button.gif" alt="Cerrar" title="Cerrar" width="12px" height="12px" onclick="mostrar_div_sumillas('0')"></td>
                   </tr>
                   </table>
                </div>
                <?php } ?>
              <?php
              } //Fin del texto de la última sumilla ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listado1" colspan="4">
                        <center><div id="div_cuerpo"></div></center>
                    </td>
                </tr>
        </table>

        </div>
        <div id='cuerpo_anexos'>
        <table width=100% border="0" class="borde_tab">
            <tr>
                <td class="listado2_ver" width="15%">&nbsp;&nbsp;Descripci&oacute;n de Anexos:</td>
                <td class="listado1" colspan="4">
                    <textarea name="desc_anex" cols="150" class="tex_area" rows="1" onKeyUp='this.value=this.value.substring(0,100)'><?php echo $desc_anex ?></textarea>
                </td>
            </tr>
        </table>
        <br>
        <?php
        ///////////////////////////     ANEXOS  ///////////////////////////
        if ($accion == "Responder") {
            //include a otro archivo, consultar del arreglo $radicado
            include "cargar_anexos_responder.php";
        }
        $boton_anexos="No";
        if ($accion=="Responder" || $accion=="Nuevo") $nurad="";
        $verrad = $nurad;
        include "$ruta_raiz/anexos.php";
        //include "$ruta_raiz/archivos_anexos/anexos.php";
        ?>
        </div>     
            
        <div id="opciones_impresion">
        <?php if ($ent!=2) {?>
        <table width=100% border="0" >
        <tr>
                <td align="left" class="listado1_ver" <? if ($_SESSION["tipo_usuario"]==2) echo "style='display:none'" ?>>Tipo de Impresi&oacute;n: </td>
                <td colspan="1" <? if ($_SESSION["tipo_usuario"]==2) echo "style='display:none'" ?>>
                    <select name="radi_tipo_impresion" id="radi_tipo_impresion" class='select' style="width:490px"  onchange="cambiar(); histop('n',this,<?="'".$radi_tipo_impresion."'"?>);" onclick="limpiardiv()">
                        <option value="1" <?php if($radi_tipo_impresion=="1") echo "selected"; ?> onclick="buscarDatosDestinatario(1);">
                            <?php 
                            //funcion de tx/tx_actualiza_opcion_impresion
                            echo descimpresion(1);?> 
                        </option>
                        <option id="opcCargo" style="display:none" value="4" <?php if($radi_tipo_impresion=="4") echo "selected"; ?> onclick="buscarDatosDestinatario(4);">
                            <?php echo descimpresion(4);?>
                        </option>
                        <option id="opcInstitucion" style="display:none" value="5" <?php if($radi_tipo_impresion=="5") echo "selected"; ?> onclick="buscarDatosDestinatario(5);">
                            <?php echo descimpresion(5);?>
                        </option>                        
                        <option value="6" <?php if($radi_tipo_impresion=="6") echo "selected"; ?> onclick="buscarDatosDestinatario(6);">
                            <?php echo descimpresion(6);?>
                        </option>
                        <option value="2" <?php if($radi_tipo_impresion=="2") echo "selected='selected'"; ?>  onclick="buscarDatosDestinatario(2);">
                        <?php echo descimpresion(2);?>
                        </option>
                        <option id="opcListas" value="3" <?php if($radi_tipo_impresion=="3") echo "selected"; ?> onclick="limpiardiv();">
                        <?php echo descimpresion(3);?>
                        </option>
                        <option value="999" <?php if($radi_tipo_impresion=="999") echo "selected"; ?> onclick="limpiardiv()">
                        <?php echo descimpresion(999);?>
                        </option>
                    </select>
                </td>        
               <td class="listado1_ver" colspan="3">
                    <?php
                    //if ($ent==1) {
                    $funcionjavaat = 'histop(\'m\',this,'.$cmb_texto.');';
                    ?>
                    Ajustar Texto: &nbsp;&nbsp;&nbsp;&nbsp;<select name="cmb_texto" id="cmb_texto" class='select' onchange="<?=$funcionjavaat?>">
                        <?php
                        $valTxt=120;
                        
                        for($i=0; $i<9; $i++)
                        {
                            if($cmb_texto==$valTxt)
                                echo "<option value='$valTxt' $funcionjava selected>$valTxt %</option>";
                            else
                                 echo "<option value='$valTxt' $funcionjava >$valTxt %</option>";
                            $valTxt = $valTxt - 5;
                        }
                        ?>
                    </select>                    
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php
                        if ($ent==1) {
                            if ($chk_plantilla==1)
                                $con_plantilla='checked';
                            else
                                $con_plantilla="0";
                            ?>
                            <input type="checkbox" name="chk_plantilla" id="chk_plantilla" value="1"  <?php echo $con_plantilla?> onclick="histop('l',this,<?="'".$con_plantilla."'"?>);"/> Utilizar Plantilla
                        <?php }
                    ?>
                </td>
            </tr>
            
            <tr><td colspan="3">                    
                    <div id="div_ver_datos" name="div_ver_datos"></div>
                </td>                
            </tr>
            
        </table>
        <?}?>

        <div id="div_estilo_impresion" style="display: none"></div>
         <div id="div_modificar_op"></div>
        <input type="hidden" name="NumDest" id="NumDest"  value="<?=$VariosDest?>">
        </div>
    <?php if (isset($mensaje)) { ?>
        <br />
        <table width="100%">
                    <tr>
                        <td class="listado5" align="center"><img src='../iconos/img_alerta_2.gif' alt="alerta">
                            <font color="red" face='Arial' size='3'><?=$mensaje?></font>
                        </td>
           </tr>
        </table>
    <?php      }       ?>
        <script type="text/javascript">
            //refrescar_pagina();
            cambio_cuerpo('<?=$codi_texto?>');
        </script>
        </td>
   </tr>
</table>
<br />
<?php $numradop = $nurad; ?>
<input type="hidden" id="num_rad" name="num_rad" value="<?=$numradop;?>"/>
<div id="div_borrar_opc_imp" style="display: none"></div>
<div id="div_historico_imp" style="display: none"></div>
 <?php 
 if ($numradop!=''){
 $rsOpcImprBusq = ObtenerDatosOpcImpresion($numradop,$db); 
 }?>
 <input type="hidden" id="codiOpcImp" name="codiOpcImp" value="<?=$rsOpcImprBusq['OPC_IMP_CODI']?>"/>
 <tr>
 <input type="hidden" id="hidden_acciones_datos" name="hidden_acciones_datos"/>
 </tr>
</form>

</body>
</html>
