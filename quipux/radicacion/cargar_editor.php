<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

session_start();
$ruta_raiz = "..";
require "$ruta_raiz/rec_session.php";
include_once "$ruta_raiz/js/fckeditor/fckeditor.php";
require_once "$ruta_raiz/funciones.php";
$fecha=date('Y-m-d');
$fecha=fechaATexto($fecha);
//FECHA EN TEXTO

    if(isset($_POST["codi_texto"]))
    {
        $sql = "select text_texto from radi_texto where text_codi=".$_POST["codi_texto"];
        $rs = $db->conn->Execute($sql);
    }
    //Para añadir texto de En respuesta al Documento No.
    if (isset($_POST['referencia']))
    {
        $sqlRef = "select radi_cuentai from radicado where radi_nume_text='".$_POST["referencia"]."'";
        $rsRef = $db->conn->Execute($sqlRef);
        $referenciaExterno = $rsRef->fields["RADI_CUENTAI"];
        if (trim($referenciaExterno)!='')
            $referenciadoc = "En respuesta al Documento No. $referenciaExterno";
        else
            $referenciadoc = "En respuesta al Documento No. ".$_POST['referencia'];
    }
    $raditexto = "";
    if (trim($rs->fields["TEXT_TEXTO"]) != "")
        $raditexto = $rs->fields["TEXT_TEXTO"];
    else {
        if ($tipo_docu == "1")
            $raditexto = "De mi consideraci&oacute;n:<br><br><br><br>Con sentimientos de distinguida consideraci&oacute;n.<br>&nbsp;<br>&nbsp;";
        elseif($tipo_docu == "8"){//resolucion
            $raditexto = "<center>CONSIDERANDO</center><br>&nbsp;<br><br>&nbsp;<br><br>&nbsp;<br>";
            $raditexto.= "<center><font color='black'><b>RESUELVE:</b></font></center><br>&nbsp;<br><br>&nbsp;<br>";
            $raditexto.= "<center><font color='black'><b>DISPOSICIÓN FINAL</b></font></center><br>&nbsp;<br><br>&nbsp;<br>";
            $raditexto.= "Dada y firmada en el Despacho de ".$_SESSION["inst_nombre"];
            $raditexto.= ", en Quito DM, ".fechaAtexto(date('Y-m-d'));
        }
    }
    if ($accion == "Responder" and $tipo_docu == "1") {
        $raditexto = "De mi consideraci&oacute;n:<br><br>$referenciadoc<br><br>$raditexto<br><br>Con sentimientos de distinguida consideraci&oacute;n.<br>&nbsp;<br>&nbsp;";
    }

    if ($_POST["esphone"]==1){
$oFCKeditor = new FCKeditor('raditexto') ;
$oFCKeditor->BasePath = "$ruta_raiz/js/fckeditor/" ;
$oFCKeditor->Height = "350" ;
$oFCKeditor->Width = "920";
$oFCKeditor->AutoDetectLanguage = false;
$oFCKeditor->DefaultLanguage = "es";
$oFCKeditor->Config['EnterMode'] = 'br';
$oFCKeditor->Value = stripslashes($raditexto) ;
$oFCKeditor->Create() ;
    }else{
        if ($rs->fields["TEXT_TEXTO"]){
        $txtmobil = array("<br />","<br>");        
        $raditexto=str_replace($txtmobil, '', $raditexto);
        }else{
            $raditexto="";
        }
        echo "<textarea id='raditexto' name='raditexto' cols='100' rows='10'>$raditexto</textarea>";
    }

?>
