<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
//die ("hola");

  $ruta_raiz = ".";
  session_start();
  include_once "$ruta_raiz/rec_session.php";
  if (isset ($replicacion) && $replicacion) $db = new ConnectionHandler("$ruta_raiz","busqueda");
  $txt_fecha_desde = trim(limpiar_sql($_GET['txt_fecha_desde']));
  $txt_fecha_hasta = trim(limpiar_sql($_GET["txt_fecha_hasta"]));
  $estado = 0+$_GET["estado"];
  $busqRadicados = trim(limpiar_sql($_GET["busqRadicados"]));
  $carpeta = 0 + $_GET["carpeta"];
  $tipoLectura = 0+$_GET["tipoLectura"];
  $tarea_tipo = 0+$_GET["slc_tarea_tipo"];
  $tarea_estado = 0+$_GET["slc_tarea_estado"];

  $slc_tipo_fecha = 0 +$_GET['slc_tipo_fecha'];
 
  $encabezado = "carpeta=$carpeta&";

  $whereFiltro = "";
  if ($carpeta!=12 and $carpeta!=13)
  if($tipoLectura!='2') $whereFiltro .= " and b.radi_leido=$tipoLectura ";
  if ($busqRadicados != "") {
  $whereFiltro .= " and (UPPER(radi_nume_text) like '%".strtoupper($busqRadicados)."%' 
                     or UPPER(radi_asunto) like '%".strtoupper($busqRadicados)."%'
                     or UPPER( radi_cuentai) like '%".strtoupper($busqRadicados)."%'
                    ) ";
//                     or radi_fech_radi::text like'%".strtoupper($busqRadicados)."%'
  }


if($orden_cambio==1) {
    if(strtolower($orderTipo)=="desc")
	$orderTipo="asc";
    else
        $orderTipo="desc";
}
//if (!$orderTipo) $orderTipo="desc";

if (!$orderTipo) {
    $orderTipo="desc";
    if ($carpeta == "1" or $carpeta=="99") $orderTipo="asc";
}


//    if ($version_light) {
//        include "$ruta_raiz/include/query/queryCuerpo_light.php";
//    } else {
        include "$ruta_raiz/include/query/queryCuerpo.php";
//    }

    echo "<br>";
    $pager = new ADODB_Pager($db,$isql,'adodb', true,$orderNo,$orderTipo,true);
    $pager->checkAll = false;
    $pager->checkTitulo = true;
    $pager->toRefLinks = $linkPagina;
    $pager->toRefVars = $encabezado;
    $pager->descCarpetasGen=$descCarpetasGen;
    $pager->descCarpetasPer=$descCarpetasPer;
    $pager->Render($rows_per_page=20,$linkPagina,$checkbox=chkAnulados);
//echo $isql;

    if ($whereFiltro == "") {
        $contador = $pager->num_rows;
        if ($carpeta==12 || $carpeta==13 || $carpeta==15) {
            $sql="select count(radi_nume_radi) as contador, 12 from hist_eventos where usua_codi_ori=".$_SESSION["usua_codi"]." and sgd_ttr_codigo=9";
            if ($carpeta==13)
                $sql="select count(*) as contador, 13 from informados where usua_codi=".$_SESSION["usua_codi"];
            if ($carpeta==15) //tareas recibidas
                $sql="select count(1) as contador, 15 from tarea where estado=1 and ".$_SESSION["usua_codi"]." in (usua_codi_dest)";
            if ($carpeta==16) //tareas enviadas
                $sql="select count(1) as contador, 16 from tarea where estado=1 and ".$_SESSION["usua_codi"]." in (usua_codi_ori)";
            $rs = $db->query($sql);
            $contador = $rs->fields["CONTADOR"];
        }
    }
?>
    
<input type="hidden" name="txt_contador" id="txt_contador" value="<?=$contador?>">
<?php
$usuario=$_SESSION["usua_codi"];
//NO LEIDOS    
    if ($carpeta==1)
    $sqlL = "--
    select count(*) as \"leidos\", esta_codi from radicado where esta_codi in (1) and radi_leido = 0 and radi_usua_actu=$usuario group by esta_codi";
    if ($carpeta==2)
    $sqlL="select count(*) as \"leidos\", 2 as esta_codi from radicado where esta_codi=2 and radi_usua_actu=$usuario and radi_leido = 0 and radi_nume_radi not in (select radi_nume_radi from tarea where estado=1 and usua_codi_ori=$usuario) group by 2";    
    if ($carpeta==6)//Eliminados
    $sqlL="select count(*) as \"leidos\", esta_codi from radicado where esta_codi in (7) and radi_leido = 0 and radi_usua_actu=$usuario group by esta_codi";
    if ($carpeta==7)//No Enviados
    $sqlL="select count(*) as \"leidos\", esta_codi from radicado where esta_codi in (3) and radi_leido = 0 and radi_usua_actu=$usuario group by esta_codi";
    if ($carpeta==8)
    $sqlL="select count(*) \"leidos\", 6 as esta_codi from radicado where esta_codi=6 and radi_nume_radi=radi_nume_temp and radi_leido = 0 and radi_usua_actu=$usuario";
    if ($carpeta==10)//Archivados
    $sqlL="select count(*) as \"leidos\", esta_codi from radicado where esta_codi in (0) and radi_leido = 0 and radi_usua_actu=$usuario group by esta_codi";
    if ($carpeta==12)//Reasignados
    $sqlL="select count(*) as \"leidos\",12 as esta_codi from 
            (select radi_nume_radi from hist_eventos where usua_codi_ori=$usuario and sgd_ttr_codigo=9) as h2
            left outer join radicado as b on h2.radi_nume_radi=b.radi_nume_radi
            where b.radi_leido = 0";    
    if ($carpeta==13)//Informados
    $sqlL="select count(*) as \"leidos\", 13 as esta_codi from informados where usua_codi=$usuario and info_leido = 0";
    if ($carpeta==15)
    $sqlL="select -- Tareas Recibidas 
    count(1) as leidos, 15 as esta_codi from ( select b.radi_nume_radi from (select * from tarea where (usua_codi_dest=$usuario) and estado=1) 
    as t left outer join tarea_hist_eventos th on th.tarea_hist_codi=t.comentario_inicio left outer join radicado b on t.radi_nume_radi=b.radi_nume_radi where 1=1 and b.radi_leido=0 ) as a ";
    if ($carpeta==16)
    $sqlL="select -- Tareas Enviadas 
    count(1) as leidos, 16 as esta_codi from ( select b.radi_nume_radi from (select * from tarea where (usua_codi_ori=$usuario) and estado=1) as t left outer join tarea_hist_eventos th on th.tarea_hist_codi=t.comentario_inicio left outer join radicado b on t.radi_nume_radi=b.radi_nume_radi where 1=1 and b.radi_leido=0 ) as a";
    //echo $sqlL;

    $noleidos=0;
    if (!$version_light) {
        $rsL = $db->query($sqlL);
        if ($rsL->fields["LEIDOS"])
            $noleidos=$rsL->fields["LEIDOS"];
    }
?>
<input type="hidden" name="txt_noleidos" id="txt_noleidos" value="<?=$noleidos?>">
