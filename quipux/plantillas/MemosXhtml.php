<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
$td=substr(trim($verrad),-1);
if ($td=="1") $tipodoc="OFICIO";
if ($td=="3") $tipodoc="MEMORANDO";
if ($td=="4") $tipodoc="CIRCULAR";

$inicio = '
<html>
<head>
<title>.: ORFEO - VISTA PREVIA :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="margin: '.$margen.';">
';

$encabezadohtml = '
<table border="0" width="100%">
<tr>
<td align="left"><table><tr><td><center><img src="'.$ruta_raiz.'/plantillas/escudo.png" width="60" height="60" /><br/><br/>
<b>PRESIDENCIA DE LA REPÚBLICA</b>
</center></td></tr></table></td>

<td align="right"><table><tr><td><center><b><font size="1">Subsecretar&iacute;a de Inform&aacute;tica<br/>'.$tipodoc.' '.$registro["textrad"].'</font></b><br/>
	<img src="'.$file.'.png" width="250" height="50" /></center></td></tr></table>
</td></tr>
</table><br /><br />
';

$encabezadopdf = '
<script type="text/php">
    $max_x = $pdf->get_width();
    $max_y = $pdf->get_height(); 
    $obj_id = $pdf->open_object();
    $fuente = "serif";
    $cadena = "'.iconv("ISO-8859-1", "UTF-8", "PRESIDENCIA DE LA REPÚBLICA").'";
    $tamano = strlen($cadena)*7;
    if (($tamano % 2) == 1) $tamano++;
    $pdf->page_text (60 , 110, $cadena, $fuente, 12);
    $pdf->image("'.$ruta_raiz.'/plantillas/escudo.png", "png", ($tamano/2 + 35), 50, 50, 50);

    $cadena = "EDIFICIO CASA VIVANCO. Av Amazonas N39-82 y Pereira. 2do. piso.";
    $tamano = strlen($cadena)*3.5;
    $pdf->page_text (($max_x - $tamano)/2 , $max_y-80, $cadena, $fuente, 8);
    $cadena = "Telf: 2448-185,  2263-161,  2463-703,  2463-704";
    $tamano = strlen($cadena)*3.5;
    $pdf->page_text (($max_x - $tamano)/2 , $max_y-65, $cadena, $fuente, 8);
    $cadena = "www.presidencia.gov.ec";
    $tamano = strlen($cadena)*3.5;
    $pdf->page_text (($max_x - $tamano)/2 , $max_y-50, $cadena, $fuente, 8);

    $pdf->close_object();
    $pdf->add_object($obj_id, "all");

    $obj_id = $pdf->open_object();
    $pdf->image("'.$file.'.png", "png", $max_x-180, 50, 170, 40);
    $cadena = "'.iconv("ISO-8859-1", "UTF-8", "Subsecretaría de Informática").'";
    $tamano = 180 - (170-strlen($cadena)*3)/2;
    $pdf->page_text ($max_x - $tamano , 25, $cadena, $fuente, 8);
    $cadena = "'.$tipodoc.' '.$registro["textrad"].'";
    $tamano = 180 - (170-strlen($cadena)*4.5)/2;
    $pdf->page_text ($max_x - $tamano , 35, $cadena, $fuente, 8);
    $cadena = "{PAGE_NUM}/{PAGE_COUNT}";
    $pdf->page_text ($max_x-60, $max_y-50, $cadena, $fuente, 8);
    $pdf->close_object();
    $pdf->add_object($obj_id, "add");

</script>
';

$texto=$registro['radi_texto'];
/*$texto=str_replace("</p>","<br />",$texto);
$texto=str_replace("<p>","",$texto);*/

if ($td=="1") {		//OFICIOS
	$cuerpo= 'Quito DM, '.$fecha.'<br/><br/><br/>
	Señor '.$usuadest["titulo"].'<br/>
	'.$usuadest["nombre"].'<br/>
	'.$usuadest["cargo"].'<br/>
	'.$usuadest["institucion"].'<br/>'
	.iconv("ISO-8859-1", "UTF-8", "$texto");
} else {
	$cuerpo= '
	<table border="0" width="100%">
	    <tr><td width="18%">&nbsp;</td><td width="62%">&nbsp;</td></tr>
	    <tr>
		<td><b>PARA:</b></td>
		<td>'.$usuadest["titulo"].' '.$usuadest["nombre"].'<br />'.$usuadest["cargo"].'</td>
	    </tr>
	    <tr><td colspan="2">&nbsp;</td></tr>
	    <tr>
		<td><b>DE:</b></td>
		<td>'.$usuarem["titulo"].' '.$usuarem["nombre"].'<br />'.$usuarem["cargo"].'</td>
	    </tr>
	    <tr><td colspan="2">&nbsp;</td></tr>
	    <tr>
		<td><b>ASUNTO:</b></td>
		<td>'.$registro["radi_asunto"].'</td>
	    </tr>
	    <tr><td colspan="2">&nbsp;</td></tr>
	    <tr>
		<td><b>FECHA:</b></td>
		<td>'.$fecha.'</td>
	    </tr>
	</table>
	<br /><br /><br />'
	.iconv("ISO-8859-1", "UTF-8", "$texto");
}

$firma = '<br /><br />Atentamente,<br /><br /><br /><br />'.
	$usuarem["titulo"].' '.$usuarem["nombre"].'<br />'.$usuarem["cargo"].
	"<br /><font size=\"1\">$Responsables</font>";

////////////////////////	CON COPIA A	///////////////////////
$CC="";
if (trim($ConCopiaA)!="") 
	$CC = '<br /><br /><table border="0" width="100%"><tr><td width="10%" valign="top">C.C.:</td><td width="90%">'.$ConCopiaA.'</td></tr></table>';


////////////////////////	ANEXOS  	///////////////////////
$Anex="";
if (trim($Anexos)!="") 
	$Anex = '<br /><br /><table border="0" width="100%"><tr><td width="10%" valign="top">Adjunto:</td><td width="90%">'.$Anexos.'</td></tr></table>';



$pie= '
<br /><br /><br />
<table border="0" width="100%">
    <tr><td><center><font size="1">EDIFICIO CASA VIVANCO. Av Amazonas N39-82 y Pereira. 2do. piso.</font></center></td></tr>
    <tr><td><center><font size="1">Telf: 2448-185,&nbsp;&nbsp;2263-161,&nbsp;&nbsp;2463-703,&nbsp;&nbsp;2463-704</font></center></td></tr>
    <tr><td><center><font size="1">www.presidencia.gov.ec</font></center></td></tr>
</table>
';

$fin = '
</body>
</html>
';


$doc_html = $inicio.$encabezadohtml.$cuerpo.$firma.$CC.$Anex.$pie.$fin;
$doc_pdf = $inicio.$encabezadopdf.$cuerpo.$firma.$CC.$Anex.$fin;



