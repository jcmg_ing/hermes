<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
// Este archivo contiene el codigo necesario para generar los archivos pdf a partir de codigo html, utilizando como plantilla un archivo pdf
// requiere las siguientes librerias:
// - TCPDF (descargar internet) - http://www.tecnick.com/public/code/cp_dpage.php?aiocp_dp=tcpdf
// - FPDI (descargar internet) - http://www.setasign.de/products/pdf-php-solutions/fpdi/
// - html2ps (instalar paquete) - Ver en internet html_topdf http://www.rustyparts.com/pdf.php
// - ps2pdf (instalar paquete ghostcript)

require_once("$ruta_raiz/js/tcpdf/tcpdf.php");
require_once("$ruta_raiz/js/fpdi/fpdi.php");

class PDF extends FPDI {

    var $_plantilla_pdf;	//Variable en la que se guarda la plantilla (archivo pdf)
    var $_total_paginas;	//total de paginas del documento pdf
    
    /* Importamos el archivo plantilla */
    function set_archivo_plantilla($archivo) {
	$this->setSourceFile($archivo);
	$this->_plantilla_pdf = $this->importPage(1);
    }

    /* Definimos el encabezado, en este caso hacemos que imprima el pdf plantilla */
    function Header() {
	$this->useTemplate($this->_plantilla_pdf); 
    }
    
    /* Definimos el pie de página, en este caso que aparezca el numero de pagina */
    function Footer() {
	$this->SetY(-15);	//Posición 15 mm arriba del filo inferior de la pagina
	$this->SetFont('helvetica','I',8);	//fuente del texto
	$this->Cell(0,10,$this->PageNo().'/'.$this->_total_paginas,0,0,'R');	//numero de página a la derecha
    }

    /* Realiza la mezcla del archivo original con la plantilla */
    function mezclar_pdf($archivo) { 
	$pagecount = $this->setSourceFile($archivo); 
	$this->_total_paginas = $pagecount;
	for ($i = 1; $i <= $pagecount; $i++) { 
	    $tplidx = $this->ImportPage($i); 
	    $s = $this->getTemplatesize($tplidx); 
	    $this->AddPage('P', array($s['w'], $s['h'])); 
	    $this->useTemplate($tplidx); 
        } 
    } 
}


/*Elimina ciertas etiquetas html que podrían ocacionar errores al generar el pdf */
function validar_html($html) {
    $html = preg_replace(':<input (.*?)type=["\']?(hidden|submit|button|image|reset|file)["\']?.*?>:i', '', $html);
    $html = preg_replace(':<style.*?>.*?</style>:is', '', $html);
    $html = preg_replace(':<img.*?/>:is', '', $html);
    $html = preg_replace(':<!--.*?-->:is', '', $html);
    $origen = array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ");
    $destino = array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;","&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;");
    $html = str_replace($origen, $destino, $html);
/*
            // prepend relative image paths with the default domain and path
            $this->_htmlString = preg_replace(':<img (.*?)src=["\']((?!/)(?!http\://).*?)["\']:i', '<img \\1 src="http://'.$this->defaultDomain.$this->defaultPath.'\\2"', $this->_htmlString);
            // prepend absolute image paths with the default domain
            $this->_htmlString = preg_replace(':<img (.*?)src=["\'](/.*?)["\']:i', '<img \\1 src="http://'.$this->defaultDomain.'\\2"', $this->_htmlString);
*/
    return $html;
}


function generar_pdf($html, $archivo_plantilla, $ruta_raiz="..") {
    $html = validar_html($html);
//echo $html;

    // Definimos los nombres de los archivos temporales que necesitamos para transformar a pdf
    $tmp_html 	= tempnam("/tmp/", 'AHTML-');
    $tmp_ps 	= str_replace("HTML-", 'PS-', $tmp_html);
    $tmp_pdf 	= str_replace("HTML-", 'PDF-', $tmp_html);
    $tmp_conf 	= "$ruta_raiz/plantillas/ps.conf"; //Archivo con la configuracion con que se generara el pdf (tamaño de la pagina, margenes, etc.)
    $tmp_result	= array();

    // Guardamos el string del html en un archivo temporal
    file_put_contents($tmp_html, $html);

    // Pasamos el codigo html a postscript (formato de impresión)
    $cmd = "/usr/bin/html2ps -f $tmp_conf -o $tmp_ps $tmp_html 2>&1";
//echo "<hr>$cmd<hr>";
    exec($cmd, $tmp_result, $retCode);
//var_dump($tmp_result);

    //pasamos de postscript a pdf (sin encabezados)
    $cmd = "/usr/bin/ps2pdf -sPAPERSIZE=a4 -I  -dAutoFilterColorImages=false -dColorImageFilter=/FlateEncode $tmp_ps '$tmp_pdf' 2>&1";
//echo "<hr>$cmd<hr>";
    exec($cmd, $tmp_result, $retCode);
//var_dump($tmp_result);

    $archivo_plantilla = "$ruta_raiz/bodega/plantillas/$archivo_plantilla";
    if (is_file($archivo_plantilla)) {

    	// Inicializamos el pdf
    	$pdf = new PDF();
    	//$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
    	//$pdf->SetAutoPageBreak(true, 40);

    	// add a page
    	//$pdf->AddPage();

    	$pdf->set_archivo_plantilla($archivo_plantilla);
    	$pdf->mezclar_pdf($tmp_pdf);

    	// borramos los archivos temporales
    	unlink($tmp_html);
    	unlink($tmp_ps);
    	unlink($tmp_pdf);

    	// desplegamos el PDF
    	return $pdf->Output("documento.pdf", 'S');
    } else {
	$resp = file_get_contents($tmp_pdf);
    	// borramos los archivos temporales
    	unlink($tmp_html);
    	unlink($tmp_ps);
    	unlink($tmp_pdf);
	return $resp;
    }
}
?>

