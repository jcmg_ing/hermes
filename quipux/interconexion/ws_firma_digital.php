<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

include "../config.php";

function grabar_archivos_firmados($usuario, $radi_nume, $archivo)
{
    $radi_nume = trim($radi_nume);
    if (strlen($radi_nume)!=20) return "0a";

    $ruta_raiz = "..";
    include_once "$ruta_raiz/obtenerdatos.php";
    include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler($ruta_raiz);
    include_once "$ruta_raiz/include/tx/Tx.php";
    include_once "$ruta_raiz/include/tx/Firma_Digital.php";
    $tx = new Tx($db);

//    $usr = ObtenerDatosUsuario($usuario, $db, "C");
    $radicado = ObtenerDatosRadicado($radi_nume, $db);
    $usr = ObtenerDatosUsuario(str_replace("-", "",$radicado["usua_rem"]), $db);
    if (substr($usr["cedula"], 0, 10) != $usuario) return "0b";


    if ($radicado["estado"] != 3) return "0c"; // Validamos que el documento este en un estado válido

    if (trim($radicado["radi_path"])=="") {
        $radi_path = "/".substr(trim($radi_nume),0,4)."/".substr(trim($radi_nume),4,6)."/$radi_nume.pdf.p7m";
        $path_arch = "$ruta_raiz/bodega".$radi_path;
    } else {
        $radi_path = trim($radicado["radi_path"]);
        while (strtoupper(substr($radi_path,-4)) == ".P7M") {
            $radi_path = substr($radi_path,0,-4);
        }
        $radi_path .= ".p7m";
        $path_arch = "$ruta_raiz/bodega".$radi_path;
    }

    $ok = file_put_contents($path_arch, base64_decode($archivo));
    if (!$ok) return "0d";

    $firma = verificaFirma("$path_arch",$ruta_raiz);
	//if ($firma["flag"]!=1) return "0";

    $persona = $firma["datos_firma"];

    $fecha = $db->conn->sysTimeStamp;
    $sql = "update radicado set radi_fech_firma=$fecha, radi_path='$radi_path', radi_tipo_archivo=1, radi_nomb_usua_firma='$persona' ".
           "where radi_nume_temp=$radi_nume and (esta_codi=4 or esta_codi=3 or radi_nume_radi=$radi_nume)";
    $ok = $db->conn->Execute($sql);

    if (!$ok) return "0e";
//	Registramos el histórico
    $tx->insertarHistorico($radi_nume, $usr["usua_codi"],  $usr["usua_codi"], "Documento Firmado Electrónicamente", 40);	//Firma Digital
//	Enviamos el documento a los usuarios
    $respFirma = $tx->envioElectronicoDocumento($radi_nume,  $usr["usua_codi"]);
    return "1";
}



    // Averiguar ruta servidor

    ini_set("soap.wsdl_cache_enabled", "0");
    $sServer = new SoapServer("$nombre_servidor/interconexion/firma.wsdl");
    //  $sServer = new SoapServer($ruta_servidor);
    $sServer->addFunction("grabar_archivos_firmados");
    $sServer->handle();
?>
