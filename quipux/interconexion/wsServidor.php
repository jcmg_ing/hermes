<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
function setNuevoRadicado($usuario, $asunto, $texto, $nom_archivo, $archivo)
{
    $ruta_raiz = "..";
    include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");
    $db->conn->SetFetchMode(ADODB_FETCH_ASSOC);
    include_once "$ruta_raiz/include/tx/Radicacion.php";
    include_once "$ruta_raiz/include/tx/Historico.php";
    include_once "$ruta_raiz/obtenerdatos.php";
    include_once "$ruta_raiz/anexos_grabar.php";

    $hist = new Historico($db);
    $rad = new Radicacion($db);
    $rad->transaccion=1;  //Indica que el commit o rollback de la transacción se manejará localmente
    $db->conn->BeginTrans();	//Inicia la transaccion

    $usr_rem = ObtenerDatosUsuario($usuario,$db,"C","U");
    if (count($usr_rem)==0)
	return "0";
//var_dump($usr_rem);
    $rad->flagRadiTexto = "0";	//Bandera: O si el documento es borrador, 1 si es un documento final
    $rad->radiFechFirma = "null";	//Fecha de firma digital
    $rad->radiUsuaRadi = $usr_rem["usua_codi"];
    $rad->radiUsuaActu = $usr_rem["usua_codi"];
    $rad->radiInstActu = $usr_rem["inst_codi"];
    $rad->radiEstado = "1";
    $rad->radiTipo = "1";		//Tipo de documento (Oficio, Memo, etc.)
    $rad->radiAsunto     = $asunto;
    $rad->textTexto      = $texto;

    $rad->radiUsuaRem = "-".$usr_rem["usua_codi"]."-";
    $rad->radiUsuaDest = "-".$usr_rem["usua_codi"]."-" ;
    $rad->radiUsuaAnte = "null" ;

    $noRadiText="";
    $noRad  = $rad->newRadicado(0, $usr_rem["depe_codi"], $noRadiText );
//var_dump($noRad);
    if(!$noRad) return "0";

    $hist->insertarHistorico($noRad, $usr_rem["usua_codi"], $usr_rem["usua_codi"], "Documento registrado desde una aplicación externa", 2);

    $nomb_arch_tmp = tempnam("/tmp/","quipux");
    file_put_contents($nomb_arch_tmp, base64_decode($archivo));
    $ok1=GrabarAnexo($db, $noRad, $nomb_arch_tmp, $nom_archivo, "Archivo cargado desde una aplicación externa", $usr_rem["usua_codi"], $ruta_raiz, 0, 0);
//var_dump($ok1);
    if ($ok1 == 0){
	$db->conn->RollbackTrans();
	return "archivo";
    }

    $hist->insertarHistorico($noRad, $usr_rem["usua_codi"], $usr_rem["usua_codi"], "$nom_archivo - Archivo adjunto desde una aplicación externa", 2);
    $db->conn->CommitTrans();
    return "$noRadiText";
  }



/////////////////////////////////////////////////////////////////////////

function crear_ciudadanos($usr, $db) {
    $recordSet["CIU_CEDULA"] 	= $db->conn->qstr($usr["cedula"]);
    $recordSet["CIU_NOMBRE"] 	= $db->conn->qstr($usr["nombre"]);
    $recordSet["CIU_APELLIDO"] 	= $db->conn->qstr($usr["apellido"]);
    $recordSet["CIU_TITULO"]	= $db->conn->qstr($usr["titulo"]);
    $recordSet["CIU_ABR_TITULO"]= $db->conn->qstr($usr["abr_titulo"]);
    $recordSet["CIU_CARGO"] 	= $db->conn->qstr($usr["cargo"]);
    $recordSet["CIU_EMPRESA"] 	= $db->conn->qstr($usr["institucion"]);
    $recordSet["CIU_EMAIL"] 	= $db->conn->qstr($usr["email"]);
    $recordSet["INST_CODI"] 	= "1";
    $db->conn->Replace("CIUDADANO", $recordSet, "CIU_CEDULA", false,false,false);

}

function set_registrar_documento($usuarios, $documento, $anexos)
{
    $ruta_raiz = "..";

    include_once "$ruta_raiz/include/db/ConnectionHandler.php";
    $db = new ConnectionHandler("$ruta_raiz");

    include_once "$ruta_raiz/include/tx/Radicacion.php";
    include_once "$ruta_raiz/include/tx/Tx.php";
    include_once "$ruta_raiz/include/tx/Historico.php";
    include_once "$ruta_raiz/obtenerdatos.php";
    include_once "$ruta_raiz/anexos_grabar.php";

    $hist = new Historico($db);
    $tx = new Tx($db);
    $rad = new Radicacion($db);
    $rad->transaccion=1;  //Indica que el commit o rollback de la transacción se manejará localmente
    $db->conn->BeginTrans();	//Inicia la transaccion

    $usua_codi = null;
    $flag_remitente = false;
    foreach ($usuarios as $tmp){  //Buscamos si existe el usuario y si no lo insrtamos en la bdd
      if (trim($tmp["cedula"])!="") {
	$usr = ObtenerDatosUsuario($tmp["cedula"],$db,"C");
	if ($usr["usua_codi"]==null) {
	    crear_ciudadanos($tmp, $db);
	    $usr = ObtenerDatosUsuario($tmp["cedula"],$db,"C");
	}
	if ($usr["usua_codi"]!=null) {
	    if ($tmp["tipo"]==1) {
	    	$rad->radiUsuaRem .= "-".$usr["usua_codi"]."-";
		$usua_rem = $usr["usua_codi"];
	    }
	    if ($tmp["tipo"]==2){
	    	$rad->radiUsuaDest .= "-".$usr["usua_codi"]."-" ;
	    	if ($usr["tipo_usuario"]==1) {
		    $usua_codi = $usr["usua_codi"];
		    $depe_codi = $usr["depe_codi"];
		    $inst_codi = $usr["inst_codi"];
		    $flag_remitente = true;
		}
	    }
	    if ($tmp["tipo"]==3)
	    	$rad->radiCCA .= "-".$usr["usua_codi"]."-" ;
	}
      } //Fin if cedula==""
    } //Fin busqueda de usuarios

    if ($usua_codi == null) {
	$db->conn->RollbackTrans();
	return "0";
    }
    $usua_codi = "-1";
    if (!isset($usua_rem)) $usua_rem = "-1";
echo "Usuario = $usua_codi <br> Dependencia = $depe_codi<hr>";
//var_dump($rad);
    $rad->radiCuentai    = $documento["referencia"];
    $rad->radiAsunto     = $documento["asunto"];
    $rad->radiFechOfic   = $documento["fecha"];
    $rad->textTexto      = $documento["resumen"];
    $rad->radiDescAnex   = $documento["desc_anexos"];

    $rad->radiTipo       = "2";
    $rad->radiEstado	 = "1";
    $rad->flagRadiTexto  = "0";
    $rad->radiFechFirma	 = "null";	//Fecha de firma digital

//Cambiar a null
    $rad->radiUsuaActu = $usua_codi;
    $rad->radiUsuaRadi = $usua_codi;
    $rad->radiUsuaAnte = "null";
    $rad->radiInstActu = "null";

    $radi_text="";
    $noRad  = $rad->newRadicado(2, $depe_codi, $radi_text );
//echo "RADI_TEXT = $radi_text <br> Radicado = $noRad<hr>";

    if ($noRad==0) {
	$db->conn->RollbackTrans();
	return "0";
    }

    $db->conn->CommitTrans();


    $radicadosSel[0]=$noRad;
    $tx->GenerarDocumentosEnvio($radicadosSel, $usua_codi, "Documento recibido desde una aplicación externa",$ruta_raiz);

    foreach ($anexos as $tmp) {
/*	if (strtolower(substr(trim($tmp["nombre"]),-4)) != ".p7m") {
	    $hist->insertarHistoricoTemporal($noRad, $usua_rem, $usua_rem, "No se adjunto archivo \"".$tmp["nombre"]."\" por no estar firmado digitalmente", 66);
	} else {/**/
	    $nomb_arch_tmp = tempnam("/tmp/","quipux");
	    file_put_contents($nomb_arch_tmp, base64_decode($tmp["archivo"]));
	    $ok = GrabarAnexo($db, $noRad, $nomb_arch_tmp, $tmp["nombre"], $tmp["descripcion"], $usua_rem, $ruta_raiz, 0, $tmp["flag_imagen"]);
	    if ($ok==0)
	    	$hist->insertarHistoricoTemporal($noRad, $usua_rem, $usua_rem, "No se adjunto archivo \"".$tmp["nombre"]."\"", 66);
	    else
	    	$hist->insertarHistoricoTemporal($noRad, $usua_rem, $usua_rem, "El archivo \"".$tmp["nombre"]."\" fue adjuntado al documento", 66);
//	}
    }


//die("2");
    return $noRad;
  }

///////////////////////////////////////////////////////////////////////////////////////














   // Averiguar ruta servidor
    $ruta_raiz = "..";
    include  "$ruta_raiz/config.php";

    ini_set("soap.wsdl_cache_enabled", "0");
   $sServer = new SoapServer("$nombre_servidor/interconexion/quipux.wsdl");
//  $sServer = new SoapServer($ruta_servidor);
   $sServer->addFunction("setNuevoRadicado");
   $sServer->addFunction("set_registrar_documento");
   $sServer->handle();
?>
