<?
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
//include "login.php";
$ruta_raiz = ".";
include_once "$ruta_raiz/funciones_interfaz.php";
include_once "$ruta_raiz/config.php";

$boton = '<input onclick="irLogin(0);" name="Submit" type="submit" class="botones_index" value="Ingresar al Sistema">';
$boton_oculto = '';
if ($activar_bloqueo_sistema) {
    if (is_file("./bodega/mensaje_bloqueo_sistema.html")) {
        $mensaje = file_get_contents("./bodega/mensaje_bloqueo_sistema.html");
        $boton = '<div id="div_mensaje_alerta_top" style="border: #B40404 1px solid; width: 450px; height: 65px; overflow: auto; background-color: #F5A9A9; -moz-border-radius:10px; -webkit-border-radius:10px;">
                    <center>
                        <table width=96% border="0" cellpadding="0" cellspacing="2">
                            <tr>
                                <td style="width: 100%; font-size: 10px; text-align: left; color: ;">'.$mensaje.'</td>
                            </tr>
                          </table>
                      </center>
                    </div>
                    <script type="text/javascript">indexTimerId = setTimeout("window.location.reload();", 300000);</script>';
        $boton_oculto = '<a href="javascript: void(0);" onclick="irLogin(1);" style="color: none;">...</a>';
    }
}

echo "<html>".html_head(true,true); /*Imprime el head definido para el sistema*/
echo html_validar_browser(); /*Valida el browser*/

?> 
<head>
    <style type="text/css">
        .tabla_left /*Contenidos de los marcos*/
        {
            border: 10px solid /*#4774AB;/*#D38785;*/#C0504E;
        }
        table.tabla_left th,
        table.tabla_left td {
            border: 3px solid /*#4774AB;/*#D38785;*/#C0504E;
        }
        .tabla_center
        {
            border: 10px solid /*#6288BC;/*#EECA5B;#BFB256#9ec45c;*/#bfb256;
        }
        table.tabla_center th,
        table.tabla_center td {
            border: 3px solid /*#6288BC/*#EECA5B;#BFB256;#9ec45c;*/#bfb256;
        }
        .tabla_right
        {
            border: 10px solid #7194AE;/*#99ABEA;/*#3692A9;*/
        }
        table.tabla_right th,
        table.tabla_right td {
            border: 3px solid #7194AE;/*#99ABEA;/*#3692A9;*/
        }
    </style>
    <link rel="stylesheet" href="estilos/bordes_redondos.css">

    <script type="text/JavaScript">
        function irLogin(admin) {
            var x = screen.width - 20;
            var y = screen.height - 80;
            var param = "";
            if (admin == 1) param = "?txt_administrador=1";
            ventana=window.open("<?=$nombre_servidor?>/login.php"+param,"QUIPUX","toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes, width="+x+", height="+y);
            ventana.moveTo(10, 40);
            ventana.focus();
        }

    </script>
</head>

<body>
    <table align="center" width="100%">
        <tr>
            <td align="center">&nbsp;</td>
            <td align="center">
                <img src="quipux-logo3.png" height="120" width="200" alt="Logo Quipux">
            </td>
            <td align="center">&nbsp;</td>
            <td height="150px" align="center" colspan="3">
                <?=$boton?>
            </td>
            <td align="center">&nbsp;</td>
        </tr>
        <tr>
            <td align="center">&nbsp;</td>
            <td align="center">
<?php
                $texto = 'El Sistema &quot;Quipux&quot; es un servicio web que la Presidencia de la Rep&uacute;blica pone a disposici&oacute;n
                          de las instituciones del sector p&uacute;blico.<br><br>
                          Para solicitar el acceso al sistema se debe:
                          <ul>
                              <li> - Enviar un oficio solicitando la creaci&oacute;n de la cuenta institucional en el sistema,
                                     dirigido al Subsecretario de Tecnolog&iacute;as de Informaci&oacute;n.</li>
                              <li> - Nombrar a un administrador institucional, el cual se har&aacute; cargo de la administraci&oacute;n del sistema
                                     en la instituci&oacute;n.</li>
                          </ul>
                          El uso del sistema no tiene ning&uacute;n costo para la instituci&oacute;n.<br><br>
                          Vea qu&eacute; Instituciones est&aacute;n utilizando el sistema
                          <a href="http://www.informatica.gob.ec/index.php?option=com_reporte_usuarios_quipux" target="_blank">
                              <font color="blue" face="Verdana" size="2" ><b>aqu&iacute; <b></font>
                          </a>';

                dibujar_tablas("rojo", "imagesproc2.jpeg", "Implantaci&oacute;n del Sistema", $texto);
?>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">
<?php
                $texto = 'Para dar un buen uso al sistema QUIPUX se recomienda seguir los siguientes procedimientos:
                          <ul>
                              <li> - Parametrizaci&oacute;n del sistema <a href="http://www.informatica.gob.ec/descargas/qparametrizacion.pdf">
                                        <font color="blue" face="Verdana" size="2" ><b>aqu&iacute;</b></font></a></li>
                              <li> - Registro de Documentos Externos <a href="http://www.informatica.gob.ec/descargas/ProcesoRegistrodeDocumentosexterno.pdf">
                                        <font color="blue" face="Verdana" size="2" ><b>aqu&iacute;</b></font></a></li>
                              <li> - Subrogación de Cargos <a href="http://www.informatica.gob.ec/descargas/STI-QUIPUX-5.2.4_Subrogacion-de-Cargo-v3.pdf">
                                        <font color="blue" face="Verdana" size="2" ><b>aqu&iacute;</b></font></a></li>
                              <li> - Obtener un respaldo de información <a href="http://www.informatica.gob.ec/descargas/STI-QUIPUX-5.2.5_Solicitud-de-Respaldos-v3.pdf">
                                        <font color="blue" face="Verdana" size="2" ><b>aqu&iacute;</b></font></a></li>
                          </ul>';

                dibujar_tablas("amarillo", "imagesimplan.jpeg", "Procedimientos", $texto);
?>
            </td>
            <td align="center">&nbsp;</td>
            <td align="center">
<?php
              $texto = 'Para cualquier duda o solicitud de nuevos requerimientos enviar un correo al administrador institucional del sistema.
                        <br>&nbsp;<br>
                        Si se olvidó la contrase&ntilde;a:
                        <ul>
                            <li> - Funcionario P&uacute;blico remitir un correo al administrador institucional de la organización.</li>
                            <li> - Ciudadano debe seguir el siguiente procedimiento.
                                <a href="http://www.informatica.gob.ec/index.php?option=com_docman&amp;task=doc_download&amp;gid=36&amp;Itemid=">
                                    <font color="blue" face="Verdana" size="2" ><b>aqu&iacute;</b></font></a></li>
                        </ul>
                        Para horarios de capacitación visite la página
                            <a href="http://www.informatica.gob.ec/index.php/index.php?option=com_events&catids=21&task=view_month" target="_blank">
                                <font color="blue" face="Verdana" size="2" ><b>aqu&iacute;</b></font></a>';

                dibujar_tablas("azul", "helpDesk.jpg", "Ayuda, Soporte y Capacitaci&oacute;n", $texto);
?>
            </td>
            <td align="center">&nbsp;</td>
        </tr>
    </table>
    <br><?=$boton_oculto?>
</body>
</html>

<?php 


function dibujar_tablas($color, $imagen, $titulo, $texto) {
    $color_fondo = 'left';
    if ($color == "amarillo") $color_fondo = 'center';
    elseif ($color == "azul") $color_fondo = 'right';
    echo '<table class="borde-left-externo" border="0" cellpadding="0">
            <tr>
                <td align="center">
                    <img src="imagenes/'.$imagen.'" width="100px" height="80px" alt="Imagen">
                </td>
            </tr>
            <tr>
                <td>
                    <div class="borde-'.$color_fondo.'">
                    <table border="0">
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <font color="white" size="4">
                                <div style="text-align:left">
                                <span style="text-decoration:underline;">'.$titulo.'</span>
                                </div>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" class="tabla_'.$color_fondo.'" cellpadding="0">
                                    <tr><td>
                                        <div style="text-align:justify">
                                        <font color="white" size="2">'.$texto.'</font></div>
                                    </td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </div>
                </td>
            </tr>
        </table>';
}

?> 