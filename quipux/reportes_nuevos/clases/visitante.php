<?php
include_once 'bd.php';

class Visitante extends bd
{
	 
	 
	 public function get_cedula_vis(){
		return $this->cedula_vis;
	}
	 public function get_nombre_vis(){
		return $this->nombre_vis;
	}
	public function get_apellido_vis(){
		return $this->apellido_vis;
	}
	public function get_tipo_vis(){
		return $this->tipo_vis;
	}
	
	public function get_entidad_vis(){
		return $this->entidad_vis;
	}
	public function get_telefono_vis(){
		return $this->telefono_vis;
	}
	public function get_correo_vis(){
		return $this->correo_vis;
	}
	public function get_direccion_vis(){
		return $this->direccion_vis;
	}
	
	public function buscar_visitante($cedula){
		$sql="select * from atencion_publico.visitantes where cedula_vis='$cedula'"; 
		$row=$this->cargarObjeto($sql);
		$this->cedula_vis=$row['cedula_vis'];
		$this->nombre_vis=$row['nombre_vis'];
		$this->apellido_vis=$row['apellido_vis'];
		$this->tipo_vis=$row['tipo_vis'];
		$this->entidad_vis=$row['entidad_vis'];
		$this->telefono_vis=$row['telefono_vis'];
		$this->correo_vis=$row['correo_vis'];
		$this->direccion_vis=$row['direccion_vis']; 
		
	}
	
  
	public function Guardar_visitante($cedula_vis,$nombre_vis,$apellido_vis,$repre_vis,$entidad_vis,$telefono_vis,$correo_vis,$direccion_vis){
		$sql="select * from atencion_publico.visitantes  where cedula_vis='$cedula_vis'";  
		$mostrar=$this->cargarObjeto($sql);
		if($mostrar!=NULL)
		{ 
			$modificar="UPDATE atencion_publico.visitantes SET  nombre_vis='$nombre_vis', apellido_vis='$apellido_vis', tipo_vis='$repre_vis', entidad_vis='$entidad_vis', telefono_vis='$telefono_vis', correo_vis='$correo_vis', direccion_vis='$direccion_vis' WHERE cedula_vis='$cedula_vis'";
			$this->Consulta($modificar); 
		
		} 
		else
		{
			$registrar="INSERT INTO atencion_publico.visitantes(cedula_vis, nombre_vis, apellido_vis, tipo_vis, entidad_vis,telefono_vis, correo_vis, direccion_vis) VALUES ('$cedula_vis','$apellido_vis','$nombre_vis','$repre_vis','$entidad_vis','$telefono_vis','$correo_vis','$direccion_vis')";
			$this->Consulta($registrar); 
		}	
	} 
	
	
	
	public function get_id_visita(){
		return $this->id_visita;
	}
	
	public function get_tipo_visita(){
		return $this->tipo_visita;
	}
	
	public function get_asunto_visita(){
		return $this->asunto_visita;
	}
	
	public function get_solucion_visita(){
		return $this->solucion_visita;
	}
	
	public function get_fecha_visita(){
		return $this->fecha_visita;
	}
	
	public function get_status_visita(){
		return $this->status_visita;
	}
	
	public function get_observacion_visita(){
		return $this->observacion_visita;
	}
	
	public function buscar_visita_adjun($cedula_vis,$numvisita){
		$sql="select * from atencion_publico.adjuntar_visita where cedula_vis='$cedula_vis' and id_visita='$numvisita' "; 
		$row=$this->cargarObjeto($sql);
		$this->cedula_vis=$row['cedula_vis'];
		$this->id_visita=$row['id_visita'];
		$this->tipo_visita=$row['tipo_visita'];
		$this->asunto_visita=$row['asunto_visita'];
		$this->redirigido=$row['redirigido'];
		$this->fecha_visita=$row['fecha_visita'];
		$this->status_visita=$row['status_visita']; 
		$this->observacion_visita=$row['observacion_visita']; 
		
	}
	
	
	
	public function Guardar_adjuntar_visitante($cedula_vis,$num_visita,$tipo_visita,$asunto_visita,$redirigido,$fecha_visita,$observacion_visita){
		$sql="select * from atencion_publico.adjuntar_visita  where cedula_vis='$cedula_vis' and id_visita='$num_visita'";  
		$mostrar=$this->cargarObjeto($sql);
		if($mostrar!=NULL)
		{ 
			$modificar="UPDATE atencion_publico.adjuntar_visita SET observacion_visita='$observacion_visita', asunto_visita='$asunto_visita', redirigido='$redirigido', status_visita='0' WHERE id_visita='$num_visita' and cedula_vis='$cedula_vis'";
			$this->Consulta($modificar); 
		
		} 
		else
		{
			$fecha_limite=$this->calculaFecha("days",30,"$fecha_visita"); 
			$registrar="
			INSERT INTO atencion_publico.adjuntar_visita(
            id_visita, cedula_vis, tipo_visita, asunto_visita, redirigido, 
            fecha_visita, status_visita, observacion_visita, fecha_limite)
    VALUES ('$num_visita', '$cedula_vis','$tipo_visita', '$asunto_visita',  '$redirigido', 
            '$fecha_visita', '1', '$observacion_visita', '$fecha_limite')";

			 
			$this->Consulta($registrar); 
		}	
	}  
	
	
	
	
	
	public function Guardar_comentar_visita($cedula_vis,$get_visita,$tipo_detalle,$asunto_comentario,$fecha_comentario){ 
		
			$registrar="INSERT INTO atencion_publico.detalle_visita(id_visita, cedula_vis, tipo_detalle, descrip_detalle, fecha_detalle)  VALUES ('$get_visita', '$cedula_vis', '$tipo_detalle', '$asunto_comentario', '$fecha_comentario')"; 
			$this->Consulta($registrar);  
			if($tipo_detalle!='Comentar'){
			$modificar="UPDATE atencion_publico.adjuntar_visita SET  status_visita='0' WHERE id_visita='$get_visita' and cedula_vis='$cedula_vis'";
			$this->Consulta($modificar); 	
		}
	}  
	 
}


?>