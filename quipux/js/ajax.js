
<script type="text/javascript">
    function nuevoAjax(objeto, metodo, url, variables) {
        var xmlhttp = false;
        try {
            xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
            } catch (E) {
                xmlhttp = false;
            }
        }

        if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
            xmlhttp = new XMLHttpRequest();
        }

        metodo = metodo.toUpperCase();
        if (metodo=='GET') {
            xmlhttp.open('GET', url+'?'+variables, true);
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4) {
                    try {
                        document.getElementById(objeto).innerHTML = xmlhttp.responseText;
                    } catch (e) { }
                }
            }
            xmlhttp.send(null);
        }

        if (metodo=='POST') {
            xmlhttp.open('POST', url, true);
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4) {
                    try {
                        document.getElementById(objeto).innerHTML = xmlhttp.responseText;
                    } catch (e) { }
                }
            }
            xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xmlhttp.send(variables);
        }
        return;
    }
</script>

