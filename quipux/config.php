<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/

// Archivo de configuracion del sistema QUIPUX

// Se recomienda OCULTAR las contraseñas y datos importantes como se explica en el archivo "config_recomendacion_seguridad.php".

// Archivo con algunas configuraciones de términos usados en el sistema
$FILE_LOCAL = "localEcuador.php";

// Activa la funcionalidad para bloquear el sistema;
$activar_bloqueo_sistema = false;

$version_light=false; // Bloquea algunas funcionalidades y aumenta parámetros en los queries para reducir la carga a los servidores
$config_bloquear_acceso_ciudadano = false; // Restringe el acceso a los ciudadanos

// Búsquedas y reportes
$config_numero_meses = 60; //Tiempo máximo de diferencia entre las fechas de búsqueda
$numeroCaracteresTexto = 0; // Número mínimo de caracteres en los campos de búsqueda


// Configuracion de la conexion con la BDD
$usuario = "postgres";
$contrasena= "postgres"; 
$servidor = "127.0.0.1:5432";
$driver = "postgres";
$db = "quipux";

// Indica si se manejan replicas o conexiones con otras BDD
$replicacion = false;

// Se definen las mismas variables que en la configuracion por defecto, seguidas por un guion bajo y un nombre que la distinga
// Para utilizar esta funcionalidad se debe enviar el nombre utilizado en las variables como parametro al crear la conexion
// Si se desea se puede ocultar los datos de la conexion en variables del servidor, como en el caso anterior
$usuario_reportes = "postgres";
$contrasena_reportes = "postgres";
$servidor_reportes = "127.0.0.1:5433";
$db_reportes = "quipux_replica";

$usuario_busqueda = "postgres";
$contrasena_busqueda = "postgres";
$servidor_busqueda = "127.0.0.1:5433";
$db_busqueda = "quipux_replica";


//Codigo de aplicacion (en caso de que se manejen varios servidores para distribución de carga)
$appID = 'appID';

//Logs y Mensajes de la aplicacion
//Muestra en pantalla los queries que se ejecutan en la bdd; 0 no muestra ningun mensaje, 1 muestra los errores, 2 muestra todos
$mostrar_logs = 0;
// Graba en la tabla logs de la bdd los queries (inserts y updates) mas importantes ejecutados; 0 no graba nada, 1 graba los errores, 2 graba todos
$grabar_logs = 2;
// Graba en una tabla de logs la página invocada y el IP que la invocó (para identificar posibles ataques desde páginas externas o desde páginas de orfeo...)
$grabar_log_paginas_visitadas = false; //Graba en la tabla log_paginas_visitadas un registro de todas las páginas visitadas por los usuarios
$grabar_log_full_backup = false; // Graba todas las sentencias de insert y update ejecutadas en el sistema


//Email del Super Administrador del Sistema QUIPUX
$amd_email = "jcmg.ing@gmail.com";
// email de la cuenta de soporte
$cuenta_mail_soporte = "jcmg.ing@gmail.com";
// email de la cuenta desde la que se enviarán los recordatorios a los usuarios
$cuenta_mail_envio = "jcmg.ing@gmail.com";

// Configuración para la conexión con otros servidores adicionales
$nombre_servidor="http://45.55.20.125//quipux";

$servidor_firma = "http://45.55.20.125//firma";

$servidor_pdf = "http://45.55.20.125//html_a_pdf";

?>
