<?

/*******************************************************************************
** Limpia cadenas numéricas, elimina cualquier caracter de otro tipo          **
*******************************************************************************/
function limpiar_numero($numero) {
    $numero = trim($numero);
    $flag_punto = false;
    $cadena = "";
    for ($i=0; $i<strlen($numero); $i++){
        $num = substr($numero,$i,1);
        if (strpos("0123456789.-", $num) !== false) {
            if ($num == "-" and $i > 0) $num = ""; //Para numeros negativos el guion debe estar al inicio
            if ($num == ".") { // Para decimales
                if ($flag_punto) $num=""; //Controla que haya un solo punto
                if ($i == 0) $num = "0.";
                $flag_punto = true;
            }
            $cadena .= $num;
        }
    }
    return $cadena;
}


/*******************************************************************************
** Limpia las cadenas de ataques SQL INJECTION, CSS y HTML INJECTION          **
** Si $html==1 no limpia la cadena de código html                             **
*******************************************************************************/
function limpiar_sql($cadena, $html=1) {
    // Limpiamos apóstrofes para ataques SQL Injection
    if (is_array($cadena)) return;
    $flag_validar_sql = 0;
    if (strpos(" $cadena", "'") !== false) $flag_validar_sql = 1;
    $cadena = str_replace("\\", "", $cadena);
    $cadena = str_replace("'", "′", $cadena); //Elimina comillas simples para evitar ataques de SQL Injection
    $cadena = str_replace("”", '"', $cadena);
    $cadena = str_replace("“", '"', $cadena);

    $carac = array("&NBSP;", "\n");
    $tmp = str_replace($carac, " ", strtoupper($cadena));

    // Validamos ataques de CSS
    $tmp = str_replace(array(" ","&AMP;","&LT;"), array("","&","<"), $tmp);
    if (strpos($tmp, "<SCRIPT") !== false) {
        $cadena = str_ireplace(array("javascript","script"), array("jaiba_street","street"), $cadena);
    }
    if ($html == 1) {
        $cadena = str_replace(array("<",">",'"'),array("&lt;","&gt;","&quot;"),$cadena);
//        $cadena = strip_tags($cadena);
//        $cadena = htmlspecialchars($cadena);
    }
    return trim($cadena);
}


/*******************************************************************************
** Registra las variables que se reciben por $GET, $_POST y $_SESSION y las   **
** limpia contra ataques SQL Injection.                                       **
** Permite dehabilitar la variable REGISTER_GLOBALS del archivo PHP.INI       **
*******************************************************************************/
function validar_register_globals() {
    // Lista de variables que se van a limpiar; $_POST se sobrepone a $_GET y $_SESSION a todas las demás
    $variables = array("_GET", "_POST", "_SESSION");
    foreach ($variables as $tipo_variable) {
        global $$tipo_variable;
        if (is_array($$tipo_variable)) {
            foreach ($$tipo_variable as $key => $value) {
                $key = trim(limpiar_sql($key));
                if ($key != "") {
                    global $$key;
                    $$key = limpiar_sql($value);
                }
            }
        }
    }
    return;
}
/*******************************************************************************
** ATENCIÓN: Ejecuto automáticamente la función; se llama al archivo desde    **
** SESSION_ORFEO.PHP para que se ejecute en todas las páginas                 **
*******************************************************************************/
validar_register_globals();



function buscar_cadena($cadena, $campo) {
//Arma el query para buscar una cadena separada por espacios en un campo de la bdd quitando las tildes y eñes

    $resp = "";
    $cadena = limpiar_sql($cadena);
    //$cadena = str_replace('á','A',str_replace('é','E',str_replace('í','I',str_replace('ó','O',str_replace('ú','U',str_replace('ñ','N',$cadena))))));
    //$cadena = str_replace('Á','A',str_replace('É','E',str_replace('Í','I',str_replace('Ó','O',str_replace('Ú','U',str_replace('Ñ','N',$cadena))))));

    $arr_buscar = explode(" ", $cadena);
    $glue = '';
    foreach ($arr_buscar as $tmp) {
        if ($tmp != "" && strlen($tmp)>=3) {
            $resp .= " $glue translate(UPPER($campo),'ÁÉÍÓÚÀÈÌÒÙÄËÏÖÜÑ','AEIOUAEIOUAEIOUN')
		      LIKE translate(upper('%" . trim($tmp) . "%'),'ÁÉÍÓÚÀÈÌÒÙÄËÏÖÜÑ','AEIOUAEIOUAEIOUN') ";
            $glue = 'and';
        }
    }
    $resp = (empty($resp)) ? 'true' : $resp;
    return $resp;
}

function buscar_nombre_cedula($cadena, $buscarInstitucion = 'N') {
    $filtro = '((' . buscar_cadena($cadena, "usua_nombre") . ') or (' . buscar_cadena($cadena, "usua_cedula") . ')';
    if($buscarInstitucion == 'S')
        $filtro .= ' or (' . buscar_cadena($cadena, "inst_nombre") . ')';
    $filtro .= ')';
    return $filtro;
}

function buscar_nombre_cedula_solicitud($cadena) {
    return '((' . buscar_cadena($cadena, "ciu_nombre") . ') or (' . buscar_cadena($cadena, "ciu_cedula") . '))';
}

function buscar_2campos($cadena, $campo1, $campo2) {
    return '((' . buscar_cadena($cadena, $campo1) . ') or (' . buscar_cadena($cadena, $campo2) . '))';
}


function p_register_globals($list = null) {
    return;
}

// Envía un email al destinaratio especificado.
// El $mensaje debe estar en HTML
// $destinatario es el email
// $nombre_dest es el nombre del destinatario
function enviarMail($mensaje, $asunto, $destinatario_ori, $nombre_dest="", $ruta_raiz=".") {
    include "$ruta_raiz/config.php";
    $tmp = explode(",", $destinatario_ori);
    foreach ($tmp as $destinatario) {
        $destinatario = trim($destinatario);
        if ($destinatario != "" and strpos($destinatario, "@") and strpos($destinatario, ".", strpos($destinatario, "@"))) {
            // Estructura de la descripcion de email.
            $email = $destinatario; //recipient
            //$email = $para; //recipient
            $subject = $asunto;

            $despedida = "<br /><br />Saludos cordiales,<br /><br />Soporte Quipux.";
            $despedida .= "<br /><br /><b>Nota: </b>Este mensaje fue enviado autom&aacute;ticamente por el sistema, por favor no lo responda.";
            $despedida .= "<br />Si tiene alguna inquietud respecto a este mensaje, comun&iacute;quese con <a href='mailto:$cuenta_mail_soporte'>$cuenta_mail_soporte</a>";

            $mail_body = str_replace("**SISTEMA**", "<a href='$nombre_servidor' target='_blank'>$nombre_servidor</a>", $mensaje);
            $mail_body = str_replace("**DESPEDIDA**", $despedida, $mail_body);

            $header = 'MIME-Version: 1.0' . "\r\n";
            $header .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            $header .= "To: $nombre_dest <" . $destinatario . ">" . "\r\n";
            $header .= "From: Quipux <$cuenta_mail_envio>" . "\r\n";

            ini_set('sendmail_from', "$cuenta_mail_envio"); //Suggested by "Some Guy"
            mail($email, $subject, $mail_body, $header); //mail command :)
        }
    }
//echo "<hr>PARA: $destinatario_ori<br>$mail_body<hr>";
}

// Genera un password randómico con un numero determinado de caracteres
function generar_password($num=8) {
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    $pass = "";
    for ($i = 0; $i < $num; ++$i) {
        $pass .= substr($chars, rand(0, 33), 1);
    }
    return $pass;
}

/**
 *  ObtenerPath
 * */
function ObtenerPath($archivo) {
    $archivo = str_replace(".p7m", "", $archivo);
    return $path_arch = "$ruta_raiz/bodega" . $archivo;
}

/* Muestra las áreas hijas de un área.
 * Si tiene permiso de bandeja de entrada y $tipo="T" muestra todas las áreas de la institución
 */

function buscar_areas_dependientes($area, $tipo="T") {
    global $db;

    $areas = $area;
    // En caso que tenga permiso de "Bandeja de entrada" consulta todas las áreas
    if ($_SESSION["ver_todos_docu"] == 1 and $tipo == "T") {
        $sql = "select depe_codi from dependencia where depe_codi<>$area and depe_estado=1 and inst_codi=" . $_SESSION["inst_codi"];
        $rs = $db->conn->Execute($sql);
        while (!$rs->EOF) {
            $areas .= "," . $rs->fields["DEPE_CODI"];
            $rs->MoveNext();
        }
    } else {
        $areas .= buscar_areas_dependientes_rec($area);
    }
    return $areas;
}

// función recursiva que busca las áreas hijas de un área
function buscar_areas_dependientes_rec($codigo) {
    global $db;
    $areas = "";
    $sql = "select depe_codi from dependencia where depe_codi_padre=$codigo and depe_codi<>$codigo and depe_estado=1";
    $rs = $db->conn->Execute($sql);
    while (!$rs->EOF) {
        $areas .= "," . $rs->fields["DEPE_CODI"];
        $areas .= buscar_areas_dependientes_rec($rs->fields["DEPE_CODI"]);
        $rs->MoveNext();
    }
    return $areas;
}

/**
 * Guardar firma en la base de datos
 * */
function GrabarFirma($db, $firDigCodi, $usua_codi, &$file, $nombre, $extension, $ruta_raiz) {
    if ($ruta_raiz == '')
        $ruta_raiz = "..";

    //Respuesta si hubo un error al anexar el archivo
    $ok = 0; //Error
    //Nombre del archivo
    $tmp = explode("\\", strtolower($nombre));
    $nomb_arch1 = $tmp[count($tmp) - 1];
    $tmp = explode("/", strtolower($nomb_arch1));
    $nomb_arch = $tmp[count($tmp) - 1];
    //Extension del archivo
    $tmp = explode(".", $nomb_arch);
    $flag_firma = false;

    if ($tmp[count($tmp) - 1] == "p7m") {
        $tmp = explode(".", str_replace(".p7m", "", $nomb_arch));
        $flag_firma = true;
    }

    $tmp_ext = $tmp[count($tmp) - 1];
    $ext_arch = substr($nomb_arch, strpos($nomb_arch, $tmp_ext));

    $tipo_arch = $rs[0]["arch_tip_codi"];
    //$anex_nombre = str_replace(" ","_",$nomb_arch);

    unset($recordSet);
    if ($firDigCodi != '')
        $recordSet["FIR_DIG_CODI"] = $firDigCodi;
    $recordSet["USUA_CODI"] = $usua_codi;
    $recordSet["FIR_DIG_CUERPO"] = $db->conn->qstr(limpiar_sql(base64_encode(file_get_contents($file))));
    //$recordSet["FIR_DIG_NOMBRE"] = $db->conn->qstr(limpiar_sql($anex_nombre));
    $recordSet["FIR_DIG_EXT"] = $db->conn->qstr(limpiar_sql($extension));

    $ok = $db->conn->Replace("FIRMA_DIGITALIZADA", $recordSet, "FIR_DIG_CODI", false, false, false, false); //true al final para ver la cadena del insert

    if ($ok) { //Si inserto correctamente
        $ok = 0;
    } else
        $ok = 1;
    return $ok;
}

//Divide las cadenas en varias líneas según un ancho determinado
function dividir_cadenas($cadena, $separador="<br>", $tamanio=60) {
    $resultado = "";
    $pos = 0;
    while (true) {
        $cadena = trim($cadena);
        if (strlen($cadena) <= $tamanio or $pos === false) {
            $resultado .= $cadena;
            return $resultado;
        }
        $pos = strpos($cadena, " ", $tamanio);
        $resultado .= substr($cadena, 0, $pos) . $separador;
        $cadena = substr($cadena, $pos);
    }
}

function validar_mail($email) {
    //autor:                teya
    //fecha:                20110418
    //motivo:               funcion que valida la existencia del mail, pero solo se puede hacer que valide el dominio
    //1. verifica si el mail tiene el formato correcto
    if (trim($email) == "")
        return true; // porque cuando se ingresa por primera vez a la pag viene en blanco
//        if (!eregi("^[_\.0-9a-z\-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,6}$",$email)) {
//            return false; //echo 'direccion no tiene el formato adecuado';
//        }
    //2. verifica si existe el dominio
    //2.1 separamos en caso de venir mas de 1 direccion de correo
    $cadena = str_replace(";",",",$email);
    $correosc = split(",", $cadena );
    
    //$correospc = split(";", $email);
    $bandera = 0;
    
    foreach ($correosc as $email1) {

        list ( $Username, $dominio ) = split("@", $email1);
        $MXHost = '';

        if (checkdnsrr($dominio, 'A') || checkdnsrr($dominio, 'MX') || checkdnsrr($dominio, 'NS')
                || checkdnsrr($dominio, 'SOA') || checkdnsrr($dominio, 'PTR') || checkdnsrr($dominio, 'CNAME')
                || checkdnsrr($dominio, 'AAAA') || checkdnsrr($dominio, 'A6') || checkdnsrr($dominio, 'SRV')
                || checkdnsrr($dominio, 'NAPTR') || checkdnsrr($dominio, 'TXT') || checkdnsrr($dominio, 'ANY')
        ) {
            $bandera = 0;
//            if ( !(getmxrr ($dominio, $MXHost)))  {
//                return false; //echo 'no mailbox';
//            }
        } else {
            $bandera = 1; // no hay dominio
            break;
        }
        // return true; // email ok
    }
    if ($bandera == 0 ) return true;
    else return false;
}

function fechaAtexto($fecha){
    $dia = substr($fecha, 8, 2);
    $mes = substr($fecha, 5, 2);
    $anio = substr($fecha, 0, 4);    
    
    /**
     * Creamos un array con los meses disponibles.
     * Agregamos un valor cualquiera al comienzo del array para que los números coincidan
     * con el valor tradicional del mes. El valor "Error" resultará útil
     **/
    $meses = array('Error', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    
    /**
     * Si el número ingresado está entre 1 y 12 asignar la parte entera.
     * De lo contrario asignar "0"
     **/
    $num_limpio = $mes >= 1 && $mes <= 12 ? intval($mes) : 0;    
    $fechaletras = "a los $dia día(s) del mes de $meses[$num_limpio] de ".anio($anio).".";
    return $fechaletras;
    //return $meses[$num_limpio];    
    //return $dia;
}
function anio($anio){
   switch ($anio) {
         case '2011':
          $anioletras="dos mil once";
             break;
         case '2012':
          $anioletras="dos mil doce";
             break;
         case '2013':
          $anioletras="dos mil trece";
             break;
          default :             
            return "";
            break;
   }
   return $anioletras;
}
//crea el combo para las pantallas de solicitud de firma de ciudadano
//$solfirma: recibe del select de cada pantalla
function combo_firma_ciudadano($sol_firma,$db){        
    $sqlCmbCiu = "select descripcion, tipo_cert_codi from tipo_certificado where estado = 1 and tipo_cert_codi not in (0,1)";    
        $rsCmbCiu = $db->conn->query($sqlCmbCiu);
        $usr_firma  = $rsCmbCiu->GetMenu2('sol_firma',$sol_firma,"",false,"","Class='select' id='sol_firma'");
        return $usr_firma;
}
include_once "$ruta_raiz/js/calendario_php/calendario_php.php";
?>
