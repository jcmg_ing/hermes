<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
/* FUNCION RECURSIVA QUE PERMITE OBTENER LA DIRECCION FISICA COMPLETA DE UN EXPEDIENTE
	$item	:	Codigo del item fisico
	$db	:	Coneccion con la BDD
	$campo	:	"C" Despliega los Codigos, "N" Despliega los nombres, "S" Despliega las siglas
	$separador:	Despliega un separador especifico, util para desplegar en tablas
*/
$ruta_raiz = isset($ruta_raiz) ? $ruta_raiz : "..";
require_once("$ruta_raiz/funciones.php"); //para traer funciones p_get y p_post
p_register_globals(array());

  function ObtenerNombreCompletoTRD($item,$db,$campo="N",$separador=" - ")
  {

    if ($item==0) return 0;
    $dato= "TRD_CODI";
    if ($campo=="N") $dato= "TRD_NOMBRE";
    $sql = "select trd_padre, $dato from trd where trd_codi=$item";
    $rs=$db->conn->query($sql);
    $codigo=$rs->fields["TRD_PADRE"];
    $dato=$rs->fields["$dato"];
    if ($codigo==0)
	return $dato;
    else
        $resp=ObtenerNombreCompletoTRD($codigo,$db,$campo,$separador).$separador.$dato;
    return $resp;

  }

/* FUNCION RECURSIVA QUE PERMITE OBTENER LA DIRECCION FISICA COMPLETA DE UN EXPEDIENTE
	$item	:	Codigo del item fisico
	$db	:	Coneccion con la BDD
	$campo	:	"C" Despliega los Codigos, "N" Despliega los nombres, "S" Despliega las siglas
	$separador:	Despliega un separador especifico, util para desplegar en tablas
*/

  function ArbolModificarTRD($item, $nivel, $nivel_act, $dependencia, $nombre, $db, $ruta_raiz="..")
  {

    if ($nivel<=0) return "";
    $sql = "select trd_nombre from trd_nivel where trd_codi>=$nivel_act and depe_codi=$dependencia order by trd_codi asc";
    $rs=$db->conn->query($sql);
    $nom_nivel = $rs->fields["TRD_NOMBRE"];
    $rs->MoveNext();
    if ($rs->EOF) $nom_nivel2=""; else $nom_nivel2 = $rs->fields["TRD_NOMBRE"];

    $sql = "select * from trd where trd_padre=$item and depe_codi=$dependencia order by trd_nombre asc";
//echo $sql;
    $rs=$db->conn->query($sql);
    $resp = "";//<tr class='listado5'>";
    while (!$rs->EOF) {
	$codi = $rs->fields["TRD_CODI"];
	$nomb = $rs->fields["TRD_NOMBRE"];
	$arch1 = $rs->fields["TRD_ARCH_GESTION"];
	$arch2 = $rs->fields["TRD_ARCH_CENTRAL"];
	if ($nivel>1) $esta = 2; else $esta = $rs->fields["TRD_ESTADO"];
	
    	$sql = "select t1+t2 as borrar from (select count(trd_codi) as t1 from trd where trd_padre=$codi) as a,
	    	(select count(trd_codi) as t2 from trd_radicado where trd_codi=$codi) as b";
    	$rs2=$db->conn->query($sql);
    	$borrar = $rs2->fields["BORRAR"];

	$nombre2 = "$nombre - $nomb";
	$tmp_ed = "$codi,'$nomb',$arch1, $arch2, $esta, $nivel_act, '$nom_nivel' ,$borrar ,'$nombre2'";
	$tmp_cr = "$codi,$nivel_act, '$nom_nivel2','$nombre2'";
	$tamanio = ($nivel_act+1)*3;

	$resp .= "<tr><td class='listado2' align='right' width='".$tamanio."%'>";
	if ($nivel>1)
	    $resp .= "<a href='javascript:;' onClick=\"MostrarFila('tr_$codi');\">
			<img src='$ruta_raiz/imagenes/add.gif' border='0' alt='' height='15px' width='15px'></a>";
	$resp .= "</td>";
	$resp .= "<td class='listado2' width='". (60-$tamanio) ."%'>$nomb</td>";
	$resp .= "<td class='listado2' width='15%'>".strtolower($nom_nivel)."</td>";
	$resp .= "<td class='listado2' width='10%'><a href='#' onClick=\"EditarItem($tmp_ed)\" class='vinculos'>Editar</a></td>";
	if ($nivel>1)
	    $resp .= "<td class='listado2' width='15%'><a href='javascript:;' onClick=\"CrearItem($tmp_cr)\" class='vinculos'>Crear ".strtolower($nom_nivel2)."</a></td>";
	else
	    $resp .= "<td class='listado2' width='15%'>&nbsp;</td>";
	$resp .= "</tr>";
	if ($nivel>1) {
	    $resp .= "<tr name='tr_$codi' id='tr_$codi' style='display:none'><td colspan='6'><table border='0' width='100%'>";
	    $resp .= ArbolModificarTRD($codi, $nivel-1, $nivel_act+1, $dependencia, $nombre2, $db, $ruta_raiz);
	    $resp .= "</table></td></tr>";
	}
	$rs->MoveNext();
    }
    return $resp;
  }

/* FUNCION RECURSIVA QUE PERMITE OBTENER LA DIRECCION FISICA COMPLETA DE UN EXPEDIENTE
	$item	:	Codigo del item fisico
	$db	:	Coneccion con la BDD
	$campo	:	"C" Despliega los Codigos, "N" Despliega los nombres, "S" Despliega las siglas
	$separador:	Despliega un separador especifico, util para desplegar en tablas
*/

  function ArbolSeleccionarTRD($item, $nivel, $nivel_act, $dependencia, $nombre, $db, $ruta_raiz="..", 
				$accion="L", $campo="T", $campo_valor=0, $id=0, $ocultar="S")
  {

    if ($nivel<=0) return "";
    $sql = "select trd_nombre from trd_nivel where trd_codi=$nivel_act and depe_codi=$dependencia";
    $rs=$db->conn->query($sql);
    $nom_nivel = $rs->fields["TRD_NOMBRE"];

    if ($ocultar=="N") $ver_fila=""; else $ver_fila="style='display:none'";
    $tamanio1 = 85;
    if ($accion=="S") $tamanio1 = 70;
    if ($campo=="T") $tamanio1 -= 15;
    $sql = "";
    if ($campo=="E") $sql .= " and trd_estado=$campo_valor ";
    if ($campo=="O") $sql .= " and trd_ocupado=$campo_valor ";
    $sql = "select * from trd where trd_padre=$item and depe_codi=$dependencia $sql order by trd_nombre asc";
//echo $sql;
    $rs=$db->conn->query($sql);
    $resp = "";//<tr class='listado5'>";
    while (!$rs->EOF) {
	$codi = $rs->fields["TRD_CODI"];
	$nomb = $rs->fields["TRD_NOMBRE"];
	if ($rs->fields["TRD_ESTADO"]==1) $estado="Activo"; else $estado="Inactivo";
	$nombre2 = "$nombre - $nomb";
	$selec = "$codi,'$nomb', $nivel_act, '$nom_nivel' ,'$nombre2'";
	$tamanio = ($nivel_act+1)*3;

	$resp .= "<tr><td class='listado2' align='right' width='".$tamanio."%'>";
	if ($nivel>1)
	    $resp .= "<a href='javascript:;' onClick=\"MostrarFila('tr".$id."_trd_$codi');\">
			<img src='$ruta_raiz/imagenes/add.gif' border='0' alt='' height='15px' width='15px'></a>";
	$resp .= "</td>";
	$resp .= "<td class='listado2' width='". ($tamanio1-$tamanio) ."%'>$nomb</td>";
	if ($campo=="T")
	    $resp .= "<td class='listado2' width='15%'>".$estado."</td>";
	$resp .= "<td class='listado2' width='15%'>".strtolower($nom_nivel)."</td>";
	if ($accion=="S") {
	    if ($nivel<=1)
	    	$resp .= "<td class='listado2' width='15%'><a href='javascript:;' onClick=\"SeleccionarTRD($selec)\">Seleccionar</a></td>";
	    else
	    	$resp .= "<td class='listado2' width='15%'>&nbsp;</td>";
	}
	$resp .= "</tr>";
	if ($nivel>1) {
	    $resp .= "<tr name='tr".$id."_trd_$codi' id='tr".$id."_trd_$codi' $ver_fila><td colspan='6'><table border='0' width='100%'>";
	    $resp .= ArbolSeleccionarTRD($codi, $nivel-1, $nivel_act+1, $dependencia,$nombre2,$db,$ruta_raiz,$accion,$campo,$campo_valor,$id,$ocultar);
	    $resp .= "</table></td></tr>";
	}
	$rs->MoveNext();
    }
    return $resp;
  }

/* FUNCION RECURSIVA QUE PERMITE OBTENER LA DIRECCION FISICA COMPLETA DE UN EXPEDIENTE
	$item	:	Codigo del item fisico
	$db	:	Coneccion con la BDD
	$campo	:	"C" Despliega los Codigos, "N" Despliega los nombres, "S" Despliega las siglas
	$separador:	Despliega un separador especifico, util para desplegar en tablas
*/

  function ActivarTRD($item, $campo, $db)
  {

    if ($item==0) return;
    $sql = "trd_estado=1, trd_fecha_hasta=null"; 
    if ($campo=="O") $sql = "trd_ocupado=0";
    $sql = "update trd set $sql where trd_codi=$item";
    $db->conn->Execute($sql);
    $sql = "select trd_padre from trd where trd_codi=$item";
    $rs=$db->conn->query($sql);
    $codigo=$rs->fields["TRD_PADRE"];
    ActivarTRD($codigo, $campo, $db);
    return;
  }

  function DesactivarTRD($item, $campo, $db)
  {
    if ($item==0) return;
    $sql = "trd_estado=1"; 
    if ($campo=="O") $sql = "trd_ocupado=0";
    $sql = "select count(trd_codi) as total from trd where $sql and trd_padre=$item";
    $rs=$db->conn->query($sql);
    $total = $rs->fields["TOTAL"];
    if ($total == 0) {
	$fecha = $db->conn->sysTimeStamp;
    	$sql = "trd_estado=0, trd_fecha_hasta=$fecha"; 
    	if ($campo=="O") $sql = "trd_ocupado=1";
    	$sql = "update trd set $sql where trd_codi=$item";
    	$db->conn->Execute($sql);
    } else
	return;
    $sql = "select trd_padre from trd where trd_codi=$item";
    $rs=$db->conn->query($sql);
    $codigo=$rs->fields["TRD_PADRE"];
    DesactivarTRD($codigo, $campo, $db);
    return;
  }

?>
