<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/
  $ruta_raiz = "..";

  session_start();
  include_once "$ruta_raiz/rec_session.php";
  if (isset ($replicacion) && $replicacion) $db = new ConnectionHandler("$ruta_raiz","busqueda");
  include_once "obtener_datos_trd.php";

////////////////	VARIABLES BASICAS	////////////////////////
  $depe_actu = $_SESSION["depe_codi"];
  $sql = "select trd_nombre from trd_nivel where depe_codi=$depe_actu";
  $rs=$db->conn->query($sql);
  $niveles = 0;
  $titulo = "";
  while (!$rs->EOF) {
    if ($titulo!="") $titulo .= " >> ";
    $titulo .= $rs->fields["TRD_NOMBRE"];
    $niveles++;		//Numero de niveles de almacenamiento
    $rs->MoveNext();
  }

include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();
include_once "$ruta_raiz/js/ajax.js";

?>
  <style>a:link, a:visited, a:hover {color: blue;}</style>
  
  <body >
  <form method="post" name="formulario"> 
    <center>
      <div id="div_lista_trd">
        <table class="borde_tab" width="80%">
<?
	    if (trim($titulo)=="") { ?>
	    	<tr><td class="titulos2" colspan="4"><center>
		    Esta <?=$_SESSION["descDependencia"]?> no tiene definidas <?=$_SESSION["descTRDpl"]?></center></td></tr>
<?	    } else { ?>
	    	<tr><td class="titulos2" colspan="4"><?=$titulo?></td></tr>
	    	<tr><td class="titulos2" width="70%">Nombre <?=($_SESSION["descTRDpl"])?></td>
		    <td class="titulos2" width="15%">Tipo</td>
		    <td class="titulos2" width="15%">Acci&oacute;n</td>
	    	</tr>
	    	<tr><td  colspan="4">
		    <table width="100%">
			<?echo ArbolSeleccionarTRD(0, $niveles,0 , $depe_actu, "", $db, $ruta_raiz,"S","T",1,0,"S");?>
		    </table></td>
	    	</tr>
<?
	}
?>

      </table>
    </div>

<?
////////////////////////	BOTONES 	/////////////////////////
?>
    <div id="div_lista_documentos"></div>

    <script>
        function MostrarFila(fila) {
            if (document.getElementById(fila).style.display=='none')
            document.getElementById(fila).style.display='';
            else
            document.getElementById(fila).style.display='none';
        }

        function SeleccionarTRD(codigo, nombre, nivel, nom_nivel, descripcion) {
            document.getElementById('div_lista_trd').style.display='none';
            nuevoAjax('div_lista_documentos', 'POST', 'lista_expediente.php', 'trd_codi='+ codigo);
        }

        function mostrar_documento(numdoc, txtdoc)
{
	var_envio='<?=$ruta_raiz?>/verradicado.php?verrad='+numdoc+'&textrad='+txtdoc+'&menu_ver_tmp=3&tipo_ventana=popup';
	window.open(var_envio,numdoc,"height=450,width=750,scrollbars=yes");
}


    </script>
  </center>
  </form>
  </body>
</html>



