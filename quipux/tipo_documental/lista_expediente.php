<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
$ruta_raiz = isset($ruta_raiz) ? $ruta_raiz : "..";
session_start();
include_once "$ruta_raiz/rec_session.php";
if (isset ($replicacion) && $replicacion && $config_db_replica_trd_lista_expediente!="") $db = new ConnectionHandler($ruta_raiz,$config_db_replica_trd_lista_expediente);

include_once "$ruta_raiz/tipo_documental/obtener_datos_trd.php";
include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();

	if($orden_cambio==1) {
	    if($orderTipo=="desc")
		$orderTipo="asc";
	    else
		$orderTipo="desc";
	}
    	if (!$orderTipo) $orderTipo="desc";

?>
<script>
var tot_exp=0;
function regresar(){
	window.location.reload();
}
function incluir_documento()
{
	window.open("<?=$ruta_raiz?>/expediente/IncluirEnExpediente.php?numrad=<?=$verrad?>","MflujoExp<?=$verrad?>","height=450,width=750,scrollbars=yes");
}

function mostrar_documento(numdoc, txtdoc)
{
	var_envio='<?=$ruta_raiz?>/verradicado.php?verrad='+numdoc+'&textrad='+txtdoc+'&menu_ver_tmp=3';
	window.open(var_envio,numdoc,"height=450,width=750,scrollbars=yes");
}

</script>

<body bgcolor="#FFFFFF" topmargin="0">

<?
    $boton = "";
    if (!isset($trd_codi)) {
        if ($nivel_seguridad_documento > 3)
            $boton = "<input type='button' onClick='CambiarTRD();' name='Submit1' value='Incluir en $descTRD' ".
                     "title='Coloca el documento en la $descTRD seleccionada' class='botones_largo'>";

        $isql = "select trd_codi from trd_radicado where radi_nume_radi=$verrad and depe_codi=".$_SESSION["depe_codi"];
    //echo $isql;
        $rs = $db->conn->query($isql);
        if($rs->EOF) {
?>
            <center>
              <table border="0" width="98%" class="borde_tab" align="center" class="titulos2">
                <tr><td align="center" class="listado1" >
                  <b>Este documento no ha sido incluido en ninguna carpeta virtual.</b>
                </td></tr>
              </table>
            </center>
<?
            $codexp = 0;
        } else {
            $codexp = $rs->fields["TRD_CODI"];
            $encabezado = "verrad=$verrad&carpeta=$carpeta&estadisticas=$estadisticas&menu_ver=4&orderTipo=$orderTipo&orderNo=";
        }
    } else {
        $codexp = $trd_codi;
    	$encabezado = "verrad=$verrad&trd_codi=$trd_codi&carpeta=0&orderTipo=$orderTipo&orderNo=";
        $boton = "<input type='button' name='btn_regresar' value='Regresar' class='botones_largo' ".
                 "onClick=\"window.location='$ruta_raiz/tipo_documental/consultar_trd.php'\">";
    }

    if ($codexp != 0) {
?>

    <table border=0 width=100% align="center" class="borde_tab" cellspacing="1" cellpadding="0" name="tbl_exp" id="tbl_exp">
	<tr align="center" class="titulos2">
    	    <td height="15" class="titulos2" colspan="6">
		<?=strtoupper($descTRD).": ".ObtenerNombreCompletoTRD($codexp,$db);?>
	    </td>
    	</tr>
    </table>
<?
    	


	$linkPagina = $PHP_SELF."?$encabezado";

	include_once "lista_expediente_query.php";
//echo "<hr>".$isql;
//  	$rs=$db->query($isql);


	$pager = new ADODB_Pager($db,$isql,'adodb', true,$orderNo,$orderTipo);
	$pager->checkAll = false;
	$pager->checkTitulo = true;
	$pager->linkCabecera =false;
	$pager->toRefLinks = $linkPagina;
	$pager->toRefVars = $encabezado;
	$pager->Render($rows_per_page=15,$linkPagina,$checkbox=chkAnulados);
    }
    
    echo "<br><center>$boton</center>";

?>

</body>
</html>
