<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses.
*------------------------------------------------------------------------------
**/
/*****************************************************************************************
**											**
******************************************************************************************/
session_start();
$ruta_raiz = ".";

require_once "$ruta_raiz/funciones.php";
p_register_globals();
include_once "$ruta_raiz/rec_session.php";
include_once "$ruta_raiz/tipo_documental/obtener_datos_trd.php";
include_once "$ruta_raiz/seguridad/obtener_nivel_seguridad.php";
include_once "$ruta_raiz/obtenerdatos.php";

if (strpos($_SERVER["HTTP_REFERER"],"cuerpo.php") !== false) {
    $menu_ver=3;
}

$nivel_seguridad_documento = obtener_nivel_seguridad_documento($db, $verrad);
if ($nivel_seguridad_documento < 3) die ("Lo sentimos, usted no tiene permisos para ver este documento");
$pagactual=1;	//Variable necesaria para txOrfeo.php, muestra los botones editar o responder
$numrad = trim($verrad);

$datosrad = ObtenerDatosRadicado($verrad,$db);
$usr_actual = ObtenerDatosUsuario($datosrad["usua_actu"],$db);
$estado = $datosrad["estado"];

$textrad = $datosrad["radi_nume_text"];
if (!$menu_ver) $menu_ver=3;	//define la pestaña de vista general por defecto
if (!$estadisticas) $estadisticas=0;
$_SESSION["estado"]=$estado ;


//////////  Definimos el path del archivo a descargar o creamos el archivo si es vista previa  //////////////////////
if($_SESSION["cargo_tipo"]=='1' or $menu_ver==3) {
    include_once "$ruta_raiz/plantillas/generar_documento.php";
    $doc = New GenerarDocumento($db);
    $path_archivo=trim($datosrad["radi_path"]);
    if (trim($path_archivo)=="" and $datosrad["radi_tipo"]!=2) {
        $path_archivo = $doc->GenerarPDF($verrad,"no");
    } else
        $path_archivo = str_replace(".p7m","",$path_archivo);

    $path_descarga = "";
    if ($path_archivo != "") {
        $tmp = explode(".",$path_archivo);
        $nombre_archivo = $textrad . "." . $tmp[count($tmp)-1];
        $path_descarga = "$ruta_raiz/archivo_descargar.php?path_arch=$path_archivo&nomb_arch=$nombre_archivo";
        $referencia_ver = $_GET['nReferencia'];
        //echo $referencia_ver;
        $datosref = ObtenerDatosRadicado($referencia_ver,$db);
        $path_referencia = $datosref["radi_path"];
        $nombre_referencia = $datosrad["radi_referencia"];
        //echo $datosref["usua_actu"];
        $usr_actual_ref = ObtenerDatosUsuario($datosref["usua_actu"],$db);
        if ($referencia_ver!='' || $nombre_referencia!='')
            $path_refe_iframe="$ruta_raiz/VistaPrevia.php?verrad=$referencia_ver&archivo=&textrad=$nombre_referencia";
        else
            $path_refe_iframe=0;
        
    }
}


include_once "$ruta_raiz/funciones_interfaz.php";
echo "<html>".html_head();

?>

<script language="JavaScript">

function window_onload() { }
    function regresar() {
        window.location.reload();
        
    }

    function CambiarTRD() {
        ventana = window.open("./tipo_documental/seleccionar_trd.php?verrad=<?=$verrad?>&textrad=<?=$textrad?>", "Tipificacion_Documento", "height=500,width=750,scrollbars=yes");
        ventana.focus();
    }

    function AsociarDocumento() {
        ventana = window.open("./asociar_documentos/asociar_documento.php?radi_nume=<?=$verrad?>", "asociar_documentos", "height=600,width=900,scrollbars=yes");
        ventana.focus();
    }

    function popup_ver_documento(radicado) {
        windowprops = "top=50,left=50,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=700,height=500";
        url = '<?=$ruta_raiz?>/verradicado.php?verrad=' + radicado + '&estadisticas=1&menu_ver=3';
        ventana = window.open(url , "ver_documento_" + radicado, windowprops);
        ventana.focus();
    }

     function desplegarContraer(cual,desde) {
          var elElemento=document.getElementById(cual);
          if(elElemento.className == 'elementoVisible') {
               elElemento.className = 'elementoOculto';
               desde.className = 'linkContraido';
          } else {
               elElemento.className = 'elementoVisible';
               desde.className = 'linkExpandido';
          }
    }

    function vista_previa(path_archivo , nombre_archivo) {
        path_referencia= '<?=$path_refe_iframe?>';        
        path_descarga = path_archivo || '';
        if (path_descarga == '') {             
            path_descarga = '<?=$path_descarga?>';
        } else {
            path_descarga = '<?=$ruta_raiz?>/archivo_descargar.php?path_arch=' + path_archivo + '&nomb_arch=' + nombre_archivo;
        }
        ver_tipo = document.getElementById('ver_tipo').value;
        
        if (ver_tipo=='E')
            document.getElementById('ifr_descargar_archivo').src=path_descarga;
        else{
            
            if (path_referencia==0)
                document.getElementById('ifr_descargar_archivo').src=path_descarga;
            else
                document.getElementById('ifr_descargar_archivo').src=path_referencia;
        }
    }


function ver_documento(tipo) {
        windowprops = "top=100,left=100,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes,width=500,height=300";

        if (tipo==1)
            url1 = <?="'$ruta_raiz/VistaPrevia.php?verrad=$verrad&archivo=&textrad=$textrad'"?>;
        else
            url1 = <?="'$ruta_raiz/VistaPrevia.php?verrad=$referencia_ver&archivo=&textrad=$nombre_referencia'"?>;
        $ventana = "";


         window.open(url1 , "Vista_Previa_<?=$noRad?>", windowprops);
        return;
    }
</script>
<body bgcolor="#FFFFFF" onLoad="window_onload();">
    <form name="form1" id="form1" action="<?=$ruta_raiz.'/tx/formEnvio.php?carpeta='.$carpeta?>" method="post">
<?
        $dirresponder = "$ruta_raiz/radicacion/NEW.php?nurad=$verrad&radi_padre=$verrad&textrad=$textrad&accion=Responder";
        $dirmodificar = "$ruta_raiz/radicacion/NEW.php?nurad=$verrad&textrad=$textrad&accion=Editar";
        $controlAgenda=1;
        if ($estadisticas==1 or $_SESSION["usua_tipo"]==2) $estado=100;
        if ($carpeta==20) $estado = 20;
        // Si ingresa desde la bandeja de documentos compartidos
        if ($datosrad["usua_actu"] != $_SESSION["usua_codi"]) {
            if ($estado != 5) $estado = 100;
            $rs = $db->conn->Execute("select count(1) as inf from informados where radi_nume_radi=$verrad and usua_codi=".$_SESSION["usua_codi"]);
            if ($rs->fields["INF"] != 0) $estado=13;
        }
        if($carpeta == 14 ) $estado = 14;        
?>
        <input type=hidden name="checkValue[<?=$verrad?>]" value='CHKANULAR'>        
        <input type=hidden id="ver_tipo" value='<?=$_GET['ver_tipo']?>'/>
    </form>

<!-- Redireccionar a NEW.php si el usuario es JEFE y el documento esta en elaboracion -->
<?php if($nivel_seguridad_documento==5 and ($irVerRad == "" or $irVerRad == "0") and $_SESSION["usua_perm_redireccionar_edicion"]==1) { ?>
    <script language="JavaScript" type="">
        window.location = "<?=$dirmodificar?>";
    </script>
<?php } ?>

<table width="100%" border="0" cellpadding="0" cellspacing="1" >
    <?php 
    
    if (isset($_GET['ver_tipo'])=='E'){ //esta en edicion ?>
    <tr>
        <td class="titulos4" width="20%">
            
            <?php echo '<a href="javascript:ver_documento(1);" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/zoom_in.png" width="15" height="15" alt="Vista Previa" border="0"><span>Ver Documento</span></a>';?>
        </td>
        <td class="titulos4" width="20%">No. Documento: &nbsp;&nbsp;&nbsp;&nbsp;<?=$datosrad["radi_nume_text"] ?></td>
        <td class="titulos4" align="left" width="30%">&nbsp;&nbsp;Usuario actual: &nbsp;&nbsp;&nbsp;&nbsp;<?=$usr_actual["nombre"]?></td>
        <td class="titulos4" align="left" width="30%">&nbsp;&nbsp;<?=$descDependencia?> actual: &nbsp;&nbsp;&nbsp;&nbsp;<?=$usr_actual["dependencia"]?></td>
    </tr>
    <?php }else{ 
       
        ?>
    <?php if (!empty($datosrad["radi_referencia"])){ ?>
    <tr>
        <td class="titulos4" width="20%">
            
            <?php echo '<a href="javascript:ver_documento(2);" class="Ntooltip"><img src="'.$ruta_raiz.'/imagenes/zoom_in.png" width="15" height="15" alt="Vista Previa" border="0"><span>Ver Referencia</span></a>';?>
        </td>
        <td class="titulos4" width="20%">Referencia: &nbsp;&nbsp;&nbsp;&nbsp;<?=$datosrad["radi_referencia"]; ?></td>
        <td class="titulos4" align="left" width="30%">&nbsp;&nbsp;Usuario actual: &nbsp;&nbsp;&nbsp;&nbsp;<?=$usr_actual_ref["nombre"]?></td>
        <td class="titulos4" align="left" width="30%">&nbsp;&nbsp;<?=$descDependencia?> actual: &nbsp;&nbsp;&nbsp;&nbsp;<?=$usr_actual_ref["dependencia"]?></td>        
    </tr>
    <?php }
    }//fin si no esta en edicion?>
</table>

<iframe  name="ifr_descargar_archivo" id="ifr_descargar_archivo" style="display: none" src="">
            Su navegador no soporta iframes, por favor actualicelo.</iframe>
<?php
//
//FUNCIONALIDAD UNICAMENTE PARA JEFES

if($path_descarga != "")
{
     //if(trim($path_archivo)!='' || $path_referencia!='') {         
    if (isset($_GET['ver_tipo'])=='E')
       $url = "$ruta_raiz/bodega".$path_archivo;     
      else
          $url = "$ruta_raiz/bodega".$path_referencia;
         $url = str_replace('.p7m', '', $url);
        $dir=$url;
        $dr=@opendir($dir);        
            if (file_exists($url))
           echo "<embed src='$url' type='text/html; charset=UTF-8' width='97%' height='100%'></embed>";
            else{
                ?>
            <table width="100%"><tr><td align="left"><font color="black">
                <div align="center"><?php echo "No se puede visualizar el archivo, favor comuníquese con el Administrador del Sistema";?>
                </div>
            </font></td></tr></table>
                <?php
            }        
    
}
?>

</body></html>
