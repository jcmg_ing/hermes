<?php
/**  Programa para el manejo de gestion documental, oficios, memorandus, circulares, acuerdos
*    Desarrollado y en otros Modificado por la SubSecretaría de Informática del Ecuador
*    Quipux    www.gestiondocumental.gov.ec
*------------------------------------------------------------------------------
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see http://www.gnu.org/licenses. 
*------------------------------------------------------------------------------
**/

/*
// Borramos el cache del navegador
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // siempre modificado
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
/* */

//error_reporting(0);
session_start();
//$krd=strtoupper($krd);
$ruta_raiz = "."; 
//if(!isset($_SESSION['dependencia'])) 
include_once "$ruta_raiz/rec_session.php";
//$fechah = date("ymd") ."_". time("hms");
?>
<html>
<head>
<title>.:: Quipux - Sistema de Gesti&oacute;n Documental ::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="imagenes/favicon.ico">

<script>
    function detectarPhone(){
            var navegador = navigator.userAgent.toLowerCase();
            if ( navigator.userAgent.match(/iPad/i) != null)//detectar ipad
              return 2;
            else{//detectar phone        
                if( navegador.search(/iphone|ipod|blackberry|android/) > -1 )
                   return 1;    
                else 
                    return 0;
            }
        }       
    //obligamos a que esté bloqueada la barra de direcciones
    window.focus();
     

    function cerrar_session() {
        windowprops = "top=0,left=0,location=no,status=no, menubar=no,scrollbars=no, resizable=no,width=5,height=5";
        url = 'cerrar_session.php?cerrar_ventana=si';
        window.open(url , "cerrar_session", windowprops);
        return;
    }
/*
    window.onbeforeunload = function() {
        window.focus();
        return "Usted ya tiene una sesión previamente inicializada en el sistema Quipux.";
    };
/* */

//window.onclose = cerrar_session();

  function cerrar_ventana()
        {
           window.close();
        }
</script>

<script type="text/javascript">
//Javascript

ns4 = (document.layers)? true:false;
ie4 = (document.all)? true:false;
document.onkeydown = keyDown;
if (ns4) document.captureEvents(Event.KEYDOWN);

function keyDown(e){
    var tecla, res = true;
    if (ns4) tecla = e.which;
    else if (ie4) tecla = event.keyCode;
    	else
	{
		var evt = arguments.length ? arguments[0] : window.event;
		tecla = evt.which;
	}
	//alert(ns4+"tecla----"+tecla);
    switch(tecla){
     case 116:
     case 117:
     case 118:
     case 222:
	res = false;
        break;
     default:
	res = true;
      break;
    }
    return res;
   }

<? if (isset($id_google_analytics) && trim($id_google_analytics) != "") { ?>
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?=$id_google_analytics?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
<? } ?>
</script>

</head>
<frameset rows="97,864*" frameborder="YES" border="1" framespacing="0" cols="*">
    <frame name="topFrame" scrolling="NO" noresize src='f_top.php' ></frame>
    <frameset cols="175,947*" border="1" framespacing="0" rows="*">
        <frame name='leftFrame' scrolling='AUTO' src='correspondencia.php' marginwidth='0' marginheight='0' scrolling='AUTO' border=1></frame>
        <frame name='mainFrame' src="cuerpo.php" scrolling='AUTO'></frame>
        <!--frame src="UntitledFrame-3"-->
    </frameset>
</frameset>
<noframes></noframes>
<body><div id="div_session"></div></body>
</html>
